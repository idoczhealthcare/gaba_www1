<?php

    Route::group(['prefix' => 'e-commerce', 'as' => 'e-commerce.'], function () {
    Route::get('shop', ['as' => 'index', 'uses' => 'ShopController@index']);
    
    Route::get('shop/{slug}', ['as' => 'show', 'uses' => 'ShopController@show']);
    
    
        
    Route::get('cart', 'CartController@index');

    Route::group(['prefix' => 'checkout'], function () {
        Route::get('/', 'CheckoutController@index');
        Route::post('/', 'CheckoutController@doCheckout');
        Route::get('step/{step}', 'CheckoutController@checkoutStep');
        Route::post('step/{step}', 'CheckoutController@saveCheckoutStep');
        Route::get('shipping-address', 'CheckoutController@checkoutShippingAddress');
        Route::get('order-success', 'CheckoutController@showOrderSuccessPage');
    });

    Route::get('products/download/{id}', 'ProductsController@downloadFile');
    Route::resource('products', 'ProductsController');
    Route::get('sale', 'SaleController@sale');
    Route::post('addsale', 'SaleController@addsale');
    Route::post('delsale', 'SaleController@delsale');
    Route::post('salestatus', 'SaleController@salestatus');
    Route::resource('categories', 'CategoriesController', ['except' => ['show']]);
   // Route::resource('categories1', 'CategoriesController1', ['except' => ['show']]);
    Route::resource('attributes', 'AttributesController', ['except' => ['show']]);
    Route::resource('tags', 'TagsController', ['except' => ['show']]);
    Route::resource('brands', 'BrandsController', ['except' => ['show']]);
    Route::post('products/{product}/create-gateway-product', ['as' => 'create-gateway-product', 'uses' => 'ProductsController@createGatewayProduct']);
    Route::get('gateway-payment/{gateway}', 'CartController@gatewayPayment');


    Route::group(['prefix' => 'products', 'as' => 'product-gallery.'], function () {
        Route::get('{product}/gallery', ['as' => 'list', 'uses' => 'ProductsController@gallery']);
        
        Route::post('{product}/gallery/upload', ['as' => 'upload', 'uses' => 'ProductsController@galleryUpload']);
        Route::post('{media}/gallery/mark-as-featured', ['as' => 'mark-as-featured', 'uses' => 'ProductsController@galleryItemFeatured']);
        Route::delete('{media}/gallery/delete', ['as' => 'delete', 'uses' => 'ProductsController@galleryItemDelete']);
        
        Route::get('{product}/gallerysub/{id}', ['as' => 'list', 'uses' => 'ProductsController@gallerysub']);
         Route::post('{product}/gallery/uploadsub', ['as' => 'upload', 'uses' => 'ProductsController@galleryUploadsub']);
        Route::post('{media}/gallery/mark-as-featuredsub', ['as' => 'mark-as-featured', 'uses' => 'ProductsController@galleryItemFeaturedsub']);
        Route::get('{media}/gallery/deletesub', ['as' => 'delete', 'uses' => 'ProductsController@galleryItemDeletesub']);
    });

    Route::group(['prefix' => 'wishlist'], function () {
        Route::post('{product}', 'WishlistController@setWishlist');
        Route::delete('{wishlist}', 'WishlistController@destroy');
        Route::get('my', ['as' => 'my-wishlist', 'uses' => 'WishlistController@myWishlist']);

    });

    Route::resource('products.sku', 'SKUController');
    Route::post('products/{product}/sku/{sku}/create-gateway-sku', ['as' => 'create-gateway-sku', 'uses' => 'SKUController@createGatewaySKU']);

    Route::get('orders/my', ['as' => 'my-orders', 'uses' => 'OrdersController@myOrders']);
    Route::get('downloads/my', ['as' => 'my-downloads', 'uses' => 'OrdersController@myDownloads']);
    Route::get('private-pages/my', ['as' => 'my-private-pages', 'uses' => 'OrdersController@myPrivatePages']);
    Route::resource('orders', 'OrdersController', ['only' => ['index', 'show', 'edit', 'update']]);
    Route::get('orders/{order}/track', 'OrdersController@track');
    Route::get('orders/{order}/download/{id}', 'OrdersController@downloadFile');

    Route::get('settings', 'ShopController@settings');
    Route::post('settings', 'ShopController@saveSettings');

    Route::get('gateway-payment/{gateway}/{order?}', 'CheckoutController@gatewayPayment');
    Route::get('gateway-payment-token/{gateway}/{order?}', 'CheckoutController@gatewayPaymentToken');

    Route::resource('coupons', 'CouponsController');
    Route::resource('shippings', 'ShippingsController');
    Route::resource('lensnumber', 'LensNumberController');
    Route::resource('nonprescription', 'NonprescriptionController');
    Route::resource('color', 'ColorController');
    Route::resource('size', 'SizeController');
});


Route::group(['prefix' => 'shop', 'as' => 'shop.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'ShopPublicController@index']);
    Route::post('{product}/rate', ['as' => 'show', 'uses' => 'RatingController@createRating']);
    Route::get('{slug}', ['as' => 'show', 'uses' => 'ShopPublicController@show']);
     Route::get('shop', ['as' => 'index', 'uses' => 'ShopController@index']);
    
    
});
Route::get('shopsale', ['as' => 'sale', 'uses' => 'ShopPublicController@shopsale']);

Route::group(['prefix' => 'cart'], function () {
    Route::get('/', 'CartPublicController@index');
    Route::post('empty', 'CartPublicController@emptyCart');
    Route::post('quantity/{itemhash}', 'CartPublicController@setQuantity');
    Route::post('{product}/add-to-cart/{sku?}', 'CartPublicController@addToCart');
   // Route::post('{product}/add-to-cart-lens/{sku?}', 'CartPublicController@addToCartlens');
});

Route::group(['prefix' => 'checkout'], function () {
    Route::get('/', 'CheckoutPublicController@index');
    Route::post('/', 'CheckoutPublicController@doCheckout');
    Route::get('step/{step}', 'CheckoutPublicController@checkoutStep');
    Route::post('step/{step}', 'CheckoutPublicController@saveCheckoutStep');
    Route::get('shipping-address', 'CheckoutPublicController@checkoutShippingAddress');
    Route::get('order-success', 'CheckoutPublicController@showOrderSuccessPage');
});
Route::get('country', ['as' => 'country', 'uses' => 'ShopPublicController@show1']);
Route::get('configurejson', ['as' => 'configurejson', 'uses' => 'ShopPublicController@configurejson']);
Route::get('configurenonprescrip', ['as' => 'configurenonprescrip', 'uses' => 'ShopPublicController@configurenonprescrip']);

Route::get('getsubcat', ['as' => 'getsubcat', 'uses' => 'ShopPublicController@getsubcat']);
Route::get('configure/{slug}', ['as' => 'configure', 'uses' => 'ShopPublicController@configure']);
Route::get('gettype', ['as' => 'gettype', 'uses' => 'ShopPublicController@gettype']);

Route::get('getsize', ['as' => 'getsize', 'uses' => 'ShopPublicController@getsize']);
Route::get('getprice', ['as' => 'getprice', 'uses' => 'ShopPublicController@getprice']);
Route::get('loadfeatureimag', ['as' => 'loadfeatureimag', 'uses' => 'ShopPublicController@loadfeatureimag']);
Route::get('shop_contact/{slug}', ['as' => 'shop_contact', 'uses' => 'ShopPublicController@shop_contact']);
Route::get('products', ['as' => 'products', 'uses' => 'ShopPublicController@products']);