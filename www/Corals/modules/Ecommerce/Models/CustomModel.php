<?php

namespace Corals\Modules\Ecommerce\Models;


use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Corals\Traits\Node\SimpleNode;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class CustomModel extends BaseModel implements HasMedia
{
    
    use PresentableTrait, LogsActivity, HasMediaTrait, SimpleNode;
      public function register()
    {
        try {
            $module = null;

            $modules = app('db')->select("SELECT provider,id,code FROM modules where enabled=1 and provider is not null and type <> 'core' order by load_order");

            foreach ($modules as $module) {
                $provider = $module->provider;
                if ($provider && class_exists($provider)) {
                    $this->app->register($provider);
                }
            }
        } catch (\Exception $exception) {
         

        }
    }
    
    
}















?>