<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;

class LenseCategory extends BaseModel
{
    protected $table = 'ecommerce_lense_category';

    public $config = 'ecommerce.models.lensecategory';
}
