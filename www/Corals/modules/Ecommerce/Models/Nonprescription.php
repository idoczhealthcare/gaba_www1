<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Nonprescription extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_nonprescription';
    
    public $config = 'ecommerce.models.ecommerce_nonprescription';

    protected $guarded = ['id'];
 


}
