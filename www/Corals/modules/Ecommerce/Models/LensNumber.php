<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensNumber extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_lense_number';
    
    public $config = 'ecommerce.models.ecommerce_lensnumber';

    protected $guarded = ['id'];
    

    public function lenstype()
    {
        return $this->belongsTo(LensType::class, 'type_id');
    }

    public function lenssubcat()
    {
        return $this->belongsTo(LensSubCat::class, 'subcat_id');
    }
      public function lenscat()
    {
        return $this->belongsTo(LensCategory::class, 'cat_id');
    }

//    public function cylindrical()
//    {
//        return $this->belongsTo(LensCylindrical::class, 'lens_cylindrical');
//    }
//
//    public function pd()
//    {
//        return $this->belongsTo(LensPupillaryDistance::class, 'lens_pd');
//    }
}
