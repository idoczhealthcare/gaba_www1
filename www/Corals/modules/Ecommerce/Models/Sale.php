<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class Sale extends BaseModel 
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_sale';
    
    /**
     *  Model configuration.
     * @var string
     */

    

    public $config = 'ecommerce.models.sale';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

  
}
