<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;;

class Style extends BaseModel
{
    protected $table = 'ecommerce_styles';

    public $config = 'ecommerce.models.style';
}
