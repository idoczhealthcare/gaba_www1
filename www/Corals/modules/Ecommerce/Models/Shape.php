<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;

class Shape extends BaseModel
{
    protected $table = 'ecommerce_shapes';

    public $config = 'ecommerce.models.shape';
}
