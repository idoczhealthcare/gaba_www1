<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class SubProduct extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_sub_product';
    public $mediaCollectionName = 'ecommerce-sub-product-gallery';
    /**
     *  Model configuration.
     * @var string
     */



    public $config = 'ecommerce.models.ecommerce_sub_product';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

   
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

}
