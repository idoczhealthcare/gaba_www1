<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensCatMapping extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_lense_mapping_table';
    
    public $config = 'ecommerce.models.ecommerce_lense_mapping_table';

    protected $guarded = ['id'];
    
    public function type()
    {
        return $this->hasMany(LensNumber::class, 'mapping_id');
    }

    public function subcat()
    {
        return $this->belongsTo(LensSubCat::class, 'len_subcat_id');
    }

    public function category()
    {
        return $this->belongsTo(LensCategory::class, 'len_cat_id');
    }

//    public function cylindrical()
//    {
//        return $this->belongsTo(LensCylindrical::class, 'lens_cylindrical');
//    }
//
//    public function pd()
//    {
//        return $this->belongsTo(LensPupillaryDistance::class, 'lens_pd');
//    }
}
