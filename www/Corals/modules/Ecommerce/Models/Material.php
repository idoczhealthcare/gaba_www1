<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;

class Material extends BaseModel
{
    protected $table = 'ecommerce_materials';

    public $config = 'ecommerce.models.material';
}
