<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\FractalPresenter;

class NonprescriptionPresenter extends FractalPresenter
{

    /**
     * @return LensTypeTransformer
     */
    public function getTransformer()
    {
        return new NonprescriptionTransformer();
    }
}