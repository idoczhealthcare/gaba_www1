<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\FractalPresenter;

class ColorPresenter extends FractalPresenter
{

    /**
     * @return ColorTransformer
     */
    public function getTransformer()
    {
        return new ColorTransformer();
    }
}