<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\BaseTransformer;
use Corals\Modules\Ecommerce\Models\Color;

class ColorTransformer extends BaseTransformer
{
    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.color.resource_url');

        parent::__construct();
    }

    /**
     * @param Shipping $shipping
     * @return array
     * @throws \Throwable
     */
    public function transform(Color $color)
    {
        $logo = '<img src = "' . $color->thumbnail . '" 
        class="img-responsive img-rounded" 
        style ="max-height: 20px;width:auto" alt = "Thumbnail" />';

        return [
            'id' => $color->id,
            'title' => $color->title,
            'logo' => $logo,
            'action' => $this->actions($color)
        ];
    }
}








