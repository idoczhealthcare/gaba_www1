<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\BaseTransformer;
use Corals\Modules\Ecommerce\Models\LensNumber;

class LensNumberTransformer extends BaseTransformer
{
    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.lensnumber.resource_url');

        parent::__construct();
    }

    /**
     * @param Shipping $shipping
     * @return array
     * @throws \Throwable
     */
    public function transform(LensNumber $lensNumber)
    {   
//        /var_dump($lensNumber);
       
        return [
     
            'id' => $lensNumber->id,
            'usage_name' => $lensNumber->usage_name,
            'type' => $lensNumber->type,
            'options' => $lensNumber->options,
            'sphere' => $lensNumber->sphere,
            'cylinder' => $lensNumber->cylinder,
            'price' => $lensNumber->price,
            'action' => $this->actions($lensNumber)
        ];
    }
}








