<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\BaseTransformer;
use Corals\Modules\Ecommerce\Models\Nonprescription;

class NonprescriptionTransformer extends BaseTransformer
{
    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.nonprescription.resource_url');

        parent::__construct();
    }

    /**
     * @param Shipping $shipping
     * @return array
     * @throws \Throwable
     */
    public function transform(Nonprescription $nonprescription)
    {   
//        /var_dump($lensNumber);
       
        return [
            'id' => $nonprescription->id,
            'type' => $nonprescription->type,
            'options' => $nonprescription->options,
            'price' => $nonprescription->price,
            'action' => $this->actions($nonprescription)
        ];
    }
}








