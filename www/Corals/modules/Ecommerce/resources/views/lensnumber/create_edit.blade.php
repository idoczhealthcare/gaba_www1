@extends('layouts.crud.create_edit')

@section('css')
@endsection

@section('content_header')
    @component('components.content_header')
        @slot('page_title')
            {{ $title_singular }}
        @endslot

        @slot('breadcrumb')
            {{ Breadcrumbs::render('ecommerce_lensnumber_create_edit') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    @parent
    <div class="row">
        <div class="col-md-8">
            @component('components.box')
                {!! CoralsForm::openForm($lensnumber) !!}
                <div class="row">
                    <div class="col-md-6">
<!--
                    -->

                        {!! CoralsForm::text('sphere','Sphere',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         {!! CoralsForm::text('cylinder','Cylinder',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                       
                        
                        
                        <div class="form-group">
                            <label for="usage_name">Usage Name</label>
                            <select data-placeholder="Select Usage Name ..." class="form-control select2-normal select2-hidden-accessible" id="usage_name" name="usage_name" tabindex="-1" aria-hidden="true">
                                @foreach($CatList as $item)
                                    <option value="{{ $item['usage_name'] }}" {{ ($lensnumber->usage_name == $item['usage_name'] ) ? "selected":'' }}> {{ $item['usage_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                         <div class="form-group">
                            <label for="type">Type</label>
                            <select data-placeholder="Select Usage Name ..." class="form-control select2-normal select2-hidden-accessible" id="usage_name" name="type" tabindex="-1" aria-hidden="true">
                                @foreach($TypeList as $item)
                                    <option value="{{ $item['type'] }}" {{ ($lensnumber->type == $item['type'] ) ? "selected":'' }}> {{ $item['type'] }}</option>
                                @endforeach
                            </select>
                        </div>
                            
                             <div class="form-group">
                            <label for="options">Options</label>
                            <select data-placeholder="Select Usage Name ..." class="form-control select2-normal select2-hidden-accessible" id="usage_name" name="options" tabindex="-1" aria-hidden="true">
                                @foreach($OptionList as $item)
                                    <option value="{{ $item['options'] }}" {{ ($lensnumber->options == $item['options'] ) ? "selected":'' }}> {{ $item['options'] }}</option>
                                @endforeach
                            </select>
                        </div>
                            
                            

                         {!! CoralsForm::text('price','Price',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}

                       
                    </div>
                    <div class="col-md-6">
                            
                    </div>
                </div>
                {!! CoralsForm::customFields($lensnumber, 'col-md-6') !!}

                <div class="row">
                    <div class="col-md-12">
                        {!! CoralsForm::formButtons() !!}
                    </div>
                </div>
                {!! CoralsForm::closeForm($lensnumber) !!}
            @endcomponent
        </div>
    </div>
@endsection

@section('js')
@endsection