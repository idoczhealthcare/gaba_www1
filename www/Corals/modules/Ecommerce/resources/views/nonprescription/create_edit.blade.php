@extends('layouts.crud.create_edit')

@section('css')
@endsection

@section('content_header')
    @component('components.content_header')
        @slot('page_title')
            {{ $title_singular }}
        @endslot

        @slot('breadcrumb')
            {{ Breadcrumbs::render('ecommerce_nonprescription_create_edit') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    @parent
    <div class="row">
        <div class="col-md-8">
            @component('components.box')
                {!! CoralsForm::openForm($nonprescription) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! CoralsForm::text('type','Ecommerce::attributes.nonprescription.type',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         {!! CoralsForm::text('options','Ecommerce::attributes.nonprescription.options',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         {!! CoralsForm::text('price','Ecommerce::attributes.nonprescription.price',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                        
                         
                        
<!--                       {!!  CoralsForm::select('id','Ecommerce::attributes.lensnumber.ecommerce_lense_category', \Ecommerce::getLensCatList(),false,null,[], 'select2') !!}-->
                       
<!--
                        {!! CoralsForm::number('price','Ecommerce::attributes.lensnumber.id',true,$lensnumber->price ?? 0.0,
                       array_merge(['right_addon'=>'<i class="fa fa-fw fa-'.strtolower(  \Settings::get('admin_currency_code', 'USD')).'"></i>',
                       'step'=>0.01,'min'=>0,'max'=>999999])) !!}
-->
                       
                    </div>
                    <div class="col-md-6">
                            
                    </div>
                </div>
                {!! CoralsForm::customFields($nonprescription, 'col-md-6') !!}

                <div class="row">
                    <div class="col-md-12">
                        {!! CoralsForm::formButtons() !!}
                    </div>
                </div>
                {!! CoralsForm::closeForm($nonprescription) !!}
            @endcomponent
        </div>
    </div>
@endsection

@section('js')
@endsection