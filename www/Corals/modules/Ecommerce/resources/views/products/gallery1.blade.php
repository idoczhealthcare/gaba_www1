
<div id="gallery{{$var}}" >
    
    @if($editable)
        {!! \Html::style('assets/corals/plugins/dropzone-4.3.0/dropzone.min.css') !!}
        {!! \Html::script('assets/corals/plugins/dropzone-4.3.0/dropzone.min.js') !!}
        <script>
            Dropzone.autoDiscover = false;
        </script>
    @endif

    <ul class="list-inline sub_gallery">
        @foreach($product->getMedia('ecommerce-sub-product-gallery_subimg'.$var) as $media)
      
            <li class="gallery-item text-center list-inline-item" style="width: 48%">
                @if($media->getCustomProperty('featured', false))
                    <span class="featured-item"><i class="fa fa-fw fa-2x fa-star"></i></span>
                @endif
                <a href="{{ $media->getUrl() }}" data-lightbox="product-gallery">
                    <img src="{{ $media->getUrl() }}" class="img-responsive" alt="Product Gallery Image"/></a>
                @if($editable)
                    <div class="item-buttons" style="display: none;">
<!--
                        <a href="{{ url('e-commerce/products/'.$media->id.'/gallery/deletesub') }}"
                           class="btn btn-sm btn-danger item-button" title="Delete Gallery Item"
                           data-action="delete" data-page_action="reloadGallery">
                            <i class="fa fa-fw fa-remove"></i>
                        </a>
--><a class="btn btn-sm btn-danger item-button" title="Delete Gallery Item" onclick='del({{$media->id}},{{$var}},"{{$product->hashed_id}}")'>
                            <i class="fa fa-fw fa-remove"></i>
                        </a>
                        @if(!$media->getCustomProperty('featured', false))
                            <a href="{{ url('e-commerce/products/'.$media->id.'/gallery/mark-as-featuredsub') }}"
                               class="btn btn-sm btn-warning item-button"
                               title="Mark as Featured" data-action="post" data-page_action="reloadGallery">
                                <i class="fa fa-fw fa-star"></i>
                            </a>
                        @endif
                    </div>
                @endif
            </li>
        @endforeach
    </ul>
    @if($editable)
        <div class="">
            <form action="{{ url($resource_url.'/'.$product->hashed_id.'/gallery/uploadsub') }}" class="dropzone"
                  id="galleryDZ{{$var}}">
                 <input type="hidden" name="colorid" value="{{ $var }}">
                {{ csrf_field() }}
            </form>
        </div>
        <script type="text/javascript">
            var galleryDZ = new Dropzone("#galleryDZ{{$var}}", {
                maxFilesize: 1, // MB
                acceptedFiles: 'image/*',
                dictDefaultMessage: '<i class="fa fa-plus fa-fw fa-5x add-photo"></i>',
                queuecomplete: function () {
                    
                    
                    $('#gallery{{$var}}').load("{{ url($resource_url.'/'.$product->hashed_id.'/gallerysub/'.$var) }}");
                       // alert();
               
                },
                error: function (file, response) {
                    var message;
                    if ($.type(response) === "string")
                        var message = response; //dropzone sends it's own error messages in string
                    else
                        var message = response.message;
                    file.previewElement.classList.add("dz-error");
                    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                    _results = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        node = _ref[_i];
                        _results.push(node.textContent = message);
                    }
                    return _results;
                }
            });
            var base_url = window.location.origin;
            function del(id,abc,hashid){
               // alert({{ $product->hashed_id }});
            $.ajax({url: window.base_url+'/e-commerce/products/'+id+'/gallery/deletesub', success: function(result){
   setTimeout(function () {
                   var url = window.base_url+'/e-commerce/products/'+ hashid +'/gallerysub/'+abc ;
                       
                         $('#gallery'+abc).load(url);
                }, 500);
  }});}
//            function reloadGallery() {
//               // alert();
//                setTimeout(function () {
//                    alert('#gallery{{$var}}');
//                    $('#gallery2').append( "<p>Test</p>" );
//                }, 500);
//            }
            function reloadGallery1() {
                //alert();
                setTimeout(function () {
                    //alert('#gallery{{$var}}');
                    $('#gallery{{$var}}').append( "<p>Test</p>" );
                }, 500);
            }
          
            function initGalleryItemButtons() {
                $(document).on("mouseenter", ".gallery-item", function (e) {
                    $(this).find(".item-buttons").show();
                });

                $(document).on("mouseleave", ".gallery-item", function (e) {
                    $(this).find(".item-buttons").hide();
                });
            }
            window.onload = function () {
                
                initGalleryItemButtons();
            }
        </script>
    @endif
</div>