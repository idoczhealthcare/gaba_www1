@extends('layouts.crud.create_edit')

@section('css')
@endsection

@section('content_header')
    @component('components.content_header')
        @slot('page_title')
            {{ $title_singular }}
        @endslot

        @slot('breadcrumb')
            {{ Breadcrumbs::render('ecommerce_product_create_edit') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    @parent
<form id="sale_frm">
    <style>
        .form-group {
            margin-bottom: 15px !important;
        }
    </style>
    <div class="col-md-6">
        
        {!! CoralsForm::select('category_id','Ecommerce::attributes.product.categories', \Ecommerce::getsalecate(),true,null,['multiple'=>false], 'select2') !!}

        <div class="form-group">
            <label for="inventory_value">Sale Percentage</label>
            <input min="0" max="100" class="form-control" id="per" placeholder=""  name="percentage" type="number" value="">
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="button" id="submit">Submit</button>
        </div>
        
    </div>
</form>

<div class="col-sm-12" id="tbl">
    <table class="table table-borderd">
        <tr>
            <th>Category</th>
            <th>Sale</th>
            <th width="150">Status</th>
            <th>Action</th>
        </tr>
        @foreach($sale as $item)
        <tr> 
            <td>{{ $item['category_id']!=0 ? \Ecommerce::getcatname($item['category_id']) : 'ALl Products' }}</td>
            <td>{{ $item['percentage'] }}%</td>
            <td><select class="form-control status">
                <option data="{{ $item['category_id'] }}" value="active" {{ $item['status']=='active' ? 'selected' : '' }}>Active</option>
                <option data="{{ $item['category_id'] }}" value="inactive" {{ $item['status']=='inactive' ? 'selected' : '' }}>InActive</option>
                </select></td>
            <td><button class="btn btn-danger del" data="{{  $item['category_id'] }}"><i class="fa fa-close"></i></button></td>
            
        </tr>
        @endforeach
    </table>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        var base_url = window.location.origin;
        $(document).on('click','#submit',function(){
            // Prevent default posting of form - put here to work in case of errors
            event.preventDefault();
            $.ajax({
                url:  window.base_url +"/e-commerce/addsale",
                type: 'POST',
                data: $('#sale_frm').serialize(),
                success: function(data)
                { 
                    //alert(data);
                   $('#tbl').load(document.URL + ' #tbl');
                }    
            });
        });
        
        $(document).on('click','.del',function()
        {
            var cat = $(this).attr('data');     
            $.ajax({
                url:  window.base_url +"/e-commerce/delsale",
                type: 'POST',
                data: {'category' : cat},
                success: function(data)
                { 
                    $('#tbl').load(document.URL + ' #tbl');
                }    
            });
        });
        
        $(document).on('change','.status',function()
        {
            var status = $(this).val(); 
            
            var cat = $('option:selected', this).attr('data');; 
            //alert(cat);
            $.ajax({
                url:  window.base_url +"/e-commerce/salestatus",
                type: 'POST',
                data: {'status' : status,'category' : cat},
                success: function(data)
                { 
                    $('#tbl').load(document.URL + ' #tbl');
                }    
            });
        });
        
    });
</script>
@endsection