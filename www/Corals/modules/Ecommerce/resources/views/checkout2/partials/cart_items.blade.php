{!! Form::open( ['url' => url($urlPrefix.'checkout/step/billing-shipping-address'),'method'=>'POST', 'class'=>'ajax-form','id'=>'checkoutForm']) !!}

<div class="row">
    <div class="col-md-12 shopping-cart">
        <div class="table-responsive">
            <table class="table color-table info-table table table-hover table-striped table-condensed">
                <tbody>
                @foreach (ShoppingCart::getItems() as $item)
                    <tr id="item-{{$item->getHash()}}" class="product-item m-t-0">
                        <td class="table-image">
                            <a href="{{ url('shop', [$item->id->product->slug]) }}" target="_blank">
                                <img src="{{ $item->id->image }}" alt="SKU Image"
                                     class="img-rounded img-responsive" width="50"></a>
                        </td>
                    </tr>

                @endforeach
                
            </table>

            <table class="table color-table info-table table table-hover table-striped table-condensed">
                <tbody>
                @foreach (ShoppingCart::getItems() as $item)
                <thead>
                <tr>
                    <th>@lang('Ecommerce::labels.cart.product')</th>
                    <th>@lang('Ecommerce::labels.cart.quantity')</th>
                    <th>@lang('Ecommerce::labels.cart.price')</th>
                    <th>Subtotal</th>
                </tr>
                </thead>
                    <tr>

                        <td>
                            <h4 class="product-title">
                                <a target="_blank"
                                   href="{{url('shop', [$item->id->product->slug])}}">{{ $item->id->product->name }}
                                    [{{ $item->id->code }}]</a>
                            </h4>
                            {!! $item->id->present('options')  !!}

                            {!! \Actions::do_action('post_billing-shipping-address', $item) !!}
                        </td>
                        <td><b>{{ $item->qty }}</b></td>
                        <td id="item-total-{{$item->getHash()}}">{{ \Payments::currency($item->qty * $item->price) }}</td>
                        <td id="sub_total">{{ ShoppingCart::subTotal() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



{!! Form::close() !!}