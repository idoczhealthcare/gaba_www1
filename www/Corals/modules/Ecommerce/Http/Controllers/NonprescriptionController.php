<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\DataTables\NonprescriptionDataTable;
use Corals\Modules\Ecommerce\Http\Requests\NonprescriptionRequest;
use Corals\Modules\Ecommerce\Models\Nonprescription;

class NonprescriptionController extends BaseController
{


    public function __construct()
    {


        $this->resource_url = config('ecommerce.models.nonprescription.resource_url');
        $this->title = 'Ecommerce::module.nonprescription.title';
        $this->title_singular = 'Ecommerce::module.nonprescription.title_singular';
        parent::__construct();
    }

    /**
     * @param LensTypeRequest $request
     * @param LensTypeDataTable $dataTable
     * @return mixed
     */
    public function index(NonprescriptionRequest $request, NonprescriptionDataTable $dataTable)
    {
        return $dataTable->render('Ecommerce::nonprescription.index');
    }

    /**
     * @param LensTypeRequest $request
     * @return $this
     */
    public function create(NonprescriptionRequest $request)
    {
        $nonprescription = new Nonprescription();

        $this->setViewSharedData(['title_singular' => trans('Corals::labels.create_title', ['title' => $this->title_singular])]);

        return view('Ecommerce::nonprescription.create_edit')->with(compact('nonprescription'));
    }

    /**
     * @param LensTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(NonprescriptionRequest $request)
    {
        try {
            $data = $request->except('users', 'products');
            $data['type'] = strtolower($request->get('type')); 
            $data['options'] = strtolower($request->get('options')); 
            Nonprescription::create($data);

            flash(trans('Corals::messages.success.created', ['item' => $this->title_singular]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, Nonprescription::class, 'store');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return LensType
     */
    public function show(NonprescriptionRequest $request, Nonprescription $nonprescription)
    {
       $nonprescription = Nonprescription::all();
        return $nonprescription;
    }
//    public function show1()
//    {
//       $nonprescription = Nonprescription::with('lenstype','lenssubcat','lenscat')->get();
//        return $nonprescription;
//    }

    /**
     * @param LensNumberRequest $request
     * @param LensNumber $lensnumber
     * @return $this
     */
    public function edit(NonprescriptionRequest $request, Nonprescription $nonprescription)
    {
        //$lensnumber = new LensNumber();
        $this->setViewSharedData(['title_singular' => trans('Corals::labels.update_title', ['title' =>$this->title_singular])]);
//ecommerce_lensnumber_create_edit
        return view('Ecommerce::nonprescription.create_edit')->with(compact('nonprescription'));
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(NonprescriptionRequest $request, Nonprescription $nonprescription)
    {
        try {
            $data = $request->except('users', 'products');
            $data['type'] = strtolower($request->get('type')); 
            $data['options'] = strtolower($request->get('options')); 
            $nonprescription->update($data);

            flash(trans('Corals::messages.success.updated', ['item' => trans('Ecommerce::module.nonprescription.index_title')]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, Nonprescription::class, 'update');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(NonprescriptionRequest $request, Nonprescription $nonprescription)
    {
        try {
            $nonprescription->delete();

            $message = ['level' => 'success', 'message' => trans('Corals::messages.success.deleted', ['item' => trans('Ecommerce::module.nonprescription.index_title')])];
        } catch (\Exception $exception) {
            log_exception($exception, Nonprescription::class, 'destroy');
            $message = ['level' => 'error', 'message' => $exception->getMessage()];
        }

        return response()->json($message);
    }

}