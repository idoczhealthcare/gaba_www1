<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Modules\CMS\Traits\SEOTools;
use Corals\Foundation\Http\Controllers\PublicBaseController;
use Corals\Modules\Ecommerce\Facades\Shop;
use Corals\Modules\Ecommerce\Models\Product;
use Corals\Modules\Ecommerce\Models\SubProduct;
use Corals\Modules\Ecommerce\Models\Color;
use Corals\Modules\Ecommerce\Models\Size;
use Corals\Modules\Ecommerce\Models\LensCategory;
use Corals\Modules\Ecommerce\Models\LensSubCat;
use Corals\Modules\Ecommerce\Models\LensType;
use Corals\Modules\Ecommerce\Models\LensNumber;
use Corals\Modules\Ecommerce\Models\Nonprescription;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Media;

class ShopPublicController extends PublicBaseController
{
    use SEOTools;

    /**
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $layout = $request->get('layout', 'grid');

        $products = Shop::getProducts($request);

        $item = [
            'title' => 'Shop',
            'meta_description' => 'E-commerce Shop',
            'url' => url('shop'),
            'type' => 'shop',
            'image' => \Settings::get('site_logo'),
            'meta_keywords' => 'shop,e-commerce,products'
        ];

        $this->setSEO((object)$item);

        $shopText = null;

        if ($request->has('search')) {
            $shopText = trans('Ecommerce::labels.shop.search_results_for', ['search' => $request->get('search')]);
        }

        $sortOptions = trans(config('ecommerce.models.shop.sort_options'));


        if (\Settings::get('ecommerce_rating_enable') == "true") {
            $sortOptions['average_rating'] = trans('Ecommerce::attributes.product.average_rating');
        }

        return view('templates.shop')->with(compact('layout', 'products', 'shopText', 'sortOptions'));
    }
    
    
    public function shopsale(Request $request)
    {
        $layout = $request->get('layout', 'grid');

        $products = Shop::getProducts_sale($request);

        $item = [
            'title' => 'Shop',
            'meta_description' => 'E-commerce Shop',
            'url' => url('shop'),
            'type' => 'shop',
            'image' => \Settings::get('site_logo'),
            'meta_keywords' => 'shop,e-commerce,products'
        ];

        $this->setSEO((object)$item);

        $shopText = null;

        if ($request->has('search')) {
            $shopText = trans('Ecommerce::labels.shop.search_results_for', ['search' => $request->get('search')]);
        }

        $sortOptions = trans(config('ecommerce.models.shop.sort_options'));


        if (\Settings::get('ecommerce_rating_enable') == "true") {
            $sortOptions['average_rating'] = trans('Ecommerce::attributes.product.average_rating');
        }

        return view('templates.shop')->with(compact('layout', 'products', 'shopText', 'sortOptions'));
    }
    
    
    
    public function show1()
    {
       return view('templates.welcome1');
    }


    public function show(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);
        }

        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());

        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $subProd_color = SubProduct::where('product_id',$product->id)->groupBy('color')->get();
        $color = [];
        foreach($subProd_color as $item):
            $color[] = Color::where('id',$item['color'])->get();
        endforeach;

        $subProd_size = SubProduct::where('product_id',$product->id)->get();

        $size = [];
        foreach($subProd_size as $item):
            $size[] = Size::where('id',$item['size'])->get();
        endforeach;
        

        $this->setSEO((object)$item);
        $lens_subcat = LensSubCat::all();
        $lens_lenstype = LensType::all();
        $lens_category = LensCategory::all();
        

        return view('templates/product_single')->with(compact('product','lens_subcat','lens_lenstype','lens_category','subProd','color','size'));
    }
    
    public function loadfeatureimag(Request $request){
        $color = $request->get("color");
        $product_id = $request->get("pid");
        
        $media_collection = 'ecommerce-sub-product-gallery_subimg'.$color;
        $product = Product::where('id',$product_id)->first();
        $subproduct = SubProduct::where('product_id',$product_id)->where('color',$color)->first();
        $url = '';
        if(!($medias = $product->getMedia($media_collection))->isEmpty()){
            $get_media = '';
            foreach ($medias as $key => $value):
                if($value->getCustomProperty('featured')){
                    $url = $value->getUrl();
                    break;
                }
            endforeach;
           
            /*foreach ($get_media as $key => $value) {
                $url[]= [$value->getUrl(),$value->id,$value->getCustomProperty('featured', false)? 'active' : ''];
            }*/
        }
         $sale_price = $subproduct->sale_price;
            $regular_price = $subproduct->price;
            $per = 0;
            if($sale_price!=''){
                $discount = (1 - ($sale_price / $regular_price)) * 100;
                $per = number_format($discount, 0, '.', '');
            }
        $price =  \Payments::currency($subproduct->price);
        //$sale=0;
        $sale = \Payments::currency($subproduct->sale_price);;
        return response([$url,$sale,$price,$per]);
        
    }

    public function getsize(Request $request){
        $color = $request->get("color");
        $product_id = $request->get("pid");


        $subProd_size = SubProduct::where('product_id',$product_id)->where('color',$color)->get();

        $size = [];
        foreach($subProd_size as $item):
            $size[] = Size::where('id',$item['size'])->get();
        endforeach;

        $media_collection = 'ecommerce-sub-product-gallery_subimg'.$color;
        $product = Product::where('id',$product_id)->first();
        //$medias = $product->getMedia($media_collection);
        $url = [];
        if(!($medias = $product->getMedia($media_collection))->isEmpty()){
            $get_media = [];
            foreach ($medias as $key => $value):
                if($value->collection_name==$media_collection){
                    $get_media[] = $value;
                }
            endforeach;
           
            foreach ($get_media as $key => $value) {
                $url[]= [$value->getUrl(),$value->id,$value->getCustomProperty('featured', false)? 'active' : ''];
                # code...->getUrl()
            }
        }

        return response([$size,$url]);
    }

    public function getprice(Request $request){
        $color = $request->get("color");
        $product_id = $request->get("pid");
        $size = $request->get("size");
        $subProd_price = SubProduct::where('product_id',$product_id)->where('color',$color)->where('size',$size)->first();
        /*Old Code*/
        /*$price = \Payments::currency($subProd_price->price);
        return response($price);*/
        if($subProd_price->sale_price!=Null){
            $con_price = \Payments::currency($subProd_price->sale_price);
            $price = $subProd_price->sale_price;
            $del_price =  \Payments::currency($subProd_price->price);
            $dl = $subProd_price->price;
        }
        else{
            $con_price = \Payments::currency($subProd_price->price);
            $price = $subProd_price->price;
            $del_price = 0;
            $dl = 0;
        }
        
        return response([$price,$con_price,$del_price,$dl]);
    }

    
    public function configure(Request $request, $slug)
    {
        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);
        }
        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());



        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $this->setSEO((object)$item);
        $lens_subcat = LensSubCat::all();
        $lens_lenstype = LensType::all();
        $lens_category = LensCategory::all();
        

        return view('templates/configure')->with(compact('product','lens_subcat','lens_lenstype','lens_category'));
    }
    
    
    
//    public function configurejson(Request $request)
//    {
//        
//        //SELECT * FROM `ecommerce_lense_number` WHERE ((`sphere_min`<= 0 AND `sphere_max`>=0) AND (`cylinder_min`<=0 AND `cylinder_max` >=0)) and type_id =1 and `mapping_id`=1
//        $value = $request->get("value");
//        $value1 = $request->get("value1");
//        $typeid = $request->get("typeid");
//        $lenstype = LensType::where('type_name',$typeid)->get();
//        $cat_name = $request->get("cat_id");
//        $cat_id = LensCategory::where('len_cat_name',$cat_name)->get();
//        $subcat_name = $request->get("sub_id");
//        $lens_subcat = LensSubCat::where('len_subcat_name', $subcat_name)->get();
//        //var_dump($lens_subcat);
//        
//        //$lensnumber = LensNumber::where('sphere_min','<=', $value)->where('sphere_max','>=', $value)->get();
//       // var_dump($lensnumber);
//        $lensnumber = LensNumber::where('sphere_min','<=', $value)->where('sphere_max','>=', $value)->where('cylinder_min','<=', $value1)->where('cylinder_max','>=', $value1)->where('type_id',$lenstype[0]->id)->where('cat_id', $cat_id[0]->id)->where('subcat_id', $lens_subcat[0]->id)->get();
//        //var_dump($lensnumber);
//       
//      
//        
//        return response()->json($lensnumber);
//
//    }
    public function configurejson(Request $request)
    {
        try {
//            //SELECT * FROM `ecommerce_lense_number` WHERE ((`sphere_min`<= 0 AND `sphere_max`>=0) AND (`cylinder_min`<=0 AND `cylinder_max` >=0)) and type_id =1 and `mapping_id`=1
//            $value = $request->get("value");
//            $value1 = $request->get("value1");
//            $typeid = $request->get("typeid");
//            $lenstype = LensType::where('type_name',$typeid)->get();
//            $cat_name = $request->get("cat_id");
//            $cat_id = LensCategory::where('len_cat_name',$cat_name)->get();
//            $subcat_name = $request->get("sub_id");
//            $lens_subcat = LensSubCat::where('len_subcat_name', $subcat_name)->get();
//            //var_dump($lens_subcat);
//
//            //$lensnumber = LensNumber::where('sphere_min','<=', $value)->where('sphere_max','>=', $value)->get();
//           // var_dump($lensnumber);
//            $lensnumber = LensNumber::where('sphere_min','<=', abs($value))->where('sphere_max','>=', abs($value))->where('cylinder_min','<=', abs($value1))->where('cylinder_max','>=', abs($value1))->where('type_id',$lenstype[0]->id)->where('cat_id', $cat_id[0]->id)->where('subcat_id', $lens_subcat[0]->id)->first();
//            //var_dump($lensnumber);
//
//            /*Old Code*/
//            /*$test = \Payments::currency($lensnumber->price);
//            return response()->json($test);*/
//
//            $price = $lensnumber->price;
//            $con_price = \Payments::currency($lensnumber->price);
//            return response()->json([$price,$con_price]);
              
        $value = $request->get("value");
        $value1 = $request->get("value1");
        $typeid = strtolower($request->get("typeid"));
        $cat_name = strtolower($request->get("cat_id"));
        $subcat_name = strtolower($request->get("sub_id"));
        $lensnumber= LensNumber::whereRaw('SUBSTRING_INDEX(cylinder, "-", -1) >= '.abs($value1))->whereRaw('SUBSTRING_INDEX(cylinder, "-", 1) <= '.abs($value1))->whereRaw('SUBSTRING_INDEX(sphere, "-", -1) >= '.abs($value))->whereRaw('SUBSTRING_INDEX(sphere, "-", 1) <= '.abs($value))->where('options',$subcat_name)->where('usage_name',$cat_name)->where('type',$typeid)->first();
        $price = $lensnumber->price;
        $con_price = \Payments::currency($lensnumber->price);
        return response()->json([$price,$con_price]);
          
        } catch (\Exception $exception) {
            log_exception($exception, Product::class, 'store');
        }

    }
    
    
    public function configurenonprescrip(Request $request)
    {
         try {
        $type = strtolower($request->get("type"));
        $option = strtolower($request->get("option"));
        
        $lensnumber = Nonprescription::where('type',$type)->where('options',$option)->first();
        
        $price = $lensnumber->price;
        $con_price = \Payments::currency($lensnumber->price);
        
          flash(trans('Corals::messages.success.created', ['item' => $this->title_singular]))->success();
             return response()->json([$price,$con_price]);
        } catch (\Exception $exception) {
            log_exception($exception, Product::class, 'store');
        }
        

    }
    
    
    public function getsubcat(Request $request)
    {
        $value = $request->get("value");
        $subid = LensNumber::distinct()->select('type')->where('usage_name', $value)->get();
        return response()->json($subid);
    }

    
    public function gettype(Request $request)
    {
        $right_sphere = $request->get("right_sphere");
        $right_cylinder = $request->get("right_cylinder");
        $left_sphere = $request->get("left_sphere");
        $left_cylinder = $request->get("left_cylinder");
          
        $value = $request->get("value");
        $value1 = $request->get("value1");
        $lensnumber= LensNumber::select('options','price')->whereRaw('SUBSTRING_INDEX(cylinder, "-", -1) >= '.abs($right_cylinder))->whereRaw('SUBSTRING_INDEX(cylinder, "-", 1) <= '.abs($right_cylinder))->whereRaw('SUBSTRING_INDEX(sphere, "-", -1) >= '.abs($right_sphere))->whereRaw('SUBSTRING_INDEX(sphere, "-", 1) <= '.abs($right_sphere))->where('usage_name',$value1)->where('type',$value)->get();
                $lensnumber1= LensNumber::select('options','price')->whereRaw('SUBSTRING_INDEX(cylinder, "-", -1) >= '.abs($left_cylinder))->whereRaw('SUBSTRING_INDEX(cylinder, "-", 1) <= '.abs($left_cylinder))->whereRaw('SUBSTRING_INDEX(sphere, "-", -1) >= '.abs($left_sphere))->whereRaw('SUBSTRING_INDEX(sphere, "-", 1) <= '.abs($left_sphere))->where('usage_name',$value1)->where('type',$value)->get();
//        foreach($lensnumber as $item):
//             echo $item->options;
//        endforeach;
        
       
       
        
//        $subid = LensNumber::distinct()->select('options')->where('usage_name', $value1)->where('type', $value)->get();
        //return response()->json($lensnumber);
        return response()->json([$lensnumber,$lensnumber1]);
    }
    
        //shop_contact
    public function shop_contact(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);

        }
        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());

        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $this->setSEO((object)$item);
        $lens_subcat = LensSubCat::all();
        $lens_lenstype = LensType::all();
        $lens_category = LensCategory::all();
        

        return view('templates/product_lens')->with(compact('product','lens_subcat','lens_lenstype','lens_category'));
    }
    
        public function products(){
        $products = Product::pluck('name')->all();
        return $products;
    }
    
    
    
    
    
    
    
    
    
    
    
    
}