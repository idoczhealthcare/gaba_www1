<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\Models\Sale;
use Corals\Modules\Ecommerce\Models\SKU;
use Corals\Modules\Ecommerce\Models\Product;
use Corals\Modules\Ecommerce\Models\SubProduct;
use Corals\Modules\Ecommerce\Facades\Shop;
use Illuminate\Http\Request;


use Corals\Modules\Ecommerce\Http\Requests\SaleRequest;

class SaleController extends BaseController
{


    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.sale.resource_url');
        $this->title = 'Ecommerce::module.sale.title';
        $this->title_singular = 'Ecommerce::module.sale.title_singular';
        parent::__construct();
    }

    /**
     * @param LensTypeRequest $request
     * @param LensTypeDataTable $dataTable
     * @return mixed
     */
    public function sale()
    {
        $sale = Sale::get();
        return view('Ecommerce::products.sale')->with(compact('sale'));
    }
    
    /*public function get_pids(){
        Product::where('category_id',$id)
    }*/
    
    public function addsale(SaleRequest $request)
    {
       // $category_id   = $request->get('category_id');
        $data['category_id'] = $request->get('category_id');
        $data['percentage']     = $request->get('percentage');
        $after = '';
        $test='dfhtd';
        $subafter = '';
        $sale = Sale::where('category_id',$data['category_id'])->first();
        if($sale!=null){
            
            Sale::where('category_id',$data['category_id'])->update($data);
           
             if($data['category_id']==0){
                $data2['status'] = 'inactive';
                Sale::where('category_id','!=',0)->update($data2);
                $test='test';
                $pids =Product::all();
                foreach($pids as $item):
                 $test = $item->id;
                    $reg_price =SKU::where('product_id',$item->id)->first();
                   // $after = $reg_price['regular_price'];
                    $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $data['percentage'])/100));
                    SKU::where('product_id',$item->id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $data['percentage'])/100));
                        //SubProduct::where('id',$sub->id)->update(['sale_price'=>$subafter]);
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
            }
             else
            {
                $test='test1';
                $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$data['category_id'])->get();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->product_id)->first();
                   // $after = $reg_price['regular_price'];
                    $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $data['percentage'])/100));
                    SKU::where('product_id',$item->product_id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->product_id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $data['percentage'])/100));
                        //SubProduct::where('id',$sub->id)->update(['sale_price'=>$subafter]);
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
            }
        }
        else{
            
           
            Sale::create($data);
            
          
           
            if($data['category_id']==0)
            { 
                $data2['status'] = 'inactive';
                Sale::where('category_id','!=',0)->update($data2);
                //Sale::where('category_id',0)->update(['status','active']);
                $pids =Product::all();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->id)->first();
                   // $after = $reg_price['regular_price'];
                    $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $data['percentage'])/100));
                    SKU::where('product_id',$item->id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $data['percentage'])/100));
                        //SubProduct::where('id',$sub->id)->update(['sale_price'=>$subafter]);
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
            }
             else
            { $test='test3';
                $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$data['category_id'])->get();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->product_id)->first();
                   // $after = $reg_price['regular_price'];
                    $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $data['percentage'])/100));
                    SKU::where('product_id',$item->product_id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->product_id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $data['percentage'])/100));
                        //SubProduct::where('id',$sub->id)->update(['sale_price'=>$subafter]);
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
            }
            
        }
       // $a =var_dump(Sale::get());
        //$testtt=DB::table('ecommerce_category_product')->get();
        
        return $test;
        //var_dump(Sale::get());
    }
    
    public function delsale(Request $request)
    {
        $data['category_id']     = $request->get('category');
        $after = '';
        $subafter = '';
        $sale = Sale::where('category_id',$data['category_id'])->first();
        if($sale!=null){
            Sale::where('category_id',$data['category_id'])->delete();
            
            if($data['category_id']==0)
            { 
                $test='test2';
                $pids =Product::all();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->id)->first();
                    $after = null;
                    SKU::where('product_id',$item->id)->update(['sale_price'=>$after]);
                    $subporduct=SubProduct::where('product_id',$item->id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        $subafter = null;
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
                $sale1 = Sale::where('status','active')->where('category_id','!=',0)->get();
                foreach($sale1 as $saleitem){
                    $test = 'asjkfbasjk';
                    $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$saleitem['category_id'])->get();
                    foreach($pids as $item):
                        $reg_price =SKU::where('product_id',$item->product_id)->first();
                        $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $saleitem['percentage'])/100));
                        SKU::where('product_id',$item->product_id)->update(['sale_price'=>$after]);
                        
                        $subporduct=SubProduct::where('product_id',$item->product_id)->get();
                        foreach($subporduct as $sub):
                            $sub_regular = SubProduct::where('id',$sub->id)->first();
                            $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $saleitem['percentage'])/100));
                            DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                        endforeach;
                    endforeach;
                }
            }
            else{
                $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$data['category_id'])->get();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->product_id)->first();
                    $after = null;
                    SKU::where('product_id',$item->product_id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->product_id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        $subafter = null;
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
            }
        }
        return $subafter;
    }
    
    
    public function salestatus(Request $request)
    {
        $data['category_id']     = $request->get('category');
        $data['status']     = $request->get('status');
        
        $after = '';
        $subafter = '';
        $test = '';
        $sale = Sale::where('category_id',$data['category_id'])->first();
        if($sale!=null){
            Sale::where('category_id',$data['category_id'])->update($data);
            $data['percentage'] = $sale->percentage;
            if($data['category_id']==0)
            { 
                $test='test2';  
                $pids =Product::all();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->id)->first();
                    if($data['status']=='active'){
                        $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $data['percentage'])/100));
                    }
                    else{
                        $after = null;
                    }

                    SKU::where('product_id',$item->id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        if($data['status']=='active'){
                            $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $data['percentage'])/100));
                        }
                        else{
                            $subafter = null;
                        }
                       
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
                $sale1 = Sale::where('status','active')->where('category_id','!=',0)->get();
                foreach($sale1 as $saleitem){
                    $test = 'asjkfbasjk';
                    $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$saleitem['category_id'])->get();
                    foreach($pids as $item):
                        $reg_price =SKU::where('product_id',$item->product_id)->first();
                        $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $saleitem['percentage'])/100));
                        SKU::where('product_id',$item->product_id)->update(['sale_price'=>$after]);
                        
                        $subporduct=SubProduct::where('product_id',$item->product_id)->get();
                        foreach($subporduct as $sub):
                            $sub_regular = SubProduct::where('id',$sub->id)->first();
                            $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $saleitem['percentage'])/100));
                            DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                        endforeach;
                    endforeach;
                }
               //  $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$data['category_id'])->get();
            }
            else{
                $test='test1';
                $pids =DB::table('ecommerce_category_product')->select('product_id')->where('category_id',$data['category_id'])->get();
                foreach($pids as $item):
                    $reg_price =SKU::where('product_id',$item->product_id)->first();
                    if($data['status']=='active'){
                        $after = ($reg_price['regular_price']  - (($reg_price['regular_price']  * $data['percentage'])/100));
                    }
                    else{
                        $after = null;
                    }
                    
                    SKU::where('product_id',$item->product_id)->update(['sale_price'=>$after]);

                    $subporduct=SubProduct::where('product_id',$item->product_id)->get();
                    foreach($subporduct as $sub):
                        $sub_regular = SubProduct::where('id',$sub->id)->first();
                        if($data['status']=='active'){
                            $subafter = ($sub_regular['price']  - (($sub_regular['price']  * $data['percentage'])/100));
                        }
                        else{
                            $subafter = null;
                        }
                        DB::table('ecommerce_sub_product')->where('id',$sub->id)->update(['sale_price'=>$subafter]);
                    endforeach;
                endforeach;
            }
        }
        return $test;
    }
    

}