<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\DataTables\LensNumberDataTable;
use Corals\Modules\Ecommerce\Http\Requests\LensNumberRequest;
use Corals\Modules\Ecommerce\Models\LensNumber;

class LensNumberController extends BaseController
{


    public function __construct()
    {


        $this->resource_url = config('ecommerce.models.lensnumber.resource_url');
        $this->title = 'Ecommerce::module.lensnumber.title';
        $this->title_singular = 'Ecommerce::module.lensnumber.title_singular';
        parent::__construct();
    }

    /**
     * @param LensTypeRequest $request
     * @param LensTypeDataTable $dataTable
     * @return mixed
     */
    public function index(LensNumberRequest $request, LensNumberDataTable $dataTable)
    {
        return $dataTable->render('Ecommerce::lensnumber.index');
    }

    /**
     * @param LensTypeRequest $request
     * @return $this
     */
    public function create(LensNumberRequest $request)
    {
        $lensnumber = new LensNumber();
//        print_r (explode("-",$lensnumber->sphere));
        $CatList = LensNumber::distinct()->select('usage_name')->get();
        $OptionList = LensNumber::distinct()->select('options')->get();
        $TypeList = LensNumber::distinct()->select('type')->get();
        $this->setViewSharedData(['title_singular' => trans('Corals::labels.create_title', ['title' => $this->title_singular])]);

        return view('Ecommerce::lensnumber.create_edit')->with(compact('lensnumber','CatList','OptionList','TypeList'));
    }

    /**
     * @param LensTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(LensNumberRequest $request)
    {
        try {
            $data = $request->except('users', 'products','sphere_min','sphere_max','cylinder_min','cylinder_max');
            $data->sphere = $request->get('sphere_min').'-'.$request->get('sphere_max');
            $data->cylinder = $request->get('cylinder_min').'-'.$request->get('cylinder_max');

            LensNumber::create($data);

            flash(trans('Corals::messages.success.created', ['item' => $this->title_singular]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, LensNumber::class, 'store');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return LensType
     */
    public function show(LensNumberRequest $request, LensNumber $lensnumber)
    {
       $lensNumber = LensNumber::with('lenstype','lenssubcat','lenscat')->get();
        return $lensNumber;
    }
    public function show1()
    {
       $lensNumber = LensNumber::with('lenstype','lenssubcat','lenscat')->get();
        return $lensNumber;
    }

    /**
     * @param LensNumberRequest $request
     * @param LensNumber $lensnumber
     * @return $this
     */
    public function edit(LensNumberRequest $request, LensNumber $lensnumber)
    {
        //$lensnumber = new LensNumber();
         $CatList = LensNumber::distinct()->select('usage_name')->get();
        $OptionList = LensNumber::distinct()->select('options')->get();
        $TypeList = LensNumber::distinct()->select('type')->get();
        $this->setViewSharedData(['title_singular' => trans('Corals::labels.update_title', ['title' =>$this->title_singular])]);
//ecommerce_lensnumber_create_edit
        return view('Ecommerce::lensnumber.create_edit')->with(compact('lensnumber','CatList','OptionList','TypeList'));
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(LensNumberRequest $request, LensNumber $lensnumber)
    {
        try {
            $data = $request->except('users', 'products');

            $lensnumber->update($data);

            flash(trans('Corals::messages.success.updated', ['item' => trans('Ecommerce::module.lensnumber.index_title')]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, LensNumber::class, 'update');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(LensNumberRequest $request, LensNumber $lensnumber)
    {
        try {
            $lensnumber->delete();

            $message = ['level' => 'success', 'message' => trans('Corals::messages.success.deleted', ['item' => trans('Ecommerce::module.lensnumber.index_title')])];
        } catch (\Exception $exception) {
            log_exception($exception, LensNumber::class, 'destroy');
            $message = ['level' => 'error', 'message' => $exception->getMessage()];
        }

        return response()->json($message);
    }

}