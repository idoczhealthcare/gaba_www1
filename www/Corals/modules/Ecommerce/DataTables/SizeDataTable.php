<?php

namespace Corals\Modules\Ecommerce\DataTables;

use Corals\Foundation\DataTables\BaseDataTable;
use Corals\Modules\Ecommerce\Models\Size;
use Corals\Modules\Ecommerce\Transformers\SizeTransformer;
use Yajra\DataTables\EloquentDataTable;

class SizeDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $this->setResourceUrl(config('ecommerce.models.Size.resource_url'));

        $dataTable = new EloquentDataTable($query);

        return $dataTable->setTransformer(new SizeTransformer());
    }

    /**
     * Get query source of dataTable.
     * @param LensType $model
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function query(Size $model)
    {
        return $model->newQuery();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        //TODO: Set Translations
        return [
            'id' => ['visible' => false],
            'title' => ['title' => 'Title','searchable' => true],
            'name' => ['name' => 'Name','searchable' => true]
        ];
    }

    protected function getOptions()
    {
        return ['has_action' => true];
    }
}
