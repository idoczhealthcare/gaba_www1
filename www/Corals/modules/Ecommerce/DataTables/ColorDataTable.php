<?php

namespace Corals\Modules\Ecommerce\DataTables;

use Corals\Foundation\DataTables\BaseDataTable;
use Corals\Modules\Ecommerce\Models\Color;
use Corals\Modules\Ecommerce\Transformers\ColorTransformer;
use Yajra\DataTables\EloquentDataTable;

class ColorDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $this->setResourceUrl(config('ecommerce.models.Color.resource_url'));

        $dataTable = new EloquentDataTable($query);

        return $dataTable->setTransformer(new ColorTransformer());
    }

    /**
     * Get query source of dataTable.
     * @param LensType $model
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function query(Color $model)
    {
        return $model->newQuery();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        //TODO: Set Translations
        return [
            'id' => ['visible' => false],
            'title' => ['title' => 'Title','searchable' => true],
            'logo' => ['title' => 'Thumbnail','searchable' => false]
        ];
    }

    protected function getOptions()
    {
        return ['has_action' => true];
    }
}
