<?php

namespace Corals\Modules\Ecommerce\DataTables;

use Corals\Foundation\DataTables\BaseDataTable;
use Corals\Modules\Ecommerce\Models\Nonprescription;
use Corals\Modules\Ecommerce\Transformers\NonprescriptionTransformer;
use Yajra\DataTables\EloquentDataTable;

class NonprescriptionDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $this->setResourceUrl(config('ecommerce.models.nonprescription.resource_url'));

        $dataTable = new EloquentDataTable($query);

        return $dataTable->setTransformer(new NonprescriptionTransformer());
    }

    /**
     * Get query source of dataTable.
     * @param LensType $model
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function query(Nonprescription $model)
    {
        
        return $model->newQuery();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        //TODO: Set Translations
        return [
            'id' => ['visible' => false],
          
            'type' => ['title' => 'Type','searchable' => true],
            'options' =>['title' => 'Options','searchable' => true],
         
            'price' => ['title' => 'Price','searchable' => true],

        ];
    }
//protected function getFilters()
//    {
//        return [
//            'name' => ['title' => trans('Ecommerce::attributes.product.title_name'), 'class' => 'col-md-2', 'type' => 'text', 'condition' => 'like', 'active' => true],
//            'description' => ['title' => trans('Ecommerce::attributes.product.description'), 'class' => 'col-md-3', 'type' => 'text', 'condition' => 'like', 'active' => true],
//            'brand.id' => ['title' =>  trans('Ecommerce::attributes.product.brand'), 'class' => 'col-md-2', 'type' => 'select2', 'options' => \Ecommerce::getBrandsList(), 'active' => true],
//            'status' => ['title' => trans('Ecommerce::attributes.product.status_product'), 'class' => 'col-md-2', 'checked_value' => 'active', 'type' => 'boolean', 'active' => true],
//        ];
//    }
    protected function getOptions()
    {
        return ['has_action' => true];
    }
}
