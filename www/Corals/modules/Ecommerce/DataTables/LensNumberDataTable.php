<?php

namespace Corals\Modules\Ecommerce\DataTables;

use Corals\Foundation\DataTables\BaseDataTable;
use Corals\Modules\Ecommerce\Models\LensNumber;
use Corals\Modules\Ecommerce\Transformers\LensNumberTransformer;
use Yajra\DataTables\EloquentDataTable;

class LensNumberDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $this->setResourceUrl(config('ecommerce.models.lensnumber.resource_url'));
//        Log::emergency($query);
//Log::alert($query);
//Log::critical($query);
//Log::error($query);
//Log::warning($query);
//Log::notice($query);
//Log::info($message);
//Log::debug($message);
        $dataTable = new EloquentDataTable($query);

        return $dataTable->setTransformer(new LensNumberTransformer());
    }

    /**
     * Get query source of dataTable.
     * @param LensType $model
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function query(LensNumber $model)
    {
        
        return $model->newQuery();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        //TODO: Set Translations
        return [
            'id' => ['visible' => false],
            'usage_name' => ['title' => 'Usage','searchable' => true],
            'type' => ['title' => 'Type','searchable' => true],
            'options' => ['title' => 'Options','searchable' => true],
            'sphere' => ['title' => 'Sphere','searchable' => true],        
            'cylinder' => ['title' => 'Cylinder','searchable' => true],
            'price' => ['title' => 'Price','searchable' => true],

        ];
    }

    protected function getOptions()
    {
        return ['has_action' => true];
    }
}
