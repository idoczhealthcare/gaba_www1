$(document).ready(function() {
        $('.testimonials-slider').slick( {
                dots: false, autoplay: true, autoplaySpeed: 5000, infinite: true, speed: 300, slidesToShow: 2, adaptiveHeight: true
            }
        );
        $(".main-client-strip-slider").slick( {
                slidesToShow: 1, arrows: true, infinite: false, dots: false, infinite: true,
            }
        );
        $(".mobile-client-strip-slider").slick( {
                arrows:true, infinite:false, dots:false, infinite:true, slidesToShow:1, slidesToScroll:1, responsive:[ {
                    breakpoint:800, settings: {
                        arrows: true, slidesToShow: 1
                    }
                }
                ]
            }
        );
        $('.price-slider').slick( {
                dots:true, infinite:false, speed:300, slidesToShow:3, slidesToScroll:1, autoplay:true, autoplaySpeed:2000, responsive:[ {
                    breakpoint:1024, settings: {
                        slidesToShow: 2, slidesToScroll: 1, infinite: true, dots: true
                    }
                }
                    , {
                        breakpoint:600, settings: {
                            slidesToShow: 2, slidesToScroll: 2
                        }
                    }
                    , {
                        breakpoint:480, settings: {
                            slidesToShow: 1, slidesToScroll: 1
                        }
                    }
                ]
            }
        );
        $(".sliderxs").slick( {
                arrows:false, dots:true, autoplay:true, adaptiveHeight:true, responsive:[ {
                    breakpoint: 10000, settings: "unslick"
                }
                    , {
                        breakpoint:767, settings: {
                            unslick: true
                        }
                    }
                ]
            }
        );
    }

);