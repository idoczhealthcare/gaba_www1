@extends('layouts.master')

@section('css')
    <style type="text/css">
        .sw-theme-arrows > ul.step-anchor {
            display: flex !important;
        }

        .shipping-options label {
            padding-left: 0;
            display: block;
        }

        .sw-btn-group .btn {
            background-color: #f5f5f5;
            border-color: #e1e7ec;
        }

        .sw-btn-group .btn:hover {
            background-color: #dcdbdb;
        }

        .btn-group .btn:first-child:not(:last-child):not(.dropdown-toggle).btn-secondary, .btn-group .btn:first-child:not(:last-child):not(.dropdown-toggle).btn-outline-secondary {
            border-right: 1px solid #e1e7ec;
        }

        .btn-group .btn:first-child:not(:last-child):not(.dropdown-toggle) {
            margin-right: 10px;
            padding-right: 22px;

        }

        #checkoutWizard2,#checkoutWizard{
            /*border: 2px solid #a29f9f;*/
            margin-bottom: 100px;
            box-shadow: 0px 5px 10px 0px #0000002e;
        }
        #checkoutWizard2 nav.navbar.btn-toolbar.sw-toolbar.sw-toolbar-bottom{
            display: none !important;
        }

        .nav-tabs{
            margin-top: 0px !important;
        }

        .tab-content img{
            height: 303px;
        }

        div#checkoutSteps{
          
        }

        body {
            line-height: normal !important;
        }

        .nav-tabs > li > a::after{
            background: #f5f5f5 !important;
        }
        /*.sw-theme-arrows > ul.step-anchor > li:hover > a:before{
                content: " " !important;
                display: block !important;
                width: 0 !important;
                height: 0 !important;
                border-top: 50px solid transparent !important;
                border-bottom: 50px solid transparent !important;
                border-left: 30px solid #ddd !important;
                position: absolute !important;
                top: 50% !important;
                margin-top: -50px !important;
                margin-left: 1px !important;
                left: 100% !important;
                z-index: 1 !important;
        }*/
         .nav-tabs > li:hover > a::after{
            transform: scale(0) !important;
        }
        li a::before{
            border-left: 30px solid #f5f5f5 !important;
        }
        .sw-theme-arrows > ul.step-anchor > li.active > a:after{
            transform: scale(0) !important;
        }
        .sw-theme-arrows > ul.step-anchor > li.active > a{

            background-color: #262626 !important;
        }
        li.active a::before{
            border-left: 30px solid #262626 !important;
            margin-left: -1px !important;
        }
        .table-striped>tbody>tr:nth-of-type(odd){background-color: #ffffff!important;}
        .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default{
            background-color: #262626;
            border-color: #262626;}
        li.done a::before{
            border-left: 30px solid #404040 !important;
            margin-left: -1px !important;
        }
    </style>
@endsection

@section('editable_content')
    @php \Actions::do_action('pre_content', null, null) @endphp


    <div class="container-fluid my-5" style="margin-top: 10%">
        <div class="col-md-8">
            <div id="checkoutWizard">
                <ul>
                    <li>
                        <a href="#billing-shipping-address"
                       data-content-url="{{url('checkout/step/billing-shipping-address')}}">
                        @lang('corals-ecommerce-basic::labels.template.checkout.address')<br/>
                        <small></small>
                        </a>
                    </li>

                    @if($enable_shipping)
                    <li><a href="#shipping-method"
                           data-content-url="{{url('checkout/step/shipping-method')}}">
                            @lang('corals-ecommerce-basic::labels.template.checkout.select_shipping')<br/>
                            <small></small>
                        </a>
                    </li>
                    @endif
             
                    <li>
                        <a href="#select-payment" data-content-url="{{url('checkout/step/select-payment')}}">
                        @lang('corals-ecommerce-basic::labels.template.checkout.select_payment')<br/>
                        <small></small>
                        </a>
                    </li>
                    <li>
                        <a href="#order-review" data-content-url="{{url('checkout/step/order-review')}}">
                        @lang('corals-ecommerce-basic::labels.template.checkout.order_review')<br/>
                        <small></small>
                        </a>
                    </li>    
                </ul> 
                
                <div class="mt-3" id="checkoutSteps">
                    <div id="billing-shipping-address" class="checkoutStep">
                        
                    </div>
                    @if($enable_shipping)
                        <div id="shipping-method" class="checkoutStep">
                        </div>
                    @endif
               
                    <div id="select-payment" class="checkoutStep">
                    </div>
                    <div id="order-review" class="checkoutStep">
                    </div>
                </div>
        </div>
    </div>



            <div class="col-md-4">
                <div id="checkoutWizard2">
                    <ul>  
                    <li>
                        <a href="#billing-shipping-address" data-content-url="{{url('checkout/step/cart-details')}}">
                            @lang('corals-ecommerce-basic::labels.template.checkout.detail')<br/>
                        </a>
                    </li>
                    </ul>
            <div class="mt-3" id="checkoutSteps2">
                <div id="billing-shipping-address" class="checkoutStep2">
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
    @include('Ecommerce::checkout.checkout_script')
@endsection