@extends('layouts.master')

@section('css')

<style type="text/css">
    /*3step checkout page start*/
.step{background-color: #f9f9f9;margin-top: 100px;}
.steps-form {
    display: table;
    width: 100%;
    position: relative; }
.btn-default:active{color: #fff!important;background-color: #404040!important;border-color: #8c8c8c;outline: none}
 .btn-default{padding: 10px 20px;font-weight: 600;border-radius: 4px;margin-top: 10px;background-color: #404040 !important;color: #ffff!important;
}
.btn-indigo:active {background-color: #404040 !important;color: #fff!important;}
.btn:active:focus:hover:active, .btn:focus:hover:active {background-color:none!important;color: none!important;outline: none!important;}
.steps-form .steps-row {
    display: table-row; }
.steps-form .steps-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc; }
.steps-form .steps-row .steps-step {
    display: table-cell;
    text-align: center;
    position: relative; }
.steps-form .steps-row .steps-step p {
    margin-top: 0.5rem; }
.steps-form .steps-row .steps-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important; }
.steps-form .steps-row .steps-step .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
    margin-top: 0; }
.step .first-step-box{
    background-color: white;
    padding: 30px;
    border-radius: 20px;
    box-shadow: 0px 2px 10px 0px;
    border: 1px solid #f9f9f9;
    margin-top: 30px;
}
.first-step-box >h3{text-align: center;}
.step .first-step-box:hover{background-color: #e9e8e800;color: #fff!important;border: none;box-shadow: 0px 4px 13px 0px #726b6b8c;}
.stp-hed h4{font-size: 15px;}
.stp-hed{text-align: center; border-bottom: 1px solid;border-bottom-style: dotted;border-bottom-color: #ed4f20;}
.option-content{margin-top: 25px;}
.option-content .text-box {width: 53.9%;display: table-cell;text-align: left;vertical-align: middle;}
.text-box p{font-size: 15px; line-height: 1.5;}
.option-content .text-box {
    width: 65.9%;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
}
.option-content .img-box, .option-content .img-box-three, .option-content .img-box-two {width: 34.1%;display: table-cell;vertical-align: middle;}
.option-content .img-box .im-rx-distance {background-position: -2px -2px;}
.add-rx-container {
    position: relative;
    padding: 2% 2% 2% 3%;
    border-bottom: 1px solid #d3d3d3;
}
.add-rx-container .clearfix{clear: both;list-style: none;padding: 0;}
.add-rx-container .add-rx-title {
    width: 17%;
    padding-left: 0;
    padding-right: 0;
    text-align: center;
    font-weight: 500;
}
.add-rx-container li {
    width: 20%;
    float: left;
    padding: 6px 3% 2%;
}

.add-rx-container .add-rx-title>label {
    font-size: 14px;
}
.add-rx-container .add-rx-title>label {
    margin-bottom: 0;
    color: #404040;
    font-weight: 800;
}
.add-rx-container .add-rx-title>label>span {
    display: block;
    font-size: 12px;
    white-space: nowrap;
    color: #a6a6a6;
}
.add-rx-container .add-rx-title {
    padding-top: 9%;
    border-right: 1px solid #f1f1f1;
}
.add-rx-container li {
    width: 20%;
    float: left;
    padding: 6px 3% 2%;
}
.add-rx-container .rx-name-title {
    text-align: center;
}
.add-rx-container .rx-name-title {
    margin-bottom: 0;
    padding-top: 26.5%;
    font-size: 14px;
    font-weight: 500;
    line-height: 1.3;
    white-space: nowrap;
    color: #a6a6a6;
    font-weight: 600;
}
.rx-select>select {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    width: 100%;
    z-index: 1;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    background-color: #f9f9f9;
    border: none;
    border-bottom: 1px solid #cdcdcd;
    display: inline-block;
    padding: .5rem 2rem .5rem 1rem;
    border-radius: 0;
    position: relative;
    width: 119%\9;
}
.add-rx-container li>label {
    display: block;
    padding: 17% 0;
    margin-bottom: 11%;
}
.rx-input input {
    line-height: normal;
    BACKGROUND-COLOR: #f9f9f9;
    border-bottom: 1px solid #cdcdcd;
}

.rx-select>select>option {
    color: #000;
}
.rx-select:after {
    position: absolute;
    bottom: 15px;
    right: .46875rem;
    z-index: 3;
    content: "\e61c";
    color: #999;
    font-style: normal;
    font-weight: 400;
    font-size: 1.3rem;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    margin-bottom: -.3em;
    pointer-events: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    z-index: 0\9;
}
.add-pd-container .add-rx-title, .add-prism-container .add-rx-title, .add-rx-container .add-rx-title {
    width: 17%;
    padding-left: 0;
    padding-right: 0;
    text-align: center;
    font-weight: 500;
}
.add-pd-container .add-rx-title {
    width: calc(17% + 1px);
}
.add-pd-container li {
    display: inline-block;
    float: left;
    padding: 5px;
    list-style: none;
}
.rx-input, .rx-select {
    height: 33px;
    font-weight: 700;
    font-size: 16px;
}
.add-pd-container {
    position: relative;
    padding: 2% 0% 4% 0%;
}
.add-pd-container .rx-pd-checkbox {
    padding-right: 2%;
    padding-left: 3%;
    white-space: nowrap;
}
.add-pd-container .pd-add-two {
    width: 20%;
    padding-right: 3%;
    padding-left: 3%;
    display: none;
}
.rx-input>input, .rx-input>select, .rx-select>input, .rx-select>select {
    height: 100%;
}
.checkbox-ui {
    position: relative;
    display: inline-block;
    width: 19px;
    height: 19px;
    vertical-align: middle;
}
.checkbox-ui label {
    cursor: pointer;
    position: absolute;
    width: 19px;
    height: 19px;
    top: 0;
    left: 0;
    border: 1px solid #ccc;
    background: url(./assets/img/im-checked.png) 4px 25px no-repeat #fff;
}
.card-result h3{font-size: 28px;margin: 0 0 10px;}
.card-result h6{font-size: 20px;color: #b4b4b5;font-weight: 400;}
.buying-glass span{text-align: center;display: inherit;}
.glasses-rate{margin-top: 10px;}
.glasses-rate p{font-size: 18px;font-weight: 500;}
.glasses-rate span{font-size: 25px;font-weight: 800;}
.glasses-buy-bth{margin-top: 30px;text-align: center;}
.glasses-buy-bth a{background-color: #231f20;color: #fff;font-size: 20px;padding: 10px 30px 10px 30px;}
.glasses-buy-bth a:hover{background-color: #ed4e24;}
.panel-body .shipping{padding: 0px;}
/*3step checkout form start*/
</style>
@endsection

@section('editable_content')
    @php \Actions::do_action('pre_content',$product, null) @endphp
    <!--
 <select id="cat">
                @foreach ($lens_category as $lens)
        <option name='category' value="{{ $lens->id }}">{{ $lens->len_cat_name }}</option>
                @endforeach
            </select>
                <select id="subcat">
@foreach ($lens_subcat as $lens)
        <option value="{{ $lens->id }}">{{ $lens->len_subcat_name }}</option>
                @endforeach
            </select>
                <select id="lenstype">
@foreach ($lens_lenstype as $lens)
        <option value="{{ $lens->id }}">{{ $lens->type_name }}</option>
                @endforeach
            </select>
-->
    <!--                    <button onclick="myfun()">hello</button>-->
    <!-- Page Content-->
    <div class="container padding-bottom-3x my-5 m_t_12em">
        <div class="row">
            <!-- Product Gallery-->
            <div class="col-md-8">
                    <div class="card-body mb-4">
                        <!-- Stepper -->
                        <div class="step">
                            <!-- Steps form -->
                            <div class="card">
                                <div class="card-body mb-4">
                                    <!-- Stepper -->
                                    <div class="steps-form">
                                        <div class="steps-row setup-panel">
                                            <div class="steps-step">
                                                <a href="#step-9" type="button" class="btn btn-indigo btn-circle">1</a>
                                                <p>Usage 1</p>
                                            </div>
                                            <div class="steps-step">
                                                <a href="#step-10" type="button" class="btn btn-default btn-circle" >2</a>
                                                <p>Prescription</p>
                                            </div>
                                            <div class="steps-step">
                                                <a href="#step-11" type="button" class="btn btn-default btn-circle">3</a>
                                                <p>Lens Type</p>
                                            </div>
                                            <div class="steps-step">
                                                <a href="#step-12" type="button" class="btn btn-default btn-circle">4</a>
                                                <p>Lens Option</p>
                                            </div>
                                        </div>
                                    </div>

                                    <form role="form" action="" method="post">

                                        <!-- First Step -->

                                        <div class="row setup-content" id="step-9">
                                            <div class="col-md-12">
                                                <div class="row">
                                                   
                                                        <div class="col-md-6">

                                                            <div class="first-step-box nextBtn" onclick="getsubcat1(this.id)" id='{{ $lens_category[0]->id }}'>
                                                                <div class="stp-hed">
                                                                    <input type="hidden" value="{{ $lens_category[0]->id }}" id="cat">
                                                                    <h4  id="catname" >{{ $lens_category[0]->len_cat_name }}</h4>
                                                                </div>
                                                                <div class="option-content">
                                                                    <div class="img-box">
                                                                        <figure>
                                                                            <img src="../assets/themes/ecommerce-basic/img/lens-icon-1.png">
                                                                        </figure>
                                                                    </div>
                                                                    <div class="text-box">
                                                                        <p class="option-intro">
                                                                            General use lenses for seeing things far away                                                        </p>
                                                                    </div>
                                                                    <p class="option-help"></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                               
                                                                           <div class="col-md-6">

                                                            <div class="first-step-box nextBtn" onclick="getsubcat1(this.id)" id='{{ $lens_category[1]->id }}'>
                                                                <div class="stp-hed">
                                                                    <input type="hidden" value="{{ $lens_category[1]->id }}" id="cat">
                                                                    <h4  id="catname" >{{ $lens_category[1]->len_cat_name }}</h4>
                                                                </div>
                                                                <div class="option-content">
                                                                    <div class="img-box">
                                                                        <figure>
                                                                            <img src="../assets/themes/ecommerce-basic/img/lens-icon-2.png">
                                                                        </figure>
                                                                    </div>
                                                                    <div class="text-box">
                                                                        <p class="option-intro">
                                                                            General use lenses for seeing things far away                                                        </p>
                                                                    </div>
                                                                    <p class="option-help"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <!--
                                <div class="col-md-6">
                                    <div class="first-step-box" id="distance">
                                        <div class="stp-hed">
                                            <h4 id="getValue">READING</h4>
                                        </div>
                                        <div class="option-content">
                                            <div class="img-box">
                                                <figure>
                                                    <img src="assets/img/icon-for-check-out-form.png">
                                                </figure>
                                            </div>
                                            <div class="text-box">
                                                <p class="option-intro">
                                                    General use lenses for seeing things up close                                                      </p>
                                            </div>
                                            <p class="option-help"></p>
                                        </div>
                                    </div>
                                </div>
-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="first-step-box nextBtn">
                                                            <div class="stp-hed">
                                                                <h4 id="getValue">MULTIFOCAL</h4>
                                                            </div>
                                                            <div class="option-content">
                                                                <div class="img-box">
                                                                    <figure>
                                                                        <img src="../assets/themes/ecommerce-basic/img/lens-icon-3.png">
                                                                    </figure>
                                                                </div>
                                                                <div class="text-box">
                                                                    <p class="option-intro">
                                                                        Lenses for seeing things both close and far away                                                     </p>
                                                                </div>
                                                                <p class="option-help"></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="first-step-box nextBtn">
                                                            <div class="stp-hed">
                                                                <h4 id="getValue">NON-PRESCRIPTION</h4>
                                                            </div>
                                                            <div class="option-content">
                                                                <div class="img-box">
                                                                    <figure>
                                                                        <img src="../assets/themes/ecommerce-basic/img/lens-icon-4.png">
                                                                    </figure>
                                                                </div>
                                                                <div class="text-box">
                                                                    <p class="option-intro">
                                                                        Lenses without any prescription                                                      </p>
                                                                </div>
                                                                <p class="option-help"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn-primary btn-indigo btn-rounded nextBtn float-right" type="button" onclick="getsubcat()">Next</button>
                                            </div>
                                        </div>

                                        <!-- Second Step -->
                                        <div class="row setup-content" id="step-10">
                                            <div class="col-md-12">
                                                <div class="add-rx-container">
                                                    <ul class="clearfix">
                                                        <li class="add-rx-title">
                                                            <label>OD<span>(Right eye)</span></label>
                                                            <label>OS<span>(Left eye)</span></label>
                                                        </li>
                                                        <li class="rx-sph">
                                                            <label class="rx-name-title"><span>Sphere</span> <em><i>(</i>SPH<i>)</i></em></label>
                                                            <label>
                                                                <div class="rx-select od-sph">
                                                                    <select id="right_sphere" name="reyesphere" class="" autofocus>
                                                                        <option value="-12.00">-12.00</option>
                                                                        <option value="-11.75">-11.75</option>
                                                                        <option value="-11.50">-11.50</option>
                                                                        <option value="-11.25">-11.25</option>
                                                                        <option value="-11.00">-11.00</option>
                                                                        <option value="-10.75">-10.75</option>
                                                                        <option value="-10.50">-10.50</option>
                                                                        <option value="-10.25">-10.25</option>
                                                                        <option value="-10.00">-10.00</option>
                                                                        <option value="-9.75">-9.75</option>
                                                                        <option value="-9.50">-9.50</option>
                                                                        <option value="-9.25">-9.25</option>
                                                                        <option value="-9.00">-9.00</option>
                                                                        <option value="-8.75">-8.75</option>
                                                                        <option value="-8.50">-8.50</option>
                                                                        <option value="-8.25">-8.25</option>
                                                                        <option value="-8.00">-8.00</option>
                                                                        <option value="-7.75">-7.75</option>
                                                                        <option value="-7.50">-7.50</option>
                                                                        <option value="-7.25">-7.25</option>
                                                                        <option value="-7.00">-7.00</option>
                                                                        <option value="-6.75">-6.75</option>
                                                                        <option value="-6.50">-6.50</option>
                                                                        <option value="-6.25">-6.25</option>
                                                                        <option value="-6.00">-6.00</option>
                                                                        <option value="-5.75">-5.75</option>
                                                                        <option value="-5.50">-5.50</option>
                                                                        <option value="-5.25">-5.25</option>
                                                                        <option value="-5.00">-5.00</option>
                                                                        <option value="-4.75">-4.75</option>
                                                                        <option value="-4.50">-4.50</option>
                                                                        <option value="-4.25">-4.25</option>
                                                                        <option value="-4.00">-4.00</option>
                                                                        <option value="-3.75">-3.75</option>
                                                                        <option value="-3.50">-3.50</option>
                                                                        <option value="-3.25">-3.25</option>
                                                                        <option value="-3.00">-3.00</option>
                                                                        <option value="-2.75">-2.75</option>
                                                                        <option value="-2.50">-2.50</option>
                                                                        <option value="-2.25">-2.25</option>
                                                                        <option value="-2.00">-2.00</option>
                                                                        <option value="-1.75">-1.75</option>
                                                                        <option value="-1.50">-1.50</option>
                                                                        <option value="-1.25">-1.25</option>
                                                                        <option value="-1.00">-1.00</option>
                                                                        <option value="-0.75">-0.75</option>
                                                                        <option value="-0.50">-0.50</option>
                                                                        <option value="-0.25">-0.25</option>
                                                                        <option value="0.00" selected>0.00</option>

                                                                        <option value="0.25">+0.25</option>
                                                                        <option value="0.50">+0.50</option>
                                                                        <option value="0.75">+0.75</option>
                                                                        <option value="1.00">+1.00</option>
                                                                        <option value="1.25">+1.25</option>
                                                                        <option value="1.50">+1.50</option>
                                                                        <option value="1.75">+1.75</option>
                                                                        <option value="2.00">+2.00</option>
                                                                        <option value="2.25">+2.25</option>
                                                                        <option value="2.50">+2.50</option>
                                                                        <option value="2.75">+2.75</option>
                                                                        <option value="3.00">+3.00</option>
                                                                        <option value="3.25">+3.25</option>
                                                                        <option value="3.50">+3.50</option>
                                                                        <option value="3.75">+3.75</option>
                                                                        <option value="4.00">+4.00</option>
                                                                        <option value="4.25">+4.25</option>
                                                                        <option value="4.50">+4.50</option>
                                                                        <option value="4.75">+4.75</option>
                                                                        <option value="5.00">+5.00</option>
                                                                        <option value="5.25">+5.25</option>
                                                                        <option value="5.50">+5.50</option>
                                                                        <option value="5.75">+5.75</option>
                                                                        <option value="6.00">+6.00</option>
                                                                        <option value="6.25">+6.25</option>
                                                                        <option value="6.50">+6.50</option>
                                                                        <option value="6.75">+6.75</option>
                                                                        <option value="7.00">+7.00</option>
                                                                        <option value="7.25">+7.25</option>
                                                                        <option value="7.50">+7.50</option>
                                                                        <option value="7.75">+7.75</option>
                                                                        <option value="8.00">+8.00</option>
                                                                        <option value="8.25">+8.25</option>
                                                                        <option value="8.50">+8.50</option>
                                                                        <option value="8.75">+8.75</option>
                                                                        <option value="9.00">+9.00</option>
                                                                        <option value="9.25">+9.25</option>
                                                                        <option value="9.50">+9.50</option>
                                                                        <option value="9.75">+9.75</option>
                                                                        <option value="10.00">+10.00</option>
                                                                        <option value="10.25">+10.25</option>
                                                                        <option value="10.50">+10.50</option>
                                                                        <option value="10.75">+10.75</option>
                                                                        <option value="11.00">+11.00</option>
                                                                        <option value="11.25">+11.25</option>
                                                                        <option value="11.50">+11.50</option>
                                                                        <option value="11.75">+11.75</option>
                                                                        <option value="12.00">+12.00</option>
                                                                    </select>
                                                                </div>
                                                            </label>
                                                            <label>
                                                                <div class="rx-select os-sph">
                                                                    <select id="left_sphere" name="leyesphere" class="" tabindex=2>
                                                                        <option value="-12.00">-12.00</option>
                                                                        <option value="-11.75">-11.75</option>
                                                                        <option value="-11.50">-11.50</option>
                                                                        <option value="-11.25">-11.25</option>
                                                                        <option value="-11.00">-11.00</option>
                                                                        <option value="-10.75">-10.75</option>
                                                                        <option value="-10.50">-10.50</option>
                                                                        <option value="-10.25">-10.25</option>
                                                                        <option value="-10.00">-10.00</option>
                                                                        <option value="-9.75">-9.75</option>
                                                                        <option value="-9.50">-9.50</option>
                                                                        <option value="-9.25">-9.25</option>
                                                                        <option value="-9.00">-9.00</option>
                                                                        <option value="-8.75">-8.75</option>
                                                                        <option value="-8.50">-8.50</option>
                                                                        <option value="-8.25">-8.25</option>
                                                                        <option value="-8.00">-8.00</option>
                                                                        <option value="-7.75">-7.75</option>
                                                                        <option value="-7.50">-7.50</option>
                                                                        <option value="-7.25">-7.25</option>
                                                                        <option value="-7.00">-7.00</option>
                                                                        <option value="-6.75">-6.75</option>
                                                                        <option value="-6.50">-6.50</option>
                                                                        <option value="-6.25">-6.25</option>
                                                                        <option value="-6.00">-6.00</option>
                                                                        <option value="-5.75">-5.75</option>
                                                                        <option value="-5.50">-5.50</option>
                                                                        <option value="-5.25">-5.25</option>
                                                                        <option value="-5.00">-5.00</option>
                                                                        <option value="-4.75">-4.75</option>
                                                                        <option value="-4.50">-4.50</option>
                                                                        <option value="-4.25">-4.25</option>
                                                                        <option value="-4.00">-4.00</option>
                                                                        <option value="-3.75">-3.75</option>
                                                                        <option value="-3.50">-3.50</option>
                                                                        <option value="-3.25">-3.25</option>
                                                                        <option value="-3.00">-3.00</option>
                                                                        <option value="-2.75">-2.75</option>
                                                                        <option value="-2.50">-2.50</option>
                                                                        <option value="-2.25">-2.25</option>
                                                                        <option value="-2.00">-2.00</option>
                                                                        <option value="-1.75">-1.75</option>
                                                                        <option value="-1.50">-1.50</option>
                                                                        <option value="-1.25">-1.25</option>
                                                                        <option value="-1.00">-1.00</option>
                                                                        <option value="-0.75">-0.75</option>
                                                                        <option value="-0.50">-0.50</option>
                                                                        <option value="-0.25">-0.25</option>
                                                                        <option value="0.00" selected>0.00</option>

                                                                        <option value="0.25">+0.25</option>
                                                                        <option value="0.50">+0.50</option>
                                                                        <option value="0.75">+0.75</option>
                                                                        <option value="1.00">+1.00</option>
                                                                        <option value="1.25">+1.25</option>
                                                                        <option value="1.50">+1.50</option>
                                                                        <option value="1.75">+1.75</option>
                                                                        <option value="2.00">+2.00</option>
                                                                        <option value="2.25">+2.25</option>
                                                                        <option value="2.50">+2.50</option>
                                                                        <option value="2.75">+2.75</option>
                                                                        <option value="3.00">+3.00</option>
                                                                        <option value="3.25">+3.25</option>
                                                                        <option value="3.50">+3.50</option>
                                                                        <option value="3.75">+3.75</option>
                                                                        <option value="4.00">+4.00</option>
                                                                        <option value="4.25">+4.25</option>
                                                                        <option value="4.50">+4.50</option>
                                                                        <option value="4.75">+4.75</option>
                                                                        <option value="5.00">+5.00</option>
                                                                        <option value="5.25">+5.25</option>
                                                                        <option value="5.50">+5.50</option>
                                                                        <option value="5.75">+5.75</option>
                                                                        <option value="6.00">+6.00</option>
                                                                        <option value="6.25">+6.25</option>
                                                                        <option value="6.50">+6.50</option>
                                                                        <option value="6.75">+6.75</option>
                                                                        <option value="7.00">+7.00</option>
                                                                        <option value="7.25">+7.25</option>
                                                                        <option value="7.50">+7.50</option>
                                                                        <option value="7.75">+7.75</option>
                                                                        <option value="8.00">+8.00</option>
                                                                        <option value="8.25">+8.25</option>
                                                                        <option value="8.50">+8.50</option>
                                                                        <option value="8.75">+8.75</option>
                                                                        <option value="9.00">+9.00</option>
                                                                        <option value="9.25">+9.25</option>
                                                                        <option value="9.50">+9.50</option>
                                                                        <option value="9.75">+9.75</option>
                                                                        <option value="10.00">+10.00</option>
                                                                        <option value="10.25">+10.25</option>
                                                                        <option value="10.50">+10.50</option>
                                                                        <option value="10.75">+10.75</option>
                                                                        <option value="11.00">+11.00</option>
                                                                        <option value="11.25">+11.25</option>
                                                                        <option value="11.50">+11.50</option>
                                                                        <option value="11.75">+11.75</option>
                                                                        <option value="12.00">+12.00</option>
                                                                    </select>
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li class="rx-cyl">
                                                            <label class="rx-name-title"><span>Cylinder</span> <em><i>(</i>CYL<i>)</i></em></label>
                                                            <label>
                                                                <div class="rx-select od-cyl">
                                                                    <select id="right_cylinder" name="reyecylinder" class="">

                                                                        <option value="-3.00">-3.00</option>
                                                                        <option value="-2.75">-2.75</option>
                                                                        <option value="-2.50">-2.50</option>
                                                                        <option value="-2.25">-2.25</option>
                                                                        <option value="-2.00">-2.00</option>
                                                                        <option value="-1.75">-1.75</option>
                                                                        <option value="-1.50">-1.50</option>
                                                                        <option value="-1.25">-1.25</option>
                                                                        <option value="-1.00">-1.00</option>
                                                                        <option value="-0.75">-0.75</option>
                                                                        <option value="-0.50">-0.50</option>
                                                                        <option value="-0.25">-0.25</option>
                                                                        <option value="0.00" selected>0.00</option>

                                                                        <option value="0.25">+0.25</option>
                                                                        <option value="0.50">+0.50</option>
                                                                        <option value="0.75">+0.75</option>
                                                                        <option value="1.00">+1.00</option>
                                                                        <option value="1.25">+1.25</option>
                                                                        <option value="1.50">+1.50</option>
                                                                        <option value="1.75">+1.75</option>
                                                                        <option value="2.00">+2.00</option>
                                                                        <option value="2.25">+2.25</option>
                                                                        <option value="2.50">+2.50</option>
                                                                        <option value="2.75">+2.75</option>
                                                                        <option value="3.00">+3.00</option>

                                                                    </select>
                                                                </div>
                                                            </label>
                                                            <label>
                                                                <div class="rx-select os-cyl">
                                                                    <select id="left_cylinder" name="leyecylinder" class="">

                                                                        <option value="-3.00">-3.00</option>
                                                                        <option value="-2.75">-2.75</option>
                                                                        <option value="-2.50">-2.50</option>
                                                                        <option value="-2.25">-2.25</option>
                                                                        <option value="-2.00">-2.00</option>
                                                                        <option value="-1.75">-1.75</option>
                                                                        <option value="-1.50">-1.50</option>
                                                                        <option value="-1.25">-1.25</option>
                                                                        <option value="-1.00">-1.00</option>
                                                                        <option value="-0.75">-0.75</option>
                                                                        <option value="-0.50">-0.50</option>
                                                                        <option value="-0.25">-0.25</option>
                                                                        <option value="0.00" selected>0.00</option>

                                                                        <option value="0.25">+0.25</option>
                                                                        <option value="0.50">+0.50</option>
                                                                        <option value="0.75">+0.75</option>
                                                                        <option value="1.00">+1.00</option>
                                                                        <option value="1.25">+1.25</option>
                                                                        <option value="1.50">+1.50</option>
                                                                        <option value="1.75">+1.75</option>
                                                                        <option value="2.00">+2.00</option>
                                                                        <option value="2.25">+2.25</option>
                                                                        <option value="2.50">+2.50</option>
                                                                        <option value="2.75">+2.75</option>
                                                                        <option value="3.00">+3.00</option>

                                                                    </select>
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li class="rx-axis js-rx-axis">
                                                            <label class="rx-name-title"><em>Axis</em></label>
                                                            <label>
                                                                <div class="rx-input od-axis">
                                                                    <input type="text" size="3" value="" id="reyeaxis" name="reyeaxis" class="">
                                                                </div>
                                                            </label>
                                                            <label>
                                                                <div class="rx-input os-axis">
                                                                    <input type="text" size="3" value="" id="leyeaxis" name="leyeaxis" class="">
                                                                </div>
                                                            </label>
                                                        </li>
                                                        <li class="rx-add">
                                                            <label class="rx-name-title "><em>ADD</em></label>
                                                            <label>
                                                                <div class="rx-select os-add ">
                                                                    <select id="reyeadd" name="reyeadd" class="" >
                                                                        <option value="0" selected>0</option>
                                                                        <option value="0.25" class="">+0.25</option>
                                                                        <option value="0.50" class="">+0.50</option>
                                                                        <option value="0.75" class="">+0.75</option>
                                                                        <option value="1.00">+1.00</option>
                                                                        <option value="1.25">+1.25</option>
                                                                        <option value="1.50">+1.50</option>
                                                                        <option value="1.75">+1.75</option>
                                                                        <option value="2.00">+2.00</option>
                                                                        <option value="2.25">+2.25</option>
                                                                        <option value="2.50">+2.50</option>
                                                                        <option value="2.75">+2.75</option>
                                                                        <option value="3.00">+3.00</option>
                                                                        <option value="3.25">+3.25</option>
                                                                        <option value="3.50">+3.50</option>
                                                                        <option value="3.75" class="">+3.75</option>
                                                                        <option value="4.00" class="">+4.00</option>
                                                                        <option value="4.25" class="">+4.25</option>
                                                                        <option value="4.50" class="">+4.50</option>
                                                                        <option value="4.75" class="">+4.75</option>
                                                                        <option value="5.00" class="">+5.00</option>
                                                                        <option value="5.25" class="">+5.25</option>
                                                                        <option value="5.50" class="">+5.50</option>
                                                                        <option value="5.75" class="">+5.75</option>
                                                                        <option value="6.00" class="">+6.00</option>
                                                                        <option value="6.25" class="">+6.25</option>
                                                                        <option value="6.50" class="">+6.50</option>
                                                                        <option value="6.75" class="">+6.75</option>
                                                                        <option value="7.00" class="">+7.00</option>
                                                                        <option value="7.25" class="">+7.25</option>
                                                                        <option value="7.50" class="">+7.50</option>
                                                                        <option value="7.75" class="">+7.75</option>
                                                                        <option value="8.00" class="">+8.00</option>
                                                                    </select>
                                                                </div>
                                                            </label>
                                                            <label>
                                                                <div class="rx-select od-add ">
                                                                    <select id="leyeadd" name="leyeadd" class="" >
                                                                        <option value="0" selected>0</option>
                                                                        <option value="0.25" class="">+0.25</option>
                                                                        <option value="0.50" class="">+0.50</option>
                                                                        <option value="0.75" class="">+0.75</option>
                                                                        <option value="1.00">+1.00</option>
                                                                        <option value="1.25">+1.25</option>
                                                                        <option value="1.50">+1.50</option>
                                                                        <option value="1.75">+1.75</option>
                                                                        <option value="2.00">+2.00</option>
                                                                        <option value="2.25">+2.25</option>
                                                                        <option value="2.50">+2.50</option>
                                                                        <option value="2.75">+2.75</option>
                                                                        <option value="3.00">+3.00</option>
                                                                        <option value="3.25">+3.25</option>
                                                                        <option value="3.50">+3.50</option>
                                                                        <option value="3.75" class="">+3.75</option>
                                                                        <option value="4.00" class="">+4.00</option>
                                                                        <option value="4.25" class="">+4.25</option>
                                                                        <option value="4.50" class="">+4.50</option>
                                                                        <option value="4.75" class="">+4.75</option>
                                                                        <option value="5.00" class="">+5.00</option>
                                                                        <option value="5.25" class="">+5.25</option>
                                                                        <option value="5.50" class="">+5.50</option>
                                                                        <option value="5.75" class="">+5.75</option>
                                                                        <option value="6.00" class="">+6.00</option>
                                                                        <option value="6.25" class="">+6.25</option>
                                                                        <option value="6.50" class="">+6.50</option>
                                                                        <option value="6.75" class="">+6.75</option>
                                                                        <option value="7.00" class="">+7.00</option>
                                                                        <option value="7.25" class="">+7.25</option>
                                                                        <option value="7.50" class="">+7.50</option>
                                                                        <option value="7.75" class="">+7.75</option>
                                                                        <option value="8.00" class="">+8.00</option>
                                                                    </select>
                                                                </div>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="add-pd-container">
                                                    <ul class="clearfix">
                                                        <li class="add-rx-title">
                                                            <label>PD<span>(Pupillary Distance)</span></label>
                                                        </li>
                                                        <li class="pd-add-one js-pd-add-one" style="display:block" id='pdvalue'>
                                                            <div class="rx-select pd-select">
                                                                <select id="eyepd" name="eyepd" class="" >
                                                                    <option value="46">46</option>
                                                                    <option value="47">47</option>
                                                                    <option value="48">48</option>
                                                                    <option value="49">49</option>
                                                                    <option value="50">50</option>
                                                                    <option value="51">51</option>
                                                                    <option value="52">52</option>
                                                                    <option value="53">53</option>
                                                                    <option value="54">54</option>
                                                                    <option value="55">55</option>
                                                                    <option value="56">56</option>
                                                                    <option value="57">57</option>
                                                                    <option value="58">58</option>
                                                                    <option value="59">59</option>
                                                                    <option value="60">60</option>
                                                                    <option value="61">61</option>
                                                                    <option value="62">62</option>
                                                                    <option value="63">63</option>
                                                                    <option value="64">64</option>
                                                                    <option value="65">65</option>
                                                                    <option value="66">66</option>
                                                                    <option value="67">67</option>
                                                                    <option value="68">68</option>
                                                                    <option value="69">69</option>
                                                                    <option value="70">70</option>
                                                                    <option value="71">71</option>
                                                                    <option value="72">72</option>
                                                                    <option value="73">73</option>
                                                                    <option value="74">74</option>
                                                                    <option value="75">75</option>
                                                                    <option value="76">76</option>
                                                                    <option value="77">77</option>
                                                                    <option value="78">78</option>
                                                                    <option value="79">79</option>
                                                                    <option value="80">80</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="pd-add-two js-pd-add-two" style="display: none;" id='rightpdvalue'>
                                                            <div class="rx-select od-pd">
                                                                <select id="reyepd" name="reyepd" class=" ">
                                                                    <option value="0">Right PD</option>
                                                                    <option value="23.0">23.0</option>
                                                                    <option value="23.5">23.5</option>
                                                                    <option value="24.0">24.0</option>
                                                                    <option value="24.5">24.5</option>
                                                                    <option value="25.0">25.0</option>
                                                                    <option value="25.5">25.5</option>
                                                                    <option value="26.0">26.0</option>
                                                                    <option value="26.5">26.5</option>
                                                                    <option value="27.0">27.0</option>
                                                                    <option value="27.5">27.5</option>
                                                                    <option value="28.0">28.0</option>
                                                                    <option value="28.5">28.5</option>
                                                                    <option value="29.0">29.0</option>
                                                                    <option value="29.5">29.5</option>
                                                                    <option value="30.0">30.0</option>
                                                                    <option value="30.5">30.5</option>
                                                                    <option value="31.0">31.0</option>
                                                                    <option value="31.5">31.5</option>
                                                                    <option value="32.0">32.0</option>
                                                                    <option value="32.5">32.5</option>
                                                                    <option value="33.0">33.0</option>
                                                                    <option value="33.5">33.5</option>
                                                                    <option value="34.0">34.0</option>
                                                                    <option value="34.5">34.5</option>
                                                                    <option value="35.0">35.0</option>
                                                                    <option value="35.5">35.5</option>
                                                                    <option value="36.0">36.0</option>
                                                                    <option value="36.5">36.5</option>
                                                                    <option value="37.0">37.0</option>
                                                                    <option value="37.5">37.5</option>
                                                                    <option value="38.0">38.0</option>
                                                                    <option value="38.5">38.5</option>
                                                                    <option value="39.0">39.0</option>
                                                                    <option value="39.5">39.5</option>
                                                                    <option value="40.0">40.0</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="pd-add-two js-pd-add-two" style="display: none;" id='leftpdvalue'>

                                                            <div class="rx-select os-pd">
                                                                <select id="leyepd" name="leyepd" class=" ">
                                                                    <option value="0">Left PD</option>
                                                                    <option value="25.0">25.0</option>
                                                                    <option value="25.5">25.5</option>
                                                                    <option value="26.0">26.0</option>
                                                                    <option value="26.5">26.5</option>
                                                                    <option value="27.0">27.0</option>
                                                                    <option value="27.5">27.5</option>
                                                                    <option value="28.0">28.0</option>
                                                                    <option value="28.5">28.5</option>
                                                                    <option value="29.0">29.0</option>
                                                                    <option value="29.5">29.5</option>
                                                                    <option value="30.0">30.0</option>
                                                                    <option value="30.5">30.5</option>
                                                                    <option value="31.0">31.0</option>
                                                                    <option value="31.5">31.5</option>
                                                                    <option value="32.0">32.0</option>
                                                                    <option value="32.5">32.5</option>
                                                                    <option value="33.0">33.0</option>
                                                                    <option value="33.5">33.5</option>
                                                                    <option value="34.0">34.0</option>
                                                                    <option value="34.5">34.5</option>
                                                                    <option value="35.0">35.0</option>
                                                                    <option value="35.5">35.5</option>
                                                                    <option value="36.0">36.0</option>
                                                                    <option value="36.5">36.5</option>
                                                                    <option value="37.0">37.0</option>
                                                                    <option value="37.5">37.5</option>
                                                                    <option value="38.0">38.0</option>
                                                                    <option value="38.5">38.5</option>
                                                                    <option value="39.0">39.0</option>
                                                                    <option value="39.5">39.5</option>
                                                                    <option value="40.0">40.0</option>
                                                                </select>
                                                            </div>
                                                        </li>
<!--
                                                        <li class="rx-pd-checkbox">
                                                            <label class="pointer">
                                                                <div class="checkbox-ui">
                                                                    <input type="checkbox" id="noSamePd" name="noSamePd" class="">
                                                                    <label for="noSamePd"></label>
                                                                </div>
                                                                <span class="">Two PD numbers</span>
                                                            </label>
                                                        </li>
-->
                                                        <li>
                                                            <input type="checkbox" > <span class="">Two PD numbers</span>
                                                        </li>
                                                    </ul>
                                                   
                                                  
                                                </div>
                                                <button class="btn-primary btn-indigo btn-rounded prevBtn float-left" type="button">Previous</button>
                                                <button class="btn-primary btn-indigo btn-rounded nextBtn float-right" type="button">Next</button>
                                            </div>
                                        </div>

                                        <!-- Third Step -->
                                        <div class="row setup-content" id="step-11">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6 ">
                                                            <div class="first-step-box nextBtn" onclick="gettype1(this.id)" id='1'>
                                                                <div class="stp-hed">
                                                                    <h4 id="subcat1">DISTANCE / CLEAR</h4>
                                                                </div>
                                                                <div class="option-content">
                                                                    <div class="img-box">
                                                                        <figure>
                                                                            <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                        </figure>
                                                                    </div>
                                                                    <div class="text-box">
                                                                        <p class="option-intro">
                                                                            Traditional, transparent lenses perfect</p>                                                                                                  
                                                                    </div>
                                                                    <p class="option-help"></p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="first-step-box nextBtn" onclick="gettype1(this.id)" id='2'>
                                                                <div class="stp-hed">
                                                                    <h4 id="subcat2">DISTANCE / DIGITAL</h4>
                                                                </div>
                                                                <div class="option-content">
                                                                    <div class="img-box">
                                                                        <figure>
                                                                            <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                        </figure>
                                                                    </div>
                                                                    <div class="text-box">
                                                                        <p class="option-intro">Help protect your eyes while using devices</p>                                                                                                      </p>
                                                                    </div>
                                                                    <p class="option-help"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 ">
                                                            <div class="first-step-box nextBtn" onclick="gettype1(this.id)" id='3'>
                                                                <div class="stp-hed">
                                                                    <h4 id="subcat3">DISTANCE / SUN</h4>
                                                                </div>
                                                                <div class="option-content">
                                                                    <div class="img-box">
                                                                        <figure>
                                                                            <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                        </figure>
                                                                    </div>
                                                                    <div class="text-box">
                                                                        <p class="option-intro">
                                                                            Tint or coat your lenses and turn regular frames into sunglasses</p>                                                                                                    </p>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="first-step-box nextBtn" onclick="gettype1(this.id)" id='4'>
                                                                <div class="stp-hed">
                                                                    <h4 id="subcat4">DISTANCE / TRANSITIONS</h4>
                                                                </div>
                                                                <div class="option-content">
                                                                    <div class="img-box">
                                                                        <figure>
                                                                            <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                        </figure>
                                                                    </div>
                                                                    <div class="text-box">
                                                                        <p class="option-intro">
                                                                            Lenses that darken when exposed to outside light and remain clear inside                                                     </p>
                                                                    </div>
                                                                    <p class="option-help"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="step-btn btn-indigo btn-rounded prevBtn float-left" type="button">Previous</button>
                                                    <button class="btn-primary btn-indigo btn-rounded nextBtn float-right" type="button" onclick="gettype()">Next</button>
                                                </div>
                                            </div>
                                    
                                        </div>
                                        <!-- four Step -->
                                        <div class="row setup-content" id="step-12">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="first-step-box"  onclick="myfun1(this.id)" id='a1'>
                                                                    <div class="stp-hed">
                                                                        <h4 id='typea1'>BASIC LENSES - $6.95</h4>
                                                                    </div>
                                                                    <div class="option-content">
                                                                        <div class="img-box">
                                                                            <figure>
                                                                                <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                            </figure>
                                                                        </div>
                                                                        <div class="text-box">
                                                                            <p class="option-intro">
                                                                                Quality 1.5 index lenses with anti-scratch.</p>                                                                                                    </p>
                                                                        </div>
                                                                        <p class="option-help"></p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="first-step-box" onclick="myfun1(this.id)" id='a2'>
                                                                    <div class="stp-hed">
                                                                        <h4 id='typea2'>MOST POPULAR LENSES - $18.95</h4>
                                                                    </div>
                                                                    <div class="option-content">
                                                                        <div class="img-box">
                                                                            <figure>
                                                                                <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                            </figure>
                                                                        </div>
                                                                        <div class="text-box">
                                                                            <p class="option-intro">Quality 1.59 index lenses with UV-protective.</p>                                                                                                      </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="first-step-box" onclick="myfun1(this.id)" id='a3'>
                                                                    <div class="stp-hed">
                                                                        <h4 id='typea3'>ADVANCED LENSES - $30.90</h4>
                                                                    </div>
                                                                    <div class="option-content">
                                                                        <div class="img-box">
                                                                            <figure>
                                                                                <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                            </figure>
                                                                        </div>
                                                                        <div class="text-box">
                                                                            <p class="option-intro">
                                                                                Quality 1.6 index lenses with UV-protective.</p>                                                                                                    </p>
                                                                        </div>
                                                                        <p class="option-help"></p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="first-step-box" onclick="myfun1(this.id)" id='a4'>
                                                                    <div class="stp-hed">
                                                                        <h4 id='typea4'>CUSTOMIZE</h4>
                                                                    </div>
                                                                    <div class="option-content">
                                                                        <div class="img-box">
                                                                            <figure>
                                                                                <img src="../assets/themes/ecommerce-basic/img/icon-for-check-out-form.png">
                                                                            </figure>
                                                                        </div>
                                                                        <div class="text-box">
                                                                            <p class="option-intro">
                                                                                Select your preferred lens index and coatings.                                                    </p>
                                                                        </div>
                                                                        <p class="option-help"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <button class="step-btn btn-indigo btn-rounded prevBtn float-left" type="button">Previous</button>
                                                <button class="step-btn btn-default btn-rounded float-right customSubmitBtn" type="button" onclick="myfun()">Submit</button>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>


                    </div>
</div>

            <!-- Product Info-->
            <div class="col-md-4">
                <div class="step">
                    <div class="card">
                        <div class="card-result">
                            <h3 class="padding-top-1x text-normal">{{ $product->name }}</h3>
                            <h6>@if(\Settings::get('ecommerce_rating_enable',true) === 'true')
                                    @include('partials.components.rating',['rating'=> $product->averageRating(1)[0],'rating_count'=>$product->countRating()[0] ])
                                @endif</h6>
                            <div class="buying-glass">
                                <figure>
                                                   
                    @if(!($medias = $product->getMedia('ecommerce-product-gallery'))->isEmpty())
<!--                        <div class="product-carousel owl-carousel text-center">-->

                         
                                    <img src="{{ $medias[0]->getUrl() }}" class="img-responsive img-radio mx-auto" style="width:100%" alt="Product"/>
                              

<!--                        </div>-->
                    @endif
           
                                    
                                   
                                </figure>
                                <span><a href="{{ url('shop/'.$product->slug) }}">Back to Frame Description</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="product-shop-rate">

                      <span class="h4 d-block" >Frame:  <span id='frameprice'>{!! $product->price  !!}</span></span>
                        <span class="h4 d-block" id='lensprice'>Lens:0 </span>

                        <div class="glasses-rate">
                            <p><span class="h4 d-block" id="price1">Total:0</span></p>
                        </div>
                        
                      

{!! Form::open(['url'=>'cart/'.$product->hashed_id.'/add-to-cart','method'=>'POST','class'=> 'ajax-form','data-page_action'=>"updateCart"]) !!}
                @if(!$product->isSimple)
                    @foreach($product->activeSKU as $sku)
                        @if($loop->index%4 == 0)
                            <div class="d-flex flex-wrap">
                                @endif
                                <div class="text-center sku-item mr-2" style="width: 240px;">
                                    <img src="{{ asset($sku->image) }}" class="img-responsive img-radio mx-auto">
                                    <div class="">
                                        <div class="text text-success"><i class="fa fa-check fa-4x"></i></div>
                                    </div>
                                    <div>
                                        {!! !$sku->options->isEmpty() ? $sku->presenter()['options']:'' !!}
                                    </div>
                                    @if($sku->stock_status == "in_stock")
                                        <button type="button"
                                                class="btn btn-block btn-sm btn-default btn-secondary btn-radio m-t-5">
                                            <b>{!! $sku->discount?'<del class="text-muted">'.\Payments::currency($sku->regular_price).'</del>':''  !!} {!! currency()->format($sku->price, \Settings::get('admin_currency_code'), currency()->getUserCurrency()) !!}</b>
                                        </button>
                                    @else
                                        <button type="button"
                                                class="btn btn-block btn-sm m-t-5 btn-danger">
                                            <b> @lang('corals-ecommerce-basic::labels.partial.out_stock')</b>
                                        </button>
                                    @endif
                                    <input type="checkbox" id="left-item" name="sku_hash" value="{{ $sku->hashed_id }}"
                                           class="hidden d-none disable-icheck"/>
                                </div>
                                @if($lastLoop = $loop->index%4 == 3)
                            </div>
                        @endif
                    @endforeach
                    @if(!$lastLoop)</div>@endif
            <div class="form-group">
                <span data-name="sku_hash"></span>
            </div>
            @else
                <input type="hidden" name="sku_hash" value="{{ $product->activeSKU(true)->hashed_id }}"/>
            @endif

            <div class="row m-t-20" style="display:none">
                <div class="col-md-4">
                    {!! CoralsForm::number('quantity','corals-ecommerce-basic::attributes.template.quantity', false, 1, ['min' => 1,'class'=>'form-control form-control-sm']) !!}
                </div>
            </div>

  <div class="row m-t-20">
                <div class="col-md-4">
                
                      <input type="hidden" id='lensprice1' value='test' name='price1'>
                    <input type="hidden" id='lensname' value='test' name='lens'>
                    
                    <input type="hidden" id='leyesphere1' value='test' name='leyesphere1'>
                     <input type="hidden" id='leyecylinder1' value='test' name='leyecylinder1'>
                     <input type="hidden" id='reyesphere1' value='test' name='reyesphere1'>
                     <input type="hidden" id='reyecylinder1' value='test' name='reyecylinder1'>
                     <input type="hidden" id='leyeaxis1' value='test' name='leyeaxis1'>
                     <input type="hidden" id='reyeaxis1' value='test' name='reyeaxis1'>
                     <input type="hidden" id='leyeadd1' value='test' name='leyeadd1'>
                     <input type="hidden" id='reyeadd1' value='test' name='reyeadd1'>
                     <input type="hidden" id='eyepd1' value='test' name='eyepd1'>
                     <input type="hidden" id='leyepd1' value='test' name='leyepd1'>
                    <input type="hidden" id='reyepd1' value='test' name='reyepd1'>
                    <input type="hidden" id='type_name1' value='test' name='type_name1'>




                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    @if($product->globalOptions->count())
                        {!! $product->renderProductOptions('global_options',null, ['class'=>'form-control form-control-sm']) !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(\Settings::get('ecommerce_wishlist_enable', 'true') == 'true')
                        @include('partials.components.wishlist',['wishlist'=> $product->inWishList() ])
                    @endif

                    @if($product->external_url)
                        <a href="{{ $product->external_url }}" target="_blank" class="btn btn-success"
                           title="@lang('corals-ecommerce-basic::labels.template.product_single.buy_product')">
                            <i class="fa fa-fw fa-shopping-bag"
                               aria-hidden="true"></i> @lang('corals-ecommerce-basic::labels.template.product_single.buy_product')
                        </a>
                    @elseif($product->isSimple && $product->activeSKU(true)->stock_status != "in_stock")
                        <a href="#" class="btn btn-sm btn-outline-danger"
                           title="Out Of Stock">
                            @lang('corals-ecommerce-basic::labels.partial.out_stock')
                        </a>
                    @else
                        {!! CoralsForm::button('corals-ecommerce-basic::labels.partial.add_to_cart',
                        ['class'=>'btn add-to-cart btn-sm btn-primary','id'=>'addcart'], 'submit') !!}
                    @endif
                </div>
            </div>

            {{ Form::close() }}
                </div>
</div>
</div>




                   
                       
           
             
     
<!--
            <div class="mb-2">
                <span class="text-medium">@lang('corals-ecommerce-basic::labels.template.product_single.category')</span>
                @foreach($product->activeCategories as $category)
                    <a class="" href="{{ url('shop?category='.$category->slug) }}"><b>{{ $category->name }}</b></a>
                    &nbsp;&nbsp;
                @endforeach
            </div>
-->
            @if($product->activeTags->count())
                <div class="padding-bottom-1x mb-2">
                    <span class="text-medium">@lang('corals-ecommerce-basic::labels.template.product_single.tag')</span>
                    @foreach($product->activeTags as $tag)
                        <a class="tagss" href="{{ url('shop?tag='.$tag->slug) }}"><b>{{ $tag->name }}</b></a>&nbsp;&nbsp;
                    @endforeach
                </div>
            @endif
            <hr class="mb-3">
<!--
            <div class="d-flex flex-wrap justify-content-between">
                @include('partials.components.social_share',['url'=> URL::current() , 'title'=>$product->name ])

            </div>
-->
        </div>

    </div>



@stop

@section('js')
    @parent
    @include('Ecommerce::cart.cart_script')
    <script src="../assets/themes/ecommerce-basic/js/icheck.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.slitslider.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/form_steps_option.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/main1.js"></script>
    <script>
 $(document).ready(function() {
        $('input').on('ifChecked', function(event){
            $('#pdvalue').hide();
              $('#leftpdvalue').show();
             $('#rightpdvalue').show();
            });
        $('input').on('ifUnchecked', function(event){
              $('#pdvalue').show();
              $('#leftpdvalue').hide();
             $('#rightpdvalue').hide();
            });
 
 });
        //        var cat = document.getElementById("cat").value;
        //        var subcat = document.getElementById("subcat").value;
        //        var lenstype = document.getElementById("lenstype").value;
        var cat;
        var catname;
        var str;
        function getsubcat1(id)
        {
            catname  =$('#'+id+' #catname').text();
            // cat = 'Near';
            //subcat4
            $.get(window.base_url + "/getsubcat?value=" + catname,function(data, status){
                // alert(data.msg);
                var tmpString = "";
                var ac=0;
                for (var key in data) {
                    ac++;
                    document.getElementById("subcat"+ac).innerHTML = catname+'/'+data[key]['len_subcat_name'];
                     // alert(catname+'/'+data[key]['len_subcat_name']);

                    //tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                    // alert()
                    // alert("Data: " + data[key]['len_subcat_name'] + "\nStatus: " + status);
                }

            });
        }
        
                function getsubcat(){
            catname  =$('.active #catname').text();
           

            // cat = 'Near';
            //subcat4
            $.get(window.base_url + "/getsubcat?value=" + catname,function(data, status){
                // alert(data.msg);
                var tmpString = "";
                var ac=0;
                for (var key in data) {
                    ac++;
                    document.getElementById("subcat"+ac).innerHTML = catname+'/'+data[key]['len_subcat_name'];

                    //tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                    // alert()
                    // alert("Data: " + data[key]['len_subcat_name'] + "\nStatus: " + status);
                }

            });
        }
        var catname1;
        function gettype(){
            if($('.active #subcat1').text()){
                catname1  =$('.active #subcat1').text();

            }
            if($('.active #subcat2').text()){
                catname1  =$('.active #subcat2').text();

            }
            if($('.active #subcat3').text()){
                catname1  =$('.active #subcat3').text();

            }
            if($('.active #subcat4').text()){
                catname1  =$('.active #subcat4').text();

            }

            $.get(window.base_url + "/gettype?value=" + catname1,function(data, status){

                var ac=0;
                for (var key in data) {
                    ac++;
                    document.getElementById("type"+ac).innerHTML =data[key]['type_name'];


                    //alert("Data: " + data[key]['len_subcat_name'] + "\nStatus: " + status);
                }

            });
        }
                function gettype1(id){
          
                catname1  = $('#subcat'+id).text();
                   
            
           

            $.get(window.base_url + "/gettype?value=" + catname1,function(data, status){

                var ac=0;
                for (var key in data) {
                    ac++;
                    document.getElementById("typea"+ac).innerHTML =data[key]['type_name'];


                    //alert("Data: " + data[key]['len_subcat_name'] + "\nStatus: " + status);
                }

            });
        }


        var type;
        document.getElementById("addcart").disabled = true;

          function myfun(){
            




            var price=0;
              var x = document.getElementById('frameprice').innerHTML;
            myArray = x.split(/([0-9]+)/)
        var t = x.split(myArray[0]);

            
        
    // alert(t[1]);
            
            //alert(price);
          //  alert(price);
             if($('.active #type1').text()){
            type  =$('.active #type1').text();
            
        }
        if($('.active #type2').text()){
            type  =$('.active #type2').text();
           
        }
        if($('.active #type3').text()){
            type  =$('.active #type3').text();
            
        }
        if($('.active #type4').text()){
            type  =$('.active #type4').text();
            
        }
       
             
                    //var getValue = $('#getValue').text();
                    // cat = $('.active #cat').val();
                     //str = $('.active #subcat').text();
           // alert(str);
                     //var str = "DISTANCE / DIGITAL";
                    var res = catname1.split("/");
                    var subcat = res[1];
                   // var type =type; 
                    var right_sphere = document.getElementById("right_sphere").value;
                    var right_cylinder = document.getElementById("right_cylinder").value;
            
                   // alert(getValue);
                
            
            var left_sphere = document.getElementById("left_sphere").value;
            var left_cylinder = document.getElementById("left_cylinder").value;
           
            

            
        $.get(window.base_url + "/configurejson?value=" + right_sphere + "&value1=" + right_cylinder + "&typeid=" + type  + "&cat_id=" + res[0] + "&sub_id=" + subcat,function(data, status){
           // alert(data.msg);
            
            for (var key in data) {
                //tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                price = price+parseInt(data[key]['price'], 10) ;
                 //alert(price+'1');
               // alert("Data: " + data[key]['price'] + "\nStatus: " + status);
            }
           
        });
              $.get(window.base_url + "/configurejson?value=" + left_sphere + "&value1=" + left_cylinder + "&typeid=" + type  + "&cat_id=" + res[0] + "&sub_id=" + subcat,function(data, status){
           // alert(data.msg);
            
            for (var key in data) {
                //tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                price = price+parseInt(data[key]['price'], 10) ;
            
            document.getElementById('lensprice').innerHTML = "Lens:"+myArray[0]+price;
            document.getElementById('lensname').value = catname1;
                 document.getElementById('lensprice1').value = price;
            price = price+parseInt(t[1], 10);
            document.getElementById('price1').innerHTML = 'Total:'+myArray[0]+price;
                  document.getElementById("addcart").disabled = false;
                // alert(price+"2");
               // alert("Data: " + data[key]['price'] + "\nStatus: " + status);
            }
           
        });
           
    }
           function myfun1(id){
             document.getElementById('leyesphere1').value = document.getElementById('left_sphere').value;
             document.getElementById('leyecylinder1').value = document.getElementById("left_cylinder").value;
             document.getElementById('leyeaxis1').value = document.getElementById("leyeaxis").value;
             document.getElementById('leyeadd1').value = document.getElementById('leyeadd').value;
             document.getElementById('leyepd1').value = document.getElementById('leyepd').value;

             document.getElementById('reyesphere1').value = document.getElementById('right_sphere').value;
             document.getElementById('reyecylinder1').value = document.getElementById('right_cylinder').value;
             document.getElementById('reyeaxis1').value = document.getElementById('reyeaxis').value;
             document.getElementById('reyeadd1').value = document.getElementById('reyeadd').value;
             document.getElementById('eyepd1').value = document.getElementById('eyepd').value;
             document.getElementById('reyepd1').value = document.getElementById('reyepd').value;
             document.getElementById('type_name1').value = $('#type'+id).text();
            // alert($('#type'+id).text());
             



 
                    
                
           // alert(document.getElementById("leyeaxis").value);


            var price=0;
              var x = document.getElementById('frameprice').innerHTML;
            myArray = x.split(/([0-9]+)/)
        var t = x.split(myArray[0]);

            
        
    // alert(t[1]);
           
            
            //alert(price);
          //  alert(price);
            
            type  =$('#type'+id).text();
            
       
       
             
                    //var getValue = $('#getValue').text();
                    // cat = $('.active #cat').val();
                     //str = $('.active #subcat').text();
           // alert(str);
                     //var str = "DISTANCE / DIGITAL";
                    var res = catname1.split("/");
                    var subcat = res[1];
                   // var type =type; 
                    var right_sphere = document.getElementById("right_sphere").value;
                    var right_cylinder = document.getElementById("right_cylinder").value;
            
                   // alert(getValue);
                
            
            var left_sphere = document.getElementById("left_sphere").value;
            var left_cylinder = document.getElementById("left_cylinder").value;
           
            

            
        $.get(window.base_url + "/configurejson?value=" + right_sphere + "&value1=" + right_cylinder + "&typeid=" + type  + "&cat_id=" + res[0] + "&sub_id=" + subcat,function(data, status){
           // alert(data.msg);
            
            for (var key in data) {
                //tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                price = price+parseInt(data[key]['price'], 10) ;
                 //alert(price+'1');
               // alert("Data: " + data[key]['price'] + "\nStatus: " + status);
            }
           
        });
              $.get(window.base_url + "/configurejson?value=" + left_sphere + "&value1=" + left_cylinder + "&typeid=" + type  + "&cat_id=" + res[0] + "&sub_id=" + subcat,function(data, status){
           // alert(data.msg);
            
            for (var key in data) {
                //tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                price = price+parseInt(data[key]['price'], 10) ;
            
            document.getElementById('lensprice').innerHTML = "Lens:"+myArray[0]+price;
            document.getElementById('lensname').value = catname1;
                 document.getElementById('lensprice1').value = price;
            price = price+parseInt(t[1], 10);
            document.getElementById('price1').innerHTML = 'Total:'+myArray[0]+price;
                  document.getElementById("addcart").disabled = false;
                  
                // alert(price+"2");
               // alert("Data: " + data[key]['price'] + "\nStatus: " + status);
            }
           
        });
           
    }

    



    </script>
@endsection