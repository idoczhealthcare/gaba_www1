<style>
        .rect {
            display: none;
            left: 0; ;
            position: absolute ;
            top: 2% ;
        }
.demo-container {
  width: 100%;
  height: 530px;
  position: relative;
  background: #eee;
  overflow: hidden;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px;
}

        #face {
            position: absolute !important;
            top: 50% !important;
            left: 50% !important;
            margin: -173px 0 0 -300px !important; 
        }
    
    #glasses123 {
    cursor: move !important;
    width:70px;
    height:70px;
    
    
    margin:2px;
    float:left;
}
.active {
    background: transparent !important;
   
}

    </style>



<div class="grid-item">
    <div class="product-card">
        @if($product->discount)
            <div class="product-badge text-danger" style="display:block;" id='discount{{$product->id}}'>{{ $product->discount }}% Off</div>
        @endif
        @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
            @include('partials.components.rating',['rating'=> $product->averageRating(1)[0],'rating_count'=>null])
        @endif
        <div style="min-height: 170px;" class="mt-4">
            <a  class="product-thumb link" href="{{ url('shop/'.$product->slug) }}">
                <img id="feturd_{{$product->id}}" src="{{ $product->image }}" alt="{{ $product->name }}" class="mx-auto"
                     style="max-height: 150px;width: auto;">
                @if($product->discount)
                    <div class="product-badge text-danger" style="display:block;" id='discount{{$product->id}}'>{{ $product->discount }}% Off</div>
                
                <div class="sale_div_container">
                    <div class="sale_div_wrapper">
                        <h1>sale</h1>
                    </div>
                </div>
                @endif     
            </a>

        </div>
            <div class="col-md-6 col-xs-6">
                <h3 class="product-title">
                    <span style="margin-right: 20px; color: #9da9b9;font-size: 14px;font-weight: 500;">{{ $product->brand['name'] }}</span><br>
                    <a href="{{ url('shop/'.$product->slug) }}">{{ $product->name }}</a>
                </h3>
            </div>
            <div class="col-md-6 col-xs-6">
                <h4 class="product-price">
                    @if($product->discount)
                        <del id="del{{ $product->id}}">{{ \Payments::currency($product->regular_price) }}</del>
                    @else
                        <p></p>
                    @endif
                    <div class="" id='price{{ $product->id}}'>
                    {!! $product->price !!}
                    </div>
                </h4>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="color-available">
                    <h6 class="">COLOR AVAILABLE</h6>
                    <div class="color-available">
                        @if($colors = \Shop::getSubProductColor($product->id))
                            @foreach($colors as $color)
                                   <img title="{{ $color[0]['title'] }}" src = "{{ $color[0]['thumbnail'] }}"  class="img-responsive img-rounded color_select_btn load_sub_img" data-pid='{{$product->id}}' data-cid="{{$color[0]['id']}}" style ="max-height: 20px;width:auto" alt = "Thumbnail" />
                            @endforeach
                        @endif
                    
                    </div>
                </div>
        </div>

        <div class="shop-bth">
            <a id="myBtn" data-toggle="modal" prod="{{$product->id}}" data-target="#eyetry-box-{{$product->id}}" class="btn add-to-cart btn-sm btn-primary ladda-button try_on_bttn hide1" data-style="expand-right">
                <span class="ladda-label">Try On</span><span class=""></span>
            </a>
        </div>
       
        
    </div><div class="eyetry-box" id="eyetry-box-{{$product->id}}" style="display: none;">
            <div class="try_on" id="eyetry-wrapper">
                <div onclick="close_try('{{$product->id}}')" class="tryon_cross">
                    <i class="icon-cross close_tryon"></i>
                </div>
                <div class="eyetry-sidebar">
                    <ul class="try_on_options">
                        <li><p>{{$product->id}}  This is our virtual mirror. Please select a photo to upload.</p></li>
                        <li>
                            

                        <input type="hidden" name="prod_ID" value="" id="prod_ID"  />
                        </li>
                        <li style="display: none;">
                            <p>Browse Image of Glasses</p>
                            <input id="image2" class='image2' data='{{ $product->id }}' type="file" />
                        </li>   
                        <li>
                            <button class="btn btn-primary" type="button" onclick="showGlass({{ $product->id }})">Try Glasses !</button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="increaseGlassHeight({{ $product->id }})"> increase height </button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="decreaseGlassHeight({{ $product->id }})"> decrease height </button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="increaseGlassWidth({{ $product->id }})"> increase width </button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="decreaseGlassWidth({{ $product->id }})"> decrease width </button>
                        </li>
                        
                    </ul>
                </div>
                <div class="upload_display_left">
                <div class="upload_img_container">
                   
                    <div class="upload_img_wrapper" style="width: 440px; position: relative; height: 440px; border: 1px solid #000;">
                        <div id="plus1-{{$product->id}}" class="demoDrag mydiv">
                            <div id="plus1-{{$product->id}}header" class="mydivheader"><img src="../assets/themes/ecommerce-basic/img/plus.png"></div>
                        </div>

                        <div id="plus2-{{$product->id}}"  class="demoDrag mydiv">
                            <div id="plus2-{{$product->id}}header" class="mydivheader"><img src="../assets/themes/ecommerce-basic/img/plus.png"></div>
                        </div>
                        <img id="face{{ $product->id }}" src="" style="display:none;"/>
                        <img id="crop_result{{ $product->id }}" class="crop_result">
                         <!-- <div id="glasses-{{$product->id}}" >
                         <div id="glasses-{{$product->id}}header">
                              <img id="glasses{{ $product->id }}" src="{{ $product->image }}" class="rect1" />
                         </div>
                        </div> -->
                        <div id="plus2-{{$product->id}}" >
                            <div id="plus2-{{$product->id}}header"><img id="glasses{{ $product->id }}" src="{{ $product->image }}" class="rect1" /></div>
                        </div>
                       
                    </div>
                    <!--  <button id="crop_btn{{ $product->id }}" class="btn btn-sm btn-primary ladda-button btn_crop">Cropped</button> -->
                </div>

                <div class="try-upload-tips">
                    <ul class="try-tips-list">
                        <li>
                            <i class="icon try-tips-one"></i>
                            <span class="try-tips-text">Keep your face forward and level.</span>
                        </li>
                        <li>
                            <i class="icon try-tips-two"></i>
                            <span class="try-tips-text">Use a photo without glasses.</span>
                        </li>
                        <li>
                            <i class="icon try-tips-three"></i>
                            <span class="try-tips-text">Refrain from tilting your head or using a profile picture.</span>
                        </li>
                    </ul>
                </div>

                </div>

                <div class="upload_btn_wrapper">
                    <p>Browse Your Photo</p>
                    <input id="image1" class='image1' data='{{ $product->id }}' type="file"  />
                </div>
            </div>
        </div>
    <div class="eyetry-box" id="eyetry-box-{{$product->id}}" style="display: none;">
            <div class="try_on" id="eyetry-wrapper">
                <div class="tryon_cross">
                    <i class="icon-cross close_tryon"></i>
                </div>
                <div class="col-md-3 eyetry-sidebar">
                    <ul class="try_on_options">
                        <li><p>{{$product->id}}  This is our virtual mirror. Please select a photo to upload.</p></li>
                        <li>
                            <p>Browse Your Photo</p>
                        <input id="image1" class='image1' data='{{ $product->id }}' type="file"  />

                        <input type="hidden" name="prod_ID" value="" id="prod_ID"  />
                        </li>
                        <li style="display: none;">
                            <p>Browse Image of Glasses</p>
                            <input id="image2" class='image2' data='{{ $product->id }}' type="file" />
                        </li>   
                        <li>
                            <button class="btn btn-primary" type="button" onclick="showGlass({{ $product->id }})">Try Glasses !</button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="increaseGlassHeight({{ $product->id }})"> increase height </button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="decreaseGlassHeight({{ $product->id }})"> decrease height </button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="increaseGlassWidth({{ $product->id }})"> increase width </button>
                        </li>
                        <li style="display: none;">
                            <button class="btn btn-primary" type="button" onclick="decreaseGlassWidth({{ $product->id }})"> decrease width </button>
                        </li>
                        
                    </ul>
                </div>
                <div class="col-md-5">
                   
                    <div class="upload_img_wrapper" style="width: 440px; position: relative; height: 440px; border: 1px solid #000;">
                        <div id="plus1-{{$product->id}}" class="demoDrag mydiv">
                            <div id="plus1-{{$product->id}}header" class="mydivheader"><img src="../assets/themes/ecommerce-basic/img/plus.png"></div>
                        </div>

                        <div id="plus2-{{$product->id}}"  class="demoDrag mydiv">
                            <div id="plus2-{{$product->id}}header" class="mydivheader"><img src="../assets/themes/ecommerce-basic/img/plus.png"></div>
                        </div>
                        <img id="face{{ $product->id }}" src="" style="display:none;"/>
                        <img id="crop_result{{ $product->id }}" class="crop_result">
                         <!-- <div id="glasses-{{$product->id}}" >
                         <div id="glasses-{{$product->id}}header">
                              <img id="glasses{{ $product->id }}" src="{{ $product->image }}" class="rect1" />
                         </div>
                        </div> -->
                        <div id="plus2-{{$product->id}}">
                            <div id="plus2-{{$product->id}}header"><img id="glasses{{ $product->id }}" src="{{ $product->image }}" class="rect1" /></div>
                        </div>
                       
                    </div>
                    <!--  <button id="crop_btn{{ $product->id }}" class="btn btn-sm btn-primary ladda-button btn_crop">Cropped</button> -->
                </div>
                <div class="col-md-4 try-upload-tips">
                    <ul class="try-tips-list">
                        <li>
                            <i class="icon try-tips-one"></i>
                            <span class="try-tips-text">Keep your face forward and level.</span>
                        </li>
                        <li>
                            <i class="icon try-tips-two"></i>
                            <span class="try-tips-text">Use a photo without glasses.</span>
                        </li>
                        <li>
                            <i class="icon try-tips-three"></i>
                            <span class="try-tips-text">Refrain from tilting your head or using a profile picture.</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    
</div>


