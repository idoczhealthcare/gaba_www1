<!--

<style>
        .rect {
            display: none;
            left: 0; ;
            position: absolute ;
            top: 2% ;
        }
.demo-container {
  width: 100%;
  height: 530px;
  position: relative;
  background: #eee;
  overflow: hidden;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px;
}

        #face {
            position: absolute !important;
            top: 50% !important;
            left: 50% !important;
            margin: -173px 0 0 -300px !important; 
        }
    
    #glasses123 {
    cursor: move !important;
    width:70px;
    height:70px;
    
    
    margin:2px;
    float:left;
}
.active {
    background: transparent !important;
   
}
    </style>
-->


<div class="grid-item">
    <div class="product-card">
        @if($product->discount)
            <div class="product-badge text-danger" style="display:block;" id='discount{{$product->id}}'>{{ $product->discount }}% Off</div>
        @endif
        @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
            @include('partials.components.rating',['rating'=> $product->averageRating(1)[0],'rating_count'=>null])
        @endif
        <div style="min-height: 170px;" class="mt-4">
            <a  class="product-thumb link" href="{{ url('shop/'.$product->slug) }}">
                <img id="feturd_{{$product->id}}" src="{{ $product->image }}" alt="{{ $product->name }}" class="mx-auto"
                     style="max-height: 150px;width: auto;">
            </a>
        </div>
            <div class="col-md-6">
        <h3 class="product-title">
            <span style="margin-right: 20px; color: #9da9b9;font-size: 14px;font-weight: 500;">{{ $product->brand['name'] }}</span><br>
            <a href="{{ url('shop/'.$product->slug) }}">{{ $product->name }}</a>
        </h3>
            </div>
            <div class="col-md-6">
        <h4 class="product-price">
            @if($product->discount)
                <del id="del{{ $product->id}}">{{ \Payments::currency($product->regular_price) }}</del>
            @endif
            <div class="" id='price{{ $product->id}}'>
            {!! $product->price !!}
            </div>
        </h4>
            </div>
            <div class="col-md-12">
                <div class="color-available">
                    <h6 class="">COLOR AVAILABLE</h6>
                    <div class="color-available">
                        @if($colors = \Shop::getSubProductColor($product->id))
                            @foreach($colors as $color)
                                   <img title="{{ $color[0]['title'] }}" src = "{{ $color[0]['thumbnail'] }}"  class="img-responsive img-rounded color_select_btn load_sub_img" data-pid='{{$product->id}}' data-cid="{{$color[0]['id']}}" style ="max-height: 20px;width:auto" alt = "Thumbnail" />
                            @endforeach
                        @endif
                    
                    </div>
                </div>
        </div>

        <div class="shop-bth">
        <a id="myBtn" data-toggle="modal" data-target="#{{$product->sku}}" class="btn add-to-cart btn-sm btn-primary ladda-button hide1" data-style="expand-right"><span class="ladda-label">Try On</span><span class=""></span></a>

        </div>
           <div id="{{$product->sku}}" class="modal" role="dialog">

        <!-- Modal content -->
        <div class="">
                <div class="modal-content">
           <button class="close" type="button" data-dismiss="modal" aria-label="close"><span
                                aria-hidden="true">&times;</span></button>
    
    <div class="col-md-7">
        <div class="row">
        <div class="tryon_wrap">
            	<ul>
                	<li><p class="tryon_heading"> Select Photo and Then Click Try Glasses.</p></li>
                	
                    <div class="browse_btns">    
                    	<li>
                        	<label>
                    			Browse Your Photo
                    		
                            <input id="image1" class='image1' data='{{ $product->id }}' type="file"  />

                            <input type="hidden" name="prod_ID" value="" id="prod_ID"  />
                    		</label>
                    	</li>
                    	<li>
                    		<label  style="display:none">
                    			Browse Image of Glasses
                    		<input id="image2" class='image2' data='{{ $product->id }}' type="file" />
                    		</label>
                    	</li>
                    </div>	
                    
                    <div class="direction_btns">
                        <li><button type="button" onclick="showGlass({{ $product->id }})">Try Glasses !</button></li>
                    
                    	<li style="margin-left: 15px;"><button type="button" onclick="decreaseGlassHeight({{ $product->id }})"> <img src="../assets/themes/ecommerce-basic/img/top.png"> </button></li>
                    	<li><button type="button" onclick="increaseGlassHeight({{ $product->id }})"> <img src="../assets/themes/ecommerce-basic/img/bottom.png"> </button></li>
                    	<li><button type="button" onclick="decreaseGlassWidth({{ $product->id }})"> <img src="../assets/themes/ecommerce-basic/img/left.png"> </button></li>
                    	<li><button type="button" onclick="increaseGlassWidth({{ $product->id }})"> <img src="../assets/themes/ecommerce-basic/img/right.png"> </button></li>
                    </div>    
                    	<li><p class="tryon_p1"> You can also click on the glasses and move it around to adjust</p></li>

                       
                       

                       
                </ul>
    	</div>
    </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="demo-container" id="crop-container">
                
        		<img id="face{{ $product->id }}" class="img_cropper"  src="" style="max-width: 100%;height: 100%; display:none"/>
            
                <img id="crop_result{{ $product->id }}" class="crop_result">
                <img id="glasses{{ $product->id }}" src="{{ $product->image }}" class="rect1" />
            </div>

            <!-- <div class="demo-container" id="crop-result">
                
            </div> -->

            <button id="crop_btn{{ $product->id }}" class="btn btn-sm btn-primary ladda-button btn_crop">Cropped</button>
        </div>
    </div>    
                
  
        </div>

</div>
    </div>
    </div>
    
</div>
