<a class="add-to-cart wish_btn btn btn-outline-secondary btn-sm  {{ !is_null($wishlist) ? 'active' : '' }}" 
    data-style="zoom-in" data-color="red"
   href="{{ url('e-commerce/wishlist/'.$product->hashed_id) }}"
   data-action="post" data-page_action="toggleWishListProduct"
   data-wishlist_product_hashed_id="{{$product->hashed_id}}"><i
            class="icon-heart mar_t_0 "></i><span class="wish_label">Wishlist</span></a>