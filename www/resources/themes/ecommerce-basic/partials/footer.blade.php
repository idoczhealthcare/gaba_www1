<!--Newletter area-->
<section class="newletter">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 signup-text align_center">
                <h3>SIGN UP FOR EXCLUSIVE OFFERS</h3>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 signup-form">
                <div class="input-group news-form align_center">
                    <form class="form-inline">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="inputPassword2" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="" placeholder="Email">
                            <button type="button" class="btn btn-primary mb-2 custom_submit_btn">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 social2 align_center">
                <ul>
                    <li><a class="wow fadeInUp animated animated" href="https://twitter.com/kimarotec" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://www.facebook.com/kimarotec" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://plus.google.com/kimarotec" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;"><i class="fa fa-dribbble"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- Footer area-->
<div class="footer-area">


    <div class="footer">
        <div class="container">
            <div class="row">

                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>GABA Opticals </h4>
                        <ul class="footer-menu">
                           <li><a href="{{ url('/About-us') }}">About us</a></li>
                            <li><a href="javascript:;">About Gaba Optical</a></li>
                            <li><a href="javascript:;">Our Vision</a></li>
                            <li><a href="javascript:;">Our Mission</a></li>
                            <li><a href="javascript:;">Chairman Message</a></li>
                            <li><a href="javascript:;">Our Core Team</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Quick links </h4>
                        <ul class="footer-menu">
                            <li><a href="javascript:;">20% off on Lenses</a></li>
                            <li><a href="javascript:;">20% off on Frames</a></li>
                            <li><a href="javascript:;">Buy one get one Free</a></li>
                            <li><a href="javascript:;">Starting from 2000</a></li>
                            <li><a href="javascript:;">New Arrivals</a></li>
                            <li><a href="javascript:;">Clearnace Sale</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Help Center</h4>
                        <ul class="footer-menu">
                            <li><a href="{{ url('/faq') }}">FAQs</a></li>
                            <li><a href="order-tracking.php">Order Tracking</a></li>
                            <li><a href="javascript:;">Shipping & Return Policy</a></li>
                            <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                            <li><a href="{{ url('/terms-of-use') }}">Terms of Use</a></li>
                            <li><a href="{{ url('/contact-us') }}">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Blogs</h4>
                        <ul class="footer-menu">
                            <li><a href="javascript:;">Blogs one</a></li>
                            <li><a href="javascript:;">Blogs tow</a></li>
                            <li><a href="javascript:;">Blogs three</a></li>
                            <li><a href="javascript:;">Blogs four</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInRight animated">
                   <div class="single-footer news-letter">
                        <h4></h4>
                       <p></p>

                    <!--<form>
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="E-mail ... ">
                            <span class="input-group">
                                <button class="btn btn-primary subscribe" type="button">Submit</button>
                            </span>
                        </div></form>-->
                    </div>
                    <div class="footer-payment-link">
<!--                        <img src="../assets/themes/ecommerce-basic/img/payment-method-image.png">-->
                        <ul class="footer-menu">
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-1.jpg"></a></li>
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-2.png"></a></li>
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-3.jpg"></li>
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-4.jpg"></a></li>
                            
                        </ul>
                    </div>
                </div>

            </div>
        </div>


</div>
    <div class="footer-copy">
        <div class="container">
            <div class="row">
                    <hr>
                <div class="col-md-5 col-xs-12">
                    <span class="wow fadeInUp animated animated footer_bottom_center"> © Copyrights GABA Opticals 2018. All rights reserved </span>
                </div>
                <div class="col-md-4 col-xs-12">
                    <ul class="policy_ex footer_bottom_center">
                        <li><a class="wow fadeInUp animated animated" href="javascript:;">Terms & Conditions</a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;">Privacy Policy</a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;">Sitemap</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12">
                    <ul class="social_ex">
                        <li><a class="wow fadeInUp animated animated" href="javascript:;" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-cc-paypal" aria-hidden="true"></i></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a></li>
                </ul>

                </div>
            </div>
        </div>
    </div>
</div>



<script src="../assets/themes/ecommerce-basic/js/modernizr-2.6.2.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/modernizr.custom.79639.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap-select.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap-hover-dropdown.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.easypiechart.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/owl.carousel.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/wow.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/slick.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/scripts.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/icheck.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.slitslider.js"></script>

    <script type="text/javascript" src="../assets/themes/ecommerce-basic/js/lightslider.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/main2.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/main.js"></script>
   <script src="../tracking.js-master/build/tracking-min.js"></script>
    <script src="../tracking.js-master/build/data/eye-min.js"></script>
    <script src="../tracking.js-master/build/data/face-min.js"></script>
	
   
<script>
    $('.price-slider').slick( {
            dots:true, infinite:false, speed:300, slidesToShow:3, slidesToScroll:1, autoplay:true, autoplaySpeed:2000, responsive:[ {
                breakpoint:1024, settings: {
                    slidesToShow: 2, slidesToScroll: 1, infinite: true, dots: true
                }
            }
                , {
                    breakpoint:600, settings: {
                        slidesToShow: 2, slidesToScroll: 2
                    }
                }
                , {
                    breakpoint:480, settings: {
                        slidesToShow: 1, slidesToScroll: 1
                    }
                }
            ]
        }
    );
    $(".sliderxs").slick( {
            arrows:false, dots:true, autoplay:true, adaptiveHeight:true, responsive:[ {
                breakpoint: 10000, settings: "unslick"
            }
                , {
                    breakpoint:767, settings: {
                        unslick: true
                    }
                }
            ]
        }
    );
</script>
<script type="text/javascript">
    $(function () {

        var Page = (function () {

            var $nav = $('#nav-dots > span'),
                slitslider = $('#slider').slitslider({
                    onBeforeChange: function (slide, pos) {

                        $nav.removeClass('nav-dot-current');
                        $nav.eq(pos).addClass('nav-dot-current');

                    }
                }),
                init = function () {

                    initEvents();

                },
                initEvents = function () {

                    $nav.each(function (i) {

                        $(this).on('click', function (event) {

                            var $dot = $(this);

                            if (!slitslider.isActive()) {

                                $nav.removeClass('nav-dot-current');
                                $dot.addClass('nav-dot-current');

                            }

                            slitslider.jump(i + 1);
                            return false;

                        });

                    });

                };

            return {init: init};

        })();

        Page.init();

        /**
         * Notes:
         *
         * example how to add items:
         */

        /*

         var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');

         // call the plugin's add method
         ss.add($items);

         */

    });
</script>
<script>
    
    /****** UPDATE ON 07-02-2019 FOR TRY ON********/
    var prod = '';
    $(document).on('click','.try_on_bttn',function(){
        $('.eyetry-box').css('display','none');
        var target = $(this).attr('data-target');
        $(target).css('display','block');
        var prod = $(this).attr('prod');
        dragElement(document.getElementById('plus2-'+prod));
        dragElement(document.getElementById('plus1-'+prod));

    });  
    function dragElement(elmnt) 
    { 
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if(document.getElementById(elmnt.id + "header")) {
            /* if present, the header is where you move the DIV from:*/
            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        } else {
            /* otherwise, move the DIV from anywhere inside the DIV:*/
            elmnt.onmousedown = dragMouseDown;
        }
        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }
        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        }
        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
    /****** UPDATE ON 07-02-2019 FOR TRY ON END HERE********/






    $(document).ready(function() {
        /*for mobile menu start
        */
        $('.search_btn').click(function () {
                $('.search_btn i').toggleClass('fa-close');
                $('.mobi_search').stop().slideToggle();
        });
        $(".slide_menu_dropdown_click").click(function ($this) {
                $(this).find('.slide_menu_dropdown_show').slideToggle();  

                // if($(this).find('.slide_menu_dropdown_icon').hasClass('rotate_icon'))    
                //  {
                //     $(this).find('.slide_menu_dropdown_icon').removeClass("rotate_icon");
                //  }
                //  else
                //  {
                //     $(this).find('.slide_menu_dropdown_icon').addClass("rotate_icon");  
                //  }
            });
            $(".slide_menu_footer_dropdown_click").click(function ($this) {
                $(this).find('.slide_menu_footer_dropdown_show').slideToggle();  

                // if($(this).find('.slide_menu_dropdown_icon').hasClass('rotate_icon'))    
                //  {
                //     $(this).find('.slide_menu_dropdown_icon').removeClass("rotate_icon");
                //  }
                //  else
                //  {
                //     $(this).find('.slide_menu_dropdown_icon').addClass("rotate_icon");  
                //  }
            });
            $(".custom_alert_overlay").click(function () {
                 if($('.mobile_slide_menu').hasClass('hide1')){
                                          
                    $( ".mobile_slide_menu" ).stop().animate({
                      left: "-=300"
                      }, 700);
                    $(".custom_alert_overlay").css("display","none");
                    $(".mobile_slide_menu").removeClass('hide1');
                    $(".mobile_slide_menu").addClass('show3'); 
                    // $(".mobile_slide_menu").css("display","none");               
                }
            });

            $(".hamburg_menu_toggle").click(function () {


                if($('.mobile_slide_menu').hasClass('show3')){

                        console.log("Show");
                    $(".mobile_slide_menu").css("display","block");
                                          
                    $( ".mobile_slide_menu" ).stop().animate({
                      left: "+=300"
                      }, 700);
                    $(".custom_alert_overlay").css("display","block");
                    $(".mobile_slide_menu").removeClass('show3');
                    $(".mobile_slide_menu").addClass('hide1');
                
                }
                else {      
                    
                $( ".mobile_slide_menu" ).animate({
                    left: "-=300"
                  }, 700);
                $(".mobile_slide_menu").removeClass('hide1');
                $(".mobile_slide_menu").addClass('show3');
                $(".mobile_slide_menu").css("display","none");
                }
            });

            $(".mobi_filter_btn").click(function (e) {
                e.preventDefault();
               $(".mobi_filter_dropdown").toggleClass("mobi_filter_dropdown_active"); 
            });
            $(".side_filter_slide_dropdown a").click(function ($this) {
                 $(this).next('.dropdown-menu').slideToggle(); 
            });
            $(".filter_close").click(function () {
                $(".side_filter_panel").removeClass('hide1');
                $(".side_filter_panel").addClass('show3');
                $(".side_filter_panel").css("display","none");
                $("html").css("cssText","overflow-y:auto !important");
                $(".mobi_filter").css("display","block");
            });

            

             $('.mobi_filter_side_btn').click(function(e){
                e.preventDefault();
                
                if($('.side_filter_panel').hasClass('show3')){

                        console.log("Show");
                    $(".side_filter_panel").css("display","block");
                    $(".side_filter_panel").removeClass('show3');
                    $(".side_filter_panel").addClass('hide1');
                    $("html").css("cssText","overflow-y:hidden !important");
                    $(".mobi_filter").css("display","none");
                
                }
                else {      
                    
                // $( ".side_filter_panel" ).animate({
                //   right: "-=300"
                //   }, 700);
                $(".side_filter_panel").removeClass('hide1');
                $(".side_filter_panel").addClass('show3');
                $(".side_filter_panel").css("display","none");
                $("html").css("cssText","overflow-y:auto !important");
                $(".mobi_filter").css("display","block");
                }
              });

             // $(document.body).click( function() {

             //    if ($('.side_filter_panel').hasClass('hide1')) 
             //    {
             //        $(".side_filter_panel").addClass('show3');
                    
             //        $( ".side_filter_panel" ).stop().animate({
             //          right: "-=300"
             //          }, 700);
             //        // $(".side_filter_panel").css("display","none");
             //    }
             //    });
        /*for mobile menu end
        */
        
        $(".hamburg_menu_toggle").click(function () {
                $(".mobi_navbar").toggleClass("display_mobi_navbar"); 
    });
        
        // $(".search_btn").click(function () {
        //         $(".mobi_search").css("display","block");
        //         $(".mobi_search_close").css("display","inline-block");
        //         $(".close_mobi_menu").css("display","none");
        //     });


        //     $(".mobi_search_close").click(function () {
        //         $(".mobi_search").css("display","none");
        //         $(".mobi_search_close").css("display","none");
        //         $(".close_mobi_menu").css("display","block");
        //     });
        $(".mobi_account").click(function () {
            $(".toolbar-dropdown").toggleClass("mobi_account_dropdown"); 
        });
        ///requiredfields
        $(document).on('click','.load_sub_img',function() 
        { 
           var pid = $(this).attr('data-pid');
           var cid = $(this).attr('data-cid');  
           $.get(window.base_url + "/loadfeatureimag?color=" + cid +'&pid='+pid,function(data, status){
                if(data[1]!=''){
                    $('#price'+pid).html(data[1]);
                    $('#del'+pid).html(data[2]);
                    $('#dicount'+pid).html(data[3]);

                }else{
                    $('#price'+pid).html(data[2]);
                }

                if(data[0]!=''){
                    $('#feturd_'+pid).attr('src',data[0]);
                    $('#glasses'+pid).attr('src',data[0]);
                    $('#price'+pid).attr('src',data[1]);
                    $('#del'+pid).attr('src',data[2]);
                }
                else{
                    $('#feturd_'+pid).attr('src','assets/corals/images/default_product_image.png');
                }
            });
         });
        
        $(".lense_slide_dropdown").click(function () {
            console.log("Click");   
            $(".lense_dropdown_content").slideToggle(400);
        });
        
        //$('.requiredFields').attr('disabled',true);
        ////required fields//////
            var right_sp = $('#right_sphere');
            var left_sp = $('#left_sphere');
            var right_cy = $('#right_cylinder');
            var left_cy = $('#left_cylinder');
            var raxis = $('#reyeaxis');
            var laxis = $('#leyeaxis');
            //var radd = $('#reyeadd');
            //var ladd = $('#leyeadd');

        (right_sp).on('change',function () {
            // console.log(required_reyeadd.find(":selected").text());
            console.log("Done");
            if ($(".rx-add").css('display') == 'block')
            {  
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='' ) 
                {
                    // $('.requiredFields').attr('disabled',true);
                } 
                else 
                { 
                    //$('.requiredFields').attr("disabled",false);
                }
            }
            else
            {
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                {
                    //$('.requiredFields').attr('disabled',true);
                } 
                else 
                { 
                    //$('.requiredFields').attr("disabled",false);
                }
            }
        });

        (left_sp).on('change',function () {
            // console.log(required_reyeadd.find(":selected").text());
            console.log("Done");
            if ($(".rx-add").css('display') == 'block')
            {  
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                {
                    //$('.requiredFields').attr('disabled',true);
                } 
                else 
                { 
                    //$('.requiredFields').attr("disabled",false);
                }
            }
            else
            {
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                {
                    //$('.requiredFields').attr('disabled',true);
                } 
                else 
                { 
                    //$('.requiredFields').attr("disabled",false);
                }
            }
        });

        (right_cy).on('change',function () 
        {
            // console.log(required_reyeadd.find(":selected").text());
            console.log("Done");
            if($(".rx-add").css('display') == 'block')
            {  
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='' ) 
                {
                    //$('.requiredFields').attr('disabled',true);
                } 
                else 
                { 
                    //$('.requiredFields').attr("disabled",false);
                }
            }
            else
            {
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                {
                    //$('.requiredFields').attr('disabled',true);
                } 
                else 
                { 
                    // $('.requiredFields').attr("disabled",false);
                }
                }
            });

            (left_cy).on('change',function () {
              // console.log(required_reyeadd.find(":selected").text());
              console.log("Done");
            if ($(".rx-add").css('display') == 'block')

              {  
                
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='' ) 
                  {
                    //$('.requiredFields').attr('disabled',true);
                  } 
                else 
                    { 
                    // $('.requiredFields').attr("disabled",false);
                    }
                }
            else
                {

                  if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                  {
                    // $('.requiredFields').attr('disabled',true);
                  } 
                else 
                    { 
                    //$('.requiredFields').attr("disabled",false);
                    }
                }
            });

            (raxis).on('keyup',function () {
              console.log("Done");
            if ($(".rx-add").css('display') == 'block')

              {  
                
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                  {
                    //$('.requiredFields').attr('disabled',true);
                  } 
                else 
                    { 
                    //$('.requiredFields').attr("disabled",false);
                    }
                }
            else
                {

                  if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                  {
                    //$('.requiredFields').attr('disabled',true);
                  } 
                else 
                    { 
                    //$('.requiredFields').attr("disabled",false);
                    }
                }    

            });


            (laxis).on('keyup',function () {
              console.log("Done");
            if ($(".rx-add").css('display') == 'block')

              {  
                
                if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='' ) 
                  {
                    // $('.requiredFields').attr('disabled',true);
                  } 
                else 
                    { 
                    //$('.requiredFields').attr("disabled",false);
                    }
                }
            else
                {

                  if (right_sp.find(":selected").text() == '0.00' || left_sp.find(":selected").text() == '0.00' || right_cy.find(":selected").text() == '0.00' || left_cy.find(":selected").text() == '0.00' || raxis.val()=='' || laxis.val()=='') 
                  {
                    //$('.requiredFields').attr('disabled',true);
                  } 
                else 
                    { 
                    // $('.requiredFields').attr("disabled",false);
                    }
                }    

            });



// 



           
        
        
        
//        //requiredfields
//        function autocomplete(inp, arr) {
//  /*the autocomplete function takes two arguments,
//  the text field element and an array of possible autocompleted values:*/
//  var currentFocus;
//  /*execute a function when someone writes in the text field:*/
//  inp.addEventListener("input", function(e) {
//      var a, b, i, val = this.value;
//      /*close any already open lists of autocompleted values*/
//      closeAllLists();
//      if (!val) { return false;}
//      currentFocus = -1;
//      /*create a DIV element that will contain the items (values):*/
//      a = document.createElement("DIV");
//      a.setAttribute("id", this.id + "autocomplete-list");
//      a.setAttribute("class", "autocomplete-items");
//      /*append the DIV element as a child of the autocomplete container:*/
//      this.parentNode.appendChild(a);
//      /*for each item in the array...*/
//      for (i = 0; i < arr.length; i++) {
//        /*check if the item starts with the same letters as the text field value:*/
//        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
//          /*create a DIV element for each matching element:*/
//          b = document.createElement("DIV");
//          /*make the matching letters bold:*/
//          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
//          b.innerHTML += arr[i].substr(val.length);
//          /*insert a input field that will hold the current array item's value:*/
//          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
//          /*execute a function when someone clicks on the item value (DIV element):*/
//          b.addEventListener("click", function(e) {
//              /*insert the value for the autocomplete text field:*/
//              inp.value = this.getElementsByTagName("input")[0].value;
//              /*close the list of autocompleted values,
//              (or any other open lists of autocompleted values:*/
//              closeAllLists();
//          });
//          a.appendChild(b);
//        }
//      }
//  });
//  /*execute a function presses a key on the keyboard:*/
//  inp.addEventListener("keydown", function(e) {
//      var x = document.getElementById(this.id + "autocomplete-list");
//      if (x) x = x.getElementsByTagName("div");
//      if (e.keyCode == 40) {
//        /*If the arrow DOWN key is pressed,
//        increase the currentFocus variable:*/
//        currentFocus++;
//        /*and and make the current item more visible:*/
//        addActive(x);
//      } else if (e.keyCode == 38) { //up
//        /*If the arrow UP key is pressed,
//        decrease the currentFocus variable:*/
//        currentFocus--;
//        /*and and make the current item more visible:*/
//        addActive(x);
//      } else if (e.keyCode == 13) {
//        /*If the ENTER key is pressed, prevent the form from being submitted,*/
//       
//        if (currentFocus > -1) {
//          /*and simulate a click on the "active" item:*/
//          if (x) x[currentFocus].click();
//        }
//      }
//  });
//  function addActive(x) {
//    /*a function to classify an item as "active":*/
//    if (!x) return false;
//    /*start by removing the "active" class on all items:*/
//    removeActive(x);
//    if (currentFocus >= x.length) currentFocus = 0;
//    if (currentFocus < 0) currentFocus = (x.length - 1);
//    /*add class "autocomplete-active":*/
//    x[currentFocus].classList.add("autocomplete-active");
//  }
//  function removeActive(x) {
//    /*a function to remove the "active" class from all autocomplete items:*/
//    for (var i = 0; i < x.length; i++) {
//      x[i].classList.remove("autocomplete-active");
//    }
//  }
//  function closeAllLists(elmnt) {
//    /*close all autocomplete lists in the document,
//    except the one passed as an argument:*/
//    var x = document.getElementsByClassName("autocomplete-items");
//    for (var i = 0; i < x.length; i++) {
//      if (elmnt != x[i] && elmnt != inp) {
//        x[i].parentNode.removeChild(x[i]);
//      }
//    }
//  }
//  /*execute a function when someone clicks in the document:*/
//  document.addEventListener("click", function (e) {
//      closeAllLists(e.target);
//  });
//}
        var countries;
           $.get(window.base_url + "/products",function(data, status){
                //console.log(data[0]['price']);
             countries = data;
              // alert(countries);
                //autocomplete(document.getElementById("search_bar"), countries);
            });
        
                // Get the <datalist> and <input> elements.
var dataList = document.getElementById('json-datalist');
var dataList1 = document.getElementById('json-datalist1');
var input = document.getElementById('search_bar');
var input1 = document.getElementById('search_bar1');         
 
function removeAll(){
    dataList.innerHTML = "";
    dataList1.innerHTML = "";

  }


// Create a new XMLHttpRequest.
   
var request = new XMLHttpRequest();

// Handle state changes for the request.
request.onreadystatechange = function(response) {
  if (request.readyState === 4) {
      
    if (request.status === 200) {
      // Parse the JSON
      var jsonOptions = countries;
//        var jsonOptions = [  
//    "Delhi", 
//    "Ahemdabad", 
//    "Punjab", 
//    "Uttar Pradesh", 
//    "Himachal Pradesh", 
//    "Karnatka", 
//    "Kerela", 
//    "Maharashtra", 
//    "Gujrat", 
//    "Rajasthan", 
//    "Bihar", 
//    "Tamil Nadu", 
//    "Haryana" 
//      ];
  
      // Loop over the JSON array.
     input.onclick = function(){
          removeAll();
     }
     input1.onclick = function(){
          removeAll();
     }
         //  removeAll();
        input.onkeypress = function(){
            removeAll();
      jsonOptions.forEach(function(item) {
        // Create a new <option> element.
        var option = document.createElement('option');
        // Set the value using the item in the JSON array.
        option.value = item;
        // Add the <option> element to the <datalist>.
        dataList.appendChild(option);
      });}

      input1.onkeyup = function(){
            removeAll();
      jsonOptions.forEach(function(item) {
        // Create a new <option> element.
        var option = document.createElement('option');
        // Set the value using the item in the JSON array.
        option.value = item;
        // Add the <option> element to the <datalist>.
        dataList1.appendChild(option);
      });}
    
      // Update the placeholder text.
      input.placeholder = "Search";
      input1.placeholder = "Search";
    } else {
      // An error occured :(
      input.placeholder = "Couldn't load datalist options :(";
      input1.placeholder = "Couldn't load datalist options :(";
    }
  }
};

// Update the placeholder text.
input.placeholder = "Loading options...";
input1.placeholder = "Loading options...";
// Set up and make the request.
request.open('GET', window.base_url + "/products", true);
request.send();
//
//        
//        
//  
        
        

          $(".color_select_btn").first().addClass("active");  

          $(".color_select_btn").click(function ($this) {
            
                $(".color_select_btn").removeClass("active");
                $(this).addClass("active");    

          });


          $(".size").click(function ($this) {
            
                $(".size").removeClass("active");
                $(this).addClass("active");    

          });


          $(".nav-item").first().addClass("active");  
          $(".nav-link").first().addClass("show");
          $(".tab-pane").first().addClass("active");
          $(".tab-pane").first().addClass("show");
          $(".tab-pane").first().addClass("in");
          $(".nav-link").first().addClass("active");



          $('.left_checkbox').on('ifChecked', function(event){
            event.preventDefault();
            $('.left_disable_dropdown').prop("disabled", false);
        });

        $('.left_checkbox').on('ifUnchecked', function(event){
            event.preventDefault();
            $('.left_disable_dropdown').prop("disabled", true);
        });

        $('.right_checkbox').on('ifChecked', function(event){
            event.preventDefault();
            $('.right_disable_dropdown').prop("disabled", false);
        });

        $('.right_checkbox').on('ifUnchecked', function(event){
            event.preventDefault();
            $('.right_disable_dropdown').prop("disabled", true);
        });

        
        ///
     $(".dropdown").hover(
                function() {
                    $(this).children('.dropdown-menu').not('.in .dropdown-menu').stop().slideDown("400");
                    $(".dropdown-menu").removeClass('open');
                    $(this).toggleClass('open');
                },
                function() {
                    $(this).children('.dropdown-menu').not('.in .dropdown-menu').stop().slideUp("400");
                    $(".dropdown-menu").removeClass('open');
                    $(this).toggleClass('open');
                }
             );
        var url = window.location.href;

                if(url.includes("category=contact-lens"))
                {
                    $(".topper_head").children(":nth-child(2)").addClass("active2");
                }
            else
                if(url.includes("blog"))
                {
                    $(".topper_head").children(":nth-child(3)").addClass("active2");
                }
            else
               if (url.includes("category=Eyewear")) 
            {
                $(".topper_head").children(":nth-child(1)").addClass("active2");
//            }
//            else
            }
        ///

// if (url.includes("eyeglasses")) 
//   {
//     $(".navbar-nav").children(":nth-child(1)").addClass("active");
//   }
//   else
//   if(url.includes("sunglasses"))
//   {
//   	$(".navbar-nav").children(":nth-child(2)").addClass("active");
//   }
//   else
//   if(url.includes("clearance"))
//   {
//   	alert("3");
//   	$(".navbar-nav").children(":nth-child(3)").addClass("active");
//   }
      
    $(".main-nav li").hover(function ($this) {
        $(this).addClass("active");
    }, function ($this) {
        $(this).removeClass("active");
    });

         $(".dropdown").hover(function() {
                $(this).children(".dropdown-menu").toggleClass("show");
            });
        
        if ($(window).width() > 768){
                $(window).scroll(function(){
                    if($(document).scrollTop() > 0) {
                        $(".top-navbar").css("display","none");
                         $(".tools").css("vertical-align","top");
                        $(".navbar").css("cssText", "min-height: 80px !important;");
                        $(".main-nav").css("cssText","margin-top: 16px !important");
                         $(".toolbar").css("cssText", "top: 38% !important;");
                    } else {
                        $(".top-navbar").css("display","block");
                         $(".tools").css("vertical-align","middle");
                        $(".navbar").css("cssText", "min-height: 85px !important;");
                        $(".main-nav").css("cssText","margin-top: 4px !important");
                         $(".toolbar").css("cssText", "top: 50% !important;");
                    }
                });
            }
//        $(window).scroll(function(){
//            if($(document).scrollTop() > 0) {
//                $(".top-navbar").css("display","none");
//                $(".tools").css("vertical-align","top");
//                $(".navbar").css("cssText", "min-height: 80px !important;");
//                $(".main-nav").css("cssText","margin-top: 16px !important");
//                $(".toolbar").css("cssText", "top: 30% !important;");
//            } else {
//                $(".top-navbar").css("display","block");
//                $(".tools").css("vertical-align","middle");
//                $(".navbar").css("cssText", "min-height: 85px !important;");
//                $(".main-nav").css("cssText","margin-top: 4px !important");
//                $(".toolbar").css("cssText", "top: 42% !important;");
//            }
//        });
//
//        $(window).scroll(function(){
//            if($(document).scrollTop() > 380) {
//                $(".filter-bg").css("position","fixed");
//                $(".filter-bg").css("top","80px");
//                $(".filter-bg").css("width","102%");
//                $(".filter-bg").css("z-index","9");
//
//            }
//            else{
//                $(".filter-bg").css("position","relative");
//                $(".filter-bg").css("top","0");
//            }
//        });

        $('#media').carousel({
            pause: true,
            interval: false,
        });
        
//        var modal = document.getElementById('myModal');
//
//        // Get the button that opens the modal
//        var btn = document.getElementById("myBtn");
//
//        // Get the <span> element that closes the modal
//        var span = document.getElementsByClassName("close")[0];
        
//        $(document).click(function () {
//           
//            $('#myModal').modal("hide");
//        });
//        $('.close').click(function () {
//          
//            $('#myModal').toggle();
//        });

    });

        // Get the modal


//        // When the user clicks the button, open the modal
//        btn.onclick = function() {
//            modal.style.display = "block";
//        }
//
//        // When the user clicks on <span> (x), close the modal
//        span.onclick = function() {
////            modal.style.display = "none";
//        }
//
//        // When the user clicks anywhere outside of the modal, close it
//        window.onclick = function(event) {
//            if (event.target == modal) {
//                modal.style.display = "none";
//            }
//        }
    

        /****** OLD CODE TILL 07-02-2019 FOR TRY ON START HERE********/
        /*var moving = false;
		$(function () 
        {
            $('.image1').on('change',function () 
            {                
                var prod_ID = $(this).attr('data');
                $('#face'+prod_ID).hide();
                var reader = new FileReader();
                document.getElementById('prod_ID').value=prod_ID;
                reader.onload = function (e) 
                {
                    $('#face'+prod_ID).show();
                    $('#face'+prod_ID).attr("src", e.target.result);
                    setTimeout(initCropper(prod_ID), 1000);
                }        
                reader.readAsDataURL($(this)[0].files[0]);
            });
        
            $('.image2').on('change',function () 
            {                
                var prod_ID = $(this).attr('data');
                $('#glasses'+prod_ID).hide();
                var reader = new FileReader();
                reader.onload = function (e) 
                {
                    $('#glasses'+prod_ID).show();
                    $('#glasses'+prod_ID).attr("src", e.target.result);
                    document.getElementById("glasses"+prod_ID).addEventListener("mousedown", initialClick, false);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            }); 
        });

        function initCropper(get_img)
        {
            console.log(get_img);
            
            var image = document.getElementById('face'+get_img);
            var cropper = new Cropper(image, 
            {
                aspectRatio: 1 / 1,
                crop: function(e) {
                    console.log(e.detail.x);
                    console.log(e.detail.y);
                }
            });
            // On crop button clicked
            document.getElementById('crop_btn'+get_img).addEventListener('click', function(){
                var imgurl =  cropper.getCroppedCanvas().toDataURL();
                var img = $("#crop_result"+get_img);
                var cropper_img = $('.cropper-container');
                cropper_img.css("cssText", "display: none !important;");
                img.css("display","block");
                // alert(imgurl);
                img.attr("src", imgurl);
            })
        }
	
        function showGlass(id) 
        {             
            document.getElementById("glasses"+id).addEventListener("mousedown", initialClick, false);
			var x = -1, y = -1, width = -1, height = -1;
            var face = document.getElementById('crop_result'+id);
            var tracker = new tracking.ObjectTracker("eye");
            tracker.setStepSize(1.7);
            tracking.track('#crop_result'+id, tracker);
            tracker.on('track', function (event) 
            {
                event.data.forEach(function (rect) 
                {
					if (x != -1){
						width = (rect.x + rect.width) - x + 20;
					}				
					if (x == -1 || rect.x < x)
						x = rect.x - 20;
						
					if (y == -1)
						y = rect.y;
						
					if (height = -1)
						height = rect.height + 20;
                });
				window.plot(x, y, width, height,id);
            });
        }

		// rect;
		function increaseGlassHeight(id) 
        {
           var  rect = document.getElementById('glasses'+id);
            rect.style.height = (rect.clientHeight + 10) + 'px';			
		}
		
		function decreaseGlassHeight(id) {
             var rect = document.getElementById('glasses'+id);
            rect.style.height = (rect.clientHeight - 10) + 'px';
		}

		function increaseGlassWidth(id) {
            var rect = document.getElementById('glasses'+id);
            rect.style.width =  (rect.clientWidth + 10) + 'px';
		}
		
		function decreaseGlassWidth(id) {
            var rect = document.getElementById('glasses'+id);
            rect.style.width =  (rect.clientWidth - 10) + 'px';
		}		
		
        window.plot = function (x, y, w, h,id) 
        {
            var face = document.getElementById('crop_result'+id);
            rect = document.getElementById('glasses'+id);
            rect.style.width = w + 'px';
            rect.style.height = h + 'px';
            rect.style.left = (face.offsetLeft + x) + 'px';
            rect.style.top = (face.offsetTop + y) + 'px';
        };
		
		
		function move(e)
        {
            var p = $( ".modal-content" );
            var position = p.position();
            var newX = e.clientX - 780;
			var newY = e.clientY - 100;
			image.style.left = newX + "px";
			image.style.top = newY + "px";
		}

		function initialClick(e) 
        {
			if(moving){
				document.removeEventListener("mousemove", move);
				moving = !moving;
				return;
			}
			moving = !moving;
			image = this;
			document.addEventListener("mousemove", move, false);
		}	*/		
    /****** OLD CODE TILL 07-02-2019 FOR TRY ON END HERE********/


    /****** UPDATE ON 07-02-2019 FOR TRY ON END HERE********/

    var moving = false;
        $(function () 
        {
            $('.image1').on('change',function () 
            {    
                $('.try-upload-tips').fadeOut();
                $('.eyetry-sidebar').fadeIn();
                var prod_ID = $(this).attr('data');
                $('#plus1-'+prod_ID).show();
                $('#plus2-'+prod_ID).show();
                $('#face'+prod_ID).hide();
            
                var img = $("#crop_result"+prod_ID);
                var cropper_img = $('.cropper-container');
                cropper_img.css("cssText", "display: block !important;");
                img.css("display","none");

                var reader = new FileReader();
                document.getElementById('prod_ID').value=prod_ID;
                reader.onload = function (e) 
                {
                    $('#face'+prod_ID).show();
                    $('#face'+prod_ID).attr("src", e.target.result);
                }        
                reader.readAsDataURL($(this)[0].files[0]);
            });
            
            $('.image2').on('change',function () 
            {                
                var prod_ID = $(this).attr('data');
                var reader = new FileReader();
                reader.onload = function (e) 
                {
                    $('#crop_result'+prod_ID).hide();
                    $('#glasses'+prod_ID).show();
                    $('#glasses'+prod_ID).attr("src", e.target.result);
                    document.getElementById("glasses"+prod_ID).addEventListener("mousedown", initialClick, false);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            }); 
        });

        function initCropper(get_img)
        {
            console.log(get_img);
            var image = document.getElementById('face'+get_img);
            var cropper = new Cropper(image, 
            {
                aspectRatio: 1 / 1,
                crop: function(e) {
                console.log(e.detail.x);
                console.log(e.detail.y);
            }
        });

        // On crop button clicked
        document.getElementById('crop_btn'+get_img).addEventListener('click', function()
            {
                $(".crop_result").addClass(".show_crop_result");
                var imgurl =  cropper.getCroppedCanvas().toDataURL();
                var img = $("#crop_result"+get_img);
                var cropper_img = $('.cropper-container');
                cropper_img.css("cssText", "display: none !important;");
                img.css("display","block");
                // alert(imgurl);
                img.attr("src", imgurl);
            })
        }


    
        function showGlass(id) 
        { 
            $('.eyetry-sidebar').fadeOut();    
            $('#plus1-'+id).hide();
            $('#plus2-'+id).hide();  
            //dragElement(document.getElementById('plus3-'+id));
            document.getElementById("glasses"+id).addEventListener("mousedown", initialClick, false);       
            var x = -1, y = -1, width = -1, height = -1;
            var face = document.getElementById('face'+id);
            var tracker = new tracking.ObjectTracker("eye");
            tracker.setStepSize(1.7);
            tracking.track('#face'+id, tracker);
            tracker.on('track', function (event) {
                event.data.forEach(function (rect) {
                    if (x != -1) {
                        width = (rect.x + rect.width) - x + 20;
                    }               
                    if (x == -1 || rect.x < x)
                        x = rect.x - 20;
                        
                    if (y == -1)
                        y = rect.y;
                    if (height = -1)
                        height = rect.height + 20;
                });
                window.plot(x, y, width, height,id);
            });
        }

        // rect;
        function increaseGlassHeight(id) {
           var  rect = document.getElementById('glasses'+id);
            //rect.style.width =  (rect.clientWidth + 10) + 'px';
             rect.style.width =  (rect.clientWidth + 10) + 'px';
            rect.style.height = (rect.clientHeight + 10) + 'px';            
        }
        
        function decreaseGlassHeight(id) {
            var rect = document.getElementById('glasses'+id);
            rect.style.height = (rect.clientHeight - 10) + 'px';
            rect.style.width =  (rect.clientWidth - 10) + 'px';
        }

        function increaseGlassWidth(id) {
            var rect = document.getElementById('glasses'+id);
            rect.style.width =  (rect.clientWidth + 10) + 'px';         
        }
        
        function decreaseGlassWidth(id) {
             var rect = document.getElementById('glasses'+id);
            rect.style.width =  (rect.clientWidth - 10) + 'px';
        }       
        
        window.plot = function (x, y, w, h,id) 
        {
            var face = document.getElementById('face'+id);
            rect = document.getElementById('glasses'+id);
            var c1_left = ($('#plus1-'+id).css('left')).replace(/[^.\d\s]+/gi, "");
            var c1_top = ($('#plus1-'+id).css('top')).replace(/[^.\d\s]+/gi, "");
            var c2_left = ($('#plus2-'+id).css('left')).replace(/[^.\d\s]+/gi, "");
            var c2_top =($('#plus2-'+id).css('top')).replace(/[^.\d\s]+/gi, "");

            if(Number(c1_left)>Number(c2_left))
            {
                x = (Number(c2_left) + Number(4));
                y = (Number(c2_top) + Number(9));
               
            }
            else{
                x = (Number(c1_left) + Number(4));
                y = (Number(c1_top) + Number(9));
            }
            
            var distance = Math.sqrt(Math.pow((c2_left-c1_left),2)+Math.pow((c2_top-c1_top),2));
             rect.style.width = (distance+57) + 'px';//transform: rotate(5deg);
             rect.style.height = 'auto';
             // angle in radians Number(c2_left)-Number(c1_left)
            var angleRadians = Math.atan2(c2_top-c1_top, c2_left-c1_left);

            // angle in degrees
            var angleDeg = Math.atan2((c2_top - c1_top), (c1_left -c1_left)) * 180 / Math.PI;
            var degree123 ;
            if(angleRadians==0 || angleRadians < 1){
                degree123 =  360-angleRadians-4;
            }else{
                degree123 =  360-angleRadians;
            }
            
            $('#glasses'+id).css('transform','rotate('+degree123+'deg)');
            rect.style.left = (x) + 'px';
            rect.style.top = (y) + 'px';
        };
        
        
        function move(e)
        {
            var p = $( ".modal-content" );
            var position = p.position();
            
            var newX = e.clientX - 230;
            var newY = e.clientY - 100;
            
            image.style.left = newX + "px";
            image.style.top = newY + "px";
        }
       
        function initialClick(e) 
        {
            if(moving){
                document.removeEventListener("mousemove", move);
                moving = !moving;
                return;
            }
            moving = !moving;
            image = this;
            document.addEventListener("mousemove", move, false);
        }   

        function close_try(id){
            //alert(id);
            $('#eyetry-box-'+id).hide();
            $('#eyetry-box-'+id).removeClass('show');
        }

        /****** UPDATE ON 07-02-2019 FOR TRY ON END HERE********/



  
//        var moving = false;
//
//        $(function () {
//            $('#image1').change(function () {
//                $('#face').hide();
//                var reader = new FileReader();
//                reader.onload = function (e) {
//                    $('#face').show();
//                    $('#face').attr("src", e.target.result);
//                }
//                reader.readAsDataURL($(this)[0].files[0]);
//            });
//            $('#image2').change(function () {
//                $('#glasses123').hide();
//                var reader = new FileReader();
//                reader.onload = function (e) {
//                    $('#glasses123').show();
//                    $('#glasses123').css("display","block");
//                    $('#glasses123').attr("src", e.target.result);
//                    document.getElementById("glasses123").addEventListener("mousedown", initialClick, false);
//                }
//                reader.readAsDataURL($(this)[0].files[0]);
//            });
//        });
//
//        function showGlass() {
//
//            var x = -1, y = -1, width = -1, height = -1;
//            var face = document.getElementById('face');
//
//            var tracker = new tracking.ObjectTracker("eye");
//            tracker.setStepSize(1.7);
//
//            tracking.track('#face', tracker);
//
//            tracker.on('track', function (event) {
//                event.data.forEach(function (rect) {
//                    if (x != -1) {
//                        width = (rect.x + rect.width) - x + 0;
//                    }
//
//                    if (x == -1 || rect.x < x)
//                        x = rect.x - 0;
//
//                    if (y == -1)
//                        y = rect.y;
//
//                    if (height = -1)
//                        height = rect.height + 0;
//                });
//                window.plot(x, y, width, height);
//
//            });
//        }
//
//        function increaseGlassHeight() {
//            var rect = document.getElementById('glasses123');
//            //rect.style.width =  (rect.clientWidth + 10) + 'px';
//            rect.style.height = (rect.clientHeight + 10) + 'px';
//        }
//
//        function decreaseGlassHeight() {
//            var rect = document.getElementById('glasses123');
//            //rect.style.width =  (rect.clientWidth - 10) + 'px';
//            rect.style.height = (rect.clientHeight - 10) + 'px';
//        }
//
//        function increaseGlassWidth() {
//            var rect = document.getElementById('glasses123');
//            rect.style.width =  (rect.clientWidth + 10) + 'px';
//            //rect.style.height = (rect.clientHeight + 10) + 'px';
//        }
//
//        function decreaseGlassWidth() {
//            var rect = document.getElementById('glasses123');
//            rect.style.width =  (rect.clientWidth - 10) + 'px';
//            rect.style.height = (rect.clientHeight - 10) + 'px';
//            // var left = window.getComputedStyle('glasses',null).getPropertyValue('left');
//            rect.style.left = (left - 10) + 'px';
//        }
//
//        // window.plot = function (x, y, w, h) {
//        //     var rect = document.getElementById('glasses123');
//        //     rect.style.width = w + 'px';
//        //     rect.style.height = h + 'px';
//        //     rect.style.left = (face.offsetLeft + x) + 'px';
//        //     rect.style.top = (face.offsetTop + y) + 'px';
//        // };
//
//
//        // function move(e){
//
//        //     var newX;
//        //     var newY;
//
//        //     e = e || window.event;
//        //     e.preventDefault();
//        //     // calculate the new cursor position:
//        //     var pos1 = newX - e.clientX;
//        //     var pos2 = newY - e.clientY;
//        //     newX = e.clientX;
//        //     newY = e.clientY;
//
//        //     image.style.left = newX + "px";
//        //     image.style.top = newY + "px";
//
//
//        //     // image.style.top = (image.offsetTop - pos2) + "px";
//        //     // image.style.left = (image.offsetLeft - pos1) + "px";
//        // }
//
//        function initialClick(e) {
//            if(moving){
//                document.removeEventListener("mousemove", move);
//                moving = !moving;
//                return;
//            }
//
//            moving = !moving;
//            image = this;
//
//            document.addEventListener("mousemove", move, false);
//        }
//
//        //Make the DIV element draggagle:
//dragElement(document.getElementById("glasses123"));
//
//function dragElement(elmnt) {
//  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
//  elmnt.onmousedown = dragMouseDown;
//
//  function dragMouseDown(e) {
//    e = e || window.event;
//    e.preventDefault();
//    // get the mouse cursor position at startup:
//    pos3 = e.clientX;
//    pos4 = e.clientY;
//    document.onmouseup = closeDragElement;
//    // call a function whenever the cursor moves:
//    document.onmousemove = elementDrag;
//  }
//
//  function elementDrag(e) {
//    e = e || window.event;
//    e.preventDefault();
//    // calculate the new cursor position:
//    pos1 = pos3 - e.clientX;
//    pos2 = pos4 - e.clientY;
//    pos3 = e.clientX;
//    pos4 = e.clientY;
//    // set the element's new position:
//    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
//    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
//  }
//
//  function closeDragElement() {
//    /* stop moving when mouse button is released:*/
//    document.onmouseup = null;
//    document.onmousemove = null;
//  }
//}
    </script>

    <!--3step form script start-->
    <script>
        $(document).ready(function () {


            $("fa-cart-plus").removeClass("fa-cart-plus");
            $(this).addClass("fa-shopping-cart")   

            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-indigo').addClass('btn-default');
                    $item.addClass('btn-indigo');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allPrevBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                prevStepSteps.removeAttr('disabled').trigger('click');
            });

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i< curInputs.length; i++){
                    if (!curInputs[i].validity.valid){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-indigo').trigger('click');
        });
    </script>
</body>
</html>