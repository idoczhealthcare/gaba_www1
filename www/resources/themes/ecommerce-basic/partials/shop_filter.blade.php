<div class="filter-bg">
    <div class="container">
    <div class="search-row">
<!--        <h3 class="widget-title">@lang('corals-ecommerce-basic::labels.template.shop.shop_categories')</h3>-->
<ul class="pd">
    <form id="filterForm">
                  @foreach(\Shop::getActiveCategories() as $category)
                <li  class="dropdown {{ $hasChildren = $category->hasChildren()?'has-children':'' }} parent-category" id="{{ $category->slug }}1">
 

        <a href="#"  >{{ $category->name }}<span class="caret" ></span></a>
        @if($hasChildren)

                           <ul class="dropdown-menu" role="menu">
                            @foreach($category->children as $child)
                                <li style="color:#fff;">
                                    <div class="fmbt">
                                        <input class=""
                                               name="category[]" value="{{ $child->slug }}"
                                               type="checkbox"
                                               id="ex-check-{{ $child->id }}"
                                                {{ \Shop::checkActiveKey($child->slug,'category')?'checked':'' }}>
                                        <label class=""
                                               for="ex-check-{{ $child->id }}">
                                            {{ $child->name }}
                                            ({{
                                                                \Shop::getCategoryAvailableProducts($child->id, true)
                                                                }})
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
        @endif
    </li>

     @endforeach
     <li class="dropdown dp_pr" id="price">
        <a href="#" >Price <span class="caret"></span></a>
        <ul class="dropdown-menu price_div" role="menu">

            <li>
            
        <h3 class="widget-title">@lang('corals-ecommerce-basic::labels.template.shop.price_range')</h3>
        <div class="price-range-slider"
             data-min="{{ $min = \Shop::getSKUMinPrice()??0 }}"
             data-max="{{ $max= \Shop::getSKUMaxPrice()??99999 }}"
             data-start-min="{{ request()->input('price.min', $min) }}"
             data-start-max="{{ request()->input('price.max', $max) }}"
             data-step="1">
            <div class="ui-range-slider"></div>
            <footer class="ui-range-slider-footer">
                <div class="column">
                    <div class="ui-range-values">
                        <div class="ui-range-value-min"><span id="minprice"></span>
                            <input name="price[min]" type="hidden">
                        </div>&nbsp;-&nbsp;
                        <div class="ui-range-value-max"><span id="maxprice"></span>
                            <input name="price[max]" type="hidden">
                        </div>
                    </div>
                </div>
               
            </footer>
             
        </div>
 
            </li>
        </ul>
    </li>
   
    
   <section class="post_relative">
        <div class="column post_absolute">
            <button class="btn btn-outline-primary btn-block btn-sm filter_btn inline_block"
                type="submit">@lang('corals-ecommerce-basic::labels.template.shop.filter')
            </button>

            <a class="btn btn-outline-primary btn-block btn-sm reset_btn inline_block" href="{{ url('/shop') }}">
                Reset
            </a>
        </div>
    </section>
    </form>
</ul>

    </div>

    </div>
</div>



<!-- Mobile Filter -->
<div class="mobi_filter m_t_15">
    <div class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form class="mobi_filter_form">
                        <div class="pull-left">
                            <button class="mobi_filter_btn">Sort By: <span><i class="fa fa-caret-down"></i></span>
                            </button>
                            <ul class="mobi_filter_dropdown">
                                <li>Eyeglasses</li>
                                <li>Sunglasses</li>
                                <li>Clearance</li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <button class="mobi_filter_side_btn">Filter <i class="fa fa-sliders"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>            
</div>

<div class="side_filter_panel show3">

    <form id="filterForm">
                <div class="header_content">
                    <h3 class="filter_header">filter frames by:</h3>
                    <span class="filter_close"><i class="fa fa-times"></i></span>
                </div>
                  @foreach(\Shop::getActiveCategories() as $category)
                <li  class="side_filter_slide_dropdown {{ $hasChildren = $category->hasChildren()?'has-children':'' }} parent-category" id="{{ $category->slug }}1">
 

        <a href="#" class="" >{{ $category->name }}<span class="caret" ></span></a>
        @if($hasChildren)

                           <ul class="dropdown-menu" role="menu">
                            @foreach($category->children as $child)
                                <li style="color:#fff;">
                                    <div class="fmbt">
                                        <input class=""
                                               name="category[]" value="{{ $child->slug }}"
                                               type="checkbox"
                                               id="ex-check-{{ $child->id }}"
                                                {{ \Shop::checkActiveKey($child->slug,'category')?'checked':'' }}>
                                        <label class=""
                                               for="ex-check-{{ $child->id }}">
                                            {{ $child->name }}
                                            ({{
                                                                \Shop::getCategoryAvailableProducts($child->id, true)
                                                                }})
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
        @endif
    </li>

     @endforeach
     <li class="side_filter_slide_dropdown dropdown dp_pr" id="price">
        <a href="#" >Price <span class="caret"></span></a>
        <ul class="dropdown-menu price_div" role="menu">

            <li>
            
        <h3 class="widget-title">@lang('corals-ecommerce-basic::labels.template.shop.price_range')</h3>
        <div class="price-range-slider"
             data-min="{{ $min = \Shop::getSKUMinPrice()??0 }}"
             data-max="{{ $max= \Shop::getSKUMaxPrice()??99999 }}"
             data-start-min="{{ request()->input('price.min', $min) }}"
             data-start-max="{{ request()->input('price.max', $max) }}"
             data-step="1">
            <div class="ui-range-slider"></div>
            <footer class="ui-range-slider-footer">
                <div class="column">
                    <div class="ui-range-values">
                        <div class="ui-range-value-min"><span id="minprice"></span>
                            <input name="price[min]" type="hidden">
                        </div>&nbsp;-&nbsp;
                        <div class="ui-range-value-max"><span id="maxprice"></span>
                            <input name="price[max]" type="hidden">
                        </div>
                    </div>
                </div>
               
            </footer>
             
        </div>
 
            </li>
        </ul>
    </li>
   
    <section class="btn_container">
            
            <a class="btn btn-outline-primary btn-block btn-sm inline_block mobi_filter_reset" href="{{ url('/shop') }}">
                Reset
            </a>
            <button class="btn btn-outline-primary btn-block btn-sm inline_block mobi_filter_submit"
                type="submit">@lang('corals-ecommerce-basic::labels.template.shop.filter')
            </button>
    </section>
   <!-- <section class="post_relative">
        <div class="column post_absolute">
            <button class="btn btn-outline-primary btn-block btn-sm filter_btn inline_block"
                type="submit">@lang('corals-ecommerce-basic::labels.template.shop.filter')
            </button>

            <a class="btn btn-outline-primary btn-block btn-sm reset_btn inline_block" href="{{ url('/shop') }}">
                Reset
            </a>
        </div>
    </section> -->
    </form>
</div>

<script>
    function fun11(){
        var min=document.getElementById('minprice').innerHTML;
        var max=document.getElementById('maxprice').innerHTML;
       // alert(min);
        a = window.location.href;
         var n = a.includes("?");
        if(n){
            n = a.includes("price[min]");
            if(n){
                a.replace('price[min]=','');
                a.replace('price[max]=','');
                
          window.location.href =   a+"&price[min]="+min+"&price[max]="+max;
        
            
            }else{
                 window.location.href =   a+"&price[min]="+min+"&price[max]="+max;
            }
        }
        else{
             window.location.href =   a+"?price[min]="+min+"&price[max]="+max;
        }
    }
    window.onload = function(e){ 

     a = window.location.href;
    var n = a.includes("?");
        if(n){
            var b =a.split('?');
            
            n = b[1].includes("&");
            
            if(n){
                 var c =b[1].split('&');
                  var len = c.length;
            var i;
           
                for (i = 0; i < len; i++) { 
                 // alert(c[i]);  
                    var k = c[i].split('=');
                   // alert(k[1]); 
                     var div = document.getElementById('cateee');
                div.innerHTML +=' <div class="alert alert-success alert-dismissible fade in custom_filters"><a href="#" class="close" data-dismiss="alert" onclick="rem(this.id)" id="'+k[1]+'" aria-label="close">&times;</a> <strong>'+k[1]+'</strong></div>' ;
                    
                    
                    
                  


           
            }
                
            }else{
                //alert(b[1]);
                  var k = b[1].split('=');
                 //alert(k[1]); 
                var div = document.getElementById('cateee');
               // div.innerHTML += "<span onclick='rem(this.id)' id='"+k[1]+"'>"+k[1]+"</span>";
                                div.innerHTML +=' <div class="alert alert-success alert-dismissible fade in custom_filters"><a href="#" class="close" data-dismiss="alert" onclick="rem(this.id)" id="'+k[1]+'" aria-label="close">&times;</a> <strong>'+k[1]+'</strong></div>' ;
                //alert(div.innerHTML);
            
            } 
          
             //document.getElementById('cateee').innerHTML = "<span>"+id+"</span>";
        }
      
    }
  
function rem(id){
   
    a = window.location.href;
     var n = a.includes("&");
    if(n){
    var ret = a.replace('&category%5B%5D='+id,'');
       // &price%5Bmax%5D=20000
//        ret = a.replace('&price%5Bmax%5D=20000','');
//        ret = a.replace('&price%5Bmin%5D='+id,'');
//         ret = a.replace('?price%5Bmax%5D='+id,'');
//        ret = a.replace('?price%5Bmin%5D='+id,'');
    ret = a.replace('&category='+id,'');
         ret = a.replace('category='+id+'&','');
         ret = a.replace('category%5B%5D='+id+'&','');
       // alert(ret);
     window.location.href = ret;
    }
    else{
        n = a.includes("?");
       // alert(a);
        if(n){
            ret = a.replace('?price%5Bmax%5D='+id,'');
        ret = a.replace('?price%5Bmin%5D='+id,'');
            var ret = a.replace('?category='+id,'');
            ret = a.replace('?category%5B%5D='+id,'');
            //alert(ret);
                 window.location.href = ret;

        }
    }
    console.log(ret);  
}
function fun(id){
    
    a = window.location.href;
    var n = a.includes("?");
    if(n){
         window.location.href = a+'&category='+id; 
       
   // alert();
    }
    else{
        
         window.location.href = a+'?category='+id;
               

    }
    
    
   
}




</script>
@php \Actions::do_action('post_display_ecommerce_filter') @endphp