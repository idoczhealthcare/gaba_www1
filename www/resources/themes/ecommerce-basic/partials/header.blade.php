<!-- Off-Canvas Mobile Menu-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->


<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GADA | Home page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css"/>


    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/fontello.css">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/jquery.slitslider.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/main.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/slick.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/responsive.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/cropper.css">
    
     <script src="../assets/themes/ecommerce-basic/js/jquery.min.js"></script>      
      <script src="../assets/themes/ecommerce-basic/js/common.js"></script>
      <script src="../assets/themes/ecommerce-basic/js/cropper.js"></script>
      <script src="../assets/themes/ecommerce-basic/js/jquery-cropper.js"></script>
    
    <noscript>
        <link rel="stylesheet" type="text/css" href="../assets/themes/ecommerce-basic/css/styleNoJS.css" />
    </noscript>
    <style>
        .icheckbox_square-purple, .iradio_square-purple {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 22px;
            height: 22px;
            background: url(../assets/themes/ecommerce-basic/img/purple.png) no-repeat;
            border: none;
            cursor: pointer;
        }
    .icheckbox_flat-blue, .iradio_flat-blue{    background: url(../assets/themes/ecommerce-basic/img/blue.png) no-repeat;}
    </style>
</head>    
    <body>
<header class="navbar navbar-sticky">
    <!-- Search-->

   <div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<nav class="navbar navbar-default navbar" >

    <div class="row top-navbar">
        <div class="container">
        <ul class="topper_head">
            <li class="wow fadeInDown bottom_link_selector"  data-wow-delay="0.1s"><a class="product.php active" href="{{ url('/') }}">Eyewear</a></li>
            <li class="wow fadeInDown bottom_link_selector" data-wow-delay="0.1s"><a class="product.php" href="{{ url('/shop_contact/gaba-vv') }}">Contact Lenses</a></li>
            <li class="wow fadeInDown bottom_link_selector" data-wow-delay="0.4s"><a href="{{ url('/blog') }}">Blog</a></li>
        </ul>
    </div>
    </div>
    <div class="container desktop_menu">
       <!-- Brand and toggle get grouped for better mobile display -->


       <div class="toolbar">
        <div class="inner">
            <div class="tools">
             <form class="search_form" method="get" action="{{ url('shop') }}">
                <div class="custom_search"><input id="search_bar" class="tags" value="{{ request()->get('search_bar') }}" type="text" name="search" placeholder="Search" list="json-datalist">
                     <datalist id="json-datalist" ></datalist>
                    <button class="search_btn" type="submit">
                        <i class="fa fa-search search_icon"></i>
                    </button>
                </div>
            </form>

                <div class="account">
                    <a href="#"></a><i class="fa fa-user"></i>
                    <ul class="toolbar-dropdown">
                        @auth
                            <li class="sub-menu-user">
                                <div class="user-ava">
                                    <img src="{{ user()->picture_thumb }}"
                                         alt="{{ user()->name }}">
                                </div>
                                <div class="user-info">
                                    <h6 class="user-name">{{ user()->name }}</h6>
                                    <span class="text-xs text-muted">
                                       @lang('corals-ecommerce-basic::labels.partial.member_since')
                                        <br/>
                                        {{ format_date(user()->created_at) }}
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="{{ url('dashboard') }}">@lang('corals-ecommerce-basic::labels.partial.dashboard')</a>
                            </li>
                            <li>
                                <a href="{{ url('profile') }}">@lang('corals-ecommerce-basic::labels.partial.my_profile')</a>
                            </li>
                            <li class="sub-menu-separator"></li>
                            <li><a href="{{ route('logout') }}" data-action="logout">
                                    @lang('corals-ecommerce-basic::labels.partial.logout') <i
                                            class="fa fa-sign-out fa-fw"></i></a>
                            </li>
                            @else
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-sign-in fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.login')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('register') }}">
                                        <i class="fa fa-user fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.register')
                                    </a>
                                </li>
                                @endauth
                    </ul>
                </div>
                

                 <a class="icon_heart_wrap" href="/e-commerce/wishlist/my">
                    <i class="fa fa-heart icon_heart"></i>
                    @if($wishlist = \Shop::getwishlist())
                    {{ $wishlist }}
                    @else
                    <span>0</span>
                    @endif
                </a>
                <div class="cart bounceInRight login" data-wow-delay="0.4s"><a href="{{ url('cart') }}"></a>
                    <i class="fa fa-shopping-bag fa-fw"></i>
                    <span class="count" id="cart-header-count">{{ count(\ShoppingCart::getItems()) }}</span>
                    <span class="subtotal" id="cart-header-total">
                        {{ \ShoppingCart::total() }}
                    </span>
                </div>
            </div>
        </div>        
    </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="{{ url('/') }}"><img src="{{ \Settings::get('site_logo') }}" alt="{{ \Settings::get('site_name', 'GABA') }}"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">

                <ul class="main-nav nav navbar-nav" >
                    <li class="wow fadeInDown dropdown mega-dropdown" data-wow-delay="0.4s">
    <a href="{{ url('/shop') }}">EYEGLASSES<span class="caret"></span></a>
    <ul class="dropdown-menu mega-dropdown-menu postion_absolute_left">

        <li class="col-sm-2 wow fadeIn" style="margin-left: 120px;">
            <ul>
                <li class="dropdown-header">SHOP BY GENDER</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=men') }}">Men</a></li>
                <li><a href="{{ url('/shop?category=women') }}">Women</a></li>
                <li><a href="{{ url('/shop?category=unisex') }}">Kids</a></li>
                   </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY STYLE</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=full-frame') }}"><img src="../assets/themes/ecommerce-basic/img/full-frame-menu.jpg" class="img-fluid" alt="First sample image">Full Frame</a></li>
                <li><a href="{{ url('/shop?category=') }}"><img src="../assets/themes/ecommerce-basic/img/rimless-menu.jpg" class="img-fluid" alt="First sample image">Semi-Rimless</a></li>
                <li><a href="{{ url('/shop?category=semi-rimless') }}"><img src="../assets/themes/ecommerce-basic/img/semi-rimless-menu.jpg" class="img-fluid" alt="First sample image">Rimless Slim</a></li>
                <li><a href="{{ url('/shop?category=slip-metal') }}">Slip Metal Frame</a></li>
                <li><a href="{{ url('/shop?category=thick-frame') }}">Thick Frame</a></li>
                </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY MATERIAL</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=acetate') }}">Acetate</a></li>
                <li><a href="{{ url('/shop?category=plastic') }}">Plastic</a></li>
                <li><a href="{{ url('/shop?category=metal') }}">Metal</a></li>
                <li><a href="{{ url('/shop?category=titanium') }}">Titanium</a></li>
                <li><a href="{{ url('/shop?category=TR90') }}">TR90</a></li>
                <li><a href="{{ url('/shop?category=wood') }}">Wood</a></li>
            </ul>
        </li>
        
        <li class="col-sm-4 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY SHAPE</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=oval') }}"><img src="../assets/themes/ecommerce-basic/img/oval.jpg" class="img-fluid" alt="First sample image">Oval</a></li>
                <li><a href="{{ url('/shop?category=rectangle') }}"><img src="../assets/themes/ecommerce-basic/img/rectangle.jpg" class="img-fluid" alt="First sample image">Rectangle</a></li>
                <li><a href="{{ url('/shop?category=round') }}"><img src="../assets/themes/ecommerce-basic/img/round.jpg" class="img-fluid" alt="First sample image">Round</a></li>
                <li><a href="{{ url('/shop?category=browline') }}"><img src="../assets/themes/ecommerce-basic/img/browline.jpg" class="img-fluid" alt="First sample image">Browline</a></li>
                <li><a href="{{ url('/shop?category=aviator') }}"><img src="../assets/themes/ecommerce-basic/img/aviator.jpg" class="img-fluid" alt="First sample image">Aviator</a></li>
                <li><a href="{{ url('/shop?category=cat-eye') }}"><img src="../assets/themes/ecommerce-basic/img/cateye.jpg" class="img-fluid" alt="First sample image">Cat Eye</a></li>
                <li><a href="{{ url('/shop?category=geometric') }}"><img src="../assets/themes/ecommerce-basic/img/Geometric.jpg" class="img-fluid" alt="First sample image">Geometric</a></li>
            </ul>

            
        </li>
       
    </ul>
</li>
                    <li class="wow fadeInDown dropdown" data-wow-delay="0.4s">
                        <a href="{{ url('/shop?category=sunglasses') }}">SUNGLASSES<span class="caret"></span></a>


                        <ul class="dropdown-menu mega-dropdown-menu postion_absolute_left">

        <li class="col-sm-2 wow fadeIn" style="margin-left: 120px;">
            <ul>
                <li class="dropdown-header">SHOP BY GENDER</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=men') }}">Men</a></li>
                <li><a href="{{ url('/shop?category=women') }}">Women</a></li>
                <li><a href="{{ url('/shop?category=unisex') }}">Kids</a></li>
                   </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY STYLE</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=full-frame') }}"><img src="../assets/themes/ecommerce-basic/img/full-frame-menu.jpg" class="img-fluid" alt="First sample image">Full Frame</a></li>
                <li><a href="{{ url('/shop?category=') }}"><img src="../assets/themes/ecommerce-basic/img/rimless-menu.jpg" class="img-fluid" alt="First sample image">Semi-Rimless</a></li>
                <li><a href="{{ url('/shop?category=semi-rimless') }}"><img src="../assets/themes/ecommerce-basic/img/semi-rimless-menu.jpg" class="img-fluid" alt="First sample image">Rimless Slim</a></li>
                <li><a href="{{ url('/shop?category=slip-metal') }}">Slip Metal Frame</a></li>
                <li><a href="{{ url('/shop?category=thick-frame') }}">Thick Frame</a></li>
                </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY MATERIAL</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=acetate') }}">Acetate</a></li>
                <li><a href="{{ url('/shop?category=plastic') }}">Plastic</a></li>
                <li><a href="{{ url('/shop?category=metal') }}">Metal</a></li>
                <li><a href="{{ url('/shop?category=titanium') }}">Titanium</a></li>
                <li><a href="{{ url('/shop?category=TR90') }}">TR90</a></li>
                <li><a href="{{ url('/shop?category=wood') }}">Wood</a></li>
            </ul>
        </li>
        
        <li class="col-sm-4 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY SHAPE</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=oval') }}"><img src="../assets/themes/ecommerce-basic/img/oval.jpg" class="img-fluid" alt="First sample image">Oval</a></li>
                <li><a href="{{ url('/shop?category=rectangle') }}"><img src="../assets/themes/ecommerce-basic/img/rectangle.jpg" class="img-fluid" alt="First sample image">Rectangle</a></li>
                <li><a href="{{ url('/shop?category=round') }}"><img src="../assets/themes/ecommerce-basic/img/round.jpg" class="img-fluid" alt="First sample image">Round</a></li>
                <li><a href="{{ url('/shop?category=browline') }}"><img src="../assets/themes/ecommerce-basic/img/browline.jpg" class="img-fluid" alt="First sample image">Browline</a></li>
                <li><a href="{{ url('/shop?category=aviator') }}"><img src="../assets/themes/ecommerce-basic/img/aviator.jpg" class="img-fluid" alt="First sample image">Aviator</a></li>
                <li><a href="{{ url('/shop?category=cat-eye') }}"><img src="../assets/themes/ecommerce-basic/img/cateye.jpg" class="img-fluid" alt="First sample image">Cat Eye</a></li>
                <li><a href="{{ url('/shop?category=geometric') }}"><img src="../assets/themes/ecommerce-basic/img/Geometric.jpg" class="img-fluid" alt="First sample image">Geometric</a></li>
            </ul>

            
        </li>
       
    </ul>
                    </li>
                    <!-- <li class="wow fadeInDown dropdown" data-wow-delay="0.4s">
                        <a href="{{ url('/shop?category=glasses') }}">LIMITED SALE</a>
                    </li> -->
                    <li class="wow fadeInDown dropdown mega-dropdown" data-wow-delay="0.4s">
                        <a href="{{ url('/shop?category=clearance') }}">CLEARANCE</a>
                    </li>
       
    </ul>


        </div><!-- /.navbar-collapse -->


    </div><!-- /.container-fluid -->

<!-- mobile menu -->

    <form class="site-search" method="get" action="{{ url('shop') }}">
        <input type="text" name="search" value="{{ request()->get('search') }}" placeholder="Type to search..."/>

        <div class="search-tools"><span
                    class="clear-search"> @lang('corals-ecommerce-basic::labels.partial.clear')</span>
            <span class="close-search"><i class="icon-cross"></i></span>
        </div>
    </form>

    <div class="container mobile_menu">
        <div class="">
            <div class="col-sm-12 ">
          
                <div class="pos_rel">
                    <div class="inline_block">    
                        <a href="#" class="hamburg_menu_toggle"><i class="fa fa-bars hamburg_menu"></i></a>
                    </div>
                    
                    <div class="inline_block">    
                        <a class="mobi_logo"  href="{{ url('/') }}"><img class="" src="{{ \Settings::get('site_logo') }}" alt="{{ \Settings::get('site_name', 'GABA') }}"></a>
                    </div>

                    <div class="pull-right close_mobi_menu m_m_t"> 
                    <div class="">
        <div class="">
            <div class="">

                <div class="account inline_block mobi_account">
                    <a href="#"></a><i class="fa fa-user"></i>
                    <ul class="toolbar-dropdown">
                        @auth
                            <li class="sub-menu-user">
                                <div class="user-ava">
                                    <img src="{{ user()->picture_thumb }}"
                                         alt="{{ user()->name }}">
                                </div>
                                <div class="user-info">
                                    <h6 class="user-name">{{ user()->name }}</h6>
                                    <span class="text-xs text-muted">
                                       @lang('corals-ecommerce-basic::labels.partial.member_since')
                                        <br/>
                                        {{ format_date(user()->created_at) }}
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="{{ url('dashboard') }}">@lang('corals-ecommerce-basic::labels.partial.dashboard')</a>
                            </li>
                            <li>
                                <a href="{{ url('profile') }}">@lang('corals-ecommerce-basic::labels.partial.my_profile')</a>
                            </li>
                            <li class="sub-menu-separator"></li>
                            <li><a href="{{ route('logout') }}" data-action="logout">
                                    @lang('corals-ecommerce-basic::labels.partial.logout') <i
                                            class="fa fa-sign-out fa-fw"></i></a>
                            </li>
                            @else
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-sign-in fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.login')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('register') }}">
                                        <i class="fa fa-user fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.register')
                                    </a>
                                </li>
                                @endauth
                    </ul>
                </div>
                
                <div class="inline_block ">
                        <a href="#" class="search_btn"><i class="fa fa-search"></i></a>
                </div>

                 <a class="icon_heart_wrap inline_block" href="/e-commerce/wishlist/my">
                    <i class="fa fa-heart icon_heart"></i>
                    @if($wishlist = \Shop::getwishlist())
                    {{ $wishlist }}
                    @else
                    <span>0</span>
                    @endif
                </a>
                <div class="cart bounceInRight login inline_block mobi_cart" data-wow-delay="0.4s"><a href="{{ url('cart') }}" class="mobi_cart_a"></a>
                    <i class="fa fa-shopping-bag fa-fw"></i>
                    <span class="count" id="cart-header-count">{{ count(\ShoppingCart::getItems()) }}</span>
                    <span class="subtotal" id="cart-header-total">
                        {{ \ShoppingCart::total() }}
                    </span>
                </div>
            </div>
        </div>        
    </div>
</div>

                    
                </div>    

            </div>
            <form class="mobi_search" method="get" action="{{ url('shop') }}">  
        <div class=""><input id="search_bar1" class="tags" value="{{ request()->get('search_bar') }}" type="text" name="search" placeholder="Search" list="json-datalist1">
             <datalist id="json-datalist1" ></datalist>
        </div>
    </form>
        </div>
    </div>

<!-- <div class="mobi_navbar">
    <ul class="mobi_navbar_wrapper">
        <li><a href="{{ url('/shop') }}">EYEGLASSES</a></li>
        <li><a href="{{ url('/shop?category=sunglasses') }}">SUNGLASSES</a></li>
        <li><a href="{{ url('/shop?category=clearance') }}">CLEARANCE</a></li>
    </ul>
</div> -->
</nav>

</header>
    
    <!-- <a href="#" class="mobi_search_close"><i class="fa fa-arrow-left"></i></a>  -->
    
    <div class="custom_alert_overlay"></div>
    <div class="custom_alert">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
                      <a href="#"><i class="fa fa-times cus_alert_cross_icon"></i></a>
                      <div class="cus_alert_text">

                      </div>
                      <div class="btn-group">
                          <button style="display:none; float: left;margin-right: 5px;" class="popup_button_submit non_precript">Continue</button>
                          <button class="popup_button_submit cus_alert_close change_content">Got it, thanks!</button>
                        </div>
                      
                  </div>
              </div>
          </div>      
    </div>

    <div class="mobile_slide_menu show3">
            <div class="mobo_login_wrapper">
                <div class="mobo_login">
                    <div class="vert_allign_center">
                        <a href="#" class="login_logo">
                            <img src="../assets/themes/ecommerce-basic/img/user-icon.png">
                        </a>

                        <div class="dis_inline_block">
                            <a href="{{ route('login') }}" class="mobi_login">Sign In</a>
                            <a href="{{ route('register') }}" class="mobi_account">Create Account</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile_slide_menu_overflow">
            <div class="timing_info_wrapper">
                <div class="timing_info">    
                    <div class="vert_allign_center">
                        <p class="slide_phone_num"><strong>(855) 393 2891</strong></p>
                        <p class="slide_phone_time">Mon-Fri 9am-12am EST<br>Sat 9am-6pm EST</p>
                    </div>
                </div>
            </div>

            <div class="slide_menu_dropdown">
                <div class="slide_menu_dropdown_click">
                    <a href="#" class="">Eyeglasses</a>
                    <span class="pull-right slide_menu_dropdown_icon"><i class="fa fa-angle-down"></i></span>
                <div class="slide_menu_dropdown_show">
                    <ul>
                        <li><a href="{{ url('/shop?category=women') }}">For Women</a></li>
                        <li><a href="{{ url('/shop?category=men') }}">For Men</a></li>
                        <li><a href="{{ url('/shop?category=kids') }}">For Kids</a></li>
                        <li><a href="{{ url('/shop?category=eyeglasses') }}">All Eyeglasses</a></li>
                    </ul>
                    
                <!-- <div class="slide_submenu_dropdown">
                    <div class="slide_submenu_dropdown_click">
                        <a href="#" class="">Eyeglasses By Category</a>
                        <span class="pull-right slide_submenu_dropdown_icon"><i class="fa fa-angle-down"></i></span>
                    </div>

                    <ul class="slide_submenu_dropdown_show">
                        <li><a href="">For Women</a></li>
                        <li><a href="">For Men</a></li>
                        <li><a href="">For Kids</a></li>
                        <li><a href="">All Eyeglasses</a></li>
                    </ul>
                </div> -->
                </div>    
                </div>
                
            </div>

            <div class="slide_menu_dropdown">
                <div class="slide_menu_dropdown_click">
                    <a href="#" class="">Sunglasses</a>
                    <span class="pull-right slide_menu_dropdown_icon"><i class="fa fa-angle-down"></i></span>

                    <div class="slide_menu_dropdown_show">
                    <ul>
                        <li><a href="{{ url('/shop?category=women') }}">For Women</a></li>
                        <li><a href="{{ url('/shop?category=men') }}">For Men</a></li>
                        <li><a href="{{ url('/shop?category=kids') }}">For Kids</a></li>
                        <li><a href="{{ url('/shop?') }}">All Eyeglasses</a></li>
                    </ul>
                </div>
                </div>
            </div>

            <div class="slide_menu_dropdown">
                <div class="slide_menu_dropdown_click">
                    <a href="{{ url('/shop?') }}" class="">Clearance</a>
                </div>
            </div>

                <ul class="slide_menu_social">
                    <li><a class="" href="https://twitter.com/kimarotec" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="" href="https://www.facebook.com/kimarotec" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="" href="https://plus.google.com/kimarotec" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="" href="https://instagram.com/kimarotec" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="" href="https://instagram.com/kimarotec" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;"><i class="fa fa-dribbble"></i></a></li>
                </ul>
            <div class="slide_menu_footer">
                
                <div class="slide_menu_footer_dropdown">
                    <div class="slide_menu_footer_dropdown_click">
                        <a href="#" class="">GABA Opticals</a>
                        <span class="pull-right slide_menu_footer_dropdown_icon"><i class="fa fa-angle-down"></i></span>

                        <div class="slide_menu_footer_dropdown_show">
                        <ul>
                            <li><a href="">About us</a></li>
                            <li><a href="">About Gaba Optical</a></li>
                            <li><a href="">Our Vision</a></li>
                            <li><a href="">Our Mission</a></li>
                            <li><a href="">Chairman Message</a></li>
                            <li><a href="">Our Core Team</a></li>
                        </ul>
                    </div>
                    </div>
                </div>

                <div class="slide_menu_footer_dropdown">
                    <div class="slide_menu_footer_dropdown_click">
                        <a href="#" class="">Quick links</a>
                        <span class="pull-right slide_menu_footer_dropdown_icon"><i class="fa fa-angle-down"></i></span>

                        <div class="slide_menu_footer_dropdown_show">
                        <ul>
                            <li><a href="">20% off on Lenses</a></li>
                            <li><a href="">20% off on Frames</a></li>
                            <li><a href="">Buy one get one Free</a></li>
                            <li><a href="">Starting from 2000</a></li>
                            <li><a href="">New Arrivals</a></li>
                            <li><a href="">Clearnace Sale</a></li>
                        </ul>
                    </div>
                    </div>
                </div>

                <div class="slide_menu_footer_dropdown">
                    <div class="slide_menu_footer_dropdown_click">
                        <a href="#" class="">Help Center</a>
                        <span class="pull-right slide_menu_footer_dropdown_icon"><i class="fa fa-angle-down"></i></span>

                        <div class="slide_menu_footer_dropdown_show">
                        <ul>
                            <li><a href="">FAQs</a></li>
                            <li><a href="">Order Tracking</a></li>
                            <li><a href="">Shipping & Return Policy</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Terms of Use</a></li>
                            <li><a href="">Contact us</a></li>
                        </ul>
                    </div>
                    </div>
                </div>

                <div class="slide_menu_footer_dropdown">
                    <div class="slide_menu_footer_dropdown_click">
                        <a href="#" class="">Blogs</a>
                        <span class="pull-right slide_menu_footer_dropdown_icon"><i class="fa fa-angle-down"></i></span>

                        <div class="slide_menu_footer_dropdown_show">
                        <ul>
                            <li><a href="">Blogs One</a></li>
                            <li><a href="">Blogs Two</a></li>
                            <li><a href="">Blogs Three</a></li>
                            <li><a href="">Blogs Four</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</body>
</html>