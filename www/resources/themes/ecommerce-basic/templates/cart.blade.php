@extends('layouts.master')

@section('editable_content')
    @include('partials.page_header',['content' => '<h1><i class="fa fa-shopping-cart fa-fw"></i> Cart</h1>'])
    @php \Actions::do_action('pre_content', null, null) @endphp

    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-1 mt-30">
        @if (count(ShoppingCart::getItems()) > 0)
            <div class="shopping-cart">

                <div class="col-md-8 table color-table info-table table table-hover table-striped table-condensed">
                    <!-- <thead>
                    <tr>
                        <th>@lang('corals-ecommerce-basic::labels.template.cart.product')</th>
                        <th class="text-center" width="200">@lang('corals-ecommerce-basic::labels.template.cart.quantity')</th>
                        <th class="text-center">@lang('corals-ecommerce-basic::labels.template.cart.price')</th>
                        <th class="text-center">
                            <a class="btn btn-sm btn-outline-danger" href="{{ url('cart/empty') }}"
                               title="Delete" data-action="post" data-page_action="site_reload">
                                @lang('corals-ecommerce-basic::labels.template.cart.clear_cart')
                            </a>
                        </th>
                    </tr>
                    </thead> -->
                <h1 class="cart_heading">My Cart</h1>                    
                    @foreach (\ShoppingCart::getItems() as $item)
                        <div id="item-{{$item->getHash()}}">
                            <div>
                                <div class="col-md-8 pro_wrapped">
                                    <div class="col-md-4">
                                        <a class="product-thumb vert_top_margin" href="{{ url('shop', [$item->id->product->slug]) }}">
                                            <img src="{{ $item->id->image }}" alt="SKU Image" class="mx-auto"
                                                 style="max-height: 100px;width: auto;">
                                        </a>
                                    </div>
                                    <div class="product-info col-md-8">
                                        <h4 class="product-title">
                                         @if($item->lens)         
                                        </h4>                                        
                                    
                                        <div class="float_right_cart">    
                                            <a class="remove-from-cart item-button"
                                               href="{{ url('cart/quantity', [$item->getHash()]) }}"
                                               data-action="post" data-style="zoom-in"
                                               data-request_data='@json(["action"=>"removeItem"])'
                                               data-page_action="updateCart"
                                               data-toggle="tooltip"
                                               title="Remove item">
                                                <i class="icon-cross"></i>
                                            </a>
                                        </div>                                 
                                 

                                         <ul class="addtocart_details">
                                                    <li> <p><span class="head">Frame:</span>   <a class="head_link" href="{{ url('shop', [$item->id->product->slug]) }}">
                                                  {!! $item->id->product->name !!} [{{$item->id->code }}] </a></p></li>
                                                    <li> 
                                                    @if($colors = \Shop::getcolor($item->color))
                                                        <p><span class="head">Color:</span>{{ $colors['title'] }}</p>
                                                     @endif
                                                    </li>
                                                    <li>
                                                        @if($size = \Shop::getsize($item->size))
                                                            <p><span class="head">Size:</span>{{ $size['name'] }}</p>
                                                        @endif
                                                    </li>
                                                    <li class="lense_slide_dropdown"> <p><span class="head">Lenses:</span>      
                                                 {{$item->lens}}
                                         <i class="fa fa-angle-down down_caret"></i></p></li>

                                                <div class="lense_dropdown_content">    @if(substr( $item->lens, 0, 3 )=='NON')
                                                    
                                                    <table class="table" style="font-size: 12px;">
                                                 
                                                    <tr>
                                                        <td colspan="2">Type</td>
                                                        <td colspan="2">{!! $item->typename !!}</td>
                                                    </tr>
                                                       
                                                     @if( $item->colorsun!=null)
                                                        <tr>
                                                        <td colspan="2">Option Color</td>
                                                        <td colspan="2">{!! $item->colorsun !!}</td>
                                                            </tr>
                                                        @endif
                                                </table>
                                                    
                                                    
                                                @else    
                                               
                                                    <table class="table" style="font-size: 12px;">
                                                    <tr>
                                                        <td>Left Eye Sphere</td>
                                                        <td>{!! $item->leyesphere !!}</td>
                                                        <td>Right Eye Sphere</td>
                                                        <td>{!! $item->reyesphere  !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Left Eye Cylinder</td>
                                                        <td>{!! $item->leyecylinder !!}</td>
                                                        <td>Right Eye Cylinder</td>
                                                        <td>{!! $item->reyecylinder  !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Left Eye Axis</td>
                                                        <td>{!! $item->leyeaxis !!}</td>
                                                        <td>Right Eye Axis</td>
                                                        <td>{!! $item->reyeaxis  !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Left Eye ADD</td>
                                                        <td>{!! $item->leyeadd !!}</td>
                                                        <td>Right Eye ADD</td>
                                                        <td>{!! $item->reyeadd !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Type</td>
                                                        <td colspan="2">{!! $item->typename !!}</td>
                                                    </tr>
                                                       
                                                    
                                                </table>
                                                     @endif
                                            </div>
                                                
                                                <li class="qty_dropdown"> 
                                                <p>
                                        <div class="">
                                    @if($item->id->allowed_quantity != 1)
                                       <form action="{{ url('cart/quantity', [$item->getHash()]) }}" method="POST"
                                              class="ajax-form form-inline" data-page_action="updateCart">
                                            {{ csrf_field() }}
                                            <a href="{{ url('cart/quantity', [$item->getHash()]) }}"
                                               class="btn btn-sm btn-warning item-button"
                                               title="Remove One" data-action="post" data-style="zoom-in"
                                               data-request_data='@json(["action"=>"decreaseQuantity"])'
                                               data-page_action="updateCart">
                                                <i class="fa fa-fw fa-minus"></i>
                                            </a>
                                            <input step="1" min="1" class="form-control form-control-sm cart-quantity"
                                                   style="width:60px;display: inline;text-align:center" type="number"
                                                   name="quantity"
                                                   data-id="{{ $item->rowId }}"
                                                   {!! $item->id->allowed_quantity>0?('max="'.$item->id->allowed_quantity.'"'):'' !!} value="{{ $item->qty }}"/>
                                            <a href="{{ url('cart/quantity', [$item->getHash()]) }}"
                                               class="btn btn-sm btn-success item-button" data-style="zoom-in"
                                               title="Add One" data-action="post" data-page_action="updateCart"
                                               data-request_data='@json(["action"=>"increaseQuantity"])'>
                                                <i class="fa fa-fw fa-plus"></i>
                                            </a>
                                        </form>
<!--
              
-->
                                    @else
                                        <input style="width:40px;text-align: center;"
                                               value="1"
                                               disabled/>
                                    @endif
                                </div>
<!--
                                                    <span class="head">Qty:</span>
                                                    <select class="qty_dropdown_show">
                                                     <option>1</option>
                                                     <option>2</option>
                                                     <option>3</option>
                                                     <option>4</option>
                                                     <option>5</option>
                                                     <option>6</option>
                                                    </select>   
-->
                                                </p>

                                                </li>

                                                <li class="sc-item-totals">
                                                      <p><strong>Price</strong> <span class="float_right" id="item-total-{{$item->getHash()}}">  {{ \Payments::currency($item->qty * $item->price) }}</span></p>
                                                </li>

                                                <a class="btn add-to-cart btn-sm btn-secondary ladda-button edit_lence_btn" href="{{ url('configure', [$item->id->product->slug]) }}" type="submit" data-style="expand-right"><span class="ladda-label">Edit Lences</span><span class="ladda-spinner"></span></a>

                                            </ul>
                                            
                                         @else

                                           
                                            <div class="float_right_cart">    
                                            <a class="remove-from-cart item-button"
                                               href="{{ url('cart/quantity', [$item->getHash()]) }}"
                                               data-action="post" data-style="zoom-in"
                                               data-request_data='@json(["action"=>"removeItem"])'
                                               data-page_action="updateCart"
                                               data-toggle="tooltip"
                                               title="Remove item">
                                                <i class="icon-cross"></i>
                                            </a>
                                        </div>                                 
                                        

                                         <ul class="addtocart_details">
                                                    <li> <p><span class="head">Frame:</span>  <a class="head_link" href="{{ url('shop', [$item->id->product->slug]) }}">
                                                  {!! $item->id->product->name !!} [{{$item->id->code }}] </a></p></li>
                                                    <li>                                                         
                                                    @if($colors = \Shop::getcolor($item->color))
                                                        <p><span class="head">Color:</span>{{ $colors['title'] }}</p>
                                                     @endif
                                                    </li>
                                                    <li>
                                                        @if($size = \Shop::getsize($item->size))
                                                            <p><span class="head">Size:</span>{{ $size['name'] }}</p>
                                                        @endif
                                                    </li>
                                                                                                    
                                                <li class="qty_dropdown"> 
                                                <p>
                                                     <div class="">
                                    @if($item->id->allowed_quantity != 1)
                                       <form action="{{ url('cart/quantity', [$item->getHash()]) }}" method="POST"
                                              class="ajax-form form-inline" data-page_action="updateCart">
                                            {{ csrf_field() }}
                                            <a href="{{ url('cart/quantity', [$item->getHash()]) }}"
                                               class="btn btn-sm btn-warning item-button"
                                               title="Remove One" data-action="post" data-style="zoom-in"
                                               data-request_data='@json(["action"=>"decreaseQuantity"])'
                                               data-page_action="updateCart">
                                                <i class="fa fa-fw fa-minus"></i>
                                            </a>
                                            <input step="1" min="1" class="form-control form-control-sm cart-quantity"
                                                   style="width:60px;display: inline;text-align:center" type="number"
                                                   name="quantity"
                                                   data-id="{{ $item->rowId }}"
                                                   {!! $item->id->allowed_quantity>0?('max="'.$item->id->allowed_quantity.'"'):'' !!} value="{{ $item->qty }}"/>
                                            <a href="{{ url('cart/quantity', [$item->getHash()]) }}"
                                               class="btn btn-sm btn-success item-button" data-style="zoom-in"
                                               title="Add One" data-action="post" data-page_action="updateCart"
                                               data-request_data='@json(["action"=>"increaseQuantity"])'>
                                                <i class="fa fa-fw fa-plus"></i>
                                            </a>
                                        </form>
<!--
              
-->
                                    @else
                                        <input style="width:40px;text-align: center;"
                                               value="1"
                                               disabled/>
                                    @endif
                                </div>
<!--
                                                    <select class="qty_dropdown_show">
                                                     <option>1</option>
                                                     <option>2</option>
                                                     <option>3</option>
                                                     <option>4</option>
                                                     <option>5</option>
                                                     <option>6</option>
                                                    </select>   
-->
                                                </p>

                                                </li>

                                                <li class="sc-item-totals">
                                                    <p><strong>Price</strong> <span class="float_right" id="item-total-{{$item->getHash()}}">  {{ \Payments::currency($item->qty * $item->price) }}</span></p>
                                                </li>

                                                

                                            </ul>
                                            
                                         @endif
<!--                                              @if($colors = \Shop::getcolor($item->color))
                                                <p class="title_wrap">{{ $colors['title'] }}</p>
                               <img title="{{ $colors['title'] }}" src = "{{ $colors['thumbnail'] }}" class="img-responsive img-rounded color_select_btn" style ="max-height: 20px;width:auto" alt = "Thumbnail"  />                     
                        @endif
                         @if($size = \Shop::getsize($item->size))
                               <p class="size_wrap">{{ $size['name'] }}</p>  -->
                        @endif
                                        
                                        {!!  $item->id->presenter()['options']  !!}
                                        {!! formatArrayAsLabels(\OrderManager::mapSelectedAttributes($item->product_options), 'success',null,true)    !!}
                                    
                                </div>
                            </div>

                            
                            <!-- <td class="text-center text-lg text-medium" id="item-total-{{$item->getHash()}}">
                                {{ \Payments::currency($item->qty * $item->price) }}
                            </td> -->
                        </div>
                        </div>
                    @endforeach

                    <div class="absolue_container pro_wrapped">
                            <ul class="addtocart_details">
                                <li> <p><span class="head">Items:</span>{{ count(\ShoppingCart::getItems()) }}</p></li>
                                <li> <p><span class="head" >Subtotal:</span id="total">{{ ShoppingCart::subTotal() }}</p></li>
                                <li class="dot_line"></li>
<!--                                <li> <p><span class="head">Order Total:</span>{{ ShoppingCart::Total() }}</p></li>-->

                            </ul>
                            <p>Shipping and tax not included</p>

                            <a class="btn add-to-cart btn-sm btn-secondary ladda-button edit_lence_btn" type="submit" data-style="expand-right" href="{{ url('checkout') }}"><span class="ladda-label">@lang('corals-ecommerce-basic::labels.template.cart.checkout')</span><span class="ladda-spinner"></span></a>
                            <a class="shop_btn" href="{{ url('shop') }}">@lang('corals-ecommerce-basic::labels.template.cart.back_shopping')</a>
                            <h5 class="gaba_cod_heading">Supported Payment Methods</h5>
                            
                            <img class="gaba_cod_logo" src="../assets/themes/ecommerce-basic/img/gaba_cod_logo4.png"> 
                    </div>
                    
                </div>
            </div>
            <div class="shopping-cart-footer">
                <div class="column">
                    <div class="column"><a class="btn btn-outline-secondary"></a>
                    </div>
                </div>
                
            </div>
            {{--<div class="shopping-cart-footer">--}}
            {{--</div>--}}
        @else
            <div class="text-center">
                <h3>@lang('corals-ecommerce-basic::labels.template.cart.have_no_item')</h3>
                <a href="{{ url('shop') }}" class="btn btn-primary btn">
                    <i class="fa fa-shopping-cart fa-fw"></i>@lang('corals-ecommerce-basic::labels.template.cart.continue_shopping')
                </a>
            </div>
        @endif

        @include('partials.featured_products',['title'=>trans('corals-ecommerce-basic::labels.template.cart.title')])
    </div>
@stop

@section('js')
    @parent
    @include('Ecommerce::cart.cart_script')

@endsection
