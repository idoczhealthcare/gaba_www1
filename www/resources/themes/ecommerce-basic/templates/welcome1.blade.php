<?php 
session_start();
$_SESSION['a']="hi";

?>
<!DOCTYPE html>
<html>
<head>
    <style>
    
body, html {
    height: 100%;
}


.main_wrapper{
	width: 100%;
	background-image: url('../assets/themes/ecommerce-basic/img/background.jpg');
    height: 100%; 
    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

.hori_center{
	height: 100%;
	display: table;
	/*margin: 0 auto;*/
}
.vert_center{
	display: table-cell;
	vertical-align: middle;
}

.float_right{
	float: right;
}
.margin_right{
	margin-right: 180px;
}
.align_center{
	width: 300px;
	display: block;
    margin: 0 auto;
    text-align: center;
}

.margin_bottom{
	margin-bottom: 20px;
}


.custom_dropdown{
	width: 350px;
	height: 50px;
	text-align: left;
	border: 1px solid #000;
    border-radius: 12px; 
    outline: none;
    border-radius: 10px !important;
}
.custom_dropdown:hover,.custom_dropdown:active,.custom_dropdown:focus,.custom_dropdown:visited{
    background-color: #fff !important;
    /*border-color: #adadad;*/
}

.custom_dropdown:hover,.custom_dropdown:active,.custom_dropdown:visited,.custom_dropdown:focus{
	background-color: #fff;
	border: 1px solid #000;
}

.custom_dropdown_menu{
	width: 350px;
	overflow: hidden;
	border-radius: 10px !important;
}

.custom_dropdown_menu li:hover {
	background: #000;
}
.custom_dropdown_menu>li>a{
	padding: 10px !important;

}
.custom_dropdown_menu>li>a:hover{
	background-color: transparent !important;
	color: #fff !important;
}
.dropdown .custom_caret{
	float: right;
	margin-right: 10px;
}

.custom_btn_enter{
/*	margin-left: 40px;*/
	background-color: #000 !important;
	width: 100%;
	height: 40px;
	color: #fff !important;
    padding: 8px !important;
}
.custom_btn_enter:hover{
	background-color: #fff !important;
	color: #000 !important;
}
.dropdown-menu{
	padding: 0 0 0 0 !important;
    margin: 12px 0 0 !important;
}

@media (max-width: 768px) {
	
	.margin_right{
		margin-right: 0 !important;
	}
	.hori_center{
		margin: 0 auto !important;
	}
	.float_right{
		float: none;
	}
	.custom_dropdown{
		width: 280px;
	}
    .custom_dropdown_menu{
		width: 280px;
	}
}

    
    </style>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


     

<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<script type="text/javascript" src="../assets/themes/ecommerce-basic/js/bootstrap-select.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
		<div class="main_wrapper">
			<div class="hori_center float_right margin_right">
				<div class="vert_center">
					<div class="wrap_content">	
						<img src="">
						<div class="dropdown margin_bottom">
						  <button class="btn btn-default dropdown-toggle custom_dropdown" type="button" data-toggle="dropdown"><span class="setValue"></span>
						  <span class="custom_caret"><i class="fas fa-chevron-down"></i></span></button>
<!--
						  <ul class="dropdown-menu custom_dropdown_menu">
						  	<li class="select_option"><a href="#">Select Your Country</a></li>
						    <li class="select_option"><a href="#">USA</a></li>
						    <li class="select_option selecte"><a href="#">Pakistan</a></li>
						  
						  </ul>
-->
                            
                             <ul class="dropdown-menu custom_dropdown_menu">
                                 	<li class="select_option"><a href="#">Select Your Country</a></li>
                                    @php \Actions::do_action('post_display_frontend_menu') @endphp
                            </ul>
						</div>
						<a href="#" onclick="fun()" class="btn btn-default btn-lg margin_bottom custom_btn_enter">ENTER</a>
					</div>	
				</div>
			</div>
		</div>
</body>
    <script>
        var url= ""
      
        function fun(){
       // alert(url);
            var ret = url.replace('country','');
           // alert(ret);
        window.location.href = ret;
    };

    $(document).ready(function () {

    	var $defaultValue = "Pakistan";
        url=document.getElementById($defaultValue).innerHTML;
    	// alert($defaultValue);
    	$(".custom_dropdown .setValue").text($defaultValue);   
    	$(".select_option").click(function () {
    		var getValue = $(this).children("a").text();
           url=document.getElementById(getValue).innerHTML;
    		$(".custom_dropdown .setValue").text(getValue);
    	});
    });
    
    </script>
</html>