-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 12:11 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestead`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(11) DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'created', 2, 'Corals\\User\\Models\\User', NULL, NULL, '{\"attributes\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"}}', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(2, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-16 01:02:52', '2018-09-16 01:02:52'),
(3, 'default', 'created', 1, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(4, 'default', 'created', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(5, 'default', 'created', 3, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(6, 'default', 'created', 4, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(7, 'default', 'created', 5, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(8, 'default', 'created', 6, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(9, 'default', 'created', 7, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(10, 'default', 'created', 8, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(11, 'default', 'created', 9, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(12, 'default', 'created', 10, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(13, 'default', 'created', 11, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(14, 'default', 'created', 12, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(15, 'default', 'created', 13, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(16, 'default', 'created', 14, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(17, 'default', 'created', 15, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(18, 'default', 'updated', 5, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(19, 'default', 'updated', 5, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:28', '2018-09-16 01:03:28'),
(20, 'default', 'updated', 1, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(21, 'default', 'updated', 1, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:03:39', '2018-09-16 01:03:39'),
(22, 'default', 'updated', 3, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:01', '2018-09-16 01:04:01'),
(23, 'default', 'updated', 3, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:02', '2018-09-16 01:04:02'),
(24, 'default', 'updated', 4, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:06', '2018-09-16 01:04:06'),
(25, 'default', 'updated', 4, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:07', '2018-09-16 01:04:07'),
(26, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:21', '2018-09-16 01:04:21'),
(27, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:21', '2018-09-16 01:04:21'),
(28, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:21', '2018-09-16 01:04:21'),
(29, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:21', '2018-09-16 01:04:21'),
(30, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:04:21', '2018-09-16 01:04:21'),
(31, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:46', '2018-09-16 01:04:46'),
(32, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:46', '2018-09-16 01:04:46'),
(33, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:46', '2018-09-16 01:04:46'),
(34, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:04:46', '2018-09-16 01:04:46'),
(35, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:04:46', '2018-09-16 01:04:46'),
(36, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:26', '2018-09-16 01:05:26'),
(37, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:26', '2018-09-16 01:05:26'),
(38, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:27', '2018-09-16 01:05:27'),
(39, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:27', '2018-09-16 01:05:27'),
(40, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:05:27', '2018-09-16 01:05:27'),
(41, 'default', 'updated', 14, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:31', '2018-09-16 01:05:31'),
(42, 'default', 'updated', 35, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":{\"PayPal_Rest\":\"PayPal\"}},\"old\":{\"value\":[]}}', '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(43, 'default', 'updated', 14, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(44, 'default', 'updated', 15, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:36', '2018-09-16 01:05:36'),
(45, 'default', 'updated', 35, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":{\"PayPal_Rest\":\"PayPal\",\"Stripe\":\"Stripe\"}},\"old\":{\"value\":{\"PayPal_Rest\":\"PayPal\"}}}', '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(46, 'default', 'updated', 15, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(47, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:47', '2018-09-16 01:05:47'),
(48, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:47', '2018-09-16 01:05:47'),
(49, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:47', '2018-09-16 01:05:47'),
(50, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:48', '2018-09-16 01:05:48'),
(51, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:05:48', '2018-09-16 01:05:48'),
(52, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:57', '2018-09-16 01:05:57'),
(53, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:57', '2018-09-16 01:05:57'),
(54, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:57', '2018-09-16 01:05:57'),
(55, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:05:58', '2018-09-16 01:05:58'),
(56, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:05:58', '2018-09-16 01:05:58'),
(57, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:29', '2018-09-16 01:06:29'),
(58, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:29', '2018-09-16 01:06:29'),
(59, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:29', '2018-09-16 01:06:29'),
(60, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:29', '2018-09-16 01:06:29'),
(61, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:06:29', '2018-09-16 01:06:29'),
(62, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:49', '2018-09-16 01:06:49'),
(63, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:49', '2018-09-16 01:06:49'),
(64, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:49', '2018-09-16 01:06:49'),
(65, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:06:50', '2018-09-16 01:06:50'),
(66, 'exception', 'installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br/>Module has been uninstalled successfully<br/>installing module failed: Me...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"install\",\"object\":\"Corals\\\\Settings\\\\Models\\\\Module\",\"message\":\"installing module failed: Method signedInteger does not exist.. An error occurred during installation!<br\\/>Module has been uninstalled successfully<br\\/>installing module failed: Method signedInteger does not exist.\"}}', '2018-09-16 01:06:50', '2018-09-16 01:06:50'),
(67, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-16 01:12:19', '2018-09-16 01:12:19'),
(68, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:12:36', '2018-09-16 01:12:36'),
(69, 'default', 'updated', 2, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-16 01:12:39', '2018-09-16 01:12:39'),
(70, 'default', 'Corals Member logged In', NULL, NULL, 2, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-16 01:56:52', '2018-09-16 01:56:52'),
(71, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cate\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `cate...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cate\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `cate`, `description`, `shipping`, `external_url`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Glasses, Glasses, Lenses, simple, , active, 0, , {\\\"width\\\":null,\\\"height\\\":null,\\\"length\\\":null,\\\"weight\\\":null,\\\"enabled\\\":0}, , 1, 1, 2018-09-16 07:00:53, 2018-09-16 07:00:53)). \"}}', '2018-09-16 02:00:53', '2018-09-16 02:00:53'),
(72, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Glasses\",\"description\":null,\"caption\":\"Glasses\",\"properties\":null}}', '2018-09-16 02:03:04', '2018-09-16 02:03:04'),
(73, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"250.00\"}}', '2018-09-16 02:03:04', '2018-09-16 02:03:04'),
(74, 'default', 'updated', 2, 'Corals\\User\\Models\\User', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"},\"old\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"}}', '2018-09-16 02:04:51', '2018-09-16 02:04:51'),
(75, 'default', 'Corals Member logged Out', NULL, NULL, 2, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-16 02:04:51', '2018-09-16 02:04:51'),
(76, 'default', 'updated', 17, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"corals-ecommerce-basic\"},\"old\":{\"value\":\"corals-basic\"}}', '2018-09-16 02:05:23', '2018-09-16 02:05:23'),
(77, 'default', 'created', 1, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Home\"}}', '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(78, 'default', 'created', 2, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"About Us\"}}', '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(79, 'default', 'created', 3, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Blog\"}}', '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(80, 'default', 'created', 4, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Pricing\"}}', '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(81, 'default', 'created', 5, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Contact Us\"}}', '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(82, 'default', 'created', 6, 'Corals\\Modules\\CMS\\Models\\Post', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Subscription Commerce Trends for 2018\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(83, 'default', 'created', 7, 'Corals\\Modules\\CMS\\Models\\Post', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Using Machine Learning to Optimize Subscription Billing\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(84, 'default', 'created', 8, 'Corals\\Modules\\CMS\\Models\\Post', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Why You Need A Blog Subscription Landing Page\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(85, 'default', 'created', 1, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Computers\",\"slug\":\"computers\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(86, 'default', 'created', 2, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Smartphone\",\"slug\":\"smartphone\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(87, 'default', 'created', 3, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gadgets\",\"slug\":\"gadgets\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(88, 'default', 'created', 4, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Technology\",\"slug\":\"technology\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(89, 'default', 'created', 5, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Engineer\",\"slug\":\"engineer\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(90, 'default', 'created', 6, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Subscriptions\",\"slug\":\"subscriptions\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(91, 'default', 'created', 7, 'Corals\\Modules\\CMS\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Billing\",\"slug\":\"billing\"}}', '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(92, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Shipping', 1, 'Corals\\User\\Models\\User', '{\"attributes\":[]}', '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(93, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Shipping', 1, 'Corals\\User\\Models\\User', '{\"attributes\":[]}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(94, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Coupon', 1, 'Corals\\User\\Models\\User', '{\"attributes\":[]}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(95, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Coupon', 1, 'Corals\\User\\Models\\User', '{\"attributes\":[]}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(96, 'default', 'created', 54, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Corals\"}}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(97, 'default', 'created', 55, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Corals\"}}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(98, 'default', 'created', 56, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"5543 Aliquet St.\"}}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(99, 'default', 'created', 57, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Fort Dodge\"}}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(100, 'default', 'created', 58, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"GA\"}}', '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(101, 'default', 'created', 59, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"20783\"}}', '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(102, 'default', 'created', 60, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"USA\"}}', '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(103, 'default', 'created', 61, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"(717) 450-4729\"}}', '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(104, 'default', 'created', 62, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"support@corals.io\"}}', '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(105, 'default', 'created', 63, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"oz\"}}', '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(106, 'default', 'created', 64, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"in\"}}', '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(107, 'default', 'created', 65, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":null}}', '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(108, 'default', 'created', 66, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"shippo_test_b3aab6f5d5ee5fb9e981906a449d74fe2e7bf9eb\"}}', '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(109, 'default', 'created', 67, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"true\"}}', '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(110, 'default', 'created', 68, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"true\"}}', '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(111, 'default', 'created', 69, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"true\"}}', '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(112, 'default', 'created', 70, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"true\"}}', '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(113, 'default', 'created', 71, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"15\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(114, 'default', 'created', 72, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"3\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(115, 'default', 'created', 73, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"1.5\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(116, 'default', 'created', 74, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"true\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(117, 'default', 'created', 3, 'Corals\\Modules\\Slider\\Models\\Slider', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-commerce Home Page Slider\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(118, 'default', 'created', 7, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-First Slide\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(119, 'default', 'created', 8, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Second Slide\"}}', '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(120, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-16 09:51:44', '2018-09-16 09:51:44'),
(121, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-17 00:02:59', '2018-09-17 00:02:59'),
(122, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_by\' in \'field list\' (SQL: insert into `ecommerce_lense_number` (`sphere_min`, `sphere_max`, `cylinder_min`, `cylinde...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_by\' in \'field list\' (SQL: insert into `ecommerce_lense_number` (`sphere_min`, `sphere_max`, `cylinder_min`, `cylinder_max`, `price`, `mapping_id`, `type_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (3.00, 4.00, 5.00, 6.00, 12345, 2, 3, 1, 1, 2018-09-17 08:23:17, 2018-09-17 08:23:17)). \"}}', '2018-09-17 03:23:17', '2018-09-17 03:23:17'),
(123, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_by\' in \'field list\' (SQL: insert into `ecommerce_lense_number` (`sphere_min`, `sphere_max`, `cylinder_min`, `cylinde...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_by\' in \'field list\' (SQL: insert into `ecommerce_lense_number` (`sphere_min`, `sphere_max`, `cylinder_min`, `cylinder_max`, `price`, `mapping_id`, `type_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (3.00, 4.00, 5.00, 6.00, 12345, 2, 5, 1, 1, 2018-09-17 08:23:57, 2018-09-17 08:23:57)). \"}}', '2018-09-17 03:23:57', '2018-09-17 03:23:57'),
(124, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_by\' in \'field list\' (SQL: insert into `ecommerce_lense_number` (`sphere_min`, `sphere_max`, `cylinder_min`, `cylinde...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_by\' in \'field list\' (SQL: insert into `ecommerce_lense_number` (`sphere_min`, `sphere_max`, `cylinder_min`, `cylinder_max`, `price`, `mapping_id`, `type_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (3.00, 4.00, 5.00, 6.00, 12345, 4, 3, 1, 1, 2018-09-17 08:24:27, 2018-09-17 08:24:27)). \"}}', '2018-09-17 03:24:27', '2018-09-17 03:24:27'),
(125, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-17 03:28:37', '2018-09-17 03:28:37'),
(126, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-17 04:19:25', '2018-09-17 04:19:25'),
(127, 'exception', 'Undefined variable: lensNumber. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"Undefined variable: lensNumber. \"}}', '2018-09-17 04:32:14', '2018-09-17 04:32:14'),
(128, 'exception', 'Undefined variable: lensNumber. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"Undefined variable: lensNumber. \"}}', '2018-09-17 04:32:35', '2018-09-17 04:32:35'),
(129, 'exception', 'Undefined variable: lensNumber. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"Undefined variable: lensNumber. \"}}', '2018-09-17 04:33:00', '2018-09-17 04:33:00'),
(130, 'exception', 'Undefined variable: lensNumber. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"Undefined variable: lensNumber. \"}}', '2018-09-17 04:33:14', '2018-09-17 04:33:14'),
(131, 'default', 'deleted', 22, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-17 04:33:56', '2018-09-17 04:33:56'),
(132, 'default', 'deleted', 21, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-17 04:34:04', '2018-09-17 04:34:04'),
(133, 'exception', 'Undefined variable: lensNumber. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\LensNumber\",\"message\":\"Undefined variable: lensNumber. \"}}', '2018-09-17 04:35:05', '2018-09-17 04:35:05'),
(134, 'default', 'updated', 20, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-17 04:35:46', '2018-09-17 04:35:46'),
(135, 'default', 'updated', 20, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-17 04:36:26', '2018-09-17 04:36:26'),
(136, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-17 23:48:11', '2018-09-17 23:48:11'),
(137, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-19 07:01:10', '2018-09-19 07:01:10'),
(138, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-20 11:22:43', '2018-09-20 11:22:43'),
(139, 'default', 'updated', 100, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-20 11:56:40', '2018-09-20 11:56:40'),
(140, 'default', 'updated', 100, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-20 11:56:56', '2018-09-20 11:56:56'),
(141, 'default', 'created', 101, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-20 12:04:59', '2018-09-20 12:04:59'),
(142, 'default', 'deleted', 101, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-20 12:05:08', '2018-09-20 12:05:08'),
(143, 'default', 'updated', 90, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-20 12:18:15', '2018-09-20 12:18:15'),
(144, 'default', 'updated', 90, 'Corals\\Modules\\Ecommerce\\Models\\LensNumber', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-20 12:18:55', '2018-09-20 12:18:55'),
(145, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-23 00:58:08', '2018-09-23 00:58:08'),
(146, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-23 02:08:46', '2018-09-23 02:08:46'),
(147, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Glasses\",\"slug\":\"glasses\"}}', '2018-09-23 02:38:37', '2018-09-23 02:38:37'),
(148, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Glasses\",\"description\":null,\"caption\":\"Glasses\",\"properties\":[]},\"old\":{\"name\":\"Glasses\",\"description\":null,\"caption\":\"Glasses\",\"properties\":null}}', '2018-09-23 02:44:42', '2018-09-23 02:44:42'),
(149, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Sunglasses\",\"slug\":\"sunglasses\"}}', '2018-09-23 02:58:10', '2018-09-23 02:58:10'),
(150, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-23 04:33:29', '2018-09-23 04:33:29'),
(151, 'exception', 'No query results for model [Menu].. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"index\",\"object\":\"Corals\\\\Menu\\\\Models\\\\Menu\",\"message\":\"No query results for model [Menu].. \"}}', '2018-09-23 05:07:58', '2018-09-23 05:07:58'),
(152, 'exception', 'No query results for model [Menu].. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"index\",\"object\":\"Corals\\\\Menu\\\\Models\\\\Menu\",\"message\":\"No query results for model [Menu].. \"}}', '2018-09-23 05:08:14', '2018-09-23 05:08:14'),
(153, 'exception', 'No query results for model [Menu].. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"index\",\"object\":\"Corals\\\\Menu\\\\Models\\\\Menu\",\"message\":\"No query results for model [Menu].. \"}}', '2018-09-23 05:26:35', '2018-09-23 05:26:35'),
(154, 'exception', 'No query results for model [Menu].. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"index\",\"object\":\"Corals\\\\Menu\\\\Models\\\\Menu\",\"message\":\"No query results for model [Menu].. \"}}', '2018-09-23 05:26:47', '2018-09-23 05:26:47'),
(155, 'exception', 'No query results for model [Menu].. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"index\",\"object\":\"Corals\\\\Menu\\\\Models\\\\Menu\",\"message\":\"No query results for model [Menu].. \"}}', '2018-09-23 05:27:20', '2018-09-23 05:27:20'),
(156, 'default', 'updated', 9, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/1537698596-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/site_logo.png\"}}', '2018-09-23 05:29:57', '2018-09-23 05:29:57'),
(157, 'default', 'updated', 11, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/1537698625-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/site_favicon.png\"}}', '2018-09-23 05:30:25', '2018-09-23 05:30:25'),
(158, 'default', 'updated', 11, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/1537698654-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/1537698625-logo.jpg\"}}', '2018-09-23 05:30:55', '2018-09-23 05:30:55'),
(159, 'default', 'updated', 10, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/1537698684-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/127.0.0.1:8000\\/uploads\\/settings\\/site_logo_white.png\"}}', '2018-09-23 05:31:25', '2018-09-23 05:31:25'),
(160, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"150.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"250.00\"}}', '2018-09-23 05:55:23', '2018-09-23 05:55:23'),
(161, 'default', 'updated', 54, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Gaba Optics\"},\"old\":{\"value\":\"Corals\"}}', '2018-09-23 06:02:13', '2018-09-23 06:02:13'),
(162, 'default', 'updated', 55, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"GABA\"},\"old\":{\"value\":\"Corals\"}}', '2018-09-23 06:02:13', '2018-09-23 06:02:13'),
(163, 'default', 'updated', 56, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"\\\\7\"},\"old\":{\"value\":\"5543 Aliquet St.\"}}', '2018-09-23 06:02:13', '2018-09-23 06:02:13'),
(164, 'default', 'updated', 14, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"atheriqbal1@gmail.com\"},\"old\":{\"value\":\"support@corals.io\"}}', '2018-09-23 06:03:26', '2018-09-23 06:03:26'),
(165, 'default', 'updated', 7, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"\\u00a9 2018 <a target=\\\"_blank\\\" href=\\\"#\\\"\\r\\n                               title=GABA\\\">GABA<\\/a>.\\r\\n                All Rights Reserved.\"},\"old\":{\"value\":\"&copy; 2018 <a target=\\\"_blank\\\" href=\\\"http:\\/\\/corals.io\\/\\\"\\r\\n                               title=\\\"Corals.io\\\">Corals.io<\\/a>.\\r\\n                All Rights Reserved.\"}}', '2018-09-23 06:04:25', '2018-09-23 06:04:25'),
(166, 'default', 'updated', 1, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"GABA Optics\"},\"old\":{\"value\":\"Corals\"}}', '2018-09-23 06:05:05', '2018-09-23 06:05:05'),
(167, 'default', 'updated', 6, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"GABA Optics\"},\"old\":{\"value\":\"corals_laraship\"}}', '2018-09-23 06:05:39', '2018-09-23 06:05:39'),
(168, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-23 06:20:22', '2018-09-23 06:20:22'),
(169, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"150.00\"}}', '2018-09-23 06:21:39', '2018-09-23 06:21:39'),
(170, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":null}}', '2018-09-23 06:22:43', '2018-09-23 06:22:43'),
(171, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Color\",\"slug\":\"color\"}}', '2018-09-23 06:32:12', '2018-09-23 06:32:12'),
(172, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Black\",\"slug\":\"black\"}}', '2018-09-23 06:32:49', '2018-09-23 06:32:49'),
(173, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Blue\",\"slug\":\"blue\"}}', '2018-09-23 06:33:19', '2018-09-23 06:33:19'),
(174, 'default', 'updated', 2, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Second Slider\"},\"old\":{\"name\":\"Second Slider\"}}', '2018-09-23 07:10:11', '2018-09-23 07:10:11'),
(175, 'default', 'updated', 1, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"First Slider\"},\"old\":{\"name\":\"First Slider\"}}', '2018-09-23 07:10:41', '2018-09-23 07:10:41'),
(176, 'default', 'updated', 2, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Second Slider\"},\"old\":{\"name\":\"Second Slider\"}}', '2018-09-23 07:11:06', '2018-09-23 07:11:06'),
(177, 'default', 'created', 9, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Third Slider\"}}', '2018-09-23 07:11:36', '2018-09-23 07:11:36'),
(178, 'default', 'updated', 9, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Third Slider\"},\"old\":{\"name\":\"Third Slider\"}}', '2018-09-23 07:11:36', '2018-09-23 07:11:36'),
(179, 'default', 'updated', 7, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-First Slide\"},\"old\":{\"name\":\"E-First Slide\"}}', '2018-09-23 07:14:48', '2018-09-23 07:14:48'),
(180, 'default', 'updated', 8, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Second Slide\"},\"old\":{\"name\":\"E-Second Slide\"}}', '2018-09-23 07:15:24', '2018-09-23 07:15:24'),
(181, 'default', 'created', 10, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Third Slide\"}}', '2018-09-23 07:16:05', '2018-09-23 07:16:05'),
(182, 'default', 'updated', 10, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Third Slide\"},\"old\":{\"name\":\"E-Third Slide\"}}', '2018-09-23 07:16:05', '2018-09-23 07:16:05'),
(183, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cate\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `is_f...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cate\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `is_featured`, `cate`, `description`, `shipping`, `external_url`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Test, test, test, simple, , active, 1, 0, , {\\\"width\\\":null,\\\"height\\\":null,\\\"length\\\":null,\\\"weight\\\":null,\\\"enabled\\\":0}, , 1, 1, 2018-09-23 12:33:52, 2018-09-23 12:33:52)). \"}}', '2018-09-23 07:33:52', '2018-09-23 07:33:52'),
(184, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Test\",\"description\":null,\"caption\":\"Test\",\"properties\":null}}', '2018-09-23 07:37:12', '2018-09-23 07:37:12'),
(185, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"500.00\",\"sale_price\":\"0.00\"}}', '2018-09-23 07:37:12', '2018-09-23 07:37:12'),
(186, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Test\",\"description\":null,\"caption\":\"Test\",\"properties\":[]},\"old\":{\"name\":\"Test\",\"description\":null,\"caption\":\"Test\",\"properties\":null}}', '2018-09-23 07:37:39', '2018-09-23 07:37:39'),
(187, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Glasses\",\"description\":null,\"caption\":\"frame\",\"properties\":[]},\"old\":{\"name\":\"Glasses\",\"description\":null,\"caption\":\"Glasses\",\"properties\":[]}}', '2018-09-23 08:20:42', '2018-09-23 08:20:42'),
(188, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-09-23 08:38:25', '2018-09-23 08:38:25'),
(189, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-23 08:38:25', '2018-09-23 08:38:25'),
(190, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-09-23 08:38:36', '2018-09-23 08:38:36'),
(191, 'default', 'created', 9, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Faq\"}}', '2018-09-23 10:08:24', '2018-09-23 10:08:24'),
(192, 'default', 'updated', 9, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Faq\"},\"old\":{\"title\":\"Faq\"}}', '2018-09-23 10:09:53', '2018-09-23 10:09:53'),
(193, 'default', 'updated', 9, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Faq\"},\"old\":{\"title\":\"Faq\"}}', '2018-09-23 10:12:14', '2018-09-23 10:12:14'),
(194, 'default', 'updated', 9, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Faq\"},\"old\":{\"title\":\"Faq\"}}', '2018-09-23 10:15:23', '2018-09-23 10:15:23'),
(195, 'default', 'updated', 9, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Faq\"},\"old\":{\"title\":\"Faq\"}}', '2018-09-23 10:16:37', '2018-09-23 10:16:37'),
(196, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.53\"}', '2018-09-24 12:25:10', '2018-09-24 12:25:10'),
(197, 'default', 'updated', 54, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Corals\"},\"old\":{\"value\":\"Gaba Optics\"}}', '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(198, 'default', 'updated', 55, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Corals\"},\"old\":{\"value\":\"GABA\"}}', '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(199, 'default', 'updated', 56, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"5543 Aliquet St.\"},\"old\":{\"value\":\"\\\\7\"}}', '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(200, 'default', 'updated', 7, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-First Slide\"},\"old\":{\"name\":\"E-First Slide\"}}', '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(201, 'default', 'updated', 8, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Second Slide\"},\"old\":{\"name\":\"E-Second Slide\"}}', '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(202, 'default', 'updated', 7, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-First Slide\"},\"old\":{\"name\":\"E-First Slide\"}}', '2018-09-24 12:38:04', '2018-09-24 12:38:04'),
(203, 'default', 'updated', 8, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Second Slide\"},\"old\":{\"name\":\"E-Second Slide\"}}', '2018-09-24 12:38:19', '2018-09-24 12:38:19'),
(204, 'default', 'updated', 10, 'Corals\\Modules\\Slider\\Models\\Slide', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"E-Third Slide\"},\"old\":{\"name\":\"E-Third Slide\"}}', '2018-09-24 12:38:33', '2018-09-24 12:38:33'),
(205, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.150\"}', '2018-09-24 13:51:17', '2018-09-24 13:51:17'),
(206, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.53\"}', '2018-09-24 13:53:07', '2018-09-24 13:53:07'),
(207, 'default', 'updated', 9, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537772029-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537698596-logo.jpg\"}}', '2018-09-24 13:53:49', '2018-09-24 13:53:49'),
(208, 'default', 'updated', 11, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537772048-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537698654-logo.jpg\"}}', '2018-09-24 13:54:08', '2018-09-24 13:54:08'),
(209, 'default', 'updated', 10, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537772085-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537698684-logo.jpg\"}}', '2018-09-24 13:54:45', '2018-09-24 13:54:45'),
(210, 'default', 'updated', 9, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537772492-logo.jpg\"},\"old\":{\"value\":\"http:\\/\\/i-doczportal.com\\/uploads\\/settings\\/1537772029-logo.jpg\"}}', '2018-09-24 14:01:32', '2018-09-24 14:01:32'),
(211, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.53\"}', '2018-09-24 16:49:10', '2018-09-24 16:49:10'),
(212, 'default', 'deleted', 4, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Color\",\"slug\":\"color\"}}', '2018-09-24 16:50:03', '2018-09-24 16:50:03'),
(213, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Category\",\"slug\":\"category\"}}', '2018-09-24 16:50:28', '2018-09-24 16:50:28'),
(214, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Sunglasses\",\"slug\":\"sunglasses\"},\"old\":{\"name\":\"Sunglasses\",\"slug\":\"sunglasses\"}}', '2018-09-24 16:58:00', '2018-09-24 16:58:00');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(215, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Glasses\",\"slug\":\"glasses\"},\"old\":{\"name\":\"Glasses\",\"slug\":\"glasses\"}}', '2018-09-24 16:58:14', '2018-09-24 16:58:14'),
(216, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Uncategorized\",\"slug\":\"uncategorized\"},\"old\":{\"name\":\"Uncategorized\",\"slug\":\"uncategorized\"}}', '2018-09-24 16:58:24', '2018-09-24 16:58:24'),
(217, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gender\",\"slug\":\"gender\"}}', '2018-09-24 16:59:18', '2018-09-24 16:59:18'),
(218, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Men\",\"slug\":\"men\"}}', '2018-09-24 16:59:39', '2018-09-24 16:59:39'),
(219, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Women\",\"slug\":\"women\"}}', '2018-09-24 16:59:57', '2018-09-24 16:59:57'),
(220, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Kids\",\"slug\":\"kids\"}}', '2018-09-24 17:00:15', '2018-09-24 17:00:15'),
(221, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Style\",\"slug\":\"style\"}}', '2018-09-24 17:00:43', '2018-09-24 17:00:43'),
(222, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Rimless\",\"slug\":\"rimless\"}}', '2018-09-24 17:01:08', '2018-09-24 17:01:08'),
(223, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Semi-Rimless\",\"slug\":\"semi-rimless\"}}', '2018-09-24 17:01:32', '2018-09-24 17:01:32'),
(224, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Full Frame\",\"slug\":\"full-frame\"}}', '2018-09-24 17:01:50', '2018-09-24 17:01:50'),
(225, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Shape\",\"slug\":\"shape\"}}', '2018-09-24 17:02:20', '2018-09-24 17:02:20'),
(226, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Aviater\",\"slug\":\"aviater\"}}', '2018-09-24 17:02:56', '2018-09-24 17:02:56'),
(227, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Cat Eye\",\"slug\":\"cat-eye\"}}', '2018-09-24 17:03:21', '2018-09-24 17:03:21'),
(228, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Material\",\"slug\":\"material\"}}', '2018-09-24 17:04:09', '2018-09-24 17:04:09'),
(229, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Titanium\",\"slug\":\"titanium\"}}', '2018-09-24 17:04:43', '2018-09-24 17:04:43'),
(230, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Stainless Steel\",\"slug\":\"stainless-steel\"}}', '2018-09-24 17:05:42', '2018-09-24 17:05:42'),
(231, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Acetate\",\"slug\":\"acetate\"}}', '2018-09-24 17:06:08', '2018-09-24 17:06:08'),
(232, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Acetate\",\"slug\":\"acetate\"},\"old\":{\"name\":\"Acetate\",\"slug\":\"acetate\"}}', '2018-09-24 17:06:19', '2018-09-24 17:06:19'),
(233, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Plastic\",\"slug\":\"plastic\"}}', '2018-09-24 17:06:45', '2018-09-24 17:06:45'),
(234, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Color\",\"slug\":\"color\"}}', '2018-09-24 17:07:05', '2018-09-24 17:07:05'),
(235, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Blue\",\"slug\":\"blue\"},\"old\":{\"name\":\"Blue\",\"slug\":\"blue\"}}', '2018-09-24 17:07:55', '2018-09-24 17:07:55'),
(236, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Black\",\"slug\":\"black\"},\"old\":{\"name\":\"Black\",\"slug\":\"black\"}}', '2018-09-24 17:08:13', '2018-09-24 17:08:13'),
(237, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.17\"}', '2018-09-25 17:04:28', '2018-09-25 17:04:28'),
(238, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"110.93.236.103\"}', '2018-09-25 17:36:11', '2018-09-25 17:36:11'),
(239, 'default', 'updated', 109, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-25 18:11:02', '2018-09-25 18:11:02'),
(240, 'default', 'updated', 109, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-25 18:11:27', '2018-09-25 18:11:27'),
(241, 'default', 'updated', 44, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-25 18:20:45', '2018-09-25 18:20:45'),
(242, 'default', 'updated', 47, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-25 18:21:01', '2018-09-25 18:21:01'),
(243, 'default', 'updated', 142, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-25 18:21:47', '2018-09-25 18:21:47'),
(244, 'default', 'updated', 109, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-25 18:22:03', '2018-09-25 18:22:03'),
(245, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"110.93.236.103\"}', '2018-09-25 23:47:45', '2018-09-25 23:47:45'),
(246, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.26\"}', '2018-09-27 13:51:54', '2018-09-27 13:51:54'),
(247, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens\",\"slug\":\"contact-lens\"}}', '2018-09-27 13:52:30', '2018-09-27 13:52:30'),
(248, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens\",\"description\":\"<p>Contact Lens<\\/p>\",\"caption\":\"Contact Lens\",\"properties\":null}}', '2018-09-27 13:53:32', '2018-09-27 13:53:32'),
(249, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"20000.00\",\"sale_price\":\"18000.00\"}}', '2018-09-27 13:53:32', '2018-09-27 13:53:32'),
(250, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens1\",\"description\":null,\"caption\":\"Contact Lens1\",\"properties\":null}}', '2018-09-27 14:18:37', '2018-09-27 14:18:37'),
(251, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"50.00\",\"sale_price\":null}}', '2018-09-27 14:18:37', '2018-09-27 14:18:37'),
(252, 'exception', 'No query results for model [Spatie\\MediaLibrary\\Media] 32. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Spatie\\\\MediaLibrary\\\\Media\",\"message\":\"No query results for model [Spatie\\\\MediaLibrary\\\\Media] 32. \"}}', '2018-09-27 14:22:05', '2018-09-27 14:22:05'),
(253, 'exception', 'No query results for model [Spatie\\MediaLibrary\\Media] 32. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Spatie\\\\MediaLibrary\\\\Media\",\"message\":\"No query results for model [Spatie\\\\MediaLibrary\\\\Media] 32. \"}}', '2018-09-27 14:22:56', '2018-09-27 14:22:56'),
(254, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens2\",\"description\":null,\"caption\":\"Contact Lens2\",\"properties\":null}}', '2018-09-27 14:25:19', '2018-09-27 14:25:19'),
(255, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"50.00\",\"sale_price\":null}}', '2018-09-27 14:25:19', '2018-09-27 14:25:19'),
(256, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens3\",\"description\":null,\"caption\":\"Contact Lens3\",\"properties\":null}}', '2018-09-27 14:27:20', '2018-09-27 14:27:20'),
(257, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"90.00\",\"sale_price\":null}}', '2018-09-27 14:27:20', '2018-09-27 14:27:20'),
(258, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.26\"}', '2018-09-27 17:34:25', '2018-09-27 17:34:25'),
(259, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"}}', '2018-09-27 17:36:27', '2018-09-27 17:36:27'),
(260, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-09-27 17:37:18', '2018-09-27 17:37:18'),
(261, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-09-27 17:37:19', '2018-09-27 17:37:19'),
(262, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-27 17:37:19', '2018-09-27 17:37:19'),
(263, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-27 17:37:19', '2018-09-27 17:37:19'),
(264, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-09-27 17:37:22', '2018-09-27 17:37:22'),
(265, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-09-27 17:37:38', '2018-09-27 17:37:38'),
(266, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-09-27 17:37:40', '2018-09-27 17:37:40'),
(267, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"}}', '2018-09-27 17:37:56', '2018-09-27 17:37:56'),
(268, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - Daily\",\"slug\":\"disposable-daily\"}}', '2018-09-27 17:38:47', '2018-09-27 17:38:47'),
(269, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-09-27 17:39:46', '2018-09-27 17:39:46'),
(270, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-27 17:39:46', '2018-09-27 17:39:46'),
(271, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-27 17:39:46', '2018-09-27 17:39:46'),
(272, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - 2 week\",\"slug\":\"disposable-2-week\"}}', '2018-09-27 17:39:52', '2018-09-27 17:39:52'),
(273, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - Monthly\",\"slug\":\"disposable-monthly\"}}', '2018-09-27 17:40:48', '2018-09-27 17:40:48'),
(274, 'default', 'updated', 28, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - Daily\",\"slug\":\"disposable-daily\"},\"old\":{\"name\":\"Disposable - Daily\",\"slug\":\"disposable-daily\"}}', '2018-09-27 17:41:13', '2018-09-27 17:41:13'),
(275, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Toric & Astigmatism\",\"slug\":\"toric-astigmatism\"}}', '2018-09-27 17:42:52', '2018-09-27 17:42:52'),
(276, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Multifocal & Bifocal\",\"slug\":\"multifocal-bifocal\"}}', '2018-09-27 17:43:54', '2018-09-27 17:43:54'),
(277, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Color & Enhancing\",\"slug\":\"color-enhancing\"}}', '2018-09-27 17:44:56', '2018-09-27 17:44:56'),
(278, 'default', 'updated', 32, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Multifocal & Bifocal\",\"slug\":\"multifocal-bifocal\"},\"old\":{\"name\":\"Multifocal & Bifocal\",\"slug\":\"multifocal-bifocal\"}}', '2018-09-27 17:45:12', '2018-09-27 17:45:12'),
(279, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Extended & Wear\",\"slug\":\"extended-wear\"}}', '2018-09-27 17:46:22', '2018-09-27 17:46:22'),
(280, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Daily Wear\",\"slug\":\"daily-wear\"}}', '2018-09-27 17:46:50', '2018-09-27 17:46:50'),
(281, 'default', 'created', 36, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Prosthetic Lenses\",\"slug\":\"prosthetic-lenses\"}}', '2018-09-27 17:48:01', '2018-09-27 17:48:01'),
(282, 'default', 'created', 37, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Bandage Lenses\",\"slug\":\"bandage-lenses\"}}', '2018-09-27 17:48:31', '2018-09-27 17:48:31'),
(283, 'default', 'created', 38, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Multi-purpose\",\"slug\":\"multi-purpose\"}}', '2018-09-27 17:49:02', '2018-09-27 17:49:02'),
(284, 'default', 'created', 39, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Cleaners & Salines\",\"slug\":\"cleaners-salines\"}}', '2018-09-27 17:49:54', '2018-09-27 17:49:54'),
(285, 'default', 'created', 40, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gas Permeable\",\"slug\":\"gas-permeable\"}}', '2018-09-27 17:50:33', '2018-09-27 17:50:33'),
(286, 'default', 'created', 41, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Travel Packs\",\"slug\":\"travel-packs\"}}', '2018-09-27 17:50:57', '2018-09-27 17:50:57'),
(287, 'default', 'created', 42, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Hydrogen Peroxide\",\"slug\":\"hydrogen-peroxide\"}}', '2018-09-27 17:51:32', '2018-09-27 17:51:32'),
(288, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.38\"}', '2018-09-29 01:39:45', '2018-09-29 01:39:45'),
(289, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.88.100.186\"}', '2018-09-29 01:47:49', '2018-09-29 01:47:49'),
(290, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"39.50.140.90\"}', '2018-09-29 21:03:52', '2018-09-29 21:03:52'),
(291, 'default', 'updated', 27, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"},\"old\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"}}', '2018-09-29 21:05:18', '2018-09-29 21:05:18'),
(292, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.56\"}', '2018-09-30 14:43:28', '2018-09-30 14:43:28'),
(293, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"18175.00\"}}', '2018-09-30 14:44:04', '2018-09-30 14:44:04'),
(294, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 14:44:04', '2018-09-30 14:44:04'),
(295, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 14:44:04', '2018-09-30 14:44:04'),
(296, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 14:44:04', '2018-09-30 14:44:04'),
(297, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"18175.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"18175.00\"}}', '2018-09-30 14:44:06', '2018-09-30 14:44:06'),
(298, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"18175.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"18175.00\"}}', '2018-09-30 14:44:22', '2018-09-30 14:44:22'),
(299, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 14:44:22', '2018-09-30 14:44:22'),
(300, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 14:44:22', '2018-09-30 14:44:22'),
(301, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 14:44:22', '2018-09-30 14:44:22'),
(302, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"18175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"18175.00\"}}', '2018-09-30 14:44:24', '2018-09-30 14:44:24'),
(303, 'exception', 'pay Gateway Order Failed. Invalid request: unsupported Content-Type . If error persists and you need assistance, please contact support@stripe.com.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"doCheckout\",\"object\":\"CheckOutController\",\"message\":\"pay Gateway Order Failed. Invalid request: unsupported Content-Type . If error persists and you need assistance, please contact support@stripe.com.. \"}}', '2018-09-30 14:49:15', '2018-09-30 14:49:15'),
(304, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"18175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"18175.00\"}}', '2018-09-30 15:04:10', '2018-09-30 15:04:10'),
(305, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 15:04:10', '2018-09-30 15:04:10'),
(306, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 15:04:10', '2018-09-30 15:04:10'),
(307, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-09-30 15:04:10', '2018-09-30 15:04:10'),
(308, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"18175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"18175.00\"}}', '2018-09-30 15:04:13', '2018-09-30 15:04:13'),
(309, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.51\"}', '2018-10-01 14:09:05', '2018-10-01 14:09:05'),
(310, 'default', 'updated', 26, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"},\"old\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"}}', '2018-10-01 14:10:03', '2018-10-01 14:10:03'),
(311, 'default', 'updated', 25, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens\",\"slug\":\"contact-lens\"},\"old\":{\"name\":\"Contact Lens\",\"slug\":\"contact-lens\"}}', '2018-10-01 14:10:22', '2018-10-01 14:10:22'),
(312, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.51\"}', '2018-10-01 14:24:42', '2018-10-01 14:24:42'),
(313, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-01 14:38:37', '2018-10-01 14:38:37'),
(314, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.51\"}', '2018-10-01 14:38:55', '2018-10-01 14:38:55'),
(315, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.51\"}', '2018-10-01 15:40:58', '2018-10-01 15:40:58'),
(316, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-01 16:29:20', '2018-10-01 16:29:20'),
(317, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 16:29:20', '2018-10-01 16:29:20'),
(318, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-01 16:29:22', '2018-10-01 16:29:22'),
(319, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-01 16:29:28', '2018-10-01 16:29:28'),
(320, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 16:29:28', '2018-10-01 16:29:28'),
(321, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"0.00\"}}', '2018-10-01 16:29:30', '2018-10-01 16:29:30'),
(322, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"0.00\"}}', '2018-10-01 17:16:42', '2018-10-01 17:16:42'),
(323, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:16:42', '2018-10-01 17:16:42'),
(324, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:16:42', '2018-10-01 17:16:42'),
(325, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"175.00\"}}', '2018-10-01 17:16:45', '2018-10-01 17:16:45'),
(326, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"175.00\"}}', '2018-10-01 17:21:03', '2018-10-01 17:21:03'),
(327, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:21:03', '2018-10-01 17:21:03'),
(328, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:21:03', '2018-10-01 17:21:03'),
(329, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:21:03', '2018-10-01 17:21:03'),
(330, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2125.00\"}}', '2018-10-01 17:21:04', '2018-10-01 17:21:04'),
(331, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2125.00\"}}', '2018-10-01 17:57:46', '2018-10-01 17:57:46'),
(332, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:57:46', '2018-10-01 17:57:46'),
(333, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:57:46', '2018-10-01 17:57:46'),
(334, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:57:46', '2018-10-01 17:57:46'),
(335, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2125.00\"}}', '2018-10-01 17:57:49', '2018-10-01 17:57:49'),
(336, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-10-01 17:58:52', '2018-10-01 17:58:52'),
(337, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:58:52', '2018-10-01 17:58:52'),
(338, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 17:58:52', '2018-10-01 17:58:52'),
(339, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.201\"}', '2018-10-01 18:34:31', '2018-10-01 18:34:31'),
(340, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-01 18:36:56', '2018-10-01 18:36:56'),
(341, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1950.00\"}}', '2018-10-01 18:36:57', '2018-10-01 18:36:57'),
(342, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 18:36:57', '2018-10-01 18:36:57'),
(343, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 18:36:57', '2018-10-01 18:36:57'),
(344, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1950.00\"}}', '2018-10-01 18:37:04', '2018-10-01 18:37:04'),
(345, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1950.00\"}}', '2018-10-01 18:37:05', '2018-10-01 18:37:05'),
(346, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2125.00\"}}', '2018-10-01 18:42:36', '2018-10-01 18:42:36'),
(347, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 18:42:36', '2018-10-01 18:42:36'),
(348, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 18:42:36', '2018-10-01 18:42:36'),
(349, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-01 18:42:37', '2018-10-01 18:42:37'),
(350, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2125.00\"}}', '2018-10-01 18:42:39', '2018-10-01 18:42:39'),
(351, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.68.108.230\"}', '2018-10-01 22:03:38', '2018-10-01 22:03:38'),
(352, 'default', 'updated', 36, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"PKR\"},\"old\":{\"value\":\"USD\"}}', '2018-10-01 22:04:09', '2018-10-01 22:04:09'),
(353, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.68.108.230\"}', '2018-10-01 22:05:27', '2018-10-01 22:05:27'),
(354, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.7\"}', '2018-10-02 11:23:27', '2018-10-02 11:23:27'),
(355, 'default', 'created', 43, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Size\",\"slug\":\"size\"}}', '2018-10-02 12:16:43', '2018-10-02 12:16:43'),
(356, 'default', 'created', 44, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Extra Small\",\"slug\":\"extra-small\"}}', '2018-10-02 12:22:31', '2018-10-02 12:22:31'),
(357, 'default', 'created', 45, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Small\",\"slug\":\"small\"}}', '2018-10-02 12:23:26', '2018-10-02 12:23:26'),
(358, 'default', 'created', 46, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Medium\",\"slug\":\"medium\"}}', '2018-10-02 12:24:10', '2018-10-02 12:24:10'),
(359, 'default', 'created', 47, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Large\",\"slug\":\"large\"}}', '2018-10-02 12:24:36', '2018-10-02 12:24:36'),
(360, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.16\"}', '2018-10-02 14:32:27', '2018-10-02 14:32:27'),
(361, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.30\"}', '2018-10-03 14:22:11', '2018-10-03 14:22:11'),
(362, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.150\"}', '2018-10-03 15:15:27', '2018-10-03 15:15:27'),
(363, 'default', 'updated', 8, 'Corals\\Modules\\CMS\\Models\\Post', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Why You Need A Blog Subscription Landing Page\"},\"old\":{\"title\":\"Why You Need A Blog Subscription Landing Page\"}}', '2018-10-03 15:28:33', '2018-10-03 15:28:33'),
(364, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.30\"}', '2018-10-03 16:43:31', '2018-10-03 16:43:31'),
(365, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-10-03 16:45:23', '2018-10-03 16:45:23'),
(366, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-03 16:45:23', '2018-10-03 16:45:23'),
(367, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-10-03 16:45:26', '2018-10-03 16:45:26'),
(368, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.50\"}', '2018-10-04 14:54:43', '2018-10-04 14:54:43'),
(369, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Category\",\"slug\":\"category\"},\"old\":{\"name\":\"Category\",\"slug\":\"category\"}}', '2018-10-04 15:39:22', '2018-10-04 15:39:22'),
(370, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.50\"}', '2018-10-04 16:19:48', '2018-10-04 16:19:48'),
(371, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.57\"}', '2018-10-07 15:33:46', '2018-10-07 15:33:46'),
(372, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.57\"}', '2018-10-07 19:19:08', '2018-10-07 19:19:08'),
(373, 'default', 'created', 10, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"TERMS OF USE\"}}', '2018-10-07 19:29:33', '2018-10-07 19:29:33'),
(374, 'default', 'updated', 10, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"TERMS OF USE\"},\"old\":{\"title\":\"TERMS OF USE\"}}', '2018-10-07 19:32:01', '2018-10-07 19:32:01'),
(375, 'default', 'created', 11, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Privacy Policy\"}}', '2018-10-07 19:34:36', '2018-10-07 19:34:36'),
(376, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.57\"}', '2018-10-07 19:50:37', '2018-10-07 19:50:37'),
(377, 'default', 'updated', 1, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 19:51:21', '2018-10-07 19:51:21'),
(378, 'default', 'updated', 54, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"GABA-Opticals\"},\"old\":{\"value\":\"Corals\"}}', '2018-10-07 19:56:56', '2018-10-07 19:56:56'),
(379, 'default', 'updated', 55, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"GABA-Opticals\"},\"old\":{\"value\":\"Corals\"}}', '2018-10-07 19:56:56', '2018-10-07 19:56:56'),
(380, 'default', 'updated', 57, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"Karachi\"},\"old\":{\"value\":\"Fort Dodge\"}}', '2018-10-07 19:56:57', '2018-10-07 19:56:57'),
(381, 'default', 'updated', 58, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"PK\"},\"old\":{\"value\":\"GA\"}}', '2018-10-07 19:56:57', '2018-10-07 19:56:57'),
(382, 'default', 'updated', 59, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"75800\"},\"old\":{\"value\":\"20783\"}}', '2018-10-07 19:56:57', '2018-10-07 19:56:57'),
(383, 'default', 'updated', 60, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"PKR\"},\"old\":{\"value\":\"USA\"}}', '2018-10-07 19:56:57', '2018-10-07 19:56:57'),
(384, 'default', 'updated', 62, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":\"gaba@gmail.com\"},\"old\":{\"value\":\"support@corals.io\"}}', '2018-10-07 19:56:57', '2018-10-07 19:56:57'),
(385, 'default', 'created', 16, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 20:15:42', '2018-10-07 20:15:42'),
(386, 'default', 'updated', 16, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 20:16:16', '2018-10-07 20:16:16'),
(387, 'default', 'updated', 35, 'Corals\\Settings\\Models\\Setting', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"value\":{\"PayPal_Rest\":\"PayPal\",\"Stripe\":\"Stripe\",\"Cash\":\"Cash on delivery (COD)\"}},\"old\":{\"value\":{\"PayPal_Rest\":\"PayPal\",\"Stripe\":\"Stripe\"}}}', '2018-10-07 20:16:16', '2018-10-07 20:16:16'),
(388, 'default', 'updated', 16, 'Corals\\Settings\\Models\\Module', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 20:16:16', '2018-10-07 20:16:16'),
(389, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-07 20:19:28', '2018-10-07 20:19:28'),
(390, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-07 20:19:29', '2018-10-07 20:19:29'),
(391, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 20:19:29', '2018-10-07 20:19:29'),
(392, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-07 20:19:33', '2018-10-07 20:19:33'),
(393, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-07 20:22:37', '2018-10-07 20:22:37'),
(394, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 20:22:37', '2018-10-07 20:22:37'),
(395, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"0.00\"}}', '2018-10-07 20:22:39', '2018-10-07 20:22:39'),
(396, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"0.00\"}}', '2018-10-07 20:22:46', '2018-10-07 20:22:46'),
(397, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"500.00\",\"sale_price\":\"0.00\"},\"old\":{\"regular_price\":\"500.00\",\"sale_price\":\"0.00\"}}', '2018-10-07 20:22:47', '2018-10-07 20:22:47'),
(398, 'default', 'updated', 109, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-07 20:31:01', '2018-10-07 20:31:01'),
(399, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.57\"}', '2018-10-07 21:28:48', '2018-10-07 21:28:48'),
(400, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.24\"}', '2018-10-08 18:34:31', '2018-10-08 18:34:31'),
(401, 'default', 'updated', 11, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Privacy Policy\"},\"old\":{\"title\":\"Privacy Policy\"}}', '2018-10-08 19:02:33', '2018-10-08 19:02:33'),
(402, 'default', 'updated', 11, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Privacy Policy\"},\"old\":{\"title\":\"Privacy Policy\"}}', '2018-10-08 19:12:09', '2018-10-08 19:12:09'),
(403, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.42\"}', '2018-10-09 12:13:24', '2018-10-09 12:13:24'),
(404, 'default', 'updated', 11, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"Privacy Policy\"},\"old\":{\"title\":\"Privacy Policy\"}}', '2018-10-09 12:16:25', '2018-10-09 12:16:25'),
(405, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.42\"}', '2018-10-09 12:34:11', '2018-10-09 12:34:11'),
(406, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-09 12:35:11', '2018-10-09 12:35:11'),
(407, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2125.00\"}}', '2018-10-09 12:35:11', '2018-10-09 12:35:11'),
(408, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-09 12:35:11', '2018-10-09 12:35:11'),
(409, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-09 12:35:11', '2018-10-09 12:35:11'),
(410, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2125.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2125.00\"}}', '2018-10-09 12:35:14', '2018-10-09 12:35:14'),
(411, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.42\"}', '2018-10-09 15:09:08', '2018-10-09 15:09:08'),
(412, 'default', 'updated', 10, 'Corals\\Modules\\CMS\\Models\\Page', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"title\":\"TERMS OF USE\"},\"old\":{\"title\":\"TERMS OF USE\"}}', '2018-10-09 15:21:19', '2018-10-09 15:21:19'),
(413, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.11\"}', '2018-10-10 12:01:13', '2018-10-10 12:01:13'),
(414, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.11\"}', '2018-10-10 13:06:13', '2018-10-10 13:06:13'),
(415, 'default', 'updated', 1, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-10 13:07:39', '2018-10-10 13:07:39'),
(416, 'default', 'updated', 1, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-10 13:08:53', '2018-10-10 13:08:53'),
(417, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"39.50.185.63\"}', '2018-10-13 22:04:54', '2018-10-13 22:04:54'),
(418, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.56\"}', '2018-10-15 13:23:29', '2018-10-15 13:23:29'),
(419, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-15 13:23:36', '2018-10-15 13:23:36'),
(420, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-10-15 13:23:36', '2018-10-15 13:23:36'),
(421, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-10-15 13:23:48', '2018-10-15 13:23:48'),
(422, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2950.00\"}}', '2018-10-15 13:23:52', '2018-10-15 13:23:52'),
(423, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2950.00\"}}', '2018-10-15 13:23:57', '2018-10-15 13:23:57'),
(424, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2950.00\"}}', '2018-10-15 13:24:03', '2018-10-15 13:24:03'),
(425, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2950.00\"}}', '2018-10-15 13:43:43', '2018-10-15 13:43:43'),
(426, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:43', '2018-10-15 13:43:43'),
(427, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:43', '2018-10-15 13:43:43'),
(428, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:43', '2018-10-15 13:43:43'),
(429, 'default', 'created', 36, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:43', '2018-10-15 13:43:43'),
(430, 'default', 'created', 37, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:43', '2018-10-15 13:43:43'),
(431, 'default', 'created', 38, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:44', '2018-10-15 13:43:44'),
(432, 'default', 'created', 39, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:44', '2018-10-15 13:43:44'),
(433, 'default', 'created', 40, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-15 13:43:44', '2018-10-15 13:43:44'),
(434, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2300.00\"}}', '2018-10-15 13:43:47', '2018-10-15 13:43:47'),
(435, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2300.00\"}}', '2018-10-15 13:44:34', '2018-10-15 13:44:34'),
(436, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-15 13:44:35', '2018-10-15 13:44:35'),
(437, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-15 13:44:35', '2018-10-15 13:44:35'),
(438, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-15 13:44:35', '2018-10-15 13:44:35'),
(439, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-15 13:44:35', '2018-10-15 13:44:35'),
(440, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-15 13:44:35', '2018-10-15 13:44:35');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(441, 'default', 'updated', 28, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - Daily\",\"slug\":\"disposable-daily\"},\"old\":{\"name\":\"Disposable - Daily\",\"slug\":\"disposable-daily\"}}', '2018-10-15 13:56:24', '2018-10-15 13:56:24'),
(442, 'default', 'updated', 29, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - 2 week\",\"slug\":\"disposable-2-week\"},\"old\":{\"name\":\"Disposable - 2 week\",\"slug\":\"disposable-2-week\"}}', '2018-10-15 13:57:02', '2018-10-15 13:57:02'),
(443, 'default', 'updated', 30, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Disposable - Monthly\",\"slug\":\"disposable-monthly\"},\"old\":{\"name\":\"Disposable - Monthly\",\"slug\":\"disposable-monthly\"}}', '2018-10-15 13:57:21', '2018-10-15 13:57:21'),
(444, 'default', 'updated', 31, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Toric & Astigmatism\",\"slug\":\"toric-astigmatism\"},\"old\":{\"name\":\"Toric & Astigmatism\",\"slug\":\"toric-astigmatism\"}}', '2018-10-15 13:57:55', '2018-10-15 13:57:55'),
(445, 'default', 'updated', 32, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Multifocal & Bifocal\",\"slug\":\"multifocal-bifocal\"},\"old\":{\"name\":\"Multifocal & Bifocal\",\"slug\":\"multifocal-bifocal\"}}', '2018-10-15 13:58:35', '2018-10-15 13:58:35'),
(446, 'default', 'updated', 33, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Color & Enhancing\",\"slug\":\"color-enhancing\"},\"old\":{\"name\":\"Color & Enhancing\",\"slug\":\"color-enhancing\"}}', '2018-10-15 13:59:11', '2018-10-15 13:59:11'),
(447, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Extended & Wear\",\"slug\":\"extended-wear\"},\"old\":{\"name\":\"Extended & Wear\",\"slug\":\"extended-wear\"}}', '2018-10-15 13:59:36', '2018-10-15 13:59:36'),
(448, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Daily Wear\",\"slug\":\"daily-wear\"},\"old\":{\"name\":\"Daily Wear\",\"slug\":\"daily-wear\"}}', '2018-10-15 14:00:02', '2018-10-15 14:00:02'),
(449, 'default', 'updated', 36, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Prosthetic Lenses\",\"slug\":\"prosthetic-lenses\"},\"old\":{\"name\":\"Prosthetic Lenses\",\"slug\":\"prosthetic-lenses\"}}', '2018-10-15 14:00:25', '2018-10-15 14:00:25'),
(450, 'default', 'updated', 37, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Bandage Lenses\",\"slug\":\"bandage-lenses\"},\"old\":{\"name\":\"Bandage Lenses\",\"slug\":\"bandage-lenses\"}}', '2018-10-15 14:00:50', '2018-10-15 14:00:50'),
(451, 'default', 'updated', 26, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"},\"old\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"}}', '2018-10-15 14:07:49', '2018-10-15 14:07:49'),
(452, 'default', 'updated', 27, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"},\"old\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"}}', '2018-10-15 14:08:05', '2018-10-15 14:08:05'),
(453, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens3\",\"description\":null,\"caption\":\"Contact Lens3\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens3\",\"description\":null,\"caption\":\"Contact Lens3\",\"properties\":null}}', '2018-10-15 14:35:52', '2018-10-15 14:35:52'),
(454, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens2\",\"description\":null,\"caption\":\"Contact Lens2\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens2\",\"description\":null,\"caption\":\"Contact Lens2\",\"properties\":null}}', '2018-10-15 14:36:10', '2018-10-15 14:36:10'),
(455, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens1\",\"description\":null,\"caption\":\"Contact Lens1\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens1\",\"description\":null,\"caption\":\"Contact Lens1\",\"properties\":null}}', '2018-10-15 14:36:28', '2018-10-15 14:36:28'),
(456, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"110.93.236.103\"}', '2018-10-16 23:07:08', '2018-10-16 23:07:08'),
(457, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Brand', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"GABA\"}}', '2018-10-16 23:09:41', '2018-10-16 23:09:41'),
(458, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Tag', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"SALE\"}}', '2018-10-16 23:14:39', '2018-10-16 23:14:39'),
(459, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-16 23:15:31', '2018-10-16 23:15:31'),
(460, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"GABA WR576\",\"description\":null,\"caption\":\"frame\",\"properties\":[]},\"old\":{\"name\":\"Glasses\",\"description\":null,\"caption\":\"frame\",\"properties\":[]}}', '2018-10-16 23:17:08', '2018-10-16 23:17:08'),
(461, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-16 23:24:52', '2018-10-16 23:24:52'),
(462, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.41\"}', '2018-10-16 23:34:57', '2018-10-16 23:34:57'),
(463, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Attribute', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-16 23:36:28', '2018-10-16 23:36:28'),
(464, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gaba Frame 1\",\"description\":\"<p>Frame 1 Description<\\/p>\",\"caption\":\"Caption Frame\",\"properties\":null}}', '2018-10-16 23:38:12', '2018-10-16 23:38:12'),
(465, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-10-16 23:38:12', '2018-10-16 23:38:12'),
(466, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.12\"}', '2018-10-17 23:00:55', '2018-10-17 23:00:55'),
(467, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gaba Frame GB1\",\"description\":\"<p>Gaba Frame GB1 of extreme high quality<\\/p>\",\"caption\":\"Gaba Frame GB1\",\"properties\":null}}', '2018-10-17 23:23:17', '2018-10-17 23:23:17'),
(468, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"100.00\",\"sale_price\":null}}', '2018-10-17 23:23:17', '2018-10-17 23:23:17'),
(469, 'exception', 'No query results for model [Spatie\\MediaLibrary\\Media] 43. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Spatie\\\\MediaLibrary\\\\Media\",\"message\":\"No query results for model [Spatie\\\\MediaLibrary\\\\Media] 43. \"}}', '2018-10-17 23:31:00', '2018-10-17 23:31:00'),
(470, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gaba Frame GB1\",\"description\":\"<p>Gaba Frame GB1 of extreme high quality<\\/p>\",\"caption\":\"Gaba Frame GB1\",\"properties\":[]},\"old\":{\"name\":\"Gaba Frame GB1\",\"description\":\"<p>Gaba Frame GB1 of extreme high quality<\\/p>\",\"caption\":\"Gaba Frame GB1\",\"properties\":null}}', '2018-10-17 23:35:55', '2018-10-17 23:35:55'),
(471, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.12\"}', '2018-10-18 01:44:07', '2018-10-18 01:44:07'),
(472, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.88.44.171\"}', '2018-10-18 01:48:07', '2018-10-18 01:48:07'),
(473, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.12\"}', '2018-10-18 01:48:29', '2018-10-18 01:48:29'),
(474, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.88.44.171\"}', '2018-10-18 01:50:45', '2018-10-18 01:50:45'),
(475, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"150.00\",\"sale_price\":\"200.00\"},\"old\":{\"regular_price\":\"100.00\",\"sale_price\":null}}', '2018-10-18 01:56:49', '2018-10-18 01:56:49'),
(476, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba g2\",\"description\":null,\"caption\":\"g2\",\"properties\":null}}', '2018-10-18 01:58:00', '2018-10-18 01:58:00'),
(477, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"100.00\",\"sale_price\":null}}', '2018-10-18 01:58:00', '2018-10-18 01:58:00'),
(478, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.7\"}', '2018-10-18 11:53:57', '2018-10-18 11:53:57'),
(479, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.4\"}', '2018-10-18 16:37:57', '2018-10-18 16:37:57'),
(480, 'exception', 'No query results for model [Spatie\\MediaLibrary\\Media] 30. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Spatie\\\\MediaLibrary\\\\Media\",\"message\":\"No query results for model [Spatie\\\\MediaLibrary\\\\Media] 30. \"}}', '2018-10-18 16:45:58', '2018-10-18 16:45:58'),
(481, 'exception', 'No query results for model [Spatie\\MediaLibrary\\Media] 30. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Spatie\\\\MediaLibrary\\\\Media\",\"message\":\"No query results for model [Spatie\\\\MediaLibrary\\\\Media] 30. \"}}', '2018-10-18 16:49:29', '2018-10-18 16:49:29'),
(482, 'exception', 'No query results for model [Spatie\\MediaLibrary\\Media] 29. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Spatie\\\\MediaLibrary\\\\Media\",\"message\":\"No query results for model [Spatie\\\\MediaLibrary\\\\Media] 29. \"}}', '2018-10-18 16:54:18', '2018-10-18 16:54:18'),
(483, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba g2\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"g2\",\"properties\":[]},\"old\":{\"name\":\"gaba g2\",\"description\":null,\"caption\":\"g2\",\"properties\":null}}', '2018-10-18 17:24:02', '2018-10-18 17:24:02'),
(484, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gaba Frame GB1\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Gaba Frame GB1\",\"properties\":[]},\"old\":{\"name\":\"Gaba Frame GB1\",\"description\":\"<p>Gaba Frame GB1 of extreme high quality<\\/p>\",\"caption\":\"Gaba Frame GB1\",\"properties\":[]}}', '2018-10-18 17:24:55', '2018-10-18 17:24:55'),
(485, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gaba Frame 1\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Caption Frame\",\"properties\":[]},\"old\":{\"name\":\"Gaba Frame 1\",\"description\":\"<p>Frame 1 Description<\\/p>\",\"caption\":\"Caption Frame\",\"properties\":null}}', '2018-10-18 17:25:14', '2018-10-18 17:25:14'),
(486, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens3\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Contact Lens3\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens3\",\"description\":null,\"caption\":\"Contact Lens3\",\"properties\":[]}}', '2018-10-18 17:25:35', '2018-10-18 17:25:35'),
(487, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens2\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Contact Lens2\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens2\",\"description\":null,\"caption\":\"Contact Lens2\",\"properties\":[]}}', '2018-10-18 17:25:54', '2018-10-18 17:25:54'),
(488, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens1\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Contact Lens1\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens1\",\"description\":null,\"caption\":\"Contact Lens1\",\"properties\":[]}}', '2018-10-18 17:26:16', '2018-10-18 17:26:16'),
(489, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Contact Lens\",\"properties\":[]},\"old\":{\"name\":\"Contact Lens\",\"description\":\"<p>Contact Lens<\\/p>\",\"caption\":\"Contact Lens\",\"properties\":null}}', '2018-10-18 17:26:32', '2018-10-18 17:26:32'),
(490, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Test\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Test\",\"properties\":[]},\"old\":{\"name\":\"Test\",\"description\":null,\"caption\":\"Test\",\"properties\":[]}}', '2018-10-18 17:26:46', '2018-10-18 17:26:46'),
(491, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"GABA WR576\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"frame\",\"properties\":[]},\"old\":{\"name\":\"GABA WR576\",\"description\":null,\"caption\":\"frame\",\"properties\":[]}}', '2018-10-18 17:29:27', '2018-10-18 17:29:27'),
(492, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.4\"}', '2018-10-18 18:32:11', '2018-10-18 18:32:11'),
(493, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-10-18 18:33:24', '2018-10-18 18:33:24'),
(494, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-18 18:33:24', '2018-10-18 18:33:24'),
(495, 'default', 'created', 41, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-18 18:33:24', '2018-10-18 18:33:24'),
(496, 'default', 'created', 42, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-18 18:33:24', '2018-10-18 18:33:24'),
(497, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.4\"}', '2018-10-18 18:37:05', '2018-10-18 18:37:05'),
(498, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.62\"}', '2018-10-19 23:41:28', '2018-10-19 23:41:28'),
(499, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.62\"}', '2018-10-19 23:42:28', '2018-10-19 23:42:28'),
(500, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.7\"}', '2018-10-23 03:00:10', '2018-10-23 03:00:10'),
(501, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-23 03:00:40', '2018-10-23 03:00:40'),
(502, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-10-23 03:00:40', '2018-10-23 03:00:40'),
(503, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-23 03:00:40', '2018-10-23 03:00:40'),
(504, 'default', 'created', 43, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-23 03:00:40', '2018-10-23 03:00:40'),
(505, 'default', 'created', 44, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-23 03:00:40', '2018-10-23 03:00:40'),
(506, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.88.44.167\"}', '2018-10-23 03:06:20', '2018-10-23 03:06:20'),
(507, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.88.44.167\"}', '2018-10-23 03:45:59', '2018-10-23 03:45:59'),
(508, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-23 04:28:26', '2018-10-23 04:28:26'),
(509, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.201\"}', '2018-10-23 12:18:28', '2018-10-23 12:18:28'),
(510, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-23 12:19:26', '2018-10-23 12:19:26'),
(511, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-23 12:19:27', '2018-10-23 12:19:27'),
(512, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-23 12:19:27', '2018-10-23 12:19:27'),
(513, 'default', 'created', 45, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-23 12:19:27', '2018-10-23 12:19:27'),
(514, 'default', 'created', 46, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-23 12:19:27', '2018-10-23 12:19:27'),
(515, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"117.102.60.69\"}', '2018-10-24 00:31:26', '2018-10-24 00:31:26'),
(516, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"117.102.60.69\"}', '2018-10-24 00:40:38', '2018-10-24 00:40:38'),
(517, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 00:46:54', '2018-10-24 00:46:54'),
(518, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:48:27', '2018-10-24 00:48:27'),
(519, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:48:27', '2018-10-24 00:48:27'),
(520, 'default', 'created', 47, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:48:27', '2018-10-24 00:48:27'),
(521, 'default', 'created', 48, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:48:27', '2018-10-24 00:48:27'),
(522, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:52:20', '2018-10-24 00:52:20'),
(523, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:52:52', '2018-10-24 00:52:52'),
(524, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:53:17', '2018-10-24 00:53:17'),
(525, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:54:55', '2018-10-24 00:54:55'),
(526, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:54:55', '2018-10-24 00:54:55'),
(527, 'default', 'created', 49, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:54:55', '2018-10-24 00:54:55'),
(528, 'default', 'created', 50, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:54:55', '2018-10-24 00:54:55'),
(529, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:55:04', '2018-10-24 00:55:04'),
(530, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:55:12', '2018-10-24 00:55:12'),
(531, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:55:12', '2018-10-24 00:55:12'),
(532, 'default', 'created', 51, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:55:12', '2018-10-24 00:55:12'),
(533, 'default', 'created', 52, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 00:55:12', '2018-10-24 00:55:12'),
(534, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-24 00:55:14', '2018-10-24 00:55:14'),
(535, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:18:54', '2018-10-24 01:18:54'),
(536, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 01:18:54', '2018-10-24 01:18:54'),
(537, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:18:54', '2018-10-24 01:18:54'),
(538, 'default', 'created', 53, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:18:54', '2018-10-24 01:18:54'),
(539, 'default', 'created', 54, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:18:54', '2018-10-24 01:18:54'),
(540, 'default', 'updated', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 01:19:02', '2018-10-24 01:19:02'),
(541, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:25', '2018-10-24 01:23:25'),
(542, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:25', '2018-10-24 01:23:25'),
(543, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:25', '2018-10-24 01:23:25'),
(544, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:25', '2018-10-24 01:23:25'),
(545, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:27', '2018-10-24 01:23:27'),
(546, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:27', '2018-10-24 01:23:27'),
(547, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:23:27', '2018-10-24 01:23:27'),
(548, 'default', 'updated', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1175.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 01:30:22', '2018-10-24 01:30:22'),
(549, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:30:23', '2018-10-24 01:30:23'),
(550, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:30:23', '2018-10-24 01:30:23'),
(551, 'default', 'created', 55, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:30:23', '2018-10-24 01:30:23'),
(552, 'default', 'created', 56, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:30:23', '2018-10-24 01:30:23'),
(553, 'default', 'created', 57, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:30:23', '2018-10-24 01:30:23'),
(554, 'default', 'created', 58, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:30:23', '2018-10-24 01:30:23'),
(555, 'default', 'updated', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1175.00\"}}', '2018-10-24 01:30:26', '2018-10-24 01:30:26'),
(556, 'default', 'updated', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1175.00\"}}', '2018-10-24 01:30:59', '2018-10-24 01:30:59'),
(557, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-10-24 01:30:59', '2018-10-24 01:30:59'),
(558, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-10-24 01:30:59', '2018-10-24 01:30:59'),
(559, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-24 01:30:59', '2018-10-24 01:30:59'),
(560, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-24 01:30:59', '2018-10-24 01:30:59'),
(561, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 01:50:56', '2018-10-24 01:50:56'),
(562, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-10-24 01:50:56', '2018-10-24 01:50:56'),
(563, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:50:56', '2018-10-24 01:50:56'),
(564, 'default', 'created', 59, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:50:56', '2018-10-24 01:50:56'),
(565, 'default', 'created', 60, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:50:56', '2018-10-24 01:50:56'),
(566, 'default', 'updated', 15, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-10-24 01:50:57', '2018-10-24 01:50:57'),
(567, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:50:57', '2018-10-24 01:50:57'),
(568, 'default', 'created', 61, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:50:57', '2018-10-24 01:50:57'),
(569, 'default', 'created', 62, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:50:57', '2018-10-24 01:50:57'),
(570, 'default', 'updated', 15, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"350.00\"}}', '2018-10-24 01:53:19', '2018-10-24 01:53:19'),
(571, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:53:19', '2018-10-24 01:53:19'),
(572, 'default', 'created', 63, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:53:19', '2018-10-24 01:53:19'),
(573, 'default', 'created', 64, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 01:53:19', '2018-10-24 01:53:19'),
(574, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 02:06:46', '2018-10-24 02:06:46'),
(575, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-24 02:06:46', '2018-10-24 02:06:46'),
(576, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 02:06:46', '2018-10-24 02:06:46'),
(577, 'default', 'created', 65, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 02:06:46', '2018-10-24 02:06:46'),
(578, 'default', 'created', 66, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 02:06:46', '2018-10-24 02:06:46'),
(579, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-24 02:06:49', '2018-10-24 02:06:49'),
(580, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-24 02:07:00', '2018-10-24 02:07:00'),
(581, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-10-24 02:07:00', '2018-10-24 02:07:00'),
(582, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-10-24 02:07:00', '2018-10-24 02:07:00'),
(583, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.5\"}', '2018-10-24 14:28:26', '2018-10-24 14:28:26'),
(584, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-24 14:29:03', '2018-10-24 14:29:03'),
(585, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 14:29:04', '2018-10-24 14:29:04'),
(586, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 14:29:04', '2018-10-24 14:29:04'),
(587, 'default', 'created', 67, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 14:29:04', '2018-10-24 14:29:04'),
(588, 'default', 'created', 68, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-24 14:29:04', '2018-10-24 14:29:04'),
(589, 'default', 'updated', 16, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 14:29:07', '2018-10-24 14:29:07'),
(590, 'default', 'updated', 16, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-24 14:29:08', '2018-10-24 14:29:08'),
(591, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"196.62.172.162\"}', '2018-10-27 00:26:46', '2018-10-27 00:26:46'),
(592, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-27 00:27:36', '2018-10-27 00:27:36'),
(593, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-10-27 00:27:37', '2018-10-27 00:27:37'),
(594, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 00:27:37', '2018-10-27 00:27:37'),
(595, 'default', 'created', 69, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 00:27:37', '2018-10-27 00:27:37'),
(596, 'default', 'created', 70, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 00:27:37', '2018-10-27 00:27:37'),
(597, 'default', 'updated', 17, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-10-27 00:27:39', '2018-10-27 00:27:39'),
(598, 'default', 'updated', 17, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-10-27 00:27:47', '2018-10-27 00:27:47'),
(599, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-27 00:27:47', '2018-10-27 00:27:47'),
(600, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"},\"old\":{\"regular_price\":\"200.00\",\"sale_price\":\"175.00\"}}', '2018-10-27 00:27:47', '2018-10-27 00:27:47'),
(601, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.12\"}', '2018-10-27 16:22:34', '2018-10-27 16:22:34'),
(602, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-27 16:34:53', '2018-10-27 16:34:53'),
(603, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-10-27 16:34:54', '2018-10-27 16:34:54'),
(604, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 16:34:54', '2018-10-27 16:34:54'),
(605, 'default', 'created', 71, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 16:34:54', '2018-10-27 16:34:54'),
(606, 'default', 'created', 72, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 16:34:54', '2018-10-27 16:34:54'),
(607, 'default', 'updated', 18, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-10-27 16:34:58', '2018-10-27 16:34:58'),
(608, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.21\"}', '2018-10-27 21:38:52', '2018-10-27 21:38:52'),
(609, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-27 21:42:17', '2018-10-27 21:42:17'),
(610, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-27 21:42:18', '2018-10-27 21:42:18'),
(611, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 21:42:18', '2018-10-27 21:42:18'),
(612, 'default', 'created', 73, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 21:42:18', '2018-10-27 21:42:18'),
(613, 'default', 'created', 74, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-27 21:42:18', '2018-10-27 21:42:18'),
(614, 'default', 'updated', 19, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-27 21:42:22', '2018-10-27 21:42:22'),
(615, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"103.92.22.215\"}', '2018-10-28 19:28:31', '2018-10-28 19:28:31'),
(616, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"103.92.22.215\"}', '2018-10-29 02:33:13', '2018-10-29 02:33:13'),
(617, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"103.92.22.215\"}', '2018-10-29 03:21:21', '2018-10-29 03:21:21'),
(618, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"103.92.22.215\"}', '2018-10-29 09:45:55', '2018-10-29 09:45:55'),
(619, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"103.92.22.215\"}', '2018-10-29 09:47:49', '2018-10-29 09:47:49'),
(620, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"103.217.178.17\"}', '2018-10-29 13:57:39', '2018-10-29 13:57:39'),
(621, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.35\"}', '2018-10-29 19:43:50', '2018-10-29 19:43:50'),
(622, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.51\"}', '2018-10-29 23:42:56', '2018-10-29 23:42:56'),
(623, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-29 23:44:11', '2018-10-29 23:44:11'),
(624, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1350.00\"}}', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(625, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(626, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(627, 'default', 'created', 75, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(628, 'default', 'created', 76, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(629, 'default', 'created', 77, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(630, 'default', 'created', 78, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-29 23:44:12', '2018-10-29 23:44:12'),
(631, 'default', 'created', 3, 'Corals\\User\\Models\\User', NULL, NULL, '{\"attributes\":{\"name\":\"Demo\",\"email\":\"myidmailinator.com@mailinator.com\"}}', '2018-10-30 15:29:04', '2018-10-30 15:29:04'),
(632, 'default', 'Demo registered', NULL, NULL, 3, 'Corals\\User\\Models\\User', '{\"ip\":\"123.201.70.118\"}', '2018-10-30 15:29:04', '2018-10-30 15:29:04'),
(633, 'default', 'Demo logged In', NULL, NULL, 3, 'Corals\\User\\Models\\User', '{\"ip\":\"123.201.70.118\"}', '2018-10-30 15:29:05', '2018-10-30 15:29:05'),
(634, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"123.201.70.118\"}', '2018-10-30 18:56:53', '2018-10-30 18:56:53'),
(635, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"123.201.70.118\"}', '2018-10-30 19:04:47', '2018-10-30 19:04:47'),
(636, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"123.201.70.30\"}', '2018-10-30 19:05:19', '2018-10-30 19:05:19'),
(637, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"123.201.70.30\"}', '2018-10-30 21:07:26', '2018-10-30 21:07:26'),
(638, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"111.88.199.71\"}', '2018-10-31 01:57:31', '2018-10-31 01:57:31'),
(639, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-10-31 02:23:43', '2018-10-31 02:23:43'),
(640, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:23:44', '2018-10-31 02:23:44'),
(641, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:23:44', '2018-10-31 02:23:44'),
(642, 'default', 'created', 79, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:23:44', '2018-10-31 02:23:44'),
(643, 'default', 'created', 80, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:23:44', '2018-10-31 02:23:44'),
(644, 'default', 'updated', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:32:16', '2018-10-31 02:32:16'),
(645, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:32:16', '2018-10-31 02:32:16');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(646, 'default', 'created', 81, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:32:16', '2018-10-31 02:32:16'),
(647, 'default', 'created', 82, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:32:16', '2018-10-31 02:32:16'),
(648, 'default', 'updated', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:33:20', '2018-10-31 02:33:20'),
(649, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:33:20', '2018-10-31 02:33:20'),
(650, 'default', 'created', 83, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:33:20', '2018-10-31 02:33:20'),
(651, 'default', 'created', 84, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:33:20', '2018-10-31 02:33:20'),
(652, 'default', 'updated', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:34:43', '2018-10-31 02:34:43'),
(653, 'default', 'updated', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:35:14', '2018-10-31 02:35:14'),
(654, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:35:14', '2018-10-31 02:35:14'),
(655, 'default', 'created', 85, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:35:14', '2018-10-31 02:35:14'),
(656, 'default', 'created', 86, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:35:14', '2018-10-31 02:35:14'),
(657, 'default', 'updated', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:41:25', '2018-10-31 02:41:25'),
(658, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:41:25', '2018-10-31 02:41:25'),
(659, 'default', 'created', 87, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:41:25', '2018-10-31 02:41:25'),
(660, 'default', 'created', 88, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-10-31 02:41:25', '2018-10-31 02:41:25'),
(661, 'default', 'updated', 21, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"1000.00\"}}', '2018-10-31 02:41:38', '2018-10-31 02:41:38'),
(662, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"122.8.63.175\"}', '2018-11-01 08:33:23', '2018-11-01 08:33:23'),
(663, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-01 08:43:42', '2018-11-01 08:43:42'),
(664, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:43:42', '2018-11-01 08:43:42'),
(665, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:43:42', '2018-11-01 08:43:42'),
(666, 'default', 'created', 89, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:43:42', '2018-11-01 08:43:42'),
(667, 'default', 'created', 90, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:43:42', '2018-11-01 08:43:42'),
(668, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:43:45', '2018-11-01 08:43:45'),
(669, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:50:18', '2018-11-01 08:50:18'),
(670, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:50:18', '2018-11-01 08:50:18'),
(671, 'default', 'created', 91, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:50:18', '2018-11-01 08:50:18'),
(672, 'default', 'created', 92, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:50:18', '2018-11-01 08:50:18'),
(673, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:50:20', '2018-11-01 08:50:20'),
(674, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:50:28', '2018-11-01 08:50:28'),
(675, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:50:28', '2018-11-01 08:50:28'),
(676, 'default', 'created', 93, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:50:28', '2018-11-01 08:50:28'),
(677, 'default', 'created', 94, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-01 08:50:28', '2018-11-01 08:50:28'),
(678, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:50:30', '2018-11-01 08:50:30'),
(679, 'default', 'updated', 22, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"350.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"350.00\"}}', '2018-11-01 08:50:35', '2018-11-01 08:50:35'),
(680, 'exception', 'Gateway createPaymentToken failed Array\n(\n    [name] => AUTHENTICATION_FAILURE\n    [message] => Authentication failed due to invalid authentication credentials or a missing Authori...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"generatePaymentToken\",\"object\":\"CartController\",\"message\":\"Gateway createPaymentToken failed Array\\n(\\n    [name] => AUTHENTICATION_FAILURE\\n    [message] => Authentication failed due to invalid authentication credentials or a missing Authorization header.\\n    [links] => Array\\n        (\\n            [0] => Array\\n                (\\n                    [href] => https:\\/\\/developer.paypal.com\\/docs\\/api\\/overview\\/#error\\n                    [rel] => information_link\\n                )\\n\\n        )\\n\\n)\\n. \"}}', '2018-11-01 08:50:45', '2018-11-01 08:50:45'),
(681, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"39.51.49.210\"}', '2018-11-01 15:19:29', '2018-11-01 15:19:29'),
(682, 'default', 'updated', 26, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"},\"old\":{\"name\":\"Contact Lens Type\",\"slug\":\"contact-lens-type\"}}', '2018-11-01 15:21:54', '2018-11-01 15:21:54'),
(683, 'default', 'updated', 27, 'Corals\\Modules\\Ecommerce\\Models\\Category', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"},\"old\":{\"name\":\"Contact Lens Solutions\",\"slug\":\"contact-lens-solutions\"}}', '2018-11-01 15:22:14', '2018-11-01 15:22:14'),
(684, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"202.5.142.240\"}', '2018-11-02 16:18:29', '2018-11-02 16:18:29'),
(685, 'default', 'created', 4, 'Corals\\User\\Models\\User', NULL, NULL, '{\"attributes\":{\"name\":\"Ather\",\"email\":\"atheriqbal1@gmail.com\"}}', '2018-11-02 16:40:16', '2018-11-02 16:40:16'),
(686, 'default', 'Ather registered', NULL, NULL, 4, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.201\"}', '2018-11-02 16:40:16', '2018-11-02 16:40:16'),
(687, 'default', 'Ather logged In', NULL, NULL, 4, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.201\"}', '2018-11-02 16:40:16', '2018-11-02 16:40:16'),
(688, 'default', 'updated', 4, 'Corals\\User\\Models\\User', 4, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Ather\",\"email\":\"atheriqbal1@gmail.com\"},\"old\":{\"name\":\"Ather\",\"email\":\"atheriqbal1@gmail.com\"}}', '2018-11-02 16:41:35', '2018-11-02 16:41:35'),
(689, 'default', 'Ather logged Out', NULL, NULL, 4, 'Corals\\User\\Models\\User', '{\"ip\":\"202.52.32.201\"}', '2018-11-02 16:41:35', '2018-11-02 16:41:35'),
(690, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"110.93.236.103\"}', '2018-11-09 22:19:18', '2018-11-09 22:19:18'),
(691, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-09 22:20:04', '2018-11-09 22:20:04'),
(692, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2300.00\"}}', '2018-11-09 22:20:05', '2018-11-09 22:20:05'),
(693, 'exception', 'SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'leyeaxis\' cannot be null (SQL: insert into `ecommerce_order_lens` (`lens`, `leyesphere`, `leyecylinder`, `reyesphere`,...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'leyeaxis\' cannot be null (SQL: insert into `ecommerce_order_lens` (`lens`, `leyesphere`, `leyecylinder`, `reyesphere`, `reyecylinder`, `leyeaxis`, `reyeaxis`, `leyeadd`, `reyeadd`, `eyepd`, `leyepd`, `reyepd`, `type_name`, `order_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Distance\\/Clear, 0.00, 0.00, 0.00, 0.00, , , 0, 0, 46, 0, 0, Basic 1.50, 23, 1, 1, 2018-11-09 15:20:05, 2018-11-09 15:20:05)). \"}}', '2018-11-09 22:20:05', '2018-11-09 22:20:05'),
(694, 'default', 'updated', 142, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-09 22:44:31', '2018-11-09 22:44:31'),
(695, 'default', 'updated', 142, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-09 22:45:52', '2018-11-09 22:45:52'),
(696, 'default', 'updated', 1, 'Corals\\Modules\\Payment\\Models\\Currency', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-09 22:46:08', '2018-11-09 22:46:08'),
(697, 'default', 'updated', 1, 'Corals\\Modules\\Slider\\Models\\Slider', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Home Page Slider\"},\"old\":{\"name\":\"Home Page Slider\"}}', '2018-11-09 23:35:27', '2018-11-09 23:35:27'),
(698, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.17\"}', '2018-11-13 22:44:47', '2018-11-13 22:44:47'),
(699, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-13 22:52:57', '2018-11-13 22:52:57'),
(700, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2300.00\"}}', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(701, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(702, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(703, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(704, 'default', 'created', 95, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(705, 'default', 'created', 96, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(706, 'default', 'created', 97, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(707, 'default', 'created', 98, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(708, 'default', 'created', 99, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-13 22:52:58', '2018-11-13 22:52:58'),
(709, 'default', 'updated', 24, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2300.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2300.00\"}}', '2018-11-13 22:53:03', '2018-11-13 22:53:03'),
(710, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.63\"}', '2018-11-19 15:07:38', '2018-11-19 15:07:38'),
(711, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.63\"}', '2018-11-19 15:09:21', '2018-11-19 15:09:21'),
(712, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.233.63\"}', '2018-11-19 15:24:49', '2018-11-19 15:24:49'),
(713, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"45.116.232.53\"}', '2018-11-22 13:21:33', '2018-11-22 13:21:33'),
(714, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"::1\"}', '2018-11-22 03:23:46', '2018-11-22 03:23:46'),
(715, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"::1\"}', '2018-11-22 03:24:19', '2018-11-22 03:24:19'),
(716, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-22 05:14:15', '2018-11-22 05:14:15'),
(717, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-22 05:24:19', '2018-11-22 05:24:19'),
(718, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-22 05:26:27', '2018-11-22 05:26:27'),
(719, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, NULL, NULL, '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-11-23 00:20:46', '2018-11-23 00:20:46'),
(720, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 00:49:37', '2018-11-23 00:49:37'),
(721, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-23 00:51:09', '2018-11-23 00:51:09'),
(722, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-11-23 00:51:10', '2018-11-23 00:51:10'),
(723, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 00:51:10', '2018-11-23 00:51:10'),
(724, 'default', 'created', 100, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 00:51:10', '2018-11-23 00:51:10'),
(725, 'default', 'created', 101, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 00:51:10', '2018-11-23 00:51:10'),
(726, 'default', 'updated', 2, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"},\"old\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"}}', '2018-11-23 00:57:46', '2018-11-23 00:57:46'),
(727, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 00:58:00', '2018-11-23 00:58:00'),
(728, 'default', 'Corals Member logged In', NULL, NULL, 2, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 00:58:35', '2018-11-23 00:58:35'),
(729, 'default', 'updated', 2, 'Corals\\User\\Models\\User', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"},\"old\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"}}', '2018-11-23 01:01:26', '2018-11-23 01:01:26'),
(730, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\Order', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-11-23 01:01:27', '2018-11-23 01:01:27'),
(731, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 2, 'Corals\\User\\Models\\User', '[]', '2018-11-23 01:01:27', '2018-11-23 01:01:27'),
(732, 'default', 'created', 102, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 2, 'Corals\\User\\Models\\User', '[]', '2018-11-23 01:01:27', '2018-11-23 01:01:27'),
(733, 'default', 'created', 103, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 2, 'Corals\\User\\Models\\User', '[]', '2018-11-23 01:01:27', '2018-11-23 01:01:27'),
(734, 'default', 'updated', 26, 'Corals\\Modules\\Ecommerce\\Models\\Order', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-11-23 01:01:44', '2018-11-23 01:01:44'),
(735, 'default', 'updated', 26, 'Corals\\Modules\\Ecommerce\\Models\\Order', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-11-23 01:02:11', '2018-11-23 01:02:11'),
(736, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-11-23 01:02:16', '2018-11-23 01:02:16'),
(737, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1000.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1000.00\",\"sale_price\":null}}', '2018-11-23 01:02:16', '2018-11-23 01:02:16'),
(738, 'default', 'updated', 2, 'Corals\\User\\Models\\User', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"},\"old\":{\"name\":\"Corals Member\",\"email\":\"member@corals.io\"}}', '2018-11-23 01:32:23', '2018-11-23 01:32:23'),
(739, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\Order', 2, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"100.00\"}}', '2018-11-23 01:32:23', '2018-11-23 01:32:23'),
(740, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 2, 'Corals\\User\\Models\\User', '[]', '2018-11-23 01:32:23', '2018-11-23 01:32:23'),
(741, 'default', 'created', 104, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 2, 'Corals\\User\\Models\\User', '[]', '2018-11-23 01:32:23', '2018-11-23 01:32:23'),
(742, 'default', 'created', 105, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 2, 'Corals\\User\\Models\\User', '[]', '2018-11-23 01:32:24', '2018-11-23 01:32:24'),
(743, 'default', 'Corals Member logged Out', NULL, NULL, 2, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 02:40:02', '2018-11-23 02:40:02'),
(744, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 02:40:16', '2018-11-23 02:40:16'),
(745, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-23 05:35:57', '2018-11-23 05:35:57'),
(746, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"100.00\"}}', '2018-11-23 05:35:58', '2018-11-23 05:35:58'),
(747, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:35:58', '2018-11-23 05:35:58'),
(748, 'default', 'created', 106, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:35:59', '2018-11-23 05:35:59'),
(749, 'default', 'created', 107, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:35:59', '2018-11-23 05:35:59'),
(750, 'default', 'updated', 28, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"100.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"100.00\"}}', '2018-11-23 05:46:32', '2018-11-23 05:46:32'),
(751, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:46:32', '2018-11-23 05:46:32'),
(752, 'default', 'created', 108, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:46:32', '2018-11-23 05:46:32'),
(753, 'default', 'created', 109, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:46:32', '2018-11-23 05:46:32'),
(754, 'default', 'updated', 28, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"100.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"100.00\"}}', '2018-11-23 05:46:52', '2018-11-23 05:46:52'),
(755, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:46:52', '2018-11-23 05:46:52'),
(756, 'default', 'created', 110, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:46:52', '2018-11-23 05:46:52'),
(757, 'default', 'created', 111, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:46:53', '2018-11-23 05:46:53'),
(758, 'default', 'updated', 28, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"100.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"100.00\"}}', '2018-11-23 05:47:15', '2018-11-23 05:47:15'),
(759, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:47:15', '2018-11-23 05:47:15'),
(760, 'default', 'created', 112, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:47:15', '2018-11-23 05:47:15'),
(761, 'default', 'created', 113, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 05:47:15', '2018-11-23 05:47:15'),
(762, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 05:48:20', '2018-11-23 05:48:20'),
(763, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, NULL, NULL, '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-11-23 05:49:47', '2018-11-23 05:49:47'),
(764, 'exception', 'Undefined index: email (View: C:\\xampp\\htdocs\\laraship_ecom\\resources\\themes\\ecommerce-basic\\components\\address.blade.php) (View: C:\\xampp\\htdocs\\laraship_ecom\\resources\\themes\\eco...', NULL, NULL, NULL, NULL, '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Undefined index: email (View: C:\\\\xampp\\\\htdocs\\\\laraship_ecom\\\\resources\\\\themes\\\\ecommerce-basic\\\\components\\\\address.blade.php) (View: C:\\\\xampp\\\\htdocs\\\\laraship_ecom\\\\resources\\\\themes\\\\ecommerce-basic\\\\components\\\\address.blade.php). \"}}', '2018-11-23 06:13:13', '2018-11-23 06:13:13'),
(765, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 06:15:19', '2018-11-23 06:15:19'),
(766, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-23 06:28:02', '2018-11-23 06:28:02'),
(767, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"350.00\"}}', '2018-11-23 06:28:03', '2018-11-23 06:28:03'),
(768, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 06:28:03', '2018-11-23 06:28:03'),
(769, 'default', 'created', 114, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 06:28:03', '2018-11-23 06:28:03'),
(770, 'default', 'created', 115, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-23 06:28:03', '2018-11-23 06:28:03'),
(771, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-23 06:51:33', '2018-11-23 06:51:33'),
(772, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 01:19:12', '2018-11-26 01:19:12'),
(773, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-26 01:21:04', '2018-11-26 01:21:04'),
(774, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-11-26 01:21:06', '2018-11-26 01:21:06'),
(775, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:21:07', '2018-11-26 01:21:07'),
(776, 'default', 'created', 116, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:21:07', '2018-11-26 01:21:07'),
(777, 'default', 'created', 117, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:21:07', '2018-11-26 01:21:07'),
(778, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-26 01:25:11', '2018-11-26 01:25:11'),
(779, 'default', 'updated', 30, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"175.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-11-26 01:25:13', '2018-11-26 01:25:13'),
(780, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:25:14', '2018-11-26 01:25:14'),
(781, 'default', 'created', 118, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:25:14', '2018-11-26 01:25:14'),
(782, 'default', 'created', 119, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:25:14', '2018-11-26 01:25:14'),
(783, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-11-26 01:29:31', '2018-11-26 01:29:31'),
(784, 'default', 'updated', 30, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"175.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"175.00\"}}', '2018-11-26 01:29:33', '2018-11-26 01:29:33'),
(785, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:29:33', '2018-11-26 01:29:33'),
(786, 'default', 'created', 120, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:29:34', '2018-11-26 01:29:34'),
(787, 'default', 'created', 121, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-26 01:29:34', '2018-11-26 01:29:34'),
(788, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 01:29:50', '2018-11-26 01:29:50'),
(789, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 02:11:44', '2018-11-26 02:11:44'),
(790, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 02:43:44', '2018-11-26 02:43:44'),
(791, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 04:57:25', '2018-11-26 04:57:25'),
(792, 'default', 'Super User logged Out', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 05:10:52', '2018-11-26 05:10:52'),
(793, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-26 05:42:31', '2018-11-26 05:42:31'),
(794, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-27 02:28:13', '2018-11-27 02:28:13'),
(795, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-27 23:42:11', '2018-11-27 23:42:11'),
(796, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"test prod\",\"description\":\"<p>Lorem Ipsum dummy text<\\/p>\",\"caption\":\"test prod\",\"properties\":null}}', '2018-11-27 23:51:07', '2018-11-27 23:51:07'),
(797, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"150.00\",\"sale_price\":\"150.00\"}}', '2018-11-27 23:51:07', '2018-11-27 23:51:07'),
(798, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'categories\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`,...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'categories\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `categories`, `description`, `shipping`, `external_url`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (test, test, test, simple, 1, active, 2, , {\\\"width\\\":null,\\\"height\\\":null,\\\"length\\\":null,\\\"weight\\\":null,\\\"enabled\\\":0}, , 1, 1, 2018-11-28 06:29:24, 2018-11-28 06:29:24)). \"}}', '2018-11-28 01:29:25', '2018-11-28 01:29:25'),
(799, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba abc prod\",\"description\":\"<p>gaba abc prod<\\/p>\",\"caption\":\"gaba abc prod\",\"properties\":null}}', '2018-11-28 02:00:18', '2018-11-28 02:00:18'),
(800, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-11-28 02:00:19', '2018-11-28 02:00:19'),
(801, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::subproduct(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::subproduct(). \"}}', '2018-11-28 02:00:19', '2018-11-28 02:00:19'),
(802, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc prod\",\"description\":null,\"caption\":\"abc prod\",\"properties\":null}}', '2018-11-28 02:06:58', '2018-11-28 02:06:58'),
(803, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":\"200.00\"}}', '2018-11-28 02:06:58', '2018-11-28 02:06:58'),
(804, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `2`, `product_id`, `created_by`, `updated_by`, `updated_a...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `2`, `product_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Brown, Black, 12, 1, 1, 2018-11-28 07:06:58, 2018-11-28 07:06:58)). \"}}', '2018-11-28 02:06:58', '2018-11-28 02:06:58'),
(805, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"check test\",\"description\":null,\"caption\":\"check test\",\"properties\":null}}', '2018-11-28 02:16:16', '2018-11-28 02:16:16'),
(806, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"23.00\",\"sale_price\":\"23.00\"}}', '2018-11-28 02:16:16', '2018-11-28 02:16:16'),
(807, 'default', 'deleted', 13, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"check test\",\"description\":null,\"caption\":\"check test\",\"properties\":null}}', '2018-11-28 02:23:41', '2018-11-28 02:23:41'),
(808, 'default', 'deleted', 12, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc prod\",\"description\":null,\"caption\":\"abc prod\",\"properties\":null}}', '2018-11-28 02:24:06', '2018-11-28 02:24:06'),
(809, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"check test\",\"description\":\"<p>CZ<\\/p>\",\"caption\":\"check test\",\"properties\":null}}', '2018-11-28 02:27:54', '2018-11-28 02:27:54'),
(810, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"533.00\",\"sale_price\":null}}', '2018-11-28 02:27:54', '2018-11-28 02:27:54'),
(811, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `product_id`, `created_by`, `updated_by`, `updated_at`, `...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `product_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Blue, 14, 1, 1, 2018-11-28 07:27:54, 2018-11-28 07:27:54)). \"}}', '2018-11-28 02:27:54', '2018-11-28 02:27:54'),
(812, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc test\",\"description\":null,\"caption\":\"abc test\",\"properties\":null}}', '2018-11-28 02:31:03', '2018-11-28 02:31:03'),
(813, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2333.00\",\"sale_price\":null}}', '2018-11-28 02:31:03', '2018-11-28 02:31:03'),
(814, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `product_id`, `created_by`, `updated_by`, `updated_at`, `...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `product_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Blue, 15, 1, 1, 2018-11-28 07:31:04, 2018-11-28 07:31:04)). \"}}', '2018-11-28 02:31:04', '2018-11-28 02:31:04'),
(815, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-28 02:34:55', '2018-11-28 02:34:55'),
(816, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"232.00\",\"sale_price\":null}}', '2018-11-28 02:34:55', '2018-11-28 02:34:55'),
(817, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-28 02:35:57', '2018-11-28 02:35:57'),
(818, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"232.00\",\"sale_price\":null}}', '2018-11-28 02:35:57', '2018-11-28 02:35:57'),
(819, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-28 02:36:15', '2018-11-28 02:36:15'),
(820, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"232.00\",\"sale_price\":null}}', '2018-11-28 02:36:15', '2018-11-28 02:36:15'),
(821, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-28 02:37:07', '2018-11-28 02:37:07'),
(822, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"232.00\",\"sale_price\":null}}', '2018-11-28 02:37:07', '2018-11-28 02:37:07'),
(823, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"da\",\"description\":null,\"caption\":\"adf\",\"properties\":null}}', '2018-11-28 02:39:43', '2018-11-28 02:39:43'),
(824, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"232.00\",\"sale_price\":null}}', '2018-11-28 02:39:43', '2018-11-28 02:39:43'),
(825, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 02:39:43', '2018-11-28 02:39:43'),
(826, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 02:39:43', '2018-11-28 02:39:43'),
(827, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 02:39:43', '2018-11-28 02:39:43'),
(828, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"dada\",\"description\":null,\"caption\":\"dada\",\"properties\":null}}', '2018-11-28 03:18:37', '2018-11-28 03:18:37'),
(829, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"200.00\",\"sale_price\":null}}', '2018-11-28 03:18:37', '2018-11-28 03:18:37'),
(830, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"dada\",\"description\":null,\"caption\":\"dada\",\"properties\":null}}', '2018-11-28 03:24:45', '2018-11-28 03:24:45'),
(831, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2333.00\",\"sale_price\":null}}', '2018-11-28 03:24:45', '2018-11-28 03:24:45'),
(832, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"dada\",\"description\":null,\"caption\":\"dada\",\"properties\":null}}', '2018-11-28 03:28:37', '2018-11-28 03:28:37'),
(833, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2333.00\",\"sale_price\":null}}', '2018-11-28 03:28:38', '2018-11-28 03:28:38'),
(834, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `product_id`, `created_by`, `updated_by`, `updated_at`, `...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'1\' in \'field list\' (SQL: insert into `ecommerce_sub_product` (`1`, `product_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Black, 23, 1, 1, 2018-11-28 08:28:38, 2018-11-28 08:28:38)). \"}}', '2018-11-28 03:28:38', '2018-11-28 03:28:38'),
(835, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"testt\",\"description\":null,\"caption\":\"testt\",\"properties\":null}}', '2018-11-28 05:18:00', '2018-11-28 05:18:00'),
(836, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"150.00\",\"sale_price\":null}}', '2018-11-28 05:18:00', '2018-11-28 05:18:00'),
(837, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"testt\",\"description\":null,\"caption\":\"testt\",\"properties\":null}}', '2018-11-28 06:16:32', '2018-11-28 06:16:32'),
(838, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"150.00\",\"sale_price\":null}}', '2018-11-28 06:16:32', '2018-11-28 06:16:32'),
(839, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 06:16:33', '2018-11-28 06:16:33'),
(840, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 06:16:33', '2018-11-28 06:16:33'),
(841, 'default', 'deleted', 25, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"testt\",\"description\":null,\"caption\":\"testt\",\"properties\":null}}', '2018-11-28 06:48:58', '2018-11-28 06:48:58'),
(842, 'default', 'deleted', 24, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"testt\",\"description\":null,\"caption\":\"testt\",\"properties\":null}}', '2018-11-28 06:49:24', '2018-11-28 06:49:24'),
(843, 'default', 'deleted', 23, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"dada\",\"description\":null,\"caption\":\"dada\",\"properties\":null}}', '2018-11-28 06:49:50', '2018-11-28 06:49:50'),
(844, 'default', 'deleted', 22, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"dada\",\"description\":null,\"caption\":\"dada\",\"properties\":null}}', '2018-11-28 06:50:14', '2018-11-28 06:50:14'),
(845, 'default', 'deleted', 21, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"dada\",\"description\":null,\"caption\":\"dada\",\"properties\":null}}', '2018-11-28 06:50:38', '2018-11-28 06:50:38'),
(846, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"test gaba prod again\",\"description\":null,\"caption\":\"test gaba prod again\",\"properties\":null}}', '2018-11-28 06:53:01', '2018-11-28 06:53:01'),
(847, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"150.00\",\"sale_price\":\"150.00\"}}', '2018-11-28 06:53:01', '2018-11-28 06:53:01'),
(848, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 06:53:01', '2018-11-28 06:53:01'),
(849, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 06:53:01', '2018-11-28 06:53:01'),
(850, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 06:53:02', '2018-11-28 06:53:02'),
(851, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-28 06:53:02', '2018-11-28 06:53:02'),
(852, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-28 23:59:24', '2018-11-28 23:59:24'),
(853, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-29 00:50:27', '2018-11-29 00:50:27'),
(854, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"175.00\"}}', '2018-11-29 00:53:45', '2018-11-29 00:53:45'),
(855, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 00:53:45', '2018-11-29 00:53:45'),
(856, 'default', 'created', 122, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 00:53:46', '2018-11-29 00:53:46'),
(857, 'default', 'created', 123, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 00:53:46', '2018-11-29 00:53:46'),
(858, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 01:39:27', '2018-11-29 01:39:27'),
(859, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::addMedia(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::addMedia(). \"}}', '2018-11-29 01:39:27', '2018-11-29 01:39:27'),
(860, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'thumbnail\' in \'field list\' (SQL: update `ecommerce_colors` set `updated_at` = 2018-11-29 06:42:06, `thumbnail` = C:\\xampp\\tm...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'thumbnail\' in \'field list\' (SQL: update `ecommerce_colors` set `updated_at` = 2018-11-29 06:42:06, `thumbnail` = C:\\\\xampp\\\\tmp\\\\php24E3.tmp where `id` = 1). \"}}', '2018-11-29 01:42:06', '2018-11-29 01:42:06');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(861, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 01:43:56', '2018-11-29 01:43:56'),
(862, 'default', 'deleted', 1, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 01:45:51', '2018-11-29 01:45:51'),
(863, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 01:46:55', '2018-11-29 01:46:55'),
(864, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::addMedia(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::addMedia(). \"}}', '2018-11-29 01:46:55', '2018-11-29 01:46:55'),
(865, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 01:50:44', '2018-11-29 01:50:44'),
(866, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Brand', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc-d\"}}', '2018-11-29 01:55:40', '2018-11-29 01:55:40'),
(867, 'exception', 'SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry \'brown\' for key \'ecommerce_colors_name_unique\' (SQL: insert into `ecommerce_colors` (`title`, `created_by`, `u...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry \'brown\' for key \'ecommerce_colors_name_unique\' (SQL: insert into `ecommerce_colors` (`title`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (brown, 1, 1, 2018-11-29 07:20:10, 2018-11-29 07:20:10)). \"}}', '2018-11-29 02:20:10', '2018-11-29 02:20:10'),
(868, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:21:07', '2018-11-29 02:21:07'),
(869, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::addMedia(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::addMedia(). \"}}', '2018-11-29 02:21:07', '2018-11-29 02:21:07'),
(870, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:24:55', '2018-11-29 02:24:55'),
(871, 'default', 'deleted', 2, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:26:58', '2018-11-29 02:26:58'),
(872, 'default', 'deleted', 4, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:30:41', '2018-11-29 02:30:41'),
(873, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:31:31', '2018-11-29 02:31:31'),
(874, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::addMedia(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::addMedia(). \"}}', '2018-11-29 02:31:31', '2018-11-29 02:31:31'),
(875, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:33:52', '2018-11-29 02:33:52'),
(876, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::addMedia(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::addMedia(). \"}}', '2018-11-29 02:33:52', '2018-11-29 02:33:52'),
(877, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 02:44:51', '2018-11-29 02:44:51'),
(878, 'default', 'deleted', 5, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 03:28:05', '2018-11-29 03:28:05'),
(879, 'default', 'deleted', 7, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 03:28:24', '2018-11-29 03:28:24'),
(880, 'default', 'deleted', 6, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 03:32:33', '2018-11-29 03:32:33'),
(881, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 03:33:13', '2018-11-29 03:33:13'),
(882, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'thumbnail\' in \'field list\' (SQL: update `ecommerce_colors` set `updated_at` = 2018-11-29 08:36:34, `thumbnail` = C:\\xampp\\tm...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'thumbnail\' in \'field list\' (SQL: update `ecommerce_colors` set `updated_at` = 2018-11-29 08:36:34, `thumbnail` = C:\\\\xampp\\\\tmp\\\\phpF612.tmp where `id` = 8). \"}}', '2018-11-29 03:36:34', '2018-11-29 03:36:34'),
(883, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-29 03:39:53', '2018-11-29 03:39:53'),
(884, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-11-29 23:58:14', '2018-11-29 23:58:14'),
(885, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'thumbnail\' in \'field list\' (SQL: update `ecommerce_colors` set `title` = Golden, `updated_at` = 2018-11-30 05:27:43, `thumbn...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Color\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'thumbnail\' in \'field list\' (SQL: update `ecommerce_colors` set `title` = Golden, `updated_at` = 2018-11-30 05:27:43, `thumbnail` = C:\\\\xampp\\\\tmp\\\\php670E.tmp where `id` = 9). \"}}', '2018-11-30 00:27:44', '2018-11-30 00:27:44'),
(886, 'default', 'deleted', 9, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-30 00:28:06', '2018-11-30 00:28:06'),
(887, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-30 00:31:12', '2018-11-30 00:31:12'),
(888, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-30 00:33:08', '2018-11-30 00:33:08'),
(889, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-30 00:34:24', '2018-11-30 00:34:24'),
(890, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-30 00:36:11', '2018-11-30 00:36:11'),
(891, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-11-30 00:37:49', '2018-11-30 00:37:49'),
(892, 'default', 'deleted', 8, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Gaba Frame GB1\",\"description\":\"<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.<\\/p>\",\"caption\":\"Gaba Frame GB1\",\"properties\":[]}}', '2018-11-30 01:07:17', '2018-11-30 01:07:17'),
(893, 'default', 'deleted', 20, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"da\",\"description\":null,\"caption\":\"adf\",\"properties\":null}}', '2018-11-30 01:07:50', '2018-11-30 01:07:50'),
(894, 'default', 'deleted', 19, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-30 01:08:13', '2018-11-30 01:08:13'),
(895, 'default', 'deleted', 18, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-30 01:08:39', '2018-11-30 01:08:39'),
(896, 'default', 'deleted', 17, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-30 01:09:06', '2018-11-30 01:09:06'),
(897, 'default', 'deleted', 16, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba te\",\"description\":null,\"caption\":\"gaba te\",\"properties\":null}}', '2018-11-30 01:09:32', '2018-11-30 01:09:32'),
(898, 'default', 'deleted', 15, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc test\",\"description\":null,\"caption\":\"abc test\",\"properties\":null}}', '2018-11-30 01:09:57', '2018-11-30 01:09:57'),
(899, 'default', 'deleted', 14, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"check test\",\"description\":\"<p>CZ<\\/p>\",\"caption\":\"check test\",\"properties\":null}}', '2018-11-30 01:10:21', '2018-11-30 01:10:21'),
(900, 'default', 'deleted', 11, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba abc prod\",\"description\":\"<p>gaba abc prod<\\/p>\",\"caption\":\"gaba abc prod\",\"properties\":null}}', '2018-11-30 01:10:46', '2018-11-30 01:10:46'),
(901, 'default', 'deleted', 10, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"test prod\",\"description\":\"<p>Lorem Ipsum dummy text<\\/p>\",\"caption\":\"test prod\",\"properties\":null}}', '2018-11-30 01:11:12', '2018-11-30 01:11:12'),
(902, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-03 01:16:18', '2018-12-03 01:16:18'),
(903, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 01:32:33', '2018-12-03 01:32:33'),
(904, 'exception', 'Undefined variable: data. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Size\",\"message\":\"Undefined variable: data. \"}}', '2018-12-03 02:07:21', '2018-12-03 02:07:21'),
(905, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:20:36', '2018-12-03 02:20:36'),
(906, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:23:04', '2018-12-03 02:23:04'),
(907, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:25:15', '2018-12-03 02:25:15'),
(908, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:29:48', '2018-12-03 02:29:48'),
(909, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:31:32', '2018-12-03 02:31:32'),
(910, 'exception', 'Undefined variable: data. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Size\",\"message\":\"Undefined variable: data. \"}}', '2018-12-03 02:32:05', '2018-12-03 02:32:05'),
(911, 'default', 'deleted', 1, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:33:03', '2018-12-03 02:33:03'),
(912, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:33:51', '2018-12-03 02:33:51'),
(913, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:34:36', '2018-12-03 02:34:36'),
(914, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:35:03', '2018-12-03 02:35:03'),
(915, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:36:05', '2018-12-03 02:36:05'),
(916, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:36:37', '2018-12-03 02:36:37'),
(917, 'default', 'deleted', 1, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 02:37:21', '2018-12-03 02:37:21'),
(918, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'size\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `size`, `brand_id`, `status...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'size\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `size`, `brand_id`, `status`, `description`, `shipping`, `external_url`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (abbb, abbb, abbb, simple, 3, 1, active, , {\\\"width\\\":null,\\\"height\\\":null,\\\"length\\\":null,\\\"weight\\\":null,\\\"enabled\\\":0}, , 1, 1, 2018-12-03 10:10:46, 2018-12-03 10:10:46)). \"}}', '2018-12-03 05:10:47', '2018-12-03 05:10:47'),
(919, 'default', 'deleted', 26, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"test gaba prod again\",\"description\":null,\"caption\":\"test gaba prod again\",\"properties\":null}}', '2018-12-03 05:23:30', '2018-12-03 05:23:30'),
(920, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-abcd\",\"description\":null,\"caption\":\"gaba-abcd\",\"properties\":null}}', '2018-12-03 05:34:17', '2018-12-03 05:34:17'),
(921, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"70.00\",\"sale_price\":\"70.00\"}}', '2018-12-03 05:34:17', '2018-12-03 05:34:17'),
(922, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 05:34:18', '2018-12-03 05:34:18'),
(923, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 05:34:18', '2018-12-03 05:34:18'),
(924, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abbbb\",\"description\":null,\"caption\":\"abbbb\",\"properties\":null}}', '2018-12-03 05:39:06', '2018-12-03 05:39:06'),
(925, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"577.00\",\"sale_price\":\"577.00\"}}', '2018-12-03 05:39:06', '2018-12-03 05:39:06'),
(926, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 05:39:06', '2018-12-03 05:39:06'),
(927, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-03 05:39:06', '2018-12-03 05:39:06'),
(928, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-03 23:42:23', '2018-12-03 23:42:23'),
(929, 'default', 'updated', 28, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abbbb\",\"description\":null,\"caption\":\"abbbb\",\"properties\":[]},\"old\":{\"name\":\"abbbb\",\"description\":null,\"caption\":\"abbbb\",\"properties\":null}}', '2018-12-04 01:40:38', '2018-12-04 01:40:38'),
(930, 'exception', 'Undefined offset: 2. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 2. \"}}', '2018-12-04 01:56:06', '2018-12-04 01:56:06'),
(931, 'exception', 'SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry \'3\' for key \'PRIMARY\' (SQL: update `ecommerce_sub_product` set `color` = 8, `size` = 2, `price` = 1200, `quant...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry \'3\' for key \'PRIMARY\' (SQL: update `ecommerce_sub_product` set `color` = 8, `size` = 2, `price` = 1200, `quantity` = 1200, `id` = 3, `updated_at` = 2018-12-04 07:14:01 where `ecommerce_sub_product`.`product_id` = 28 and `ecommerce_sub_product`.`product_id` is not null). \"}}', '2018-12-04 02:14:02', '2018-12-04 02:14:02'),
(932, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-optics-abcd\",\"description\":null,\"caption\":\"gaba-optics-abcd\",\"properties\":null}}', '2018-12-04 02:53:50', '2018-12-04 02:53:50'),
(933, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"150.00\",\"sale_price\":\"150.00\"}}', '2018-12-04 02:53:50', '2018-12-04 02:53:50'),
(934, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-04 02:53:50', '2018-12-04 02:53:50'),
(935, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-04 02:53:50', '2018-12-04 02:53:50'),
(936, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-04 02:53:50', '2018-12-04 02:53:50'),
(937, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-04 02:53:51', '2018-12-04 02:53:51'),
(938, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-04 02:53:51', '2018-12-04 02:53:51'),
(939, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-04 02:53:51', '2018-12-04 02:53:51'),
(940, 'default', 'updated', 29, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-optics-abcd\",\"description\":null,\"caption\":\"gaba-optics-abcd\",\"properties\":[]},\"old\":{\"name\":\"gaba-optics-abcd\",\"description\":null,\"caption\":\"gaba-optics-abcd\",\"properties\":null}}', '2018-12-04 03:10:00', '2018-12-04 03:10:00'),
(941, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-04 06:29:50', '2018-12-04 06:29:50'),
(942, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-04 06:29:52', '2018-12-04 06:29:52'),
(943, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-04 06:29:54', '2018-12-04 06:29:54'),
(944, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-05 00:25:09', '2018-12-05 00:25:09'),
(945, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-sub-prod\",\"description\":null,\"caption\":\"gaba-sub-prod\",\"properties\":null}}', '2018-12-05 01:41:02', '2018-12-05 01:41:02'),
(946, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-05 01:41:02', '2018-12-05 01:41:02'),
(947, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-05 01:41:02', '2018-12-05 01:41:02'),
(948, 'default', 'updated', 30, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-sub-prod\",\"description\":null,\"caption\":\"gaba-sub-prod\",\"properties\":[]},\"old\":{\"name\":\"gaba-sub-prod\",\"description\":null,\"caption\":\"gaba-sub-prod\",\"properties\":null}}', '2018-12-05 01:41:49', '2018-12-05 01:41:49'),
(949, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-05 05:32:27', '2018-12-05 05:32:27'),
(950, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:04:00', '2018-12-05 06:04:00'),
(951, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:04:01', '2018-12-05 06:04:01'),
(952, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:04:02', '2018-12-05 06:04:02'),
(953, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:05:34', '2018-12-05 06:05:34'),
(954, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:05:44', '2018-12-05 06:05:44'),
(955, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:05:50', '2018-12-05 06:05:50'),
(956, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-05 06:05:53', '2018-12-05 06:05:53'),
(957, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-06 23:39:47', '2018-12-06 23:39:47'),
(958, 'exception', 'Undefined variable: subimg. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: subimg. \"}}', '2018-12-07 01:15:41', '2018-12-07 01:15:41'),
(959, 'exception', 'File `C:\\xampp\\tmp/php1844.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/php1844.tmp` does not exist. \"}}', '2018-12-07 01:52:44', '2018-12-07 01:52:44'),
(960, 'exception', 'File `C:\\xampp\\tmp/phpFEBC.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/phpFEBC.tmp` does not exist. \"}}', '2018-12-07 01:54:48', '2018-12-07 01:54:48'),
(961, 'exception', 'File `C:\\xampp\\tmp/php5F5C.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/php5F5C.tmp` does not exist. \"}}', '2018-12-07 02:02:52', '2018-12-07 02:02:52'),
(962, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-07 04:10:49', '2018-12-07 04:10:49'),
(963, 'exception', 'File `C:\\xampp\\tmp/php9CCF.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/php9CCF.tmp` does not exist. \"}}', '2018-12-07 06:36:10', '2018-12-07 06:36:10'),
(964, 'exception', 'File `C:\\xampp\\tmp/phpCAE3.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/phpCAE3.tmp` does not exist. \"}}', '2018-12-07 06:37:26', '2018-12-07 06:37:26'),
(965, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-09 23:49:48', '2018-12-09 23:49:48'),
(966, 'exception', 'File `C:\\xampp\\tmp/php1D9D.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/php1D9D.tmp` does not exist. \"}}', '2018-12-09 23:51:03', '2018-12-09 23:51:03'),
(967, 'exception', 'File `C:\\xampp\\tmp/php6A46.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/php6A46.tmp` does not exist. \"}}', '2018-12-10 00:00:07', '2018-12-10 00:00:07'),
(968, 'exception', 'Property [mediaCollectionName] does not exist on this collection instance.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Property [mediaCollectionName] does not exist on this collection instance.. \"}}', '2018-12-10 00:49:41', '2018-12-10 00:49:41'),
(969, 'exception', 'Undefined property: Illuminate\\Database\\Eloquent\\Relations\\HasMany::$mediaCollectionName. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined property: Illuminate\\\\Database\\\\Eloquent\\\\Relations\\\\HasMany::$mediaCollectionName. \"}}', '2018-12-10 00:51:03', '2018-12-10 00:51:03'),
(970, 'exception', 'File `C:\\xampp\\tmp/phpA9AE.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/phpA9AE.tmp` does not exist. \"}}', '2018-12-10 00:56:05', '2018-12-10 00:56:05'),
(971, 'exception', 'File `C:\\xampp\\tmp/php440A.tmp` does not exist. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"File `C:\\\\xampp\\\\tmp\\/php440A.tmp` does not exist. \"}}', '2018-12-10 00:58:56', '2018-12-10 00:58:56'),
(972, 'exception', 'Undefined offset: 6. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 6. \"}}', '2018-12-10 01:04:17', '2018-12-10 01:04:17'),
(973, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:53:54', '2018-12-10 06:53:54'),
(974, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:54:09', '2018-12-10 06:54:09'),
(975, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:54:25', '2018-12-10 06:54:25'),
(976, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-variant\",\"description\":null,\"caption\":\"gaba-variant\",\"properties\":null}}', '2018-12-10 06:57:38', '2018-12-10 06:57:38'),
(977, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-10 06:57:38', '2018-12-10 06:57:38'),
(978, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:38', '2018-12-10 06:57:38'),
(979, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:38', '2018-12-10 06:57:38'),
(980, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:38', '2018-12-10 06:57:38'),
(981, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:38', '2018-12-10 06:57:38'),
(982, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:39', '2018-12-10 06:57:39'),
(983, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:39', '2018-12-10 06:57:39'),
(984, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:39', '2018-12-10 06:57:39'),
(985, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:39', '2018-12-10 06:57:39'),
(986, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-10 06:57:39', '2018-12-10 06:57:39'),
(987, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-10 23:31:21', '2018-12-10 23:31:21'),
(988, 'default', 'deleted', 30, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-sub-prod\",\"description\":null,\"caption\":\"gaba-sub-prod\",\"properties\":[]}}', '2018-12-11 00:20:21', '2018-12-11 00:20:21'),
(989, 'default', 'deleted', 29, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-optics-abcd\",\"description\":null,\"caption\":\"gaba-optics-abcd\",\"properties\":[]}}', '2018-12-11 00:20:37', '2018-12-11 00:20:37'),
(990, 'default', 'deleted', 31, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-variant\",\"description\":null,\"caption\":\"gaba-variant\",\"properties\":null}}', '2018-12-11 00:20:51', '2018-12-11 00:20:51'),
(991, 'default', 'deleted', 28, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abbbb\",\"description\":null,\"caption\":\"abbbb\",\"properties\":[]}}', '2018-12-11 00:21:08', '2018-12-11 00:21:08'),
(992, 'default', 'deleted', 27, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-abcd\",\"description\":null,\"caption\":\"gaba-abcd\",\"properties\":null}}', '2018-12-11 00:21:26', '2018-12-11 00:21:26'),
(993, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-abc\",\"description\":null,\"caption\":\"gaba-abc\",\"properties\":null}}', '2018-12-11 00:23:37', '2018-12-11 00:23:37'),
(994, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":\"1500.00\"}}', '2018-12-11 00:23:37', '2018-12-11 00:23:37'),
(995, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 00:23:37', '2018-12-11 00:23:37'),
(996, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 00:23:37', '2018-12-11 00:23:37'),
(997, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 00:23:38', '2018-12-11 00:23:38'),
(998, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 00:23:38', '2018-12-11 00:23:38'),
(999, 'default', 'updated', 32, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-abc\",\"description\":null,\"caption\":\"gaba-abc\",\"properties\":[]},\"old\":{\"name\":\"gaba-abc\",\"description\":null,\"caption\":\"gaba-abc\",\"properties\":null}}', '2018-12-11 00:27:40', '2018-12-11 00:27:40'),
(1000, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":null}}', '2018-12-11 05:40:36', '2018-12-11 05:40:36'),
(1001, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"0.00\",\"sale_price\":\"1500.00\"}}', '2018-12-11 05:40:36', '2018-12-11 05:40:36'),
(1002, 'exception', 'Undefined offset: 2. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 2. \"}}', '2018-12-11 05:40:36', '2018-12-11 05:40:36'),
(1003, 'default', 'updated', 33, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":[]},\"old\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":null}}', '2018-12-11 05:41:41', '2018-12-11 05:41:41'),
(1004, 'exception', 'Undefined variable: subdata. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: subdata. \"}}', '2018-12-11 05:41:41', '2018-12-11 05:41:41'),
(1005, 'default', 'deleted', 33, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":[]}}', '2018-12-11 05:41:53', '2018-12-11 05:41:53'),
(1006, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":null}}', '2018-12-11 05:43:12', '2018-12-11 05:43:12'),
(1007, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-11 05:43:12', '2018-12-11 05:43:12'),
(1008, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 05:43:12', '2018-12-11 05:43:12'),
(1009, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 05:43:12', '2018-12-11 05:43:12'),
(1010, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-11 05:43:12', '2018-12-11 05:43:12'),
(1011, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":[]},\"old\":{\"name\":\"gaba-vv\",\"description\":null,\"caption\":\"gaba-vv\",\"properties\":null}}', '2018-12-11 05:43:57', '2018-12-11 05:43:57'),
(1012, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-11 05:55:41', '2018-12-11 05:55:41'),
(1013, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-11 05:55:57', '2018-12-11 05:55:57'),
(1014, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-11 05:56:07', '2018-12-11 05:56:07'),
(1015, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-11 23:51:11', '2018-12-11 23:51:11'),
(1016, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-new-test\",\"description\":\"<p>Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;<\\/p>\",\"caption\":\"gaba-new-test\",\"properties\":null}}', '2018-12-12 00:02:39', '2018-12-12 00:02:39'),
(1017, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-12 00:02:39', '2018-12-12 00:02:39'),
(1018, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-12 00:02:40', '2018-12-12 00:02:40'),
(1019, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-12 00:02:40', '2018-12-12 00:02:40'),
(1020, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-12 00:02:40', '2018-12-12 00:02:40'),
(1021, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-12 00:02:40', '2018-12-12 00:02:40'),
(1022, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-new-test\",\"description\":\"<p>Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;<\\/p>\",\"caption\":\"gaba-new-test\",\"properties\":[]},\"old\":{\"name\":\"gaba-new-test\",\"description\":\"<p>Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;<\\/p>\",\"caption\":\"gaba-new-test\",\"properties\":null}}', '2018-12-12 00:06:59', '2018-12-12 00:06:59'),
(1023, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-13 06:27:51', '2018-12-13 06:27:51'),
(1024, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 06:29:02', '2018-12-13 06:29:02'),
(1025, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2600.00\"}}', '2018-12-13 06:29:05', '2018-12-13 06:29:05'),
(1026, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:29:05', '2018-12-13 06:29:05'),
(1027, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:29:05', '2018-12-13 06:29:05'),
(1028, 'default', 'created', 124, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:29:06', '2018-12-13 06:29:06'),
(1029, 'default', 'created', 125, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:29:06', '2018-12-13 06:29:06'),
(1030, 'default', 'created', 126, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:29:06', '2018-12-13 06:29:06'),
(1031, 'default', 'created', 127, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:29:06', '2018-12-13 06:29:06'),
(1032, 'default', 'updated', 32, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2600.00\"}}', '2018-12-13 06:29:14', '2018-12-13 06:29:14'),
(1033, 'default', 'updated', 32, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"2600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2600.00\"}}', '2018-12-13 06:30:06', '2018-12-13 06:30:06'),
(1034, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:30:06', '2018-12-13 06:30:06'),
(1035, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:30:06', '2018-12-13 06:30:06'),
(1036, 'default', 'created', 128, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:30:07', '2018-12-13 06:30:07'),
(1037, 'default', 'created', 129, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:30:07', '2018-12-13 06:30:07'),
(1038, 'default', 'created', 130, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:30:07', '2018-12-13 06:30:07'),
(1039, 'default', 'created', 131, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 06:30:07', '2018-12-13 06:30:07'),
(1040, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 07:13:59', '2018-12-13 07:13:59'),
(1041, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::OrderSubProd(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::OrderSubProd(). \"}}', '2018-12-13 07:14:01', '2018-12-13 07:14:01'),
(1042, 'default', 'updated', 32, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2600.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"2600.00\"}}', '2018-12-13 07:15:12', '2018-12-13 07:15:12'),
(1043, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 07:23:27', '2018-12-13 07:23:27'),
(1044, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1100.00\"}}', '2018-12-13 07:23:29', '2018-12-13 07:23:29'),
(1045, 'exception', 'Property [id] does not exist on this collection instance.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Property [id] does not exist on this collection instance.. \"}}', '2018-12-13 07:23:29', '2018-12-13 07:23:29'),
(1046, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 07:24:45', '2018-12-13 07:24:45'),
(1047, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::OrderSubProd(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::OrderSubProd(). \"}}', '2018-12-13 07:24:46', '2018-12-13 07:24:46'),
(1048, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 07:26:07', '2018-12-13 07:26:07'),
(1049, 'exception', 'Call to undefined method Illuminate\\Database\\Query\\Builder::OrderSubProd(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Call to undefined method Illuminate\\\\Database\\\\Query\\\\Builder::OrderSubProd(). \"}}', '2018-12-13 07:26:08', '2018-12-13 07:26:08'),
(1050, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 07:31:27', '2018-12-13 07:31:27'),
(1051, 'default', 'updated', 33, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"1100.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1100.00\"}}', '2018-12-13 07:31:29', '2018-12-13 07:31:29'),
(1052, 'exception', 'Property [id] does not exist on this collection instance.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Property [id] does not exist on this collection instance.. \"}}', '2018-12-13 07:31:29', '2018-12-13 07:31:29'),
(1053, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-13 23:50:10', '2018-12-13 23:50:10');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1054, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 23:55:04', '2018-12-13 23:55:04'),
(1055, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-13 23:55:06', '2018-12-13 23:55:06'),
(1056, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 23:55:07', '2018-12-13 23:55:07'),
(1057, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 23:55:07', '2018-12-13 23:55:07'),
(1058, 'exception', 'Undefined variable: items_SubProd. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Undefined variable: items_SubProd. \"}}', '2018-12-13 23:55:08', '2018-12-13 23:55:08'),
(1059, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-13 23:56:46', '2018-12-13 23:56:46'),
(1060, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-13 23:56:49', '2018-12-13 23:56:49'),
(1061, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 23:56:50', '2018-12-13 23:56:50'),
(1062, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-13 23:56:50', '2018-12-13 23:56:50'),
(1063, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'updated_by\' in \'field list\' (SQL: insert into `ecommerce_order_subproduct` (`subprod_id`, `order_id`, `created_by`, `updated...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'updated_by\' in \'field list\' (SQL: insert into `ecommerce_order_subproduct` (`subprod_id`, `order_id`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (29, 34, 1, 1, 2018-12-14 04:56:50, 2018-12-14 04:56:50)). \"}}', '2018-12-13 23:56:50', '2018-12-13 23:56:50'),
(1064, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 00:01:28', '2018-12-14 00:01:28'),
(1065, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 00:01:30', '2018-12-14 00:01:30'),
(1066, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:30', '2018-12-14 00:01:30'),
(1067, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:30', '2018-12-14 00:01:30'),
(1068, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:30', '2018-12-14 00:01:30'),
(1069, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:30', '2018-12-14 00:01:30'),
(1070, 'default', 'created', 124, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:31', '2018-12-14 00:01:31'),
(1071, 'default', 'created', 125, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:31', '2018-12-14 00:01:31'),
(1072, 'default', 'created', 126, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:31', '2018-12-14 00:01:31'),
(1073, 'default', 'created', 127, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:01:31', '2018-12-14 00:01:31'),
(1074, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 00:05:27', '2018-12-14 00:05:27'),
(1075, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 00:05:29', '2018-12-14 00:05:29'),
(1076, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:30', '2018-12-14 00:05:30'),
(1077, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:30', '2018-12-14 00:05:30'),
(1078, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:31', '2018-12-14 00:05:31'),
(1079, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:31', '2018-12-14 00:05:31'),
(1080, 'default', 'created', 128, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:31', '2018-12-14 00:05:31'),
(1081, 'default', 'created', 129, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:32', '2018-12-14 00:05:32'),
(1082, 'default', 'created', 130, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:32', '2018-12-14 00:05:32'),
(1083, 'default', 'created', 131, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:05:32', '2018-12-14 00:05:32'),
(1084, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 00:06:12', '2018-12-14 00:06:12'),
(1085, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 00:06:23', '2018-12-14 00:06:23'),
(1086, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 00:06:26', '2018-12-14 00:06:26'),
(1087, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 00:06:48', '2018-12-14 00:06:48'),
(1088, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:06:58', '2018-12-14 00:06:58'),
(1089, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:06:58', '2018-12-14 00:06:58'),
(1090, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:06:58', '2018-12-14 00:06:58'),
(1091, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:06:59', '2018-12-14 00:06:59'),
(1092, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 00:13:58', '2018-12-14 00:13:58'),
(1093, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1600.00\"}}', '2018-12-14 00:14:00', '2018-12-14 00:14:00'),
(1094, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:00', '2018-12-14 00:14:00'),
(1095, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:01', '2018-12-14 00:14:01'),
(1096, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:01', '2018-12-14 00:14:01'),
(1097, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:01', '2018-12-14 00:14:01'),
(1098, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:01', '2018-12-14 00:14:01'),
(1099, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:01', '2018-12-14 00:14:01'),
(1100, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1101, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1102, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1103, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1104, 'default', 'created', 132, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1105, 'default', 'created', 133, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1106, 'default', 'created', 134, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:02', '2018-12-14 00:14:02'),
(1107, 'default', 'created', 135, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:03', '2018-12-14 00:14:03'),
(1108, 'default', 'created', 136, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:03', '2018-12-14 00:14:03'),
(1109, 'default', 'created', 137, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:03', '2018-12-14 00:14:03'),
(1110, 'default', 'created', 138, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:03', '2018-12-14 00:14:03'),
(1111, 'default', 'created', 139, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:03', '2018-12-14 00:14:03'),
(1112, 'default', 'created', 140, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:04', '2018-12-14 00:14:04'),
(1113, 'default', 'created', 141, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:14:04', '2018-12-14 00:14:04'),
(1114, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1600.00\"}}', '2018-12-14 00:14:09', '2018-12-14 00:14:09'),
(1115, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1600.00\"}}', '2018-12-14 00:24:57', '2018-12-14 00:24:57'),
(1116, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 00:25:00', '2018-12-14 00:25:00'),
(1117, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 00:25:01', '2018-12-14 00:25:01'),
(1118, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 00:25:01', '2018-12-14 00:25:01'),
(1119, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 00:25:01', '2018-12-14 00:25:01'),
(1120, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 00:25:01', '2018-12-14 00:25:01'),
(1121, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 00:25:02', '2018-12-14 00:25:02'),
(1122, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:25:02', '2018-12-14 00:25:02'),
(1123, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:25:02', '2018-12-14 00:25:02'),
(1124, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:25:02', '2018-12-14 00:25:02'),
(1125, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 00:25:02', '2018-12-14 00:25:02'),
(1126, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 00:30:20', '2018-12-14 00:30:20'),
(1127, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4000.00\"}}', '2018-12-14 00:30:22', '2018-12-14 00:30:22'),
(1128, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:22', '2018-12-14 00:30:22'),
(1129, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:22', '2018-12-14 00:30:22'),
(1130, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1131, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1132, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1133, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1134, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1135, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1136, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:23', '2018-12-14 00:30:23'),
(1137, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:24', '2018-12-14 00:30:24'),
(1138, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:24', '2018-12-14 00:30:24'),
(1139, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:24', '2018-12-14 00:30:24'),
(1140, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:24', '2018-12-14 00:30:24'),
(1141, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:24', '2018-12-14 00:30:24'),
(1142, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:24', '2018-12-14 00:30:24'),
(1143, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:25', '2018-12-14 00:30:25'),
(1144, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:25', '2018-12-14 00:30:25'),
(1145, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:25', '2018-12-14 00:30:25'),
(1146, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:25', '2018-12-14 00:30:25'),
(1147, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:30:25', '2018-12-14 00:30:25'),
(1148, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4000.00\"}}', '2018-12-14 00:30:30', '2018-12-14 00:30:30'),
(1149, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 00:58:09', '2018-12-14 00:58:09'),
(1150, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4000.00\"}}', '2018-12-14 00:58:12', '2018-12-14 00:58:12'),
(1151, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:12', '2018-12-14 00:58:12'),
(1152, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:12', '2018-12-14 00:58:12'),
(1153, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:12', '2018-12-14 00:58:12'),
(1154, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1155, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1156, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1157, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1158, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1159, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1160, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:13', '2018-12-14 00:58:13'),
(1161, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:14', '2018-12-14 00:58:14'),
(1162, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:14', '2018-12-14 00:58:14'),
(1163, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:14', '2018-12-14 00:58:14'),
(1164, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:14', '2018-12-14 00:58:14'),
(1165, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 00:58:14', '2018-12-14 00:58:14'),
(1166, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4000.00\"}}', '2018-12-14 00:58:18', '2018-12-14 00:58:18'),
(1167, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:02:58', '2018-12-14 01:02:58'),
(1168, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4000.00\"}}', '2018-12-14 01:03:03', '2018-12-14 01:03:03'),
(1169, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:03', '2018-12-14 01:03:03'),
(1170, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:03', '2018-12-14 01:03:03'),
(1171, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:04', '2018-12-14 01:03:04'),
(1172, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:04', '2018-12-14 01:03:04'),
(1173, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:04', '2018-12-14 01:03:04'),
(1174, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:04', '2018-12-14 01:03:04'),
(1175, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:05', '2018-12-14 01:03:05'),
(1176, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:05', '2018-12-14 01:03:05'),
(1177, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:05', '2018-12-14 01:03:05'),
(1178, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:05', '2018-12-14 01:03:05'),
(1179, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4000.00\"}}', '2018-12-14 01:03:08', '2018-12-14 01:03:08'),
(1180, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:08', '2018-12-14 01:03:08'),
(1181, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:08', '2018-12-14 01:03:08'),
(1182, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:08', '2018-12-14 01:03:08'),
(1183, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:08', '2018-12-14 01:03:08'),
(1184, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:09', '2018-12-14 01:03:09'),
(1185, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:09', '2018-12-14 01:03:09'),
(1186, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:09', '2018-12-14 01:03:09'),
(1187, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:09', '2018-12-14 01:03:09'),
(1188, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:09', '2018-12-14 01:03:09'),
(1189, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:03:09', '2018-12-14 01:03:09'),
(1190, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4000.00\"}}', '2018-12-14 01:03:12', '2018-12-14 01:03:12'),
(1191, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4000.00\"}}', '2018-12-14 01:03:14', '2018-12-14 01:03:14'),
(1192, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4000.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4000.00\"}}', '2018-12-14 01:03:33', '2018-12-14 01:03:33'),
(1193, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 01:03:36', '2018-12-14 01:03:36'),
(1194, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 01:03:36', '2018-12-14 01:03:36'),
(1195, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 01:03:36', '2018-12-14 01:03:36'),
(1196, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:03:37', '2018-12-14 01:03:37'),
(1197, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:03:37', '2018-12-14 01:03:37'),
(1198, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:15:29', '2018-12-14 01:15:29'),
(1199, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:15:31', '2018-12-14 01:15:31'),
(1200, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:15:31', '2018-12-14 01:15:31'),
(1201, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:15:31', '2018-12-14 01:15:31'),
(1202, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:15:36', '2018-12-14 01:15:36'),
(1203, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:16:51', '2018-12-14 01:16:51'),
(1204, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:16:53', '2018-12-14 01:16:53'),
(1205, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:17:00', '2018-12-14 01:17:00'),
(1206, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:17:12', '2018-12-14 01:17:12'),
(1207, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:18:29', '2018-12-14 01:18:29'),
(1208, 'exception', 'A non-numeric value encountered. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"Deduct\",\"object\":\"Inventory\",\"message\":\"A non-numeric value encountered. \"}}', '2018-12-14 01:18:29', '2018-12-14 01:18:29'),
(1209, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:22:13', '2018-12-14 01:22:13'),
(1210, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:22:15', '2018-12-14 01:22:15'),
(1211, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:22:15', '2018-12-14 01:22:15'),
(1212, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:22:15', '2018-12-14 01:22:15'),
(1213, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:22:20', '2018-12-14 01:22:20'),
(1214, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:22:31', '2018-12-14 01:22:31'),
(1215, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:22:32', '2018-12-14 01:22:32'),
(1216, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:25:47', '2018-12-14 01:25:47'),
(1217, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:25:49', '2018-12-14 01:25:49'),
(1218, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:25:49', '2018-12-14 01:25:49'),
(1219, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:25:49', '2018-12-14 01:25:49'),
(1220, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:25:54', '2018-12-14 01:25:54'),
(1221, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:26:08', '2018-12-14 01:26:08'),
(1222, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:26:09', '2018-12-14 01:26:09'),
(1223, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:29:05', '2018-12-14 01:29:05'),
(1224, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:29:06', '2018-12-14 01:29:06'),
(1225, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:29:07', '2018-12-14 01:29:07'),
(1226, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:29:07', '2018-12-14 01:29:07'),
(1227, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:29:13', '2018-12-14 01:29:13'),
(1228, 'default', 'updated', 5, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:29:23', '2018-12-14 01:29:23'),
(1229, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:29:25', '2018-12-14 01:29:25'),
(1230, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:32:12', '2018-12-14 01:32:12'),
(1231, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:32:14', '2018-12-14 01:32:14'),
(1232, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:32:15', '2018-12-14 01:32:15'),
(1233, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:32:15', '2018-12-14 01:32:15'),
(1234, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:32:19', '2018-12-14 01:32:19'),
(1235, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:32:29', '2018-12-14 01:32:29'),
(1236, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 01:32:30', '2018-12-14 01:32:30'),
(1237, 'default', 'updated', 6, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"completed\",\"amount\":\"5000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5000.00\"}}', '2018-12-14 01:38:28', '2018-12-14 01:38:28'),
(1238, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:50:10', '2018-12-14 01:50:10'),
(1239, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-12-14 01:50:12', '2018-12-14 01:50:12'),
(1240, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-12-14 01:50:12', '2018-12-14 01:50:12'),
(1241, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:54:53', '2018-12-14 01:54:53'),
(1242, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"0.00\"}}', '2018-12-14 01:54:54', '2018-12-14 01:54:54'),
(1243, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-12-14 01:54:55', '2018-12-14 01:54:55'),
(1244, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 01:57:43', '2018-12-14 01:57:43'),
(1245, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"400.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"200.00\"}}', '2018-12-14 01:57:45', '2018-12-14 01:57:45'),
(1246, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:57:45', '2018-12-14 01:57:45'),
(1247, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 01:57:46', '2018-12-14 01:57:46'),
(1248, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"400.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"400.00\"}}', '2018-12-14 01:57:51', '2018-12-14 01:57:51'),
(1249, 'default', 'updated', 7, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"400.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"400.00\"}}', '2018-12-14 01:58:04', '2018-12-14 01:58:04'),
(1250, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 01:58:06', '2018-12-14 01:58:06'),
(1251, 'exception', 'Undefined variable: order_items. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"Deduct\",\"object\":\"Inventory\",\"message\":\"Undefined variable: order_items. \"}}', '2018-12-14 01:58:06', '2018-12-14 01:58:06'),
(1252, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:02:44', '2018-12-14 02:02:44'),
(1253, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:02:46', '2018-12-14 02:02:46'),
(1254, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:02:46', '2018-12-14 02:02:46'),
(1255, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:02:47', '2018-12-14 02:02:47'),
(1256, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:02:54', '2018-12-14 02:02:54'),
(1257, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:03:24', '2018-12-14 02:03:24'),
(1258, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 02:03:26', '2018-12-14 02:03:26'),
(1259, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:04:11', '2018-12-14 02:04:11'),
(1260, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:04:12', '2018-12-14 02:04:12'),
(1261, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:04:12', '2018-12-14 02:04:12'),
(1262, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:04:16', '2018-12-14 02:04:16'),
(1263, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:04:30', '2018-12-14 02:04:30'),
(1264, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 02:04:31', '2018-12-14 02:04:31'),
(1265, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:05:37', '2018-12-14 02:05:37'),
(1266, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:05:37', '2018-12-14 02:05:37'),
(1267, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:05:37', '2018-12-14 02:05:37'),
(1268, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:05:41', '2018-12-14 02:05:41'),
(1269, 'default', 'updated', 8, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:06:04', '2018-12-14 02:06:04'),
(1270, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 02:06:05', '2018-12-14 02:06:05'),
(1271, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:09:06', '2018-12-14 02:09:06'),
(1272, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:09:09', '2018-12-14 02:09:09'),
(1273, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:09:09', '2018-12-14 02:09:09'),
(1274, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:09:10', '2018-12-14 02:09:10'),
(1275, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:09:14', '2018-12-14 02:09:14');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1276, 'default', 'updated', 9, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:09:27', '2018-12-14 02:09:27'),
(1277, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:09:29', '2018-12-14 02:09:29'),
(1278, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:14:58', '2018-12-14 02:14:58'),
(1279, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:15:01', '2018-12-14 02:15:01'),
(1280, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:15:01', '2018-12-14 02:15:01'),
(1281, 'default', 'created', 36, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:15:01', '2018-12-14 02:15:01'),
(1282, 'default', 'updated', 10, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:15:14', '2018-12-14 02:15:14'),
(1283, 'default', 'updated', 10, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"600.00\"}}', '2018-12-14 02:15:34', '2018-12-14 02:15:34'),
(1284, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:15:36', '2018-12-14 02:15:36'),
(1285, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:19:48', '2018-12-14 02:19:48'),
(1286, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:19:50', '2018-12-14 02:19:50'),
(1287, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:19:50', '2018-12-14 02:19:50'),
(1288, 'default', 'created', 37, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:19:51', '2018-12-14 02:19:51'),
(1289, 'default', 'updated', 11, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:19:55', '2018-12-14 02:19:55'),
(1290, 'default', 'updated', 11, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:20:08', '2018-12-14 02:20:08'),
(1291, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:20:09', '2018-12-14 02:20:09'),
(1292, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:41:34', '2018-12-14 02:41:34'),
(1293, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:41:36', '2018-12-14 02:41:36'),
(1294, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:41:36', '2018-12-14 02:41:36'),
(1295, 'default', 'created', 38, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:41:36', '2018-12-14 02:41:36'),
(1296, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:41:41', '2018-12-14 02:41:41'),
(1297, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:41:52', '2018-12-14 02:41:52'),
(1298, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:41:54', '2018-12-14 02:41:54'),
(1299, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:47:23', '2018-12-14 02:47:23'),
(1300, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:47:26', '2018-12-14 02:47:26'),
(1301, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:47:26', '2018-12-14 02:47:26'),
(1302, 'default', 'created', 39, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:47:26', '2018-12-14 02:47:26'),
(1303, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:47:31', '2018-12-14 02:47:31'),
(1304, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:47:54', '2018-12-14 02:47:54'),
(1305, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:47:56', '2018-12-14 02:47:56'),
(1306, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:51:35', '2018-12-14 02:51:35'),
(1307, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:51:37', '2018-12-14 02:51:37'),
(1308, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:51:37', '2018-12-14 02:51:37'),
(1309, 'default', 'created', 40, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:51:38', '2018-12-14 02:51:38'),
(1310, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:51:42', '2018-12-14 02:51:42'),
(1311, 'default', 'updated', 12, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"300.00\"}}', '2018-12-14 02:51:53', '2018-12-14 02:51:53'),
(1312, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:51:55', '2018-12-14 02:51:55'),
(1313, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 02:55:45', '2018-12-14 02:55:45'),
(1314, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:55:47', '2018-12-14 02:55:47'),
(1315, 'default', 'created', 36, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:55:47', '2018-12-14 02:55:47'),
(1316, 'default', 'created', 41, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 02:55:47', '2018-12-14 02:55:47'),
(1317, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:55:53', '2018-12-14 02:55:53'),
(1318, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 02:56:06', '2018-12-14 02:56:06'),
(1319, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 02:56:08', '2018-12-14 02:56:08'),
(1320, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 03:01:59', '2018-12-14 03:01:59'),
(1321, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 03:02:01', '2018-12-14 03:02:01'),
(1322, 'default', 'created', 37, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 03:02:01', '2018-12-14 03:02:01'),
(1323, 'default', 'created', 38, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 03:02:01', '2018-12-14 03:02:01'),
(1324, 'default', 'created', 42, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 03:02:02', '2018-12-14 03:02:02'),
(1325, 'default', 'created', 43, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 03:02:02', '2018-12-14 03:02:02'),
(1326, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"300.00\"}}', '2018-12-14 03:02:20', '2018-12-14 03:02:20'),
(1327, 'default', 'updated', 13, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"300.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"300.00\"}}', '2018-12-14 03:02:49', '2018-12-14 03:02:49'),
(1328, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 03:02:51', '2018-12-14 03:02:51'),
(1329, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 03:02:51', '2018-12-14 03:02:51'),
(1330, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 03:09:27', '2018-12-14 03:09:27'),
(1331, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 03:09:29', '2018-12-14 03:09:29'),
(1332, 'default', 'created', 39, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 03:09:30', '2018-12-14 03:09:30'),
(1333, 'default', 'created', 44, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 03:09:30', '2018-12-14 03:09:30'),
(1334, 'default', 'updated', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 03:09:35', '2018-12-14 03:09:35'),
(1335, 'default', 'updated', 14, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"200.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"200.00\"}}', '2018-12-14 03:09:45', '2018-12-14 03:09:45'),
(1336, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 03:09:47', '2018-12-14 03:09:47'),
(1337, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 03:11:08', '2018-12-14 03:11:08'),
(1338, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-14 04:55:14', '2018-12-14 04:55:14'),
(1339, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2600.00\"}}', '2018-12-14 04:55:16', '2018-12-14 04:55:16'),
(1340, 'default', 'created', 40, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:17', '2018-12-14 04:55:17'),
(1341, 'default', 'created', 41, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:17', '2018-12-14 04:55:17'),
(1342, 'default', 'created', 42, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:17', '2018-12-14 04:55:17'),
(1343, 'default', 'created', 43, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:17', '2018-12-14 04:55:17'),
(1344, 'default', 'created', 45, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:18', '2018-12-14 04:55:18'),
(1345, 'default', 'created', 46, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:18', '2018-12-14 04:55:18'),
(1346, 'default', 'created', 47, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:18', '2018-12-14 04:55:18'),
(1347, 'default', 'created', 48, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 04:55:18', '2018-12-14 04:55:18'),
(1348, 'default', 'updated', 15, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2600.00\"}}', '2018-12-14 04:55:26', '2018-12-14 04:55:26'),
(1349, 'default', 'updated', 15, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2600.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2600.00\"}}', '2018-12-14 04:55:38', '2018-12-14 04:55:38'),
(1350, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 04:55:40', '2018-12-14 04:55:40'),
(1351, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 04:55:41', '2018-12-14 04:55:41'),
(1352, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 04:55:41', '2018-12-14 04:55:41'),
(1353, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 04:55:41', '2018-12-14 04:55:41'),
(1354, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 04:56:24', '2018-12-14 04:56:24'),
(1355, 'default', 'updated', 34, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"},\"old\":{\"regular_price\":\"250.00\",\"sale_price\":\"250.00\"}}', '2018-12-14 04:56:24', '2018-12-14 04:56:24'),
(1356, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 04:56:25', '2018-12-14 04:56:25'),
(1357, 'default', 'updated', 35, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"},\"old\":{\"regular_price\":\"2500.00\",\"sale_price\":\"2500.00\"}}', '2018-12-14 04:56:25', '2018-12-14 04:56:25'),
(1358, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:07:24', '2018-12-14 06:07:24'),
(1359, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:07:52', '2018-12-14 06:07:52'),
(1360, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:08:15', '2018-12-14 06:08:15'),
(1361, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Size', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:08:41', '2018-12-14 06:08:41'),
(1362, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:09:22', '2018-12-14 06:09:22'),
(1363, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:10:24', '2018-12-14 06:10:24'),
(1364, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:10:59', '2018-12-14 06:10:59'),
(1365, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:11:37', '2018-12-14 06:11:37'),
(1366, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\Color', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:12:10', '2018-12-14 06:12:10'),
(1367, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"ST MICHEL\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"ST MICHEL\",\"properties\":null}}', '2018-12-14 06:15:59', '2018-12-14 06:15:59'),
(1368, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-14 06:15:59', '2018-12-14 06:15:59'),
(1369, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:15:59', '2018-12-14 06:15:59'),
(1370, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:15:59', '2018-12-14 06:15:59'),
(1371, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:15:59', '2018-12-14 06:15:59'),
(1372, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:16:00', '2018-12-14 06:16:00'),
(1373, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-14 06:16:00', '2018-12-14 06:16:00'),
(1374, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-14 06:34:35', '2018-12-14 06:34:35'),
(1375, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"ST MICHEL\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"ST MICHEL\",\"properties\":[]},\"old\":{\"name\":\"ST MICHEL\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"ST MICHEL\",\"properties\":null}}', '2018-12-14 06:34:50', '2018-12-14 06:34:50'),
(1376, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-17 00:09:40', '2018-12-17 00:09:40'),
(1377, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-17 02:15:00', '2018-12-17 02:15:00'),
(1378, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4650.00\"}}', '2018-12-17 02:15:01', '2018-12-17 02:15:01'),
(1379, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-12-17 02:15:02', '2018-12-17 02:15:02'),
(1380, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-17 02:15:28', '2018-12-17 02:15:28'),
(1381, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4650.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4650.00\"}}', '2018-12-17 02:15:30', '2018-12-17 02:15:30'),
(1382, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-12-17 02:15:30', '2018-12-17 02:15:30'),
(1383, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-17 02:17:50', '2018-12-17 02:17:50'),
(1384, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4650.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4650.00\"}}', '2018-12-17 02:17:52', '2018-12-17 02:17:52'),
(1385, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-12-17 02:17:52', '2018-12-17 02:17:52'),
(1386, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-17 02:21:54', '2018-12-17 02:21:54'),
(1387, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"Pending\",\"amount\":\"4650.00\"},\"old\":{\"status\":\"Pending\",\"amount\":\"4650.00\"}}', '2018-12-17 02:21:56', '2018-12-17 02:21:56'),
(1388, 'exception', 'Trying to get property \'id\' of non-object. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"checkoutStep\",\"object\":\"CheckOutController\",\"message\":\"Trying to get property \'id\' of non-object. \"}}', '2018-12-17 02:21:56', '2018-12-17 02:21:56'),
(1389, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-17 02:29:23', '2018-12-17 02:29:23'),
(1390, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-12-17 02:29:25', '2018-12-17 02:29:25'),
(1391, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 02:29:25', '2018-12-17 02:29:25'),
(1392, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 02:29:25', '2018-12-17 02:29:25'),
(1393, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 02:29:25', '2018-12-17 02:29:25'),
(1394, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 02:29:26', '2018-12-17 02:29:26'),
(1395, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-12-17 02:29:34', '2018-12-17 02:29:34'),
(1396, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-12-17 02:29:48', '2018-12-17 02:29:48'),
(1397, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-17 02:29:56', '2018-12-17 02:29:56'),
(1398, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-17 02:29:57', '2018-12-17 02:29:57'),
(1399, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-17 02:44:58', '2018-12-17 02:44:58'),
(1400, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-12-17 02:45:00', '2018-12-17 02:45:00'),
(1401, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 02:45:00', '2018-12-17 02:45:00'),
(1402, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 02:45:01', '2018-12-17 02:45:01'),
(1403, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-12-17 02:45:05', '2018-12-17 02:45:05'),
(1404, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"1000.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"1000.00\"}}', '2018-12-17 02:45:15', '2018-12-17 02:45:15'),
(1405, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-17 02:45:16', '2018-12-17 02:45:16'),
(1406, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 03:41:12', '2018-12-17 03:41:12'),
(1407, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 03:42:01', '2018-12-17 03:42:01'),
(1408, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 03:43:50', '2018-12-17 03:43:50'),
(1409, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 03:45:02', '2018-12-17 03:45:02'),
(1410, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:11:51', '2018-12-17 05:11:51'),
(1411, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:14:29', '2018-12-17 05:14:29'),
(1412, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:17:48', '2018-12-17 05:17:48'),
(1413, 'exception', 'count(): Parameter must be an array or an object that implements Countable. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"count(): Parameter must be an array or an object that implements Countable. \"}}', '2018-12-17 05:19:48', '2018-12-17 05:19:48'),
(1414, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:21:40', '2018-12-17 05:21:40'),
(1415, 'exception', 'Undefined variable: subimg. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: subimg. \"}}', '2018-12-17 05:27:40', '2018-12-17 05:27:40'),
(1416, 'exception', 'Array to string conversion. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Array to string conversion. \"}}', '2018-12-17 05:29:38', '2018-12-17 05:29:38'),
(1417, 'exception', 'A non-numeric value encountered. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"A non-numeric value encountered. \"}}', '2018-12-17 05:31:23', '2018-12-17 05:31:23'),
(1418, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:34:13', '2018-12-17 05:34:13'),
(1419, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:35:34', '2018-12-17 05:35:34'),
(1420, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'clr\' in \'field list\' (SQL: update `ecommerce_products` set `updated_at` = 2018-12-17 10:41:30, `clr` = 1 where `id` = 1). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'clr\' in \'field list\' (SQL: update `ecommerce_products` set `updated_at` = 2018-12-17 10:41:30, `clr` = 1 where `id` = 1). \"}}', '2018-12-17 05:41:31', '2018-12-17 05:41:31'),
(1421, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'clr\' in \'field list\' (SQL: update `ecommerce_products` set `updated_at` = 2018-12-17 10:44:28, `clr` = 1 where `id` = 1). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'clr\' in \'field list\' (SQL: update `ecommerce_products` set `updated_at` = 2018-12-17 10:44:28, `clr` = 1 where `id` = 1). \"}}', '2018-12-17 05:44:28', '2018-12-17 05:44:28'),
(1422, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:45:51', '2018-12-17 05:45:51'),
(1423, 'exception', 'Invalid argument supplied for foreach(). ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Invalid argument supplied for foreach(). \"}}', '2018-12-17 05:48:13', '2018-12-17 05:48:13'),
(1424, 'exception', 'Undefined variable: products. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: products. \"}}', '2018-12-17 06:02:07', '2018-12-17 06:02:07'),
(1425, 'exception', 'Method create does not exist.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Method create does not exist.. \"}}', '2018-12-17 06:03:30', '2018-12-17 06:03:30'),
(1426, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:04:58', '2018-12-17 06:04:58'),
(1427, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:04:58', '2018-12-17 06:04:58'),
(1428, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:04:58', '2018-12-17 06:04:58'),
(1429, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:04:58', '2018-12-17 06:04:58'),
(1430, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:04:59', '2018-12-17 06:04:59'),
(1431, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:15', '2018-12-17 06:08:15'),
(1432, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1433, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1434, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1435, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1436, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1437, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1438, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:16', '2018-12-17 06:08:16'),
(1439, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:17', '2018-12-17 06:08:17'),
(1440, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:08:17', '2018-12-17 06:08:17'),
(1441, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:19', '2018-12-17 06:14:19'),
(1442, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:19', '2018-12-17 06:14:19'),
(1443, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:19', '2018-12-17 06:14:19'),
(1444, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:19', '2018-12-17 06:14:19'),
(1445, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:19', '2018-12-17 06:14:19'),
(1446, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1447, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1448, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1449, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1450, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1451, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1452, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1453, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1454, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1455, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:20', '2018-12-17 06:14:20'),
(1456, 'default', 'created', 36, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:21', '2018-12-17 06:14:21'),
(1457, 'default', 'created', 37, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:21', '2018-12-17 06:14:21'),
(1458, 'default', 'created', 38, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:21', '2018-12-17 06:14:21'),
(1459, 'default', 'created', 39, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:21', '2018-12-17 06:14:21'),
(1460, 'default', 'created', 40, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:14:21', '2018-12-17 06:14:21'),
(1461, 'exception', 'Undefined variable: subimg. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: subimg. \"}}', '2018-12-17 06:17:33', '2018-12-17 06:17:33'),
(1462, 'exception', 'Undefined variable: subimg. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: subimg. \"}}', '2018-12-17 06:25:50', '2018-12-17 06:25:50'),
(1463, 'default', 'deleted', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"ST MICHEL\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"ST MICHEL\",\"properties\":[]}}', '2018-12-17 06:26:11', '2018-12-17 06:26:11'),
(1464, 'exception', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'sub_new\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `i...', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"store\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'sub_new\' in \'field list\' (SQL: insert into `ecommerce_products` (`name`, `caption`, `slug`, `type`, `brand_id`, `status`, `is_featured`, `sub_new`, `description`, `shipping`, `external_url`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (gaba-test, gaba-test, gaba-test, simple, 1, active, 1, , <p>Lorem Ipsum&nbsp;gaba-test&nbsp;Lorem Ipsum&nbsp;gaba-test&nbsp;Lorem Ipsum&nbsp;gaba-test&nbsp;Lorem Ipsum&nbsp;gaba-test<\\/p>, {\\\"width\\\":null,\\\"height\\\":null,\\\"length\\\":null,\\\"weight\\\":null,\\\"enabled\\\":0}, , 1, 1, 2018-12-17 11:28:00, 2018-12-17 11:28:00)). \"}}', '2018-12-17 06:28:00', '2018-12-17 06:28:00'),
(1465, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc-ddd\",\"description\":null,\"caption\":\"abc-ddd\",\"properties\":null}}', '2018-12-17 06:29:38', '2018-12-17 06:29:38'),
(1466, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-17 06:29:38', '2018-12-17 06:29:38'),
(1467, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:29:38', '2018-12-17 06:29:38'),
(1468, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:29:38', '2018-12-17 06:29:38'),
(1469, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:29:38', '2018-12-17 06:29:38'),
(1470, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"abc-ddd\",\"description\":null,\"caption\":\"abc-ddd\",\"properties\":[]},\"old\":{\"name\":\"abc-ddd\",\"description\":null,\"caption\":\"abc-ddd\",\"properties\":null}}', '2018-12-17 06:30:15', '2018-12-17 06:30:15'),
(1471, 'exception', 'Undefined variable: sub_new. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined variable: sub_new. \"}}', '2018-12-17 06:30:15', '2018-12-17 06:30:15'),
(1472, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:46:58', '2018-12-17 06:46:58'),
(1473, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:46:58', '2018-12-17 06:46:58'),
(1474, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:46:59', '2018-12-17 06:46:59'),
(1475, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:48:21', '2018-12-17 06:48:21'),
(1476, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:48:21', '2018-12-17 06:48:21');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1477, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:48:22', '2018-12-17 06:48:22'),
(1478, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:50:34', '2018-12-17 06:50:34'),
(1479, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:50:35', '2018-12-17 06:50:35'),
(1480, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:50:35', '2018-12-17 06:50:35'),
(1481, 'exception', 'Undefined offset: 3. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 3. \"}}', '2018-12-17 06:55:04', '2018-12-17 06:55:04'),
(1482, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:56:53', '2018-12-17 06:56:53'),
(1483, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:56:54', '2018-12-17 06:56:54'),
(1484, 'default', 'created', 15, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:56:54', '2018-12-17 06:56:54'),
(1485, 'default', 'created', 16, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:56:54', '2018-12-17 06:56:54'),
(1486, 'default', 'created', 17, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:58:15', '2018-12-17 06:58:15'),
(1487, 'default', 'created', 18, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:58:15', '2018-12-17 06:58:15'),
(1488, 'default', 'created', 19, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:58:15', '2018-12-17 06:58:15'),
(1489, 'exception', 'Undefined offset: 3. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 3. \"}}', '2018-12-17 06:58:15', '2018-12-17 06:58:15'),
(1490, 'default', 'created', 20, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:59:20', '2018-12-17 06:59:20'),
(1491, 'default', 'created', 21, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:59:20', '2018-12-17 06:59:20'),
(1492, 'default', 'created', 22, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:59:21', '2018-12-17 06:59:21'),
(1493, 'default', 'created', 23, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 06:59:21', '2018-12-17 06:59:21'),
(1494, 'default', 'created', 24, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:00:52', '2018-12-17 07:00:52'),
(1495, 'default', 'created', 25, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:00:52', '2018-12-17 07:00:52'),
(1496, 'default', 'created', 26, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:00:52', '2018-12-17 07:00:52'),
(1497, 'default', 'created', 27, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:00:53', '2018-12-17 07:00:53'),
(1498, 'exception', 'Undefined offset: 3. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 3. \"}}', '2018-12-17 07:08:01', '2018-12-17 07:08:01'),
(1499, 'exception', 'Undefined offset: 3. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"update\",\"object\":\"Corals\\\\Modules\\\\Ecommerce\\\\Models\\\\Product\",\"message\":\"Undefined offset: 3. \"}}', '2018-12-17 07:16:19', '2018-12-17 07:16:19'),
(1500, 'default', 'created', 28, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:21:39', '2018-12-17 07:21:39'),
(1501, 'default', 'created', 29, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:21:40', '2018-12-17 07:21:40'),
(1502, 'default', 'created', 30, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:21:40', '2018-12-17 07:21:40'),
(1503, 'default', 'created', 31, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:21:40', '2018-12-17 07:21:40'),
(1504, 'default', 'created', 32, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:23:26', '2018-12-17 07:23:26'),
(1505, 'default', 'created', 33, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:23:26', '2018-12-17 07:23:26'),
(1506, 'default', 'created', 34, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:23:26', '2018-12-17 07:23:26'),
(1507, 'default', 'created', 35, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:23:27', '2018-12-17 07:23:27'),
(1508, 'default', 'created', 36, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:24:53', '2018-12-17 07:24:53'),
(1509, 'default', 'created', 37, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:24:53', '2018-12-17 07:24:53'),
(1510, 'default', 'created', 38, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:24:53', '2018-12-17 07:24:53'),
(1511, 'default', 'created', 39, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:24:53', '2018-12-17 07:24:53'),
(1512, 'default', 'created', 40, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:27:49', '2018-12-17 07:27:49'),
(1513, 'default', 'created', 41, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-17 07:28:45', '2018-12-17 07:28:45'),
(1514, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-17 23:29:46', '2018-12-17 23:29:46'),
(1515, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-new-prod\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"gaba-new-prod\",\"properties\":null}}', '2018-12-18 00:22:45', '2018-12-18 00:22:45'),
(1516, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 00:22:45', '2018-12-18 00:22:45'),
(1517, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:22:46', '2018-12-18 00:22:46'),
(1518, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:22:46', '2018-12-18 00:22:46'),
(1519, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:22:46', '2018-12-18 00:22:46'),
(1520, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:22:46', '2018-12-18 00:22:46'),
(1521, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-new-prod\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"gaba-new-prod\",\"properties\":[]},\"old\":{\"name\":\"gaba-new-prod\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"gaba-new-prod\",\"properties\":null}}', '2018-12-18 00:23:49', '2018-12-18 00:23:49'),
(1522, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 00:27:59', '2018-12-18 00:27:59'),
(1523, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4150.00\"}}', '2018-12-18 00:28:00', '2018-12-18 00:28:00'),
(1524, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:28:00', '2018-12-18 00:28:00'),
(1525, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:28:00', '2018-12-18 00:28:00'),
(1526, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:28:01', '2018-12-18 00:28:01'),
(1527, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:28:01', '2018-12-18 00:28:01'),
(1528, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:28:01', '2018-12-18 00:28:01'),
(1529, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 00:28:01', '2018-12-18 00:28:01'),
(1530, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4150.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4150.00\"}}', '2018-12-18 00:28:06', '2018-12-18 00:28:06'),
(1531, 'default', 'updated', 4, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4150.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4150.00\"}}', '2018-12-18 00:28:12', '2018-12-18 00:28:12'),
(1532, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 00:28:15', '2018-12-18 00:28:15'),
(1533, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 00:28:15', '2018-12-18 00:28:15'),
(1534, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 00:28:15', '2018-12-18 00:28:15'),
(1535, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:35:04', '2018-12-18 00:35:04'),
(1536, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:35:19', '2018-12-18 00:35:19'),
(1537, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:36:31', '2018-12-18 00:36:31'),
(1538, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:36:41', '2018-12-18 00:36:41'),
(1539, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:36:42', '2018-12-18 00:36:42'),
(1540, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:36:42', '2018-12-18 00:36:42'),
(1541, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:40:49', '2018-12-18 00:40:49'),
(1542, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:40:49', '2018-12-18 00:40:49'),
(1543, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:40:50', '2018-12-18 00:40:50'),
(1544, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:41:23', '2018-12-18 00:41:23'),
(1545, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:42:35', '2018-12-18 00:42:35'),
(1546, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:43:46', '2018-12-18 00:43:46'),
(1547, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:44:24', '2018-12-18 00:44:24'),
(1548, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:45:16', '2018-12-18 00:45:16'),
(1549, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:45:32', '2018-12-18 00:45:32'),
(1550, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:45:42', '2018-12-18 00:45:42'),
(1551, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:47:14', '2018-12-18 00:47:14'),
(1552, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:47:27', '2018-12-18 00:47:27'),
(1553, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:54:05', '2018-12-18 00:54:05'),
(1554, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:54:05', '2018-12-18 00:54:05'),
(1555, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:54:06', '2018-12-18 00:54:06'),
(1556, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:55:32', '2018-12-18 00:55:32'),
(1557, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 00:55:33', '2018-12-18 00:55:33'),
(1558, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:07:15', '2018-12-18 01:07:15'),
(1559, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:07:15', '2018-12-18 01:07:15'),
(1560, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:36:10', '2018-12-18 01:36:10'),
(1561, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:36:13', '2018-12-18 01:36:13'),
(1562, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:36:24', '2018-12-18 01:36:24'),
(1563, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:37:46', '2018-12-18 01:37:46'),
(1564, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:37:48', '2018-12-18 01:37:48'),
(1565, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 01:39:46', '2018-12-18 01:39:46'),
(1566, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 01:51:13', '2018-12-18 01:51:13'),
(1567, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4450.00\"}}', '2018-12-18 01:51:15', '2018-12-18 01:51:15'),
(1568, 'default', 'created', 0, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 01:51:16', '2018-12-18 01:51:16'),
(1569, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 01:51:16', '2018-12-18 01:51:16'),
(1570, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderSubProd', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 01:51:16', '2018-12-18 01:51:16'),
(1571, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 01:51:16', '2018-12-18 01:51:16'),
(1572, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 01:51:17', '2018-12-18 01:51:17'),
(1573, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 01:51:17', '2018-12-18 01:51:17'),
(1574, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4450.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4450.00\"}}', '2018-12-18 01:51:22', '2018-12-18 01:51:22'),
(1575, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4450.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4450.00\"}}', '2018-12-18 01:51:34', '2018-12-18 01:51:34'),
(1576, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 01:51:42', '2018-12-18 01:51:42'),
(1577, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 01:51:42', '2018-12-18 01:51:42'),
(1578, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 01:51:42', '2018-12-18 01:51:42'),
(1579, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 02:19:19', '2018-12-18 02:19:19'),
(1580, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-12-18 02:19:21', '2018-12-18 02:19:21'),
(1581, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:19:22', '2018-12-18 02:19:22'),
(1582, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:19:22', '2018-12-18 02:19:22'),
(1583, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:19:22', '2018-12-18 02:19:22'),
(1584, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-12-18 02:19:26', '2018-12-18 02:19:26'),
(1585, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"2950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"2950.00\"}}', '2018-12-18 02:19:35', '2018-12-18 02:19:35'),
(1586, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:19:37', '2018-12-18 02:19:37'),
(1587, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:19:37', '2018-12-18 02:19:37'),
(1588, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 02:26:59', '2018-12-18 02:26:59'),
(1589, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5450.00\"}}', '2018-12-18 02:27:01', '2018-12-18 02:27:01'),
(1590, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:27:02', '2018-12-18 02:27:02'),
(1591, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:27:02', '2018-12-18 02:27:02'),
(1592, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:27:02', '2018-12-18 02:27:02'),
(1593, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:27:02', '2018-12-18 02:27:02'),
(1594, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:27:03', '2018-12-18 02:27:03'),
(1595, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5450.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5450.00\"}}', '2018-12-18 02:27:07', '2018-12-18 02:27:07'),
(1596, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"5450.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"5450.00\"}}', '2018-12-18 02:27:17', '2018-12-18 02:27:17'),
(1597, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:27:19', '2018-12-18 02:27:19'),
(1598, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:27:19', '2018-12-18 02:27:19'),
(1599, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:27:19', '2018-12-18 02:27:19'),
(1600, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:27:20', '2018-12-18 02:27:20'),
(1601, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 02:39:37', '2018-12-18 02:39:37'),
(1602, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4950.00\"}}', '2018-12-18 02:39:39', '2018-12-18 02:39:39'),
(1603, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:39:40', '2018-12-18 02:39:40'),
(1604, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:39:40', '2018-12-18 02:39:40'),
(1605, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:39:40', '2018-12-18 02:39:40'),
(1606, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:39:40', '2018-12-18 02:39:40'),
(1607, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 02:39:40', '2018-12-18 02:39:40'),
(1608, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4950.00\"}}', '2018-12-18 02:39:52', '2018-12-18 02:39:52'),
(1609, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"4950.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"4950.00\"}}', '2018-12-18 02:40:03', '2018-12-18 02:40:03'),
(1610, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:40:05', '2018-12-18 02:40:05'),
(1611, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:40:05', '2018-12-18 02:40:05'),
(1612, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:40:05', '2018-12-18 02:40:05'),
(1613, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 02:40:05', '2018-12-18 02:40:05'),
(1614, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 03:10:43', '2018-12-18 03:10:43'),
(1615, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"6450.00\"}}', '2018-12-18 03:10:45', '2018-12-18 03:10:45'),
(1616, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:10:45', '2018-12-18 03:10:45'),
(1617, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:10:45', '2018-12-18 03:10:45'),
(1618, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:10:45', '2018-12-18 03:10:45'),
(1619, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:10:46', '2018-12-18 03:10:46'),
(1620, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:10:46', '2018-12-18 03:10:46'),
(1621, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:10:46', '2018-12-18 03:10:46'),
(1622, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"6450.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"6450.00\"}}', '2018-12-18 03:10:51', '2018-12-18 03:10:51'),
(1623, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"6450.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"6450.00\"}}', '2018-12-18 03:11:19', '2018-12-18 03:11:19'),
(1624, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:11:28', '2018-12-18 03:11:28'),
(1625, 'exception', 'Undefined variable: value. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"Deduct\",\"object\":\"Inventory\",\"message\":\"Undefined variable: value. \"}}', '2018-12-18 03:11:28', '2018-12-18 03:11:28'),
(1626, 'default', 'updated', 1, 'Corals\\User\\Models\\User', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"},\"old\":{\"name\":\"Super User\",\"email\":\"superuser@corals.io\"}}', '2018-12-18 03:18:14', '2018-12-18 03:18:14'),
(1627, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"9400.00\"}}', '2018-12-18 03:18:16', '2018-12-18 03:18:16'),
(1628, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(1629, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderLens', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(1630, 'default', 'created', 1, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(1631, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(1632, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(1633, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(1634, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(1635, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\OrderItem', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(1636, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"9400.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"9400.00\"}}', '2018-12-18 03:18:23', '2018-12-18 03:18:23'),
(1637, 'default', 'updated', 1, 'Corals\\Modules\\Ecommerce\\Models\\Order', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"status\":\"pending\",\"amount\":\"9400.00\"},\"old\":{\"status\":\"pending\",\"amount\":\"9400.00\"}}', '2018-12-18 03:18:33', '2018-12-18 03:18:33'),
(1638, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:18:35', '2018-12-18 03:18:35'),
(1639, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:18:35', '2018-12-18 03:18:35'),
(1640, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:18:35', '2018-12-18 03:18:35'),
(1641, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:18:36', '2018-12-18 03:18:36'),
(1642, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:18:36', '2018-12-18 03:18:36'),
(1643, 'default', 'updated', 3, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"1500.00\",\"sale_price\":null},\"old\":{\"regular_price\":\"1500.00\",\"sale_price\":null}}', '2018-12-18 03:18:36', '2018-12-18 03:18:36'),
(1644, 'default', 'Super User logged In', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"ip\":\"127.0.0.1\"}', '2018-12-18 05:50:11', '2018-12-18 05:50:11'),
(1645, 'default', 'created', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-new-test\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"gaba-new-test\",\"properties\":null}}', '2018-12-18 05:53:27', '2018-12-18 05:53:27'),
(1646, 'default', 'created', 4, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"0.00\",\"sale_price\":null}}', '2018-12-18 05:53:28', '2018-12-18 05:53:28'),
(1647, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:53:28', '2018-12-18 05:53:28'),
(1648, 'default', 'created', 6, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:53:28', '2018-12-18 05:53:28'),
(1649, 'default', 'created', 7, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:53:28', '2018-12-18 05:53:28'),
(1650, 'default', 'created', 8, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:53:29', '2018-12-18 05:53:29'),
(1651, 'default', 'created', 9, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:53:29', '2018-12-18 05:53:29'),
(1652, 'exception', 'The given data was invalid.. ', NULL, NULL, 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"action\":\"destroy\",\"object\":\"Gallery\",\"message\":\"The given data was invalid.. \"}}', '2018-12-18 05:54:05', '2018-12-18 05:54:05'),
(1653, 'default', 'updated', 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"gaba-new-test\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"gaba-new-test\",\"properties\":[]},\"old\":{\"name\":\"gaba-new-test\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"gaba-new-test\",\"properties\":null}}', '2018-12-18 05:55:18', '2018-12-18 05:55:18'),
(1654, 'default', 'created', 3, 'Corals\\Modules\\Ecommerce\\Models\\Product', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"name\":\"ST MICHEL\",\"description\":\"<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.<\\/p>\",\"caption\":\"ST MICHEL\",\"properties\":null}}', '2018-12-18 05:58:39', '2018-12-18 05:58:39'),
(1655, 'default', 'created', 5, 'Corals\\Modules\\Ecommerce\\Models\\SKU', 1, 'Corals\\User\\Models\\User', '{\"attributes\":{\"regular_price\":\"2500.00\",\"sale_price\":null}}', '2018-12-18 05:58:39', '2018-12-18 05:58:39'),
(1656, 'default', 'created', 10, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:58:39', '2018-12-18 05:58:39'),
(1657, 'default', 'created', 11, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:58:39', '2018-12-18 05:58:39'),
(1658, 'default', 'created', 12, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:58:40', '2018-12-18 05:58:40'),
(1659, 'default', 'created', 13, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:58:40', '2018-12-18 05:58:40'),
(1660, 'default', 'created', 14, 'Corals\\Modules\\Ecommerce\\Models\\SubProduct', 1, 'Corals\\User\\Models\\User', '[]', '2018-12-18 05:58:40', '2018-12-18 05:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Computers', 'computers', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(2, 'Smartphone', 'smartphone', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(3, 'Gadgets', 'gadgets', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(4, 'Technology', 'technology', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(5, 'Engineer', 'engineer', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(6, 'Subscriptions', 'subscriptions', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(7, 'Billing', 'billing', 'active', 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44');

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_post`
--

INSERT INTO `category_post` (`post_id`, `category_id`) VALUES
(6, 3),
(6, 6),
(7, 6),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AX', 'Åland Islands'),
(3, 'AL', 'Albania'),
(4, 'DZ', 'Algeria'),
(5, 'AS', 'American Samoa'),
(6, 'AD', 'Andorra'),
(7, 'AO', 'Angola'),
(8, 'AI', 'Anguilla'),
(9, 'AQ', 'Antarctica'),
(10, 'AG', 'Antigua and Barbuda'),
(11, 'AR', 'Argentina'),
(12, 'AM', 'Armenia'),
(13, 'AW', 'Aruba'),
(14, 'AU', 'Australia'),
(15, 'AT', 'Austria'),
(16, 'AZ', 'Azerbaijan'),
(17, 'BS', 'Bahamas'),
(18, 'BH', 'Bahrain'),
(19, 'BD', 'Bangladesh'),
(20, 'BB', 'Barbados'),
(21, 'BY', 'Belarus'),
(22, 'BE', 'Belgium'),
(23, 'BZ', 'Belize'),
(24, 'BJ', 'Benin'),
(25, 'BM', 'Bermuda'),
(26, 'BT', 'Bhutan'),
(27, 'BO', 'Bolivia, Plurinational State of'),
(28, 'BQ', 'Bonaire, Sint Eustatius and Saba'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British Indian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CA', 'Canada'),
(41, 'CV', 'Cape Verde'),
(42, 'KY', 'Cayman Islands'),
(43, 'CF', 'Central African Republic'),
(44, 'TD', 'Chad'),
(45, 'CL', 'Chile'),
(46, 'CN', 'China'),
(47, 'CX', 'Christmas Island'),
(48, 'CC', 'Cocos (Keeling) Islands'),
(49, 'CO', 'Colombia'),
(50, 'KM', 'Comoros'),
(51, 'CG', 'Congo'),
(52, 'CD', 'Congo, the Democratic Republic of the'),
(53, 'CK', 'Cook Islands'),
(54, 'CR', 'Costa Rica'),
(55, 'CI', 'Côte d\'Ivoire'),
(56, 'HR', 'Croatia'),
(57, 'CU', 'Cuba'),
(58, 'CW', 'Curaçao'),
(59, 'CY', 'Cyprus'),
(60, 'CZ', 'Czech Republic'),
(61, 'DK', 'Denmark'),
(62, 'DJ', 'Djibouti'),
(63, 'DM', 'Dominica'),
(64, 'DO', 'Dominican Republic'),
(65, 'EC', 'Ecuador'),
(66, 'EG', 'Egypt'),
(67, 'SV', 'El Salvador'),
(68, 'GQ', 'Equatorial Guinea'),
(69, 'ER', 'Eritrea'),
(70, 'EE', 'Estonia'),
(71, 'ET', 'Ethiopia'),
(72, 'FK', 'Falkland Islands (Malvinas)'),
(73, 'FO', 'Faroe Islands'),
(74, 'FJ', 'Fiji'),
(75, 'FI', 'Finland'),
(76, 'FR', 'France'),
(77, 'GF', 'French Guiana'),
(78, 'PF', 'French Polynesia'),
(79, 'TF', 'French Southern Territories'),
(80, 'GA', 'Gabon'),
(81, 'GM', 'Gambia'),
(82, 'GE', 'Georgia'),
(83, 'DE', 'Germany'),
(84, 'GH', 'Ghana'),
(85, 'GI', 'Gibraltar'),
(86, 'GR', 'Greece'),
(87, 'GL', 'Greenland'),
(88, 'GD', 'Grenada'),
(89, 'GP', 'Guadeloupe'),
(90, 'GU', 'Guam'),
(91, 'GT', 'Guatemala'),
(92, 'GG', 'Guernsey'),
(93, 'GN', 'Guinea'),
(94, 'GW', 'Guinea-Bissau'),
(95, 'GY', 'Guyana'),
(96, 'HT', 'Haiti'),
(97, 'HM', 'Heard Island and McDonald Mcdonald Islands'),
(98, 'VA', 'Holy See (Vatican City State)'),
(99, 'HN', 'Honduras'),
(100, 'HK', 'Hong Kong'),
(101, 'HU', 'Hungary'),
(102, 'IS', 'Iceland'),
(103, 'IN', 'India'),
(104, 'ID', 'Indonesia'),
(105, 'IR', 'Iran, Islamic Republic of'),
(106, 'IQ', 'Iraq'),
(107, 'IE', 'Ireland'),
(108, 'IM', 'Isle of Man'),
(109, 'IL', 'Israel'),
(110, 'IT', 'Italy'),
(111, 'JM', 'Jamaica'),
(112, 'JP', 'Japan'),
(113, 'JE', 'Jersey'),
(114, 'JO', 'Jordan'),
(115, 'KZ', 'Kazakhstan'),
(116, 'KE', 'Kenya'),
(117, 'KI', 'Kiribati'),
(118, 'KP', 'Korea, Democratic People\'s Republic of'),
(119, 'KR', 'Korea, Republic of'),
(120, 'KW', 'Kuwait'),
(121, 'KG', 'Kyrgyzstan'),
(122, 'LA', 'Lao People\'s Democratic Republic'),
(123, 'LV', 'Latvia'),
(124, 'LB', 'Lebanon'),
(125, 'LS', 'Lesotho'),
(126, 'LR', 'Liberia'),
(127, 'LY', 'Libya'),
(128, 'LI', 'Liechtenstein'),
(129, 'LT', 'Lithuania'),
(130, 'LU', 'Luxembourg'),
(131, 'MO', 'Macao'),
(132, 'MK', 'Macedonia, the Former Yugoslav Republic of'),
(133, 'MG', 'Madagascar'),
(134, 'MW', 'Malawi'),
(135, 'MY', 'Malaysia'),
(136, 'MV', 'Maldives'),
(137, 'ML', 'Mali'),
(138, 'MT', 'Malta'),
(139, 'MH', 'Marshall Islands'),
(140, 'MQ', 'Martinique'),
(141, 'MR', 'Mauritania'),
(142, 'MU', 'Mauritius'),
(143, 'YT', 'Mayotte'),
(144, 'MX', 'Mexico'),
(145, 'FM', 'Micronesia, Federated States of'),
(146, 'MD', 'Moldova, Republic of'),
(147, 'MC', 'Monaco'),
(148, 'MN', 'Mongolia'),
(149, 'ME', 'Montenegro'),
(150, 'MS', 'Montserrat'),
(151, 'MA', 'Morocco'),
(152, 'MZ', 'Mozambique'),
(153, 'MM', 'Myanmar'),
(154, 'NA', 'Namibia'),
(155, 'NR', 'Nauru'),
(156, 'NP', 'Nepal'),
(157, 'NL', 'Netherlands'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine, State of'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Réunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'BL', 'Saint Barthélemy'),
(186, 'SH', 'Saint Helena, Ascension and Tristan da Cunha'),
(187, 'KN', 'Saint Kitts and Nevis'),
(188, 'LC', 'Saint Lucia'),
(189, 'MF', 'Saint Martin (French part)'),
(190, 'PM', 'Saint Pierre and Miquelon'),
(191, 'VC', 'Saint Vincent and the Grenadines'),
(192, 'WS', 'Samoa'),
(193, 'SM', 'San Marino'),
(194, 'ST', 'Sao Tome and Principe'),
(195, 'SA', 'Saudi Arabia'),
(196, 'SN', 'Senegal'),
(197, 'RS', 'Serbia'),
(198, 'SC', 'Seychelles'),
(199, 'SL', 'Sierra Leone'),
(200, 'SG', 'Singapore'),
(201, 'SX', 'Sint Maarten (Dutch part)'),
(202, 'SK', 'Slovakia'),
(203, 'SI', 'Slovenia'),
(204, 'SB', 'Solomon Islands'),
(205, 'SO', 'Somalia'),
(206, 'ZA', 'South Africa'),
(207, 'GS', 'South Georgia and the South Sandwich Islands'),
(208, 'SS', 'South Sudan'),
(209, 'ES', 'Spain'),
(210, 'LK', 'Sri Lanka'),
(211, 'SD', 'Sudan'),
(212, 'SR', 'Suriname'),
(213, 'SJ', 'Svalbard and Jan Mayen'),
(214, 'SZ', 'Swaziland'),
(215, 'SE', 'Sweden'),
(216, 'CH', 'Switzerland'),
(217, 'SY', 'Syrian Arab Republic'),
(218, 'TW', 'Taiwan'),
(219, 'TJ', 'Tajikistan'),
(220, 'TZ', 'Tanzania, United Republic of'),
(221, 'TH', 'Thailand'),
(222, 'TL', 'Timor-Leste'),
(223, 'TG', 'Togo'),
(224, 'TK', 'Tokelau'),
(225, 'TO', 'Tonga'),
(226, 'TT', 'Trinidad and Tobago'),
(227, 'TN', 'Tunisia'),
(228, 'TR', 'Turkey'),
(229, 'TM', 'Turkmenistan'),
(230, 'TC', 'Turks and Caicos Islands'),
(231, 'TV', 'Tuvalu'),
(232, 'UG', 'Uganda'),
(233, 'UA', 'Ukraine'),
(234, 'AE', 'United Arab Emirates'),
(235, 'GB', 'United Kingdom'),
(236, 'US', 'United States'),
(237, 'UM', 'United States Minor Outlying Islands'),
(238, 'UY', 'Uruguay'),
(239, 'UZ', 'Uzbekistan'),
(240, 'VU', 'Vanuatu'),
(241, 'VE', 'Venezuela, Bolivarian Republic of'),
(242, 'VN', 'Viet Nam'),
(243, 'VG', 'Virgin Islands, British'),
(244, 'VI', 'Virgin Islands, U.S.'),
(245, 'WF', 'Wallis and Futuna'),
(246, 'EH', 'Western Sahara'),
(247, 'YE', 'Yemen'),
(248, 'ZM', 'Zambia'),
(249, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange_rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `format`, `exchange_rate`, `active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'UAE Dirham', 'AED', 'AED', '1.0 AED', '35', 1, NULL, 1, '2017-12-03 09:15:04', '2018-11-09 22:46:08'),
(2, 'Afghanistan, Afghani', 'AFN', '؋', '؋1,0.00', '68.8225', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(3, 'Albania, Lek', 'ALL', 'Lek', '1,0.00Lek', '112.357515', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(4, 'Armenian Dram', 'AMD', '&#1423;', '1,0.00 &#1423;', '485.135', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(5, 'Netherlands Antillian Guilder', 'ANG', 'ƒ', 'ƒ1,0.00', '1.785537', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(6, 'Angola, Kwanza', 'AOA', 'Kz', 'Kz1,0.00', '165.9235', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(7, 'Argentine Peso', 'ARS', '$', '$ 1,0.00', '17.309393', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(8, 'Australian Dollar', 'AUD', '$', '$1,0.00', '1.313888', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(9, 'Aruban Guilder', 'AWG', 'ƒ', 'ƒ1,0.00', '1.786833', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(10, 'Azerbaijanian Manat', 'AZN', '₼', '1 0,00 ₼', '1.6985', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(11, 'Bosnia and Herzegovina, Convertible Marks', 'BAM', 'КМ', '1,0.00 КМ', '1.643914', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(12, 'Barbados Dollar', 'BBD', '$', '$1,0.00', '2', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(13, 'Bangladesh, Taka', 'BDT', '৳', '৳ 1,0.', '82.368', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(14, 'Bulgarian Lev', 'BGN', 'лв.', '1 0,00 лв.', '1.645125', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(15, 'Bahraini Dinar', 'BHD', '.د.', '.د. 1,0.000', '0.376976', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(16, 'Burundi Franc', 'BIF', 'FBu', '1,0.FBu', '1755.05', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(17, 'Bermudian Dollar', 'BMD', '$', '$1,0.00', '1', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(18, 'Brunei Dollar', 'BND', '$', '$1,0.', '1.347105', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(19, 'Bolivia, Boliviano', 'BOB', 'Bs', 'Bs 1,0.00', '6.911986', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(20, 'Brazilian Real', 'BRL', 'R$', 'R$ 1,0.00', '3.257225', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(21, 'Bahamian Dollar', 'BSD', '$', '$1,0.00', '1', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(22, 'Bhutan, Ngultrum', 'BTN', 'Nu.', 'Nu. 1,0.0', '64.5066', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(23, 'Botswana, Pula', 'BWP', 'P', 'P1,0.00', '10.360258', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(24, 'Belarussian Ruble', 'BYR', 'р.', '1 0,00 р.', '0', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:15:04'),
(25, 'Belize Dollar', 'BZD', 'BZ$', 'BZ$1,0.00', '2.010548', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(26, 'Canadian Dollar', 'CAD', '$', '$1,0.00', '1.26852', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(27, 'Franc Congolais', 'CDF', 'FC', '1,0.00FC', '1576', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(28, 'Swiss Franc', 'CHF', 'CHF', '1\'0.00 CHF', '0.97595', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(29, 'Chilean Peso', 'CLP', '$', '$ 1,0.00', '646.98', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(30, 'China Yuan Renminbi', 'CNY', '¥', '¥1,0.00', '6.6158', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(31, 'Colombian Peso', 'COP', '$', '$ 1,0.00', '3017.75', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(32, 'Costa Rican Colon', 'CRC', '₡', '₡1,0.00', '566.365', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(33, 'Cuban Convertible Peso', 'CUC', 'CUC', 'CUC1,0.00', '1', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(34, 'Cuban Peso', 'CUP', '$MN', '$MN1,0.00', '25.5', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(35, 'Cape Verde Escudo', 'CVE', '$', '$1,0.00', '92.9', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(36, 'Czech Koruna', 'CZK', 'Kč', '1 0,00 Kč', '21.49', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(37, 'Djibouti Franc', 'DJF', 'Fdj', '1,0.Fdj', '178.77', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(38, 'Danish Krone', 'DKK', 'kr.', '1 0,00 kr.', '6.255485', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(39, 'Dominican Peso', 'DOP', 'RD$', 'RD$1,0.00', '48.183729', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(40, 'Algerian Dinar', 'DZD', 'د.ج‏', 'د.ج‏ 1,0.00', '114.697628', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(41, 'Egyptian Pound', 'EGP', 'ج.م', 'ج.م 1,0.00', '17.673', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(42, 'Eritrea, Nakfa', 'ERN', 'Nfk', '1,0.00Nfk', '15.250973', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(43, 'Ethiopian Birr', 'ETB', 'ETB', 'ETB1,0.00', '27.393', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(44, 'Euro', 'EUR', '€', '1.0,00 €', '0.84053', 0, NULL, 1, '2017-12-03 09:15:04', '2018-09-25 18:20:45'),
(45, 'Fiji Dollar', 'FJD', '$', '$1,0.00', '2.071754', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(46, 'Falkland Islands Pound', 'FKP', '£', '£1,0.00', '0.742255', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(47, 'Pound Sterling', 'GBP', '£', '£1,0.00', '0.742255', 0, NULL, 1, '2017-12-03 09:15:04', '2018-09-25 18:21:01'),
(48, 'Georgia, Lari', 'GEL', 'Lari', '1 0,00 Lari', '2.710412', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(49, 'Ghana Cedi', 'GHS', '₵', '₵1,0.00', '4.497159', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(50, 'Gibraltar Pound', 'GIP', '£', '£1,0.00', '0.742255', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(51, 'Gambia, Dalasi', 'GMD', 'D', '1,0.00D', '47.4', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(52, 'Guatemala, Quetzal', 'GTQ', 'Q', 'Q1,0.00', '7.351959', 0, NULL, NULL, '2017-12-03 09:15:04', '2017-12-03 09:00:04'),
(53, 'Guyana Dollar', 'GYD', '$', '$1,0.00', '207.955', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(54, 'Hong Kong Dollar', 'HKD', 'HK$', 'HK$1,0.00', '7.81201', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(55, 'Honduras, Lempira', 'HNL', 'L.', 'L. 1,0.00', '23.659145', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(56, 'Croatian Kuna', 'HRK', 'kn', '1,0.00 kn', '6.3502', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(57, 'Haiti, Gourde', 'HTG', 'G', 'G1,0.00', '64.237587', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(58, 'Hungary, Forint', 'HUF', 'Ft', '1 0,00 Ft', '263.818', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(59, 'Indonesia, Rupiah', 'IDR', 'Rp', 'Rp1,0.', '13521.099889', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(60, 'New Israeli Shekel', 'ILS', '₪', '₪ 1,0.00', '3.48543', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(61, 'Indian Rupee', 'INR', '₹', '1,0.00₹', '64.5105', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(62, 'Iraqi Dinar', 'IQD', 'د.ع.‏', 'د.ع.‏ 1,0.00', '1192.35', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(63, 'Iranian Rial', 'IRR', '﷼', '﷼ 1,0/00', '34782', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(64, 'Iceland Krona', 'ISK', 'kr.', '1,0. kr.', '103.25157', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(65, 'Jamaican Dollar', 'JMD', 'J$', 'J$1,0.00', '125.575', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(66, 'Jordanian Dinar', 'JOD', 'د.ا.‏', 'د.ا.‏ 1,0.000', '0.709503', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(67, 'Japan, Yen', 'JPY', '¥', '¥1,0.', '112.125', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(68, 'Kenyan Shilling', 'KES', 'S', 'S1,0.00', '103.016172', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(69, 'Kyrgyzstan, Som', 'KGS', 'сом', '1 0-00 сом', '69.707451', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(70, 'Cambodia, Riel', 'KHR', '៛', '1,0.៛', '4036.15', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(71, 'Comoro Franc', 'KMF', 'CF', '1,0.00CF', '414.125', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(72, 'North Korean Won', 'KPW', '₩', '₩1,0.', '900', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(73, 'South Korea, Won', 'KRW', '₩', '₩1,0.', '1083.3', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(74, 'Kuwaiti Dinar', 'KWD', 'دينار‎‎‏', 'دينار‎‎‏ 1,0.000', '0.301489', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(75, 'Cayman Islands Dollar', 'KYD', '$', '$1,0.00', '0.833619', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(76, 'Kazakhstan, Tenge', 'KZT', '₸', '₸1 0-00', '331.37', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(77, 'Laos, Kip', 'LAK', '₭', '1,0.₭', '8325.35', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(78, 'Lebanese Pound', 'LBP', 'ل.ل.‏', 'ل.ل.‏ 1,0.00', '1516.4', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(79, 'Sri Lanka Rupee', 'LKR', '₨', '₨ 1,0.', '153.64', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(80, 'Liberian Dollar', 'LRD', '$', '$1,0.00', '125.525', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(81, 'Lesotho, Loti', 'LSL', 'M', '1,0.00M', '13.765', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(82, 'Libyan Dinar', 'LYD', 'د.ل.‏', 'د.ل.‏1,0.000', '1.364095', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(83, 'Moroccan Dirham', 'MAD', 'د.م.‏', 'د.م.‏ 1,0.00', '9.394867', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(84, 'Moldovan Leu', 'MDL', 'lei', '1,0.00 lei', '17.199962', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(85, 'Malagasy Ariary', 'MGA', 'Ar', 'Ar1,0.', '3215.9', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(86, 'Macedonia, Denar', 'MKD', 'ден.', '1,0.00 ден.', '51.79', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(87, 'Myanmar, Kyat', 'MMK', 'K', 'K1,0.00', '1361.65', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(88, 'Mongolia, Tugrik', 'MNT', '₮', '₮1 0,00', '2439.11045', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(89, 'Macao, Pataca', 'MOP', 'MOP$', 'MOP$1,0.00', '8.047544', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(90, 'Mauritania, Ouguiya', 'MRO', 'UM', '1,0.00UM', '355.305', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(91, 'Maltese Lira', 'MTL', '₤', '₤1,0.00', '0', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:15:05'),
(92, 'Mauritius Rupee', 'MUR', '₨', '₨1,0.00', '33.485333', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(93, 'Maldives, Rufiyaa', 'MVR', 'MVR', '1,0.0 MVR', '15.400126', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(94, 'Malawi, Kwacha', 'MWK', 'MK', 'MK1,0.00', '726.660938', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(95, 'Mexican Peso', 'MXN', '$', '$1,0.00', '18.625775', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(96, 'Malaysian Ringgit', 'MYR', 'RM', 'RM1,0.00', '4.089981', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(97, 'Mozambique Metical', 'MZN', 'MT', 'MT1,0.', '61', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(98, 'Namibian Dollar', 'NAD', '$', '$1,0.00', '13.739931', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(99, 'Nigeria, Naira', 'NGN', '₦', '₦1,0.00', '360.1', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(100, 'Nicaragua, Cordoba Oro', 'NIO', 'C$', 'C$ 1,0.00', '30.834', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(101, 'Norwegian Krone', 'NOK', 'kr', '1.0,00 kr', '8.283632', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(102, 'Nepalese Rupee', 'NPR', '₨', '₨1,0.00', '103.22', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(103, 'New Zealand Dollar', 'NZD', '$', '$1,0.00', '1.4521', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(104, 'Rial Omani', 'OMR', '﷼', '﷼ 1,0.000', '0.38484', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(105, 'Panama, Balboa', 'PAB', 'B/.', 'B/. 1,0.00', '1', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(106, 'Peru, Nuevo Sol', 'PEN', 'S/.', 'S/. 1,0.00', '3.233584', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(107, 'Papua New Guinea, Kina', 'PGK', 'K', 'K1,0.00', '3.212984', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(108, 'Philippine Peso', 'PHP', '₱', '₱1,0.00', '50.258', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(109, 'Pakistan Rupee', 'PKR', 'PKR', 'PKR 1.0', '1', 1, NULL, 1, '2017-12-03 09:15:05', '2018-10-07 20:31:01'),
(110, 'Poland, Zloty', 'PLN', 'zł', '1 0,00 zł', '3.54235', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(111, 'Paraguay, Guarani', 'PYG', '₲', '₲ 1,0.00', '5670.85', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(112, 'Qatari Rial', 'QAR', '﷼', '﷼ 1,0.00', '3.6409', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(113, 'Romania, New Leu', 'RON', 'lei', '1,0.00 lei', '3.894307', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(114, 'Serbian Dinar', 'RSD', 'Дин.', '1,0.00 Дин.', '100.34', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(115, 'Russian Ruble', 'RUB', '₽', '1 0,00 ₽', '58.8891', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(116, 'Rwanda Franc', 'RWF', 'RWF', 'RWF 1 0,00', '858.18', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(117, 'Saudi Riyal', 'SAR', '﷼', '﷼ 1,0.00', '3.7503', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(118, 'Solomon Islands Dollar', 'SBD', '$', '$1,0.00', '7.828472', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(119, 'Seychelles Rupee', 'SCR', '₨', '₨1,0.00', '13.4185', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(120, 'Sudanese Dinar', 'SDD', 'LSd', '1,0.00LSd', '0', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:15:05'),
(121, 'Swedish Krona', 'SEK', 'kr', '1 0,00 kr', '8.3545', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(122, 'Singapore Dollar', 'SGD', '$', '$1,0.00', '1.346645', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(123, 'Saint Helena Pound', 'SHP', '£', '£1,0.00', '0.742255', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(124, 'Sierra Leone, Leone', 'SLL', 'Le', 'Le1,0.00', '7607.702542', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(125, 'Somali Shilling', 'SOS', 'S', 'S1,0.00', '578.62', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(126, 'Surinam Dollar', 'SRD', '$', '$1,0.00', '7.448', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(127, 'Sao Tome and Principe, Dobra', 'STD', 'Db', 'Db1,0.00', '20597.575643', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(128, 'El Salvador Colon', 'SVC', '₡', '₡1,0.00', '8.7524', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(129, 'Syrian Pound', 'SYP', '£', '£ 1,0.00', '515.00999', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(130, 'Swaziland, Lilangeni', 'SZL', 'E', 'E1,0.00', '13.727983', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(131, 'Thailand, Baht', 'THB', '฿', '฿1,0.00', '32.591', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(132, 'Tajikistan, Somoni', 'TJS', 'TJS', '1 0;00 TJS', '8.817354', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(133, 'Turkmenistani New Manat', 'TMT', 'm', '1 0,m', '3.499986', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(134, 'Tunisian Dinar', 'TND', 'د.ت.‏', 'د.ت.‏ 1,0.000', '2.481', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(135, 'Tonga, Paanga', 'TOP', 'T$', 'T$1,0.00', '2.291837', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(136, 'Turkish Lira', 'TRY', 'TL', '₺1,0.00', '3.913204', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(137, 'Trinidad and Tobago Dollar', 'TTD', 'TT$', 'TT$1,0.00', '6.731841', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(138, 'New Taiwan Dollar', 'TWD', 'NT$', 'NT$1,0.00', '30.0165', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(139, 'Tanzanian Shilling', 'TZS', 'TSh', 'TSh1,0.00', '2241.6', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(140, 'Ukraine, Hryvnia', 'UAH', '₴', '1 0,00₴', '27.1125', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(141, 'Uganda Shilling', 'UGX', 'USh', 'USh1,0.00', '3634', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(142, 'US Dollar', 'USD', '$', '$1,0.00', '133', 1, NULL, 1, '2017-12-03 09:15:05', '2018-11-09 22:45:52'),
(143, 'Peso Uruguayo', 'UYU', '$U', '$U 1,0.00', '29.002688', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(144, 'Uzbekistan Sum', 'UZS', 'сўм', '1 0,00 сўм', '8098.1', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(145, 'Venezuela Bolivares Fuertes', 'VEF', 'Bs. F.', 'Bs. F. 1,0.00', '9.985022', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(146, 'Viet Nam, Dong', 'VND', '₫', '1,0.0 ₫', '22718.801563', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(147, 'Vanuatu, Vatu', 'VUV', 'VT', '1,0.VT', '107.205977', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(148, 'Samoa, Tala', 'WST', 'WS$', 'WS$1,0.00', '2.545678', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(149, 'Franc CFA (XAF)', 'XAF', 'F.CFA', '1,0.00 F.CFA', '551.351537', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(150, 'East Caribbean Dollar', 'XCD', '$', '$1,0.00', '2.70255', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(151, 'Franc CFA (XOF)', 'XOF', 'F.CFA', '1,0.00 F.CFA', '551.351537', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(152, 'CFP Franc', 'XPF', 'F', '1,0.00F', '100.301909', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(153, 'Yemeni Rial', 'YER', '﷼', '﷼ 1,0.00', '250.25', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(154, 'South Africa, Rand', 'ZAR', 'R', 'R 1,0.00', '13.74373', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04'),
(155, 'Zambia Kwacha', 'ZMW', 'ZK', 'ZK1,0.00', '10.177886', 0, NULL, NULL, '2017-12-03 09:15:05', '2017-12-03 09:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_value` double DEFAULT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci,
  `multi_value` text COLLATE utf8mb4_unicode_ci,
  `date_value` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_settings`
--

CREATE TABLE `custom_field_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `options_options` text COLLATE utf8mb4_unicode_ci,
  `custom_attributes` text COLLATE utf8mb4_unicode_ci,
  `default_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation_rules` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_attributes`
--

CREATE TABLE `ecommerce_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  `use_as_filter` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_attributes`
--

INSERT INTO `ecommerce_attributes` (`id`, `type`, `label`, `display_order`, `use_as_filter`, `required`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'checkbox', 'Filter-1', 1, 0, 1, 1, 1, NULL, '2018-10-16 23:36:28', '2018-10-16 23:36:28');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_attribute_options`
--

CREATE TABLE `ecommerce_attribute_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `option_order` int(11) NOT NULL,
  `option_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_display` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_brands`
--

CREATE TABLE `ecommerce_brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_brands`
--

INSERT INTO `ecommerce_brands` (`id`, `name`, `slug`, `status`, `is_featured`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'GABA', 'abc', 'active', 0, 1, 1, NULL, '2018-10-16 23:09:41', '2018-10-16 23:09:41'),
(2, 'abc-d', 'abc-d', 'active', 0, 1, 1, NULL, '2018-11-29 01:55:40', '2018-11-29 01:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_categories`
--

CREATE TABLE `ecommerce_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) UNSIGNED DEFAULT '0',
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `external_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_categories`
--

INSERT INTO `ecommerce_categories` (`id`, `name`, `slug`, `description`, `parent_id`, `status`, `is_featured`, `external_id`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Uncategorized', 'uncategorized', 'Default product category', NULL, 'inactive', 0, NULL, NULL, 1, NULL, NULL, '2018-09-24 16:58:24'),
(2, 'Glasses', 'glasses', 'Glasses', 7, 'active', 0, 'Glasses', 1, 1, NULL, '2018-09-23 02:38:37', '2018-09-24 16:58:14'),
(3, 'Sunglasses', 'sunglasses', 'Sunglasses', 7, 'active', 0, 'Sunglasses', 1, 1, NULL, '2018-09-23 02:58:10', '2018-09-24 16:58:00'),
(5, 'Black', 'black', 'Black', 24, 'active', 0, 'Black', 1, 1, NULL, '2018-09-23 06:32:49', '2018-09-24 17:08:13'),
(6, 'Blue', 'blue', 'Blue', 24, 'active', 0, 'Blue', 1, 1, NULL, '2018-09-23 06:33:19', '2018-09-24 17:07:55'),
(7, 'Category', 'category', 'Category', NULL, 'inactive', 0, 'Category', 1, 1, NULL, '2018-09-24 16:50:28', '2018-10-04 15:39:22'),
(8, 'Gender', 'gender', 'Gender', NULL, 'active', 0, 'Gender', 1, 1, NULL, '2018-09-24 16:59:18', '2018-09-24 16:59:18'),
(9, 'Men', 'men', 'Men', 8, 'active', 0, 'Men', 1, 1, NULL, '2018-09-24 16:59:38', '2018-09-24 16:59:38'),
(10, 'Women', 'women', 'Women', 8, 'active', 0, 'Women', 1, 1, NULL, '2018-09-24 16:59:57', '2018-09-24 16:59:57'),
(11, 'Kids', 'kids', 'Kids', 8, 'active', 0, 'Kids', 1, 1, NULL, '2018-09-24 17:00:15', '2018-09-24 17:00:15'),
(12, 'Style', 'style', 'Style', NULL, 'active', 0, 'Style', 1, 1, NULL, '2018-09-24 17:00:43', '2018-09-24 17:00:43'),
(13, 'Rimless', 'rimless', 'Rimless', 12, 'active', 0, 'Rimless', 1, 1, NULL, '2018-09-24 17:01:08', '2018-09-24 17:01:08'),
(14, 'Semi-Rimless', 'semi-rimless', 'Semi-Rimless', 12, 'active', 0, 'Semi-Rimless', 1, 1, NULL, '2018-09-24 17:01:32', '2018-09-24 17:01:32'),
(15, 'Full Frame', 'full-frame', 'Full Frame', 12, 'active', 0, 'Full Frame', 1, 1, NULL, '2018-09-24 17:01:50', '2018-09-24 17:01:50'),
(16, 'Shape', 'shape', 'Shape', NULL, 'active', 0, 'Shape', 1, 1, NULL, '2018-09-24 17:02:20', '2018-09-24 17:02:20'),
(17, 'Aviater', 'aviater', 'Aviater', 16, 'active', 0, 'Aviater', 1, 1, NULL, '2018-09-24 17:02:56', '2018-09-24 17:02:56'),
(18, 'Cat Eye', 'cat-eye', 'Cat Eye', 16, 'active', 0, 'Cat Eye', 1, 1, NULL, '2018-09-24 17:03:21', '2018-09-24 17:03:21'),
(19, 'Material', 'material', 'Material', NULL, 'active', 0, 'Material', 1, 1, NULL, '2018-09-24 17:04:09', '2018-09-24 17:04:09'),
(20, 'Titanium', 'titanium', 'Titanium', 19, 'active', 0, 'Titanium', 1, 1, NULL, '2018-09-24 17:04:43', '2018-09-24 17:04:43'),
(21, 'Stainless Steel', 'stainless-steel', 'Stainless Steel', 19, 'active', 0, 'Stainless Steel', 1, 1, NULL, '2018-09-24 17:05:42', '2018-09-24 17:05:42'),
(22, 'Acetate', 'acetate', 'Acetate', 19, 'active', 0, 'Acetate', 1, 1, NULL, '2018-09-24 17:06:08', '2018-09-24 17:06:19'),
(23, 'Plastic', 'plastic', 'Plastic', 19, 'active', 0, 'Plastic', 1, 1, NULL, '2018-09-24 17:06:45', '2018-09-24 17:06:45'),
(24, 'Color', 'color', 'Color', NULL, 'active', 0, 'Color', 1, 1, NULL, '2018-09-24 17:07:05', '2018-09-24 17:07:05'),
(25, 'Contact Lens', 'contact-lens', 'Contact Lens', NULL, 'inactive', 0, 'Contact Lens', 1, 1, NULL, '2018-09-27 13:52:30', '2018-10-01 14:10:22'),
(26, 'Contact Lens Type', 'contact-lens-type', NULL, NULL, 'inactive', 0, '1', 1, 1, NULL, '2018-09-27 17:36:27', '2018-11-01 15:21:54'),
(27, 'Contact Lens Solutions', 'contact-lens-solutions', NULL, NULL, 'inactive', 0, '2', 1, 1, NULL, '2018-09-27 17:37:56', '2018-11-01 15:22:14'),
(28, 'Disposable - Daily', 'disposable-daily', NULL, 26, 'active', 0, '3', 1, 1, NULL, '2018-09-27 17:38:47', '2018-10-15 13:56:24'),
(29, 'Disposable - 2 week', 'disposable-2-week', NULL, 26, 'active', 0, '4', 1, 1, NULL, '2018-09-27 17:39:52', '2018-10-15 13:57:02'),
(30, 'Disposable - Monthly', 'disposable-monthly', NULL, 26, 'active', 0, '5', 1, 1, NULL, '2018-09-27 17:40:48', '2018-10-15 13:57:21'),
(31, 'Toric & Astigmatism', 'toric-astigmatism', NULL, 26, 'active', 0, '6', 1, 1, NULL, '2018-09-27 17:42:52', '2018-10-15 13:57:55'),
(32, 'Multifocal & Bifocal', 'multifocal-bifocal', NULL, 26, 'active', 0, '7', 1, 1, NULL, '2018-09-27 17:43:54', '2018-10-15 13:58:35'),
(33, 'Color & Enhancing', 'color-enhancing', NULL, 26, 'active', 0, '8', 1, 1, NULL, '2018-09-27 17:44:56', '2018-10-15 13:59:11'),
(34, 'Extended & Wear', 'extended-wear', NULL, 26, 'active', 0, '9', 1, 1, NULL, '2018-09-27 17:46:22', '2018-10-15 13:59:36'),
(35, 'Daily Wear', 'daily-wear', NULL, 26, 'active', 0, '10', 1, 1, NULL, '2018-09-27 17:46:50', '2018-10-15 14:00:02'),
(36, 'Prosthetic Lenses', 'prosthetic-lenses', NULL, 26, 'active', 0, '11', 1, 1, NULL, '2018-09-27 17:48:01', '2018-10-15 14:00:25'),
(37, 'Bandage Lenses', 'bandage-lenses', NULL, 26, 'active', 0, '12', 1, 1, NULL, '2018-09-27 17:48:31', '2018-10-15 14:00:50'),
(38, 'Multi-purpose', 'multi-purpose', NULL, 27, 'active', 0, '13', 1, 1, NULL, '2018-09-27 17:49:02', '2018-09-27 17:49:02'),
(39, 'Cleaners & Salines', 'cleaners-salines', NULL, 27, 'active', 0, '14', 1, 1, NULL, '2018-09-27 17:49:54', '2018-09-27 17:49:54'),
(40, 'Gas Permeable', 'gas-permeable', NULL, 27, 'active', 0, '15', 1, 1, NULL, '2018-09-27 17:50:33', '2018-09-27 17:50:33'),
(41, 'Travel Packs', 'travel-packs', NULL, 27, 'active', 0, '16', 1, 1, NULL, '2018-09-27 17:50:57', '2018-09-27 17:50:57'),
(42, 'Hydrogen Peroxide', 'hydrogen-peroxide', NULL, 27, 'active', 0, '17', 1, 1, NULL, '2018-09-27 17:51:32', '2018-09-27 17:51:32'),
(43, 'Size', 'size', 'Size', NULL, 'active', 0, 'Size', 1, 1, NULL, '2018-10-02 12:16:43', '2018-10-02 12:16:43'),
(44, 'Extra Small', 'extra-small', 'Extra Small', 43, 'active', 0, 'Extra Small', 1, 1, NULL, '2018-10-02 12:22:31', '2018-10-02 12:22:31'),
(45, 'Small', 'small', 'Small', 43, 'active', 0, 'Small', 1, 1, NULL, '2018-10-02 12:23:26', '2018-10-02 12:23:26'),
(46, 'Medium', 'medium', 'Medium', 43, 'active', 0, 'Medium', 1, 1, NULL, '2018-10-02 12:24:10', '2018-10-02 12:24:10'),
(47, 'Large', 'large', 'Large', 43, 'active', 0, 'Large', 1, 1, NULL, '2018-10-02 12:24:36', '2018-10-02 12:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_category_product`
--

CREATE TABLE `ecommerce_category_product` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_category_product`
--

INSERT INTO `ecommerce_category_product` (`product_id`, `category_id`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 28),
(5, 29),
(6, 28),
(7, 2),
(7, 9),
(7, 13),
(7, 20),
(9, 2),
(32, 2),
(34, 2),
(35, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_colors`
--

CREATE TABLE `ecommerce_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_colors`
--

INSERT INTO `ecommerce_colors` (`id`, `title`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Black', 1, 1, '2018-12-14 11:09:22', '2018-12-14 06:09:22', '2018-12-14 06:09:22'),
(2, 'Bronze', 1, 1, '2018-12-14 11:10:24', '2018-12-14 06:10:24', '2018-12-14 06:10:24'),
(3, 'Golden', 1, 1, '2018-12-14 11:10:59', '2018-12-14 06:10:59', '2018-12-14 06:10:59'),
(4, 'Rose Gold', 1, 1, '2018-12-14 11:11:37', '2018-12-14 06:11:37', '2018-12-14 06:11:37'),
(5, 'Silver', 1, 1, '2018-12-14 11:12:10', '2018-12-14 06:12:10', '2018-12-14 06:12:10');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_coupons`
--

CREATE TABLE `ecommerce_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('fixed','percentage') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'fixed',
  `uses` int(11) DEFAULT NULL,
  `min_cart_total` decimal(8,2) DEFAULT NULL,
  `max_discount_value` decimal(8,2) DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` datetime DEFAULT NULL,
  `expiry` datetime DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_coupons`
--

INSERT INTO `ecommerce_coupons` (`id`, `code`, `type`, `uses`, `min_cart_total`, `max_discount_value`, `value`, `start`, `expiry`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'CORALS-FIXED', 'fixed', NULL, '500.00', NULL, '45', '2018-03-01 00:00:00', '2022-03-01 00:00:00', 1, 1, '2018-09-16 02:05:47', '2018-09-16 02:05:47'),
(2, 'CORALS-PERC', 'percentage', NULL, '500.00', NULL, '10', '2018-03-01 00:00:00', '2022-03-01 00:00:00', 1, 1, '2018-09-16 02:05:47', '2018-09-16 02:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_coupon_product`
--

CREATE TABLE `ecommerce_coupon_product` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_coupon_user`
--

CREATE TABLE `ecommerce_coupon_user` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_genders`
--

CREATE TABLE `ecommerce_genders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_lense_category`
--

CREATE TABLE `ecommerce_lense_category` (
  `id` int(11) NOT NULL,
  `len_cat_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_lense_category`
--

INSERT INTO `ecommerce_lense_category` (`id`, `len_cat_name`) VALUES
(1, 'Distance'),
(2, 'Near');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_lense_mapping_table`
--

CREATE TABLE `ecommerce_lense_mapping_table` (
  `id` int(11) NOT NULL,
  `len_cat_id` int(11) NOT NULL,
  `len_subcat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_lense_mapping_table`
--

INSERT INTO `ecommerce_lense_mapping_table` (`id`, `len_cat_id`, `len_subcat_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_lense_number`
--

CREATE TABLE `ecommerce_lense_number` (
  `id` int(11) NOT NULL,
  `sphere_min` float NOT NULL,
  `sphere_max` float NOT NULL,
  `cylinder_min` float NOT NULL,
  `cylinder_max` float NOT NULL,
  `cat_id` int(11) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_lense_number`
--

INSERT INTO `ecommerce_lense_number` (`id`, `sphere_min`, `sphere_max`, `cylinder_min`, `cylinder_max`, `cat_id`, `subcat_id`, `type_id`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 0, 4, 0, 2, 1, 1, 1, 975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 0, 4, 0, 2, 1, 1, 2, 1975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, 4, 0, 2, 1, 1, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 0, 4, 0, 2, 1, 1, 4, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 4.25, 6, 0, 2, 1, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 4.25, 6, 0, 2, 1, 1, 2, 1975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 4.25, 6, 0, 2, 1, 1, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 4.25, 6, 0, 2, 1, 1, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 6.25, 10, 0, 2, 1, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 6.25, 10, 0, 2, 1, 1, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 6.25, 10, 0, 2, 1, 1, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 6.25, 10, 0, 2, 1, 1, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 10.25, 12, 0, 2, 1, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 10.25, 12, 0, 2, 1, 1, 2, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 10.25, 12, 0, 2, 1, 1, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 10.25, 12, 0, 2, 1, 1, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 0, 4, 2.25, 3, 1, 1, 1, 1475, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 0, 4, 2.25, 3, 1, 1, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 0, 4, 2.25, 3, 1, 1, 3, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 0, 4, 2.25, 3, 1, 1, 4, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 4.25, 6, 2.25, 3, 1, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 4.25, 6, 2.25, 3, 1, 1, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 4.25, 6, 2.25, 3, 1, 1, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 4.25, 6, 2.25, 3, 1, 1, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 6.25, 10, 2.25, 3, 1, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 6.25, 10, 2.25, 3, 1, 1, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 6.25, 10, 2.25, 3, 1, 1, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 6.25, 10, 2.25, 3, 1, 1, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 10.25, 12, 2.25, 3, 1, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 10.25, 12, 2.25, 3, 1, 1, 2, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 10.25, 12, 2.25, 3, 1, 1, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 10.25, 12, 2.25, 3, 1, 1, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 0, 4, 0, 2, 1, 2, 1, 975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 0, 4, 0, 2, 1, 2, 2, 1975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 0, 4, 0, 2, 1, 2, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 0, 4, 0, 2, 1, 2, 4, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 4.25, 6, 0, 2, 1, 2, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 4.25, 6, 0, 2, 1, 2, 2, 1975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 4.25, 6, 0, 2, 1, 2, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 4.25, 6, 0, 2, 1, 2, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 6.25, 10, 0, 2, 1, 2, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 6.25, 10, 0, 2, 1, 2, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 6.25, 10, 0, 2, 1, 2, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 6.25, 10, 0, 2, 1, 2, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 10.25, 12, 0, 2, 1, 2, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 10.25, 12, 0, 2, 1, 2, 2, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 10.25, 12, 0, 2, 1, 2, 3, 4950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 10.25, 12, 0, 2, 1, 2, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 0, 4, 2.25, 3, 1, 2, 1, 1475, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 0, 4, 2.25, 3, 1, 2, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 0, 4, 2.25, 3, 1, 2, 3, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 0, 4, 2.25, 3, 1, 2, 4, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 4.25, 6, 2.25, 3, 1, 2, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 4.25, 6, 2.25, 3, 1, 2, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 4.25, 6, 2.25, 3, 1, 2, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 4.25, 6, 2.25, 3, 1, 2, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 6.25, 10, 2.25, 3, 1, 2, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 6.25, 10, 2.25, 3, 1, 2, 2, 2950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 6.25, 10, 2.25, 3, 1, 2, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 6.25, 10, 2.25, 3, 1, 2, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 10.25, 12, 2.25, 3, 1, 2, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 10.25, 12, 2.25, 3, 1, 2, 2, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 10.25, 12, 2.25, 3, 1, 2, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 10.25, 12, 2.25, 3, 1, 2, 4, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 0, 4, 3, 3, 1, 3, 5, 2450, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 0, 4, 3, 3, 1, 3, 6, 3450, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 0, 4, 3, 3, 1, 3, 7, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 0, 4, 3, 3, 1, 3, 8, 8950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 4.25, 6, 3, 3, 1, 3, 5, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 4.25, 6, 3, 3, 1, 3, 6, 6950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 4.25, 6, 3, 3, 1, 3, 7, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 4.25, 6, 3, 3, 1, 3, 8, 8950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 6.25, 10, 3, 3, 1, 3, 5, 10950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 6.25, 10, 3, 3, 1, 3, 6, 10950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 6.25, 10, 3, 3, 1, 3, 7, 19950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 6.25, 10, 3, 3, 1, 3, 8, 18950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 0, 4, 3, 3, 1, 4, 9, 2450, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 0, 4, 3, 3, 1, 4, 6, 3450, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 0, 4, 3, 3, 1, 4, 7, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 0, 4, 3, 3, 1, 4, 8, 8950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 4.25, 6, 3, 3, 1, 4, 9, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 4.25, 6, 3, 3, 1, 4, 6, 6950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 4.25, 6, 3, 3, 1, 4, 7, 9950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 4.25, 6, 3, 3, 1, 4, 8, 8950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 6.25, 10, 3, 3, 1, 4, 9, 10950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 6.25, 10, 3, 3, 1, 4, 6, 10950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 6.25, 10, 3, 3, 1, 4, 7, 19950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 6.25, 10, 3, 3, 1, 4, 8, 18950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 0, 3, 2, 2, 2, 1, 1, 975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 0, 3, 2, 2, 2, 1, 2, 1975, 1, 1, '2018-09-20 17:18:55', '2018-09-20 12:18:55'),
(91, 0, 3, 2, 2, 2, 1, 3, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 0, 3, 2, 2, 2, 1, 4, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 3.25, 6, 2, 2, 2, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 3.25, 6, 2, 2, 2, 1, 2, 1975, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 3.25, 6, 2, 2, 2, 1, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 3.25, 6, 2, 2, 2, 1, 4, 7950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 6.25, 12, 2, 2, 2, 1, 1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 6.25, 12, 2, 2, 2, 1, 2, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 6.25, 12, 2, 2, 2, 1, 3, 5950, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 6.25, 12, 2, 2, 2, 1, 4, 7950, 1, 1, '2018-09-20 16:56:56', '2018-09-20 11:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_lense_sub_category`
--

CREATE TABLE `ecommerce_lense_sub_category` (
  `id` int(11) NOT NULL,
  `len_subcat_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_lense_sub_category`
--

INSERT INTO `ecommerce_lense_sub_category` (`id`, `len_subcat_name`) VALUES
(1, 'Clear'),
(2, 'Digital'),
(3, 'Sun'),
(4, 'Transitions');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_lense_type`
--

CREATE TABLE `ecommerce_lense_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_lense_type`
--

INSERT INTO `ecommerce_lense_type` (`id`, `type_name`) VALUES
(1, 'Basic 1.50'),
(2, 'Element 1.56'),
(3, 'Thin 1.67'),
(4, 'Super Thin 1.74'),
(5, 'Solid'),
(6, 'Gradient'),
(7, 'Mirrored'),
(8, 'Polorized'),
(9, 'Basic');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_materials`
--

CREATE TABLE `ecommerce_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_orders`
--

CREATE TABLE `ecommerce_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping` text COLLATE utf8mb4_unicode_ci,
  `billing` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_orders`
--

INSERT INTO `ecommerce_orders` (`id`, `order_number`, `amount`, `currency`, `status`, `shipping`, `billing`, `user_id`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ORD-000001', '9400.00', 'PKR', 'pending', NULL, '{\"billing_address\":{\"email\":\"Aj@gma.com\",\"address_1\":\"Sunset\",\"address_2\":\"PhASE2\",\"type\":\"billing\",\"city\":\"kARACHI\",\"state\":\"sINDH\",\"zip\":\"7500\",\"country\":\"PK\"},\"payment_reference\":\"cash_kTP9P9\",\"payment_status\":\"pending\"}', 1, 1, 1, NULL, '2018-12-18 03:18:16', '2018-12-18 03:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_order_contactlens`
--

CREATE TABLE `ecommerce_order_contactlens` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `eye` varchar(50) NOT NULL,
  `basic_curve` float NOT NULL,
  `diameter` float NOT NULL,
  `power` float NOT NULL,
  `packs` varchar(200) NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_order_items`
--

CREATE TABLE `ecommerce_order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) DEFAULT NULL,
  `sku_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_options` text COLLATE utf8mb4_unicode_ci,
  `order_id` int(10) UNSIGNED NOT NULL,
  `subprod_id` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_order_items`
--

INSERT INTO `ecommerce_order_items` (`id`, `amount`, `description`, `quantity`, `sku_code`, `type`, `item_options`, `order_id`, `subprod_id`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1000.00', 'gaba-new-prod - gaba-new-prod-2', 2, 'gaba-new-prod-2', 'Product', '{\"product_options\":[]}', 1, 1, 1, 1, NULL, '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(2, '800.00', 'gaba-new-prod - gaba-new-prod-2', 1, 'gaba-new-prod-2', 'Product', '{\"product_options\":[]}', 1, 2, 1, 1, NULL, '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(3, '1950.00', 'Distance/Clear - gaba-new-prod-2', 1, 'gaba-new-prod-2', 'Product', '{\"product_options\":[]}', 1, NULL, 1, 1, NULL, '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(4, '1200.00', 'gaba-new-prod - gaba-new-prod-2', 1, 'gaba-new-prod-2', 'Product', '{\"product_options\":[]}', 1, 4, 1, 1, NULL, '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(5, '1950.00', 'Distance/Clear - gaba-new-prod-2', 1, 'gaba-new-prod-2', 'Product', '{\"product_options\":[]}', 1, NULL, 1, 1, NULL, '2018-12-18 03:18:18', '2018-12-18 03:18:18'),
(6, '1500.00', 'gaba-new-prod - gaba-new-prod-2', 1, 'gaba-new-prod-2', 'Product', '{\"product_options\":[]}', 1, 3, 1, 1, NULL, '2018-12-18 03:18:18', '2018-12-18 03:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_order_lens`
--

CREATE TABLE `ecommerce_order_lens` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `lens` varchar(100) NOT NULL,
  `leyesphere` float NOT NULL,
  `leyecylinder` float NOT NULL,
  `reyesphere` float NOT NULL,
  `reyecylinder` float NOT NULL,
  `leyeaxis` varchar(10) NOT NULL,
  `reyeaxis` varchar(10) NOT NULL,
  `leyeadd` float NOT NULL,
  `reyeadd` float NOT NULL,
  `eyepd` int(11) NOT NULL,
  `leyepd` float NOT NULL,
  `reyepd` float NOT NULL,
  `type_name` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_order_lens`
--

INSERT INTO `ecommerce_order_lens` (`id`, `order_id`, `lens`, `leyesphere`, `leyecylinder`, `reyesphere`, `reyecylinder`, `leyeaxis`, `reyeaxis`, `leyeadd`, `reyeadd`, `eyepd`, `leyepd`, `reyepd`, `type_name`, `created_by`, `updated_by`, `updated_at`, `created_at`) VALUES
(1, 1, 'Distance/Clear', -0.5, 1.75, -0.25, 1, '2', '2', 3.75, 4.25, 46, 0, 0, 'Basic 1.50', 1, 1, '2018-12-18 03:18:17', '2018-12-18 03:18:17'),
(2, 1, 'Distance/Clear', -0.5, 0.75, -0.5, 0.25, '3', '3', 4, 3.5, 46, 0, 0, 'Basic 1.50', 1, 1, '2018-12-18 03:18:17', '2018-12-18 03:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_order_subproduct`
--

CREATE TABLE `ecommerce_order_subproduct` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `subprod_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_products`
--

CREATE TABLE `ecommerce_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('active','inactive','deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `color_id` int(10) UNSIGNED DEFAULT NULL,
  `material_id` int(10) UNSIGNED DEFAULT NULL,
  `shape_id` int(10) UNSIGNED DEFAULT NULL,
  `style_id` int(10) UNSIGNED DEFAULT NULL,
  `gender_id` int(10) UNSIGNED DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `shipping` text COLLATE utf8mb4_unicode_ci,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `external_url` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_products`
--

INSERT INTO `ecommerce_products` (`id`, `name`, `type`, `slug`, `description`, `status`, `brand_id`, `color_id`, `material_id`, `shape_id`, `style_id`, `gender_id`, `properties`, `shipping`, `caption`, `code`, `is_featured`, `external_url`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'gaba-new-prod', 'simple', 'gaba-new-prod', '<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.</p>', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '[]', '{\"width\":null,\"height\":null,\"length\":null,\"weight\":null,\"enabled\":0}', 'gaba-new-prod', NULL, 1, NULL, 1, 1, NULL, '2018-12-18 00:22:45', '2018-12-18 00:23:49'),
(2, 'gaba-new-test', 'simple', 'gaba-new-test', '<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.</p>', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '[]', '{\"width\":null,\"height\":null,\"length\":null,\"weight\":null,\"enabled\":0}', 'gaba-new-test', NULL, 0, NULL, 1, 1, NULL, '2018-12-18 05:53:27', '2018-12-18 05:55:18'),
(3, 'ST MICHEL', 'simple', 'st-michel', '<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.</p>', 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"width\":null,\"height\":null,\"length\":null,\"weight\":null,\"enabled\":0}', 'ST MICHEL', NULL, 0, NULL, 1, 1, NULL, '2018-12-18 05:58:39', '2018-12-18 05:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_product_attributes`
--

CREATE TABLE `ecommerce_product_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `sku_level` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_product_tag`
--

CREATE TABLE `ecommerce_product_tag` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_shapes`
--

CREATE TABLE `ecommerce_shapes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_shippings`
--

CREATE TABLE `ecommerce_shippings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(8,2) NOT NULL DEFAULT '0.00',
  `min_order_total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `exclusive` tinyint(1) NOT NULL DEFAULT '0',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_shippings`
--

INSERT INTO `ecommerce_shippings` (`id`, `name`, `priority`, `shipping_method`, `rate`, `min_order_total`, `exclusive`, `country`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '', 1, 'Shippo', '0.00', '0.00', 0, 'US', NULL, 1, 1, '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(2, '', 2, 'FlatRate', '10.00', '0.00', 0, NULL, NULL, 1, 1, '2018-09-16 02:05:46', '2018-09-16 02:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_size`
--

CREATE TABLE `ecommerce_size` (
  `id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_size`
--

INSERT INTO `ecommerce_size` (`id`, `title`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'XL', 1, 1, '2018-12-14 11:07:24', '2018-12-14 06:07:24', '2018-12-14 06:07:24'),
(2, 'L', 1, 1, '2018-12-14 11:07:52', '2018-12-14 06:07:52', '2018-12-14 06:07:52'),
(3, 'M', 1, 1, '2018-12-14 11:08:15', '2018-12-14 06:08:15', '2018-12-14 06:08:15'),
(4, 'S', 1, 1, '2018-12-14 11:08:41', '2018-12-14 06:08:41', '2018-12-14 06:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_sku`
--

CREATE TABLE `ecommerce_sku` (
  `id` int(10) UNSIGNED NOT NULL,
  `regular_price` decimal(8,2) NOT NULL,
  `sale_price` decimal(8,2) DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inventory` enum('finite','bucket','infinite') COLLATE utf8mb4_unicode_ci DEFAULT 'infinite',
  `inventory_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `shipping` text COLLATE utf8mb4_unicode_ci,
  `allowed_quantity` int(11) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_sku`
--

INSERT INTO `ecommerce_sku` (`id`, `regular_price`, `sale_price`, `code`, `inventory`, `inventory_value`, `product_id`, `status`, `shipping`, `allowed_quantity`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, '1500.00', NULL, 'abc-ddddd', 'finite', '25000', 2, 'active', NULL, 0, 1, 1, NULL, '2018-12-17 06:29:38', '2018-12-17 06:29:38'),
(3, '1500.00', NULL, 'gaba-new-prod-2', 'finite', '24973', 1, 'active', NULL, 15, 1, 1, NULL, '2018-12-18 00:22:45', '2018-12-18 03:18:36'),
(4, '0.00', NULL, '1500', 'finite', '25000', 2, 'active', NULL, 0, 1, 1, NULL, '2018-12-18 05:53:28', '2018-12-18 05:53:28'),
(5, '2500.00', NULL, 'st-michel', 'finite', '25000', 3, 'active', NULL, 0, 1, 1, NULL, '2018-12-18 05:58:39', '2018-12-18 05:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_sku_options`
--

CREATE TABLE `ecommerce_sku_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `sku_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `attribute_option_id` int(10) UNSIGNED DEFAULT NULL,
  `string_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_value` double DEFAULT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_styles`
--

CREATE TABLE `ecommerce_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_sub_product`
--

CREATE TABLE `ecommerce_sub_product` (
  `id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `color` int(10) NOT NULL,
  `size` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `price` double NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecommerce_sub_product`
--

INSERT INTO `ecommerce_sub_product` (`id`, `product_id`, `color`, `size`, `quantity`, `price`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 14996, 1000, 1, 1, '2018-12-18 08:18:35', '2018-12-18 00:22:46', '2018-12-18 03:18:35'),
(2, 1, 1, 2, 14999, 800, 1, 1, '2018-12-18 08:18:35', '2018-12-18 00:22:46', '2018-12-18 03:18:35'),
(3, 1, 2, 1, 14998, 1500, 1, 1, '2018-12-18 08:18:36', '2018-12-18 00:22:46', '2018-12-18 03:18:36'),
(4, 1, 3, 3, 11998, 1200, 1, 1, '2018-12-18 08:18:36', '2018-12-18 00:22:46', '2018-12-18 03:18:36'),
(5, 2, 1, 1, 15000, 1000, 1, 1, '2018-12-18 10:55:19', '2018-12-18 05:53:28', '2018-12-18 05:55:19'),
(6, 2, 1, 2, 15000, 800, 1, 1, '2018-12-18 10:55:19', '2018-12-18 05:53:28', '2018-12-18 05:55:19'),
(7, 2, 2, 2, 15000, 2500, 1, 1, '2018-12-18 10:55:19', '2018-12-18 05:53:28', '2018-12-18 05:55:19'),
(8, 2, 4, 3, 15000, 1300, 1, 1, '2018-12-18 10:55:19', '2018-12-18 05:53:29', '2018-12-18 05:55:19'),
(9, 2, 4, 4, 15000, 1200, 1, 1, '2018-12-18 10:55:19', '2018-12-18 05:53:29', '2018-12-18 05:55:19'),
(10, 3, 1, 1, 15000, 1000, 1, 1, '2018-12-18 10:58:39', '2018-12-18 05:58:39', '2018-12-18 05:58:39'),
(11, 3, 1, 2, 15000, 4300, 1, 1, '2018-12-18 10:58:39', '2018-12-18 05:58:39', '2018-12-18 05:58:39'),
(12, 3, 2, 2, 15000, 2000, 1, 1, '2018-12-18 10:58:40', '2018-12-18 05:58:40', '2018-12-18 05:58:40'),
(13, 3, 2, 3, 15000, 1000, 1, 1, '2018-12-18 10:58:40', '2018-12-18 05:58:40', '2018-12-18 05:58:40'),
(14, 3, 4, 3, 15000, 1000, 1, 1, '2018-12-18 10:58:40', '2018-12-18 05:58:40', '2018-12-18 05:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_sub_product_media`
--

CREATE TABLE `ecommerce_sub_product_media` (
  `id` int(10) NOT NULL,
  `sub_prod_id` int(10) NOT NULL,
  `collection_name` varchar(100) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ecommerce_tags`
--

CREATE TABLE `ecommerce_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ecommerce_tags`
--

INSERT INTO `ecommerce_tags` (`id`, `name`, `slug`, `status`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SALE', 'sale', 'active', 1, 1, NULL, '2018-10-16 23:14:39', '2018-10-16 23:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `fulltext_search`
--

CREATE TABLE `fulltext_search` (
  `id` int(10) UNSIGNED NOT NULL,
  `indexable_id` int(11) NOT NULL,
  `indexable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indexed_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `indexed_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fulltext_search`
--

INSERT INTO `fulltext_search` (`id`, `indexable_id`, `indexable_type`, `indexed_title`, `indexed_content`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'gaba-new-test Glasses', '<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.</p> gaba-new-test', 1, 1, '2018-09-23 07:37:12', '2018-12-18 05:53:30'),
(22, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'gaba-new-prod Glasses', '<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.</p> gaba-new-prod', 1, 1, '2018-12-18 00:22:47', '2018-12-18 00:22:47'),
(3, 3, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ST MICHEL Glasses', '<p>Become the 21st century saint of style with St Michel. The contrast between sleek, bronze metal and retro outsize shaped lenses makes these full-rim eyeglasses uniquely trendy. The smooth, slimline design is complemented by a keyhole nose bridge and black temple tips to create a frame that gives an instant, edgy style boost.</p> ST MICHEL', 1, 1, '2018-09-27 13:53:32', '2018-12-18 05:58:41'),
(4, 4, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'Contact Lens1 Disposable - Daily', '<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.</p> Contact Lens1', 1, 1, '2018-09-27 14:18:37', '2018-10-18 17:26:16'),
(5, 5, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'Contact Lens2 Disposable - 2 week', '<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.</p> Contact Lens2', 1, 1, '2018-09-27 14:25:19', '2018-10-18 17:25:54'),
(6, 6, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'Contact Lens3 Disposable - Daily', '<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.</p> Contact Lens3', 1, 1, '2018-09-27 14:27:20', '2018-10-18 17:25:35'),
(7, 7, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'Gaba Frame 1 SALE sale Glasses, Men, Rimless, Titanium', '<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.</p> Caption Frame', 1, 1, '2018-10-16 23:38:12', '2018-10-18 17:25:14'),
(9, 9, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'gaba g2 Glasses', '<p>The Gaba brand represents the quintessence of luxury. Modern and sexy, it is an exclusive brand that reflects an elegant lifestyle. The sunwear collection uses only the highest quality materials and offers distinctive shapes enriched with historic icons that celebrate the House of Gaba.</p> g2', 1, 1, '2018-10-18 01:58:00', '2018-10-18 17:24:02'),
(20, 34, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'gaba-vv Glasses', 'gaba-vv', 1, 1, '2018-12-11 05:43:13', '2018-12-11 05:43:13'),
(21, 35, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'gaba-new-test Glasses', '<p>Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;Lorem Ipsum dummy text goes here here hrer&nbsp;</p> gaba-new-test', 1, 1, '2018-12-12 00:02:41', '2018-12-12 00:02:41'),
(19, 32, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'gaba-abc Glasses', 'gaba-abc', 1, 1, '2018-12-11 00:23:39', '2018-12-11 00:23:39');

-- --------------------------------------------------------

--
-- Table structure for table `gateway_status`
--

CREATE TABLE `gateway_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `gateway` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` int(10) UNSIGNED NOT NULL,
  `object_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` enum('CREATED','UPDATED','CREATE_FAILED','UPDATE_FAILED','NA','DELETED','DELETE_FAILED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NA',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gateway_status`
--

INSERT INTO `gateway_status` (`id`, `gateway`, `object_type`, `object_id`, `object_reference`, `message`, `status`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 5, 'P-35A45995E790655492KEKY2A', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(2, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 4, 'P-1C8559013J153541E2KEOLQQ', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(3, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 3, 'P-9SN748465A473292R2KET6PI', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(4, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 8, 'P-3HF888155494320232KE2KFA', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(5, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 7, 'P-1VH360051Y78696492KE5NUY', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(6, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 6, NULL, NULL, 'CREATE_FAILED', NULL, NULL, NULL, NULL, NULL),
(7, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 12, 'P-07Y10119GX380004X2KF45CQ', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(8, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 11, 'P-6AG81380WF340342W2KGA2OY', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(9, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 10, 'P-9S718541AD773481G2KGGNWY', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(10, 'PayPal_Rest', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 9, 'P-3LK056448F49229252KGKMEA', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(11, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 5, 'gold', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(12, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 4, 'silver', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(13, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 3, 'bronze', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(14, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 8, 'business', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(15, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 7, 'professional', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(16, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 6, 'basic', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(17, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 12, 'platinuim', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(18, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 11, 'bushosting', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(19, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 10, 'corporate', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL),
(20, 'Stripe', 'Corals\\Modules\\Subscriptions\\Models\\Plan', 9, 'basichosting', NULL, 'CREATED', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('paid','pending','failed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `due_date` datetime NOT NULL,
  `sub_total` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `invoicable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoicable_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `itemable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemable_id` int(10) UNSIGNED NOT NULL,
  `object_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_id`, `model_type`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 1, 'Corals\\Modules\\Ecommerce\\Models\\Color', 'ecommerce-color-thumbnail', 'black', 'black.jpg', 'image/webp', 'media', 668, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 1, '2018-12-14 06:09:23', '2018-12-14 06:09:23'),
(2, 2, 'Corals\\Modules\\Ecommerce\\Models\\Color', 'ecommerce-color-thumbnail', 'Bronze', 'Bronze.jpg', 'image/webp', 'media', 1412, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 2, '2018-12-14 06:10:24', '2018-12-14 06:10:24'),
(3, 3, 'Corals\\Modules\\Ecommerce\\Models\\Color', 'ecommerce-color-thumbnail', 'golden', 'golden.jpg', 'image/webp', 'media', 322, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 3, '2018-12-14 06:10:59', '2018-12-14 06:10:59'),
(4, 4, 'Corals\\Modules\\Ecommerce\\Models\\Color', 'ecommerce-color-thumbnail', 'rose_gold', 'rose_gold.jpg', 'image/webp', 'media', 358, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 4, '2018-12-14 06:11:37', '2018-12-14 06:11:37'),
(5, 5, 'Corals\\Modules\\Ecommerce\\Models\\Color', 'ecommerce-color-thumbnail', 'Silver', 'Silver.jpg', 'image/webp', 'media', 366, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 5, '2018-12-14 06:12:10', '2018-12-14 06:12:10'),
(18, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-1', 'black-1.jpg', 'image/webp', 'media', 14300, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 6, '2018-12-18 00:23:49', '2018-12-18 00:23:49'),
(19, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-2', 'black-2.jpg', 'image/webp', 'media', 14752, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 7, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(20, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-3', 'black-3.jpg', 'image/webp', 'media', 13806, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 8, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(21, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-4', 'black-4.jpg', 'image/webp', 'media', 19936, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 9, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(22, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg2', 'bronze-1', 'bronze-1.jpg', 'image/webp', 'media', 23832, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 10, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(23, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg2', 'bronze-2', 'bronze-2.jpg', 'image/webp', 'media', 19734, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 11, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(24, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg2', 'bronze-3', 'bronze-3.jpg', 'image/webp', 'media', 22452, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 12, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(25, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg3', 'golden-1', 'golden-1.jpg', 'image/webp', 'media', 21444, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 13, '2018-12-18 00:23:50', '2018-12-18 00:23:50'),
(26, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg3', 'golden-p-2', 'golden-p-2.jpg', 'image/webp', 'media', 7794, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 14, '2018-12-18 00:23:51', '2018-12-18 00:23:51'),
(27, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg3', 'golden-p-3', 'golden-p-3.jpg', 'image/webp', 'media', 19628, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 15, '2018-12-18 00:23:51', '2018-12-18 00:23:51'),
(28, 1, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg3', 'golden-p-4', 'golden-p-4.jpg', 'image/webp', 'media', 24648, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 16, '2018-12-18 00:23:51', '2018-12-18 00:23:51'),
(29, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-1', 'black-1.jpg', 'image/webp', 'media', 14300, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 17, '2018-12-18 05:55:19', '2018-12-18 05:55:19'),
(30, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-2', 'black-2.jpg', 'image/webp', 'media', 14752, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 18, '2018-12-18 05:55:19', '2018-12-18 05:55:19'),
(31, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-3', 'black-3.jpg', 'image/webp', 'media', 13806, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 19, '2018-12-18 05:55:19', '2018-12-18 05:55:19'),
(32, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg1', 'black-4', 'black-4.jpg', 'image/webp', 'media', 19936, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 20, '2018-12-18 05:55:19', '2018-12-18 05:55:19'),
(33, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg2', 'bronze-1', 'bronze-1.jpg', 'image/webp', 'media', 23832, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 21, '2018-12-18 05:55:19', '2018-12-18 05:55:19'),
(34, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg2', 'bronze-2', 'bronze-2.jpg', 'image/webp', 'media', 19734, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 22, '2018-12-18 05:55:20', '2018-12-18 05:55:20'),
(35, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg2', 'bronze-3', 'bronze-3.jpg', 'image/webp', 'media', 22452, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 23, '2018-12-18 05:55:20', '2018-12-18 05:55:20'),
(36, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg4', 'red-rose-1', 'red-rose-1.jpg', 'image/webp', 'media', 22792, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 24, '2018-12-18 05:55:20', '2018-12-18 05:55:20'),
(37, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg4', 'red-rose-2', 'red-rose-2.jpg', 'image/webp', 'media', 11966, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 25, '2018-12-18 05:55:20', '2018-12-18 05:55:20'),
(38, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg4', 'red-rose-3', 'red-rose-3.jpg', 'image/webp', 'media', 13868, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 26, '2018-12-18 05:55:20', '2018-12-18 05:55:20'),
(39, 2, 'Corals\\Modules\\Ecommerce\\Models\\Product', 'ecommerce-sub-product-gallery_subimg4', 'red-rose-4', 'red-rose-4.jpg', 'image/webp', 'media', 30616, '[]', '{\"root\":\"user_v1oz1Yz27j\"}', 27, '2018-12-18 05:55:20', '2018-12-18 05:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_menu_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `target` enum('_blank','_self') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `parent_id`, `key`, `url`, `active_menu_url`, `icon`, `roles`, `name`, `description`, `target`, `status`, `order`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'sidebar', NULL, NULL, NULL, '[\"1\"]', 'Sidebar', 'Sidebar Root Menu', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'administration', NULL, NULL, 'fa fa-plug', '[\"1\"]', 'Administration', 'Administration Root Menu', NULL, 'active', 7, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(3, 1, NULL, 'file-manager', 'file-manager*', 'fa fa-folder-o', '[\"1\"]', 'File Manager', 'File Manager Menu Item', NULL, 'active', 6, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(4, 2, NULL, 'menus', 'menu*', 'fa fa-bars', '[\"1\"]', 'Menu', 'Menu Menu Item', NULL, 'active', 1, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(5, 2, 'notification_templates', 'notification-templates', 'notification-templates*', 'fa fa-bell-o', '[\"1\"]', 'Notification Templates', 'Notification Templates Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(6, 2, NULL, 'custom-fields', 'custom-fields*', 'fa fa-microchip', '[\"1\"]', 'Custom Fields', 'Custom Fields menu Item', NULL, 'active', 2, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(7, 2, NULL, 'settings', 'settings*', 'fa fa-gears', '[\"1\"]', 'Settings', 'Settings Menu Item', NULL, 'active', 3, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(8, 2, NULL, 'activities', 'activities*', 'fa fa-history', '[\"1\"]', 'Activities', 'Activities Menu Item', NULL, 'active', 4, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(9, 2, NULL, 'modules', 'modules*', 'fa fa-rocket', '[\"1\"]', 'Modules', 'Modules Menu Item', NULL, 'active', 5, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(10, 2, NULL, 'themes', 'themes', 'fa fa-object-group', '[\"1\"]', 'Themes', 'Themes Menu Item', NULL, 'active', 6, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(11, 2, NULL, 'cache-management', 'cache-management', 'fa fa-fighter-jet', '[\"1\"]', 'Cache Management', 'Cache Management Menu Item', NULL, 'active', 7, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(12, 1, 'user', NULL, 'users*', 'fa fa-users', '[\"1\"]', 'Users', 'Users Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(13, 12, NULL, 'users', 'users*', 'fa fa-user-o', '[\"1\"]', 'Users', 'Users List Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(14, 12, NULL, 'roles', 'roles*', 'fa fa-key', '[\"1\"]', 'Roles', 'Roles List Menu Item', NULL, 'active', 1, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(15, 1, 'utility', NULL, 'utilities*', 'fa fa-cloud', '[\"1\"]', 'Utilities', 'Utilities Menu Item', NULL, 'active', 1, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(16, 15, NULL, 'utilities/address/locations', 'utilities/address/locations*', 'fa fa-map-o', '[\"1\"]', 'Locations', 'Locations List Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(17, 15, NULL, 'utilities/tags', 'utilities/tags*', 'fa fa-tags', '[\"1\"]', 'Tags', 'Tags List Menu Item', NULL, 'active', 1, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(18, 15, NULL, 'utilities/categories', 'utilities/categories*', 'fa fa-folder-open', '[\"1\"]', 'Categories', 'Categories List Menu Item', NULL, 'active', 2, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(19, 15, NULL, 'utilities/attributes', 'utilities/attributes*', 'fa fa-sliders', '[\"1\"]', 'Attributes', 'Attributes List Menu Item', NULL, 'active', 3, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(20, 1, 'cms', NULL, 'cms*', 'fa fa-desktop', '[\"1\"]', 'CMS', 'CMS Menu Item', NULL, 'active', 2, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(21, 20, NULL, 'cms/pages', 'cms/pages*', 'fa fa-files-o', '[\"1\"]', 'Pages', 'Pages List Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(22, 20, NULL, 'cms/posts', 'cms/posts*', 'fa fa-thumb-tack', '[\"1\"]', 'Posts', 'Posts List Menu Item', NULL, 'active', 1, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(23, 20, NULL, 'cms/categories', 'cms/categories*', 'fa fa-folder-open', '[\"1\"]', 'Categories', 'Categories List Menu Item', NULL, 'active', 2, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(24, 20, NULL, 'cms/blog', 'cms/blog*', 'fa fa-book', '[\"1\",\"2\"]', 'Internal Content', 'Internal Content List Menu Item', NULL, 'active', 3, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(25, 20, NULL, 'cms/news', 'cms/news*', 'fa fa-newspaper-o', '[\"1\"]', 'News', 'News List Menu Item', NULL, 'active', 4, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(26, 1, 'payment', NULL, 'payments*', 'fa fa-money', '[\"1\"]', 'Payments', 'Payments Menu Item', NULL, 'active', 3, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(27, 26, NULL, 'payments/settings', 'payments/settings', 'fa fa-cog', '[\"1\"]', 'Payment Settings', 'Payment Settings List Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(28, 26, NULL, 'webhook-calls', 'webhook-calls', 'fa fa-anchor', '[\"1\"]', 'Webhook Calls', 'Webhook List Menu Item', NULL, 'active', 1, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(29, 26, NULL, 'invoices', 'invoices*', 'fa fa-file-text-o', '[\"1\"]', 'Invoices', 'Invoices List Menu Item', NULL, 'active', 2, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(30, 26, 'payment-taxes', 'tax/tax-classes', 'tax/tax-classes', 'fa fa-cut', '[\"1\"]', 'Tax Classes', 'Tax Classes List Menu Item', NULL, 'active', 3, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(31, 26, 'currencies', 'currencies', 'currencies*', 'fa fa-money', '[\"1\"]', 'Currencies', 'currencies List Menu Item', NULL, 'active', 4, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(32, 1, NULL, 'slider/sliders', 'slider/sliders*', 'fa fa-clone', '[\"1\"]', 'Sliders', 'Sliders List Menu Item', NULL, 'active', 4, NULL, NULL, NULL, NULL, '2018-09-23 04:42:51'),
(33, 1, 'ecommerce', '#', 'e-commerce*', 'fa fa-globe', '[\"1\",\"2\"]', 'Ecommerce', 'Ecommerce Menu Item', NULL, 'active', 5, NULL, 1, NULL, NULL, '2018-09-23 05:00:18'),
(34, 33, NULL, 'e-commerce/products', 'e-commerce/products*', 'fa fa-cube', '[\"1\"]', 'Products', 'Products List Menu Item', NULL, 'active', 0, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(35, 33, NULL, 'e-commerce/shop', 'e-commerce/shop*', 'fa fa-building', '[\"1\",\"2\"]', 'Shop', 'Shop Menu Item', NULL, 'active', 2, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(36, 33, NULL, 'e-commerce/downloads/my', 'e-commerce/downloads/my', 'fa fa-download', '[\"2\"]', 'My Downloads', 'My Downloads Menu Item', NULL, 'active', 3, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(37, 33, NULL, 'e-commerce/private-pages/my', 'e-commerce/private-pages/my', 'fa fa-file', '[\"2\"]', 'My Private Pages', 'My Private Pages Menu Item', NULL, 'active', 4, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(38, 33, NULL, 'e-commerce/orders/my', 'e-commerce/orders/my', 'fa fa-send-o', '[\"2\"]', 'My Orders', 'My Orders Menu Item', NULL, 'active', 5, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(39, 33, NULL, 'e-commerce/wishlist/my', 'e-commerce/wishlist/my', 'fa fa-heart', '[\"2\"]', 'My Wishlist', 'My Wishlist Menu Item', NULL, 'active', 6, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(40, 33, NULL, 'e-commerce/orders', 'e-commerce/orders', 'fa fa-send-o', '[\"1\"]', 'Orders', 'Orders Menu Item', NULL, 'active', 7, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(41, 33, NULL, 'e-commerce/categories', 'e-commerce/categories*', 'fa fa-folder-open', '[\"1\"]', 'Categories', 'Categories List Menu Item', NULL, 'active', 8, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(42, 33, NULL, 'e-commerce/attributes', 'e-commerce/attributes*', 'fa fa-sliders', '[\"1\"]', 'Attributes', 'Attributes List Menu Item', NULL, 'active', 9, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(43, 33, NULL, 'e-commerce/tags', 'e-commerce/tags*', 'fa fa-tags', '[\"1\"]', 'Tags', 'Tags List Menu Item', NULL, 'active', 10, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(44, 33, NULL, 'e-commerce/brands', 'e-commerce/brands*', 'fa fa-cubes', '[\"1\"]', 'Brands', 'Brands List Menu Item', NULL, 'active', 11, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(45, 33, NULL, 'e-commerce/coupons', 'e-commerce/coupons*', 'fa fa-gift', '[\"1\"]', 'Coupons', 'Coupons List Menu Item', NULL, 'active', 12, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(46, 33, NULL, 'e-commerce/shippings', 'e-commerce/shippings*', 'fa fa-truck', '[\"1\"]', 'Shipping Rules', 'Shippings List Menu Item', NULL, 'active', 13, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(47, 33, NULL, 'e-commerce/settings', 'e-commerce/settings', 'fa fa-cog', '[\"1\"]', 'Settings', 'Settings Menu Item', NULL, 'active', 14, NULL, NULL, NULL, NULL, '2018-09-23 04:42:52'),
(48, 0, 'frontend_top', NULL, NULL, NULL, NULL, 'Frontend Top', 'Frontend Top Menu', NULL, 'active', 0, 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-23 05:28:28'),
(49, 48, 'home', '/', '/', 'fa fa fa-home', NULL, 'Home', 'Home Menu Item', NULL, 'active', 0, 1, 1, NULL, '2018-09-16 02:05:45', '2018-09-16 02:05:45'),
(53, 48, NULL, 'pricing', 'pricing', NULL, NULL, 'Pricing', 'Pricing Menu Item', NULL, 'inactive', 980, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-23 05:37:16'),
(54, 48, NULL, 'contact-us', 'contact-us', NULL, NULL, 'Contact Us', 'Contact Us Menu Item', NULL, 'inactive', 980, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-23 05:37:34'),
(55, 0, 'frontend_footer', NULL, NULL, NULL, NULL, 'Frontend Footer', 'Frontend Footer Menu', NULL, 'active', 0, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(56, 55, 'footer_home', '/', '/', NULL, NULL, 'Home', 'Home Menu Item', NULL, 'active', 0, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(57, 55, NULL, 'about-us', 'about-us', NULL, NULL, 'About Us', 'About Us Menu Item', NULL, 'active', 980, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(58, 55, NULL, 'contact-us', 'contact-us', NULL, NULL, 'Contact Us', 'Contact Us Menu Item', NULL, 'active', 980, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(59, 55, NULL, 'blog', 'blog', NULL, NULL, 'Blog', 'Blog Menu Item', NULL, 'active', 980, 1, 1, NULL, '2018-09-16 02:05:46', '2018-09-16 02:05:46'),
(61, 33, NULL, 'e-commerce/lensnumber', 'e-commerce/lensnumber*', 'fa fa-eye', '[\"1\"]', 'Lens Range', 'Lens Range List Menu Item', '_self', 'active', 1, 1, 1, NULL, '2018-09-23 04:38:12', '2018-09-23 04:42:52'),
(62, 48, NULL, 'shop?category=glasses', 'shop?category=glasses*', NULL, NULL, 'Glasses', NULL, NULL, 'active', 0, 1, 1, NULL, '2018-09-23 05:40:08', '2018-09-23 05:40:08'),
(63, 48, 'shop', 'shop', 'shop', NULL, NULL, 'Shop', 'Shop Menu Item', NULL, 'active', 965, 1, 1, NULL, '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(64, 48, NULL, 'about-us', 'about-us', NULL, NULL, 'About Us', 'About Us Menu Item', NULL, 'active', 970, 1, 1, NULL, '2018-09-24 12:25:47', '2018-09-24 12:25:47'),
(65, 48, NULL, 'blog', 'blog', NULL, NULL, 'Blog', 'Blog Menu Item', NULL, 'active', 980, 1, 1, NULL, '2018-09-24 12:25:47', '2018-10-03 15:37:52');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '0000_00_00_000000_create_activity_log_table', 1),
(2, '0000_00_00_000000_create_media_table', 1),
(3, '0000_00_00_000000_create_settings_table', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_01_23_222300_create_translatable_translations_table', 1),
(7, '2017_09_13_195204_create_permission_tables', 1),
(8, '2017_09_16_000000_create_menus_table', 1),
(9, '2017_12_18_000000_create_countries_table', 1),
(10, '2017_12_31_000000_create_modules_table', 1),
(11, '2018_01_02_152913_create_fulltext_search_table', 1),
(12, '2018_02_19_000000_create_custom_fields_table', 1),
(13, '2018_07_19_000000_add_confirmation_to_users_table', 1),
(14, '2018_2_26_000000_update_users_address_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'Corals\\User\\Models\\User'),
(2, 2, 'Corals\\User\\Models\\User'),
(2, 3, 'Corals\\User\\Models\\User'),
(2, 4, 'Corals\\User\\Models\\User');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `installed` tinyint(1) NOT NULL DEFAULT '0',
  `installed_version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `load_order` int(11) NOT NULL DEFAULT '0',
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('core','module','payment') COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `license_key` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `code`, `enabled`, `installed`, `installed_version`, `load_order`, `provider`, `folder`, `type`, `notes`, `license_key`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'corals-cms', 1, 1, '1.7', 9999, 'Corals\\Modules\\CMS\\CMSServiceProvider', 'CMS', 'module', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:39'),
(2, 'corals-ecommerce', 1, 1, '1.4', 601, 'Corals\\Modules\\Ecommerce\\EcommerceServiceProvider', 'Ecommerce', 'module', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:12:39'),
(3, 'corals-payment', 1, 1, '1.4', 500, 'Corals\\Modules\\Payment\\PaymentServiceProvider', 'Payment', 'module', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:04:02'),
(4, 'corals-cms-slider', 1, 1, '1.5', 0, 'Corals\\Modules\\Slider\\SliderServiceProvider', 'Slider', 'module', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:04:07'),
(5, 'corals-utility', 1, 1, '1.0.2', 0, 'Corals\\Modules\\Utility\\UtilityServiceProvider', 'Utility', 'module', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:28'),
(6, 'corals-activity', 1, 1, '1.2', 35, 'Corals\\Activity\\ActivityServiceProvider', 'Activity', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(7, 'corals-file-manager', 1, 1, '1.3', 30, 'Corals\\Elfinder\\ElfinderServiceProvider', 'Elfinder', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(8, 'corals-foundation', 1, 1, '1.7.4', 100, 'Corals\\Foundation\\MenuServiceProvider', 'Foundation', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(9, 'corals-media', 1, 1, '1.3', 25, 'Corals\\Media\\MediaServiceProvider', 'Media', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(10, 'corals-menu', 1, 1, '1.3', 20, 'Corals\\Menu\\MenuServiceProvider', 'Menu', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:14', '2018-09-16 01:03:14'),
(11, 'corals-settings', 1, 1, '2.1', 15, 'Corals\\Settings\\SettingsServiceProvider', 'Settings', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(12, 'corals-theme', 1, 1, '1.5.5', 40, 'Corals\\Theme\\ThemeServiceProvider', 'Theme', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(13, 'corals-user', 1, 1, '1.5', 10, 'Corals\\User\\UserServiceProvider', 'User', 'core', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:15', '2018-09-16 01:03:15'),
(14, 'corals-payment-paypal', 1, 1, '1.4.2', 0, NULL, 'PayPal', 'payment', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:15', '2018-09-16 01:05:32'),
(15, 'corals-payment-stripe', 1, 1, '1.3.4', 0, NULL, 'Stripe', 'payment', NULL, NULL, 1, 1, NULL, '2018-09-16 01:03:15', '2018-09-16 01:05:37'),
(16, 'corals-payment-cash', 1, 1, '1.0', 0, NULL, 'Cash', 'payment', NULL, NULL, 1, 1, NULL, '2018-10-07 20:15:41', '2018-10-07 20:16:16');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('0eb5470d-d2e5-4c0f-a6c2-da44ee2b183a', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:58:05', '2018-12-14 01:58:05'),
('1c7c92dd-2483-45d8-95bb-a96cdd67bc87', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:47:55', '2018-12-14 02:47:55'),
('35ec55da-ea89-4b3f-972b-784d71ba985b', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:06:05', '2018-12-14 02:06:05'),
('36762f21-917d-4a43-b18d-26b939071a0e', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:09:28', '2018-12-14 02:09:28'),
('3761de7b-4bf0-4c20-84ac-7c129ecfb863', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:29:25', '2018-12-14 01:29:25'),
('3c1f1985-ae40-494f-9bfd-123af14e2577', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 03:18:35', '2018-12-18 03:18:35'),
('3d62dce2-59e1-4cfc-afc8-9431f1da65e1', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:20:09', '2018-12-14 02:20:09'),
('3d8d98a0-c726-48d6-b9b0-6571463f72b0', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 03:11:28', '2018-12-18 03:11:28'),
('3e5e91b7-3240-4018-8877-9ce0005d5b94', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:03:25', '2018-12-14 02:03:25'),
('4100803e-4726-4c1e-8d02-ba1be5311cc2', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:32:30', '2018-12-14 01:32:30'),
('45da7bf4-a8c5-4cb4-80a3-baf43cd9dbef', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 00:28:15', '2018-12-18 00:28:15'),
('53a8f352-2b2f-4bad-9627-a440c1a3d77a', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/i-doczportal.com\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', '2018-10-17 23:12:12', '2018-10-15 13:44:35', '2018-10-17 23:12:12'),
('5640fbec-df40-4a5d-bb7a-6edd8f0d07f8', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:26:09', '2018-12-14 01:26:09'),
('592ed7c6-dd79-47ad-9e88-3c39f803a551', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 03:11:08', '2018-12-14 03:11:08'),
('609ca32c-7a39-4109-80f8-316002ce30ed', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 03:02:51', '2018-12-14 03:02:51'),
('623e26c4-a0a0-4aa8-bae1-5d6ad4a6049d', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:22:32', '2018-12-14 01:22:32'),
('655a8738-706d-4c34-b6ee-003b0b66aace', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 02:40:04', '2018-12-18 02:40:04'),
('69d8d049-743b-4957-b797-b80bf4dbfc6e', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:56:08', '2018-12-14 02:56:08'),
('6db60d1c-6eec-4bea-af3d-e924583b4376', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 04:55:40', '2018-12-14 04:55:40'),
('7627e035-7fc1-486a-8ef8-e7abd1a21203', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 02:19:37', '2018-12-18 02:19:37'),
('77182b0b-ff7c-4712-8ec7-9771fb9e68c8', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-17 02:45:16', '2018-12-17 02:45:16'),
('7779268e-377c-4f34-a9e3-6342a77340b7', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:03:36', '2018-12-14 01:03:36'),
('7974f513-02fe-4d82-b1f9-3d87daa46877', 'Corals\\User\\Notifications\\UserRegisteredNotification', 3, 'Corals\\User\\Models\\User', '{\"title\":\"Welcome to Corals\",\"body\":\"<p>Welcome to <strong>Laraship<\\/strong> and thanks for registration! hope you find what you are looking for in <em>our platform<\\/em>.<\\/p>\"}', NULL, '2018-10-30 15:29:05', '2018-10-30 15:29:05'),
('82baaed4-b369-4521-bc2a-eb87d24a5b3e', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/50.63.162.247\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-10-24 02:07:00', '2018-10-24 02:07:00'),
('900296b1-6953-4d46-a023-3fc1c0cd03b1', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:41:54', '2018-12-14 02:41:54'),
('9ea5ef00-0f21-4f02-88f8-2c6fe3a118d1', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:16:53', '2018-12-14 01:16:53'),
('a54b8de7-e600-4387-a2ed-aa1b56d06c63', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-13 07:15:14', '2018-12-13 07:15:14'),
('ab0ab198-6a60-4683-928a-8354a1f24430', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 00:25:00', '2018-12-14 00:25:00'),
('af4a8e22-2c6f-4ada-ad77-e7b41e0c7ae7', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 01:51:42', '2018-12-18 01:51:42'),
('b5f38ee8-759c-4df0-a337-33cf0ad22862', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:16:59', '2018-12-14 01:16:59'),
('bb8191f2-4bcb-4fcc-9dce-b12264121915', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/i-doczportal.com\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-10-27 00:27:47', '2018-10-27 00:27:47'),
('c6eb0777-7cfd-41f8-9a54-c9373753bde9', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 04:56:24', '2018-12-14 04:56:24'),
('cede36df-08e0-4298-92a8-6bbecf7c19bc', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:18:28', '2018-12-14 01:18:28'),
('d28e7c69-9ada-4b21-91a7-ed0aafc9e669', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/i-doczportal.com\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-10-24 01:30:59', '2018-10-24 01:30:59'),
('d42efeb8-bd34-43d9-b625-1fb0f022717f', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 00:06:58', '2018-12-14 00:06:58'),
('d9d7123c-b5d2-4ed3-a662-b7f9775268e6', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-17 02:29:56', '2018-12-17 02:29:56'),
('da284a35-ab5f-428a-8a3b-07e604db699a', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-18 02:27:19', '2018-12-18 02:27:19'),
('e02ba5f7-3cb0-45fd-8309-472ff6d08eed', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/i-doczportal.com\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', '2018-10-10 12:05:54', '2018-10-07 20:22:47', '2018-10-10 12:05:54'),
('e4cbee81-f7d3-4332-b71c-f33a8fc508d3', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 01:17:12', '2018-12-14 01:17:12'),
('e4d2e63a-ea3b-4ba9-8087-7e7beac89109', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 03:09:47', '2018-12-14 03:09:47'),
('e78c6886-bd59-40a1-9947-fc801f382625', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:51:55', '2018-12-14 02:51:55'),
('e97cbf65-0876-4262-83a1-379b90042059', 'Corals\\User\\Notifications\\UserRegisteredNotification', 4, 'Corals\\User\\Models\\User', '{\"title\":\"Welcome to Corals\",\"body\":\"<p>Welcome to <strong>Laraship<\\/strong> and thanks for registration! hope you find what you are looking for in <em>our platform<\\/em>.<\\/p>\"}', NULL, '2018-11-02 16:40:16', '2018-11-02 16:40:16'),
('ec141a3b-900c-47b8-856c-e0cafe6f11f3', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 2, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-11-23 01:02:16', '2018-11-23 01:02:16'),
('faf72873-ff5d-46d3-ac1f-ad0767ea32ea', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:04:31', '2018-12-14 02:04:31'),
('ff0a3756-4b8a-418f-9f06-6e13aba311cf', 'Corals\\Modules\\Ecommerce\\Notifications\\OrderReceivedNotification', 1, 'Corals\\User\\Models\\User', '{\"title\":\"Thank You For Your Order!\",\"body\":\"<p>Thank You For Your Order! check your orders <a href=\\\"http:\\/\\/127.0.0.1:8000\\/e-commerce\\/orders\\/my\\\">Here<\\/a><\\/p>\"}', NULL, '2018-12-14 02:15:35', '2018-12-14 02:15:35');

-- --------------------------------------------------------

--
-- Table structure for table `notification_templates`
--

CREATE TABLE `notification_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friendly_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `extras` text COLLATE utf8mb4_unicode_ci,
  `via` text COLLATE utf8mb4_unicode_ci,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_templates`
--

INSERT INTO `notification_templates` (`id`, `name`, `friendly_name`, `title`, `body`, `extras`, `via`, `updated_by`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'notifications.user.registered', 'New user registration', 'Welcome to Corals', '{\"mail\":\"<table align=\\\"center\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" style=\\\"max-width:600px;\\\" width=\\\"100%\\\"><tbody><tr><td align=\\\"left\\\" style=\\\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-bottom: 15px;\\\"><p style=\\\"font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;\\\">Hello {name},<\\/p><p style=\\\"font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;\\\">Welcome to Laraship and thanks for registration! hope you find what you are looking for in our platform.<\\/p><\\/td><\\/tr><tr><td align=\\\"center\\\" style=\\\"padding: 10px 0 25px 0;\\\"><table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"><tbody><tr><td align=\\\"center\\\" bgcolor=\\\"#ed8e20\\\" style=\\\"border-radius: 5px;\\\"><a href=\\\"{dashboard_link}\\\" style=\\\"font-size: 18px; font-family: Open Sans, Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; border-radius: 5px; background-color: #ed8e20; padding: 15px 30px; border: 1px solid #ed8e20; display: block;\\\" target=\\\"_blank\\\">Visit your Dashboard<\\/a><\\/td><\\/tr><\\/tbody><\\/table><\\/td><\\/tr><\\/tbody><\\/table>\",\"database\":\"<p>Welcome to <strong>Laraship<\\/strong> and thanks for registration! hope you find what you are looking for in <em>our platform<\\/em>.<\\/p>\"}', NULL, '[\"mail\",\"database\"]', 0, 0, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(2, 'notifications.user.confirmation', 'New user email confirmation', 'Email confirmation', '{\"mail\":\"<table align=\\\"center\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" style=\\\"max-width:600px;\\\" width=\\\"100%\\\"> <tbody> <tr> <td align=\\\"left\\\" style=\\\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-bottom: 15px;\\\"> <p style=\\\"font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;\\\">Hello {name},<\\/p><p style=\\\"font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;\\\"> Please confirm your email address in order to access corals website. Click on the button below to confirm your email. <\\/p><\\/td><\\/tr><tr> <td align=\\\"center\\\" style=\\\"padding: 10px 0 25px 0;\\\"> <table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"> <tbody> <tr> <td align=\\\"center\\\" bgcolor=\\\"#ed8e20\\\" style=\\\"border-radius: 5px;\\\"> <a href=\\\"{confirmation_link}\\\" style=\\\"font-size: 18px; font-family: Open Sans, Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; border-radius: 5px; background-color: #ed8e20; padding: 15px 30px; border: 1px solid #ed8e20; display: block;\\\" target=\\\"_blank\\\"> Confirm now <\\/a> <\\/td><\\/tr><\\/tbody> <\\/table> <\\/td><\\/tr><\\/tbody><\\/table>\"}', NULL, '[\"mail\"]', 0, 0, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(3, 'notifications.e_commerce.order.received', 'Ecommerce Order Received', 'Thank You For Your Order!', '{\"mail\":\"<table align=\\\"center\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\" style=\\\"max-width:600px;\\\"> <tr> <td align=\\\"center\\\" style=\\\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;\\\"> <h2 style=\\\"font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;\\\"> Thank You For Your Order! <\\/h2> <\\/td><\\/tr><tr> <td align=\\\"left\\\" style=\\\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;\\\"> <p style=\\\"font-size: 16px; font-weight: 400; line-height: 24px; color: #777777;\\\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium iste ipsa numquam odio dolores, nam. <\\/p><\\/td><\\/tr><tr> <td align=\\\"left\\\" style=\\\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;\\\"> <h5 style=\\\"font-size: 15px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;\\\"> Order Details <\\/h5> <\\/td><\\/tr><\\/table>\",\"database\":\"<p>Thank You For Your Order! check your orders <a href=\\\"{my_orders_link}\\\">Here<\\/a><\\/p>\"}', NULL, '[\"mail\",\"database\"]', 1, 1, NULL, '2018-09-16 01:12:38', '2018-09-16 01:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Activity::activity.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(2, 'Activity::activity.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(3, 'Settings::setting.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(4, 'Settings::setting.create', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(5, 'Settings::setting.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(6, 'Settings::setting.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(7, 'Settings::custom_field_setting.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(8, 'Settings::custom_field_setting.create', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(9, 'Settings::custom_field_setting.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(10, 'Settings::custom_field_setting.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(11, 'Settings::module.manage', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(12, 'Settings::theme.manage', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(13, 'User::user.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(14, 'User::user.create', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(15, 'User::user.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(16, 'User::user.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(17, 'User::role.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(18, 'User::role.create', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(19, 'User::role.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(20, 'User::role.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(21, 'Notification::notification_template.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(22, 'Notification::notification_template.create', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(23, 'Notification::notification_template.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(24, 'Notification::notification_template.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(25, 'Notification::my_notification.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(26, 'Notification::my_notification.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(27, 'Notification::my_notification.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(28, 'Menu::menu.view', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(29, 'Menu::menu.create', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(30, 'Menu::menu.update', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(31, 'Menu::menu.delete', 'web', '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(32, 'Utility::rating.create', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(33, 'Utility::my_wishlist.access', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(34, 'Utility::location.view', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(35, 'Utility::location.create', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(36, 'Utility::location.update', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(37, 'Utility::location.delete', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(38, 'Utility::tag.view', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(39, 'Utility::tag.create', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(40, 'Utility::tag.update', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(41, 'Utility::tag.delete', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(42, 'Utility::category.view', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(43, 'Utility::category.create', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(44, 'Utility::category.update', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(45, 'Utility::category.delete', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(46, 'Utility::attribute.view', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(47, 'Utility::attribute.create', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(48, 'Utility::attribute.update', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(49, 'Utility::attribute.delete', 'web', '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(50, 'CMS::post.view', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(51, 'CMS::post.create', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(52, 'CMS::post.update', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(53, 'CMS::post.delete', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(54, 'CMS::page.view', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(55, 'CMS::page.create', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(56, 'CMS::page.update', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(57, 'CMS::page.delete', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(58, 'CMS::category.view', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(59, 'CMS::category.create', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(60, 'CMS::category.update', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(61, 'CMS::category.delete', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(62, 'CMS::news.view', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(63, 'CMS::news.create', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(64, 'CMS::news.update', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(65, 'CMS::news.delete', 'web', '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(66, 'Payment::settings.update', 'web', '2018-09-16 01:04:02', '2018-09-16 01:04:02'),
(67, 'Payment::webhook.view', 'web', '2018-09-16 01:04:02', '2018-09-16 01:04:02'),
(68, 'Payment::invoices.edit', 'web', '2018-09-16 01:04:02', '2018-09-16 01:04:02'),
(69, 'Payment::invoices.create', 'web', '2018-09-16 01:04:02', '2018-09-16 01:04:02'),
(70, 'CMS::slider.view', 'web', '2018-09-16 01:04:06', '2018-09-16 01:04:06'),
(71, 'CMS::slider.create', 'web', '2018-09-16 01:04:06', '2018-09-16 01:04:06'),
(72, 'CMS::slider.update', 'web', '2018-09-16 01:04:06', '2018-09-16 01:04:06'),
(73, 'CMS::slider.delete', 'web', '2018-09-16 01:04:06', '2018-09-16 01:04:06'),
(74, 'Ecommerce::cart.access', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(75, 'Ecommerce::shop.access', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(76, 'Ecommerce::checkout.access', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(77, 'Ecommerce::my_orders.access', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(78, 'Ecommerce::orders.access', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(79, 'Ecommerce::settings.access', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(80, 'Ecommerce::product.view', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(81, 'Ecommerce::product.create', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(82, 'Ecommerce::product.update', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(83, 'Ecommerce::product.delete', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(84, 'Ecommerce::order.update', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(85, 'Ecommerce::coupon.view', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(86, 'Ecommerce::coupon.create', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(87, 'Ecommerce::coupon.update', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(88, 'Ecommerce::coupon.delete', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(89, 'Ecommerce::category.view', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(90, 'Ecommerce::category.create', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(91, 'Ecommerce::category.update', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(92, 'Ecommerce::category.delete', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(93, 'Ecommerce::tag.view', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(94, 'Ecommerce::tag.create', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(95, 'Ecommerce::tag.update', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(96, 'Ecommerce::tag.delete', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(97, 'Ecommerce::attribute.view', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(98, 'Ecommerce::attribute.create', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(99, 'Ecommerce::attribute.update', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37'),
(100, 'Ecommerce::attribute.delete', 'web', '2018-09-16 01:12:37', '2018-09-16 01:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `postables`
--

CREATE TABLE `postables` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_id` int(11) NOT NULL,
  `postable_id` int(10) UNSIGNED NOT NULL,
  `postable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sourcable_id` int(10) UNSIGNED DEFAULT NULL,
  `sourcable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `published_at` datetime DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `internal` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('post','page','news') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured_image_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extras` longtext COLLATE utf8mb4_unicode_ci,
  `author_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `meta_keywords`, `meta_description`, `content`, `published`, `published_at`, `private`, `internal`, `type`, `template`, `featured_image_link`, `extras`, `author_id`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 'home', 'home', '<div id=\"slider\">@slider(e-commerce-home-page-slider)</div>', 1, '2017-11-16 14:26:52', 0, 0, 'page', 'home', NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(2, 'About Us', 'about-us', 'about us', 'about us', '<!-- Page Content-->\r\n<div class=\"container padding-bottom-2x mb-2\">\r\n    <div class=\"row align-items-center padding-bottom-2x\">\r\n        <div class=\"col-md-5\"><img class=\"d-block w-270 m-auto\" src=\"/assets/themes/ecommerce-basic/img/features/01.jpg\" alt=\"Online Shopping\"></div>\r\n        <div class=\"col-md-7 text-md-left text-center\">\r\n            <div class=\"mt-30 hidden-md-up\"></div>\r\n            <h2>Search, Select, Buy Online.</h2>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id purus at risus pellentesque faucibus\r\n                a quis eros. In eu fermentum leo. Integer ut eros lacus. Proin ut accumsan leo. Morbi vitae est eget\r\n                dolor consequat aliquam eget quis dolor. Mauris rutrum fermentum erat, at euismod lorem pharetra nec.\r\n                Duis erat lectus, ultrices euismod sagittis at, pharetra eu nisl. Phasellus id ante at velit tincidunt\r\n                hendrerit. Aenean dolor dolor, tristique nec placerat nec.</p><a\r\n                class=\"text-medium text-decoration-none\" href=\"https://codecanyon.net/user/corals-io/portfolio\" target=\"_blank\">View Products&nbsp;<i\r\n                class=\"icon-arrow-right\"></i></a>\r\n        </div>\r\n    </div>\r\n    <hr>\r\n    <div class=\"row align-items-center padding-top-2x padding-bottom-2x\">\r\n        <div class=\"col-md-5 order-md-2\"><img class=\"d-block w-270 m-auto\" src=\"/assets/themes/ecommerce-basic/img/features/02.jpg\" alt=\"Delivery\">\r\n        </div>\r\n        <div class=\"col-md-7 order-md-1 text-md-left text-center\">\r\n            <div class=\"mt-30 hidden-md-up\"></div>\r\n            <h2>Fast Delivery Worldwide.</h2>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id purus at risus pellentesque faucibus\r\n                a quis eros. In eu fermentum leo. Integer ut eros lacus. Proin ut accumsan leo. Morbi vitae est eget\r\n                dolor consequat aliquam eget quis dolor. Mauris rutrum fermentum erat, at euismod lorem pharetra nec.\r\n                Duis erat lectus, ultrices euismod sagittis at, pharetra eu nisl. Phasellus id ante at velit tincidunt\r\n                hendrerit. Aenean dolor dolor, tristique nec placerat nec.</p><a\r\n                class=\"text-medium text-decoration-none\" href=\"https://codecanyon.net/user/corals-io/portfolio\" target=\"_blank\">View Products&nbsp;<i\r\n                class=\"icon-arrow-right\"></i></a>\r\n        </div>\r\n    </div>\r\n    <hr>\r\n    <div class=\"row align-items-center padding-top-2x padding-bottom-2x\">\r\n        <div class=\"col-md-5\"><img class=\"d-block w-270 m-auto\" src=\"/assets/themes/ecommerce-basic/img/features/03.jpg\" alt=\"Mobile App\"></div>\r\n        <div class=\"col-md-7 text-md-left text-center\">\r\n            <div class=\"mt-30 hidden-md-up\"></div>\r\n            <h2>Great Mobile App. Shop On The Go.</h2>\r\n            <p class=\"mb-4\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id purus at risus\r\n                pellentesque faucibus a quis eros. In eu fermentum leo. Integer ut eros lacus. Proin ut accumsan leo.\r\n                Morbi vitae est eget dolor consequat aliquam eget quis dolor.</p><a class=\"market-button apple-button\"\r\n                                                                                    href=\"#\"><span class=\"mb-subtitle\">Download on the</span><span\r\n                class=\"mb-title\">App Store</span></a><a class=\"market-button google-button\" href=\"#\"><span\r\n                class=\"mb-subtitle\">Download on the</span><span class=\"mb-title\">Google Play</span></a><a\r\n                class=\"market-button windows-button\" href=\"#\"><span class=\"mb-subtitle\">Download on the</span><span\r\n                class=\"mb-title\">Windows Store</span></a>\r\n        </div>\r\n    </div>\r\n    <hr>\r\n    <div class=\"row align-items-center padding-top-2x padding-bottom-2x\">\r\n        <div class=\"col-md-5 order-md-2\"><img class=\"d-block w-270 m-auto\" src=\"/assets/themes/ecommerce-basic/img/features/04.jpg\" alt=\"Delivery\">\r\n        </div>\r\n        <div class=\"col-md-7 order-md-1 text-md-left text-center\">\r\n            <div class=\"mt-30 hidden-md-up\"></div>\r\n            <h2>Shop Offline. Cozy Outlet Stores.</h2>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id purus at risus pellentesque faucibus\r\n                a quis eros. In eu fermentum leo. Integer ut eros lacus. Proin ut accumsan leo. Morbi vitae est eget\r\n                dolor consequat aliquam eget quis dolor. Mauris rutrum fermentum erat, at euismod lorem pharetra nec.\r\n                Duis erat lectus, ultrices euismod sagittis at, pharetra eu nisl. Phasellus id ante at velit tincidunt\r\n                hendrerit. Aenean dolor dolor, tristique nec placerat nec.</p><a\r\n                class=\"text-medium text-decoration-none\" href=\"https://codecanyon.net/user/corals-io/portfolio\" target=\"_blank\">View Products&nbsp;<i\r\n                class=\"icon-arrow-right\"></i></a>\r\n        </div>\r\n    </div>\r\n    <hr>\r\n    <div class=\"text-center padding-top-3x mb-30\">\r\n        <h2>Our Core Team</h2>\r\n        <p class=\"text-muted\">People behind your awesome shopping experience.</p>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-3 col-sm-6 mb-30 text-center\"><img\r\n                class=\"d-block w-150 mx-auto img-thumbnail rounded-circle mb-2\" src=\"/assets/themes/ecommerce-basic/img/team/01.jpg\" alt=\"Team\">\r\n            <h6>Grace Wright</h6>\r\n            <p class=\"text-muted mb-2\">Founder, CEO</p>\r\n            <div class=\"social-bar\"><a class=\"social-button shape-circle sb-facebook\" href=\"#\" data-toggle=\"tooltip\"\r\n                                       data-placement=\"top\" title=\"Facebook\"><i class=\"socicon-facebook\"></i></a><a\r\n                    class=\"social-button shape-circle sb-twitter\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\"\r\n                    title=\"Twitter\"><i class=\"socicon-twitter\"></i></a><a\r\n                    class=\"social-button shape-circle sb-google-plus\" href=\"#\" data-toggle=\"tooltip\"\r\n                    data-placement=\"top\" title=\"Google +\"><i class=\"socicon-googleplus\"></i></a></div>\r\n        </div>\r\n        <div class=\"col-md-3 col-sm-6 mb-30 text-center\"><img\r\n                class=\"d-block w-150 mx-auto img-thumbnail rounded-circle mb-2\" src=\"/assets/themes/ecommerce-basic/img/team/02.jpg\" alt=\"Team\">\r\n            <h6>Taylor Jackson</h6>\r\n            <p class=\"text-muted mb-2\">Financial Director</p>\r\n            <div class=\"social-bar\"><a class=\"social-button shape-circle sb-skype\" href=\"#\" data-toggle=\"tooltip\"\r\n                                       data-placement=\"top\" title=\"Skype\"><i class=\"socicon-skype\"></i></a><a\r\n                    class=\"social-button shape-circle sb-facebook\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\"\r\n                    title=\"Facebook\"><i class=\"socicon-facebook\"></i></a><a class=\"social-button shape-circle sb-paypal\"\r\n                                                                            href=\"#\" data-toggle=\"tooltip\"\r\n                                                                            data-placement=\"top\" title=\"PayPal\"><i\r\n                    class=\"socicon-paypal\"></i></a></div>\r\n        </div>\r\n        <div class=\"col-md-3 col-sm-6 mb-30 text-center\"><img\r\n                class=\"d-block w-150 mx-auto img-thumbnail rounded-circle mb-2\" src=\"/assets/themes/ecommerce-basic/img/team/03.jpg\" alt=\"Team\">\r\n            <h6>Quinton Cross</h6>\r\n            <p class=\"text-muted mb-2\">Marketing Director</p>\r\n            <div class=\"social-bar\"><a class=\"social-button shape-circle sb-twitter\" href=\"#\" data-toggle=\"tooltip\"\r\n                                       data-placement=\"top\" title=\"Twitter\"><i class=\"socicon-twitter\"></i></a><a\r\n                    class=\"social-button shape-circle sb-google-plus\" href=\"#\" data-toggle=\"tooltip\"\r\n                    data-placement=\"top\" title=\"Google +\"><i class=\"socicon-googleplus\"></i></a><a\r\n                    class=\"social-button shape-circle sb-email\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\"\r\n                    title=\"Email\"><i class=\"socicon-mail\"></i></a></div>\r\n        </div>\r\n        <div class=\"col-md-3 col-sm-6 mb-30 text-center\"><img\r\n                class=\"d-block w-150 mx-auto img-thumbnail rounded-circle mb-2\" src=\"/assets/themes/ecommerce-basic/img/team/04.jpg\" alt=\"Team\">\r\n            <h6>Liana Mullen</h6>\r\n            <p class=\"text-muted mb-2\">Lead Designer</p>\r\n            <div class=\"social-bar\"><a class=\"social-button shape-circle sb-behance\" href=\"#\" data-toggle=\"tooltip\"\r\n                                       data-placement=\"top\" title=\"Behance\"><i class=\"socicon-behance\"></i></a><a\r\n                    class=\"social-button shape-circle sb-dribbble\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\"\r\n                    title=\"Dribbble\"><i class=\"socicon-dribbble\"></i></a><a\r\n                    class=\"social-button shape-circle sb-instagram\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\"\r\n                    title=\"Instagram\"><i class=\"socicon-instagram\"></i></a></div>\r\n        </div>\r\n    </div>\r\n</div>', 1, '2017-11-16 11:56:34', 0, 0, 'page', 'full', NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(3, 'Blog', 'blog', 'Blog', 'Blog', '', 1, '2017-11-16 11:56:34', 0, 0, 'page', 'left', NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(4, 'Pricing', 'pricing', 'Pricing', 'Pricing', '', 1, '2017-11-16 11:56:34', 0, 0, 'page', 'full', NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(5, 'Contact Us', 'contact-us', 'Contact Us', 'Contact Us', '<div class=\"row\">\r\n            <div class=\"col\">\r\n                <div class=\"text-center\">\r\n                    <h3>Drop Your Message here</h3>\r\n                    <p>You can contact us with anything related to Laraship. <br/> We\'ll get in touch with you as soon as\r\n                        possible.</p>\r\n                </div>\r\n            </div>\r\n        </div>', 1, '2017-11-16 11:56:34', 0, 0, 'page', 'contact', NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:43', '2018-09-16 02:05:43'),
(6, 'Subscription Commerce Trends for 2018', 'subscription-commerce-trends-for-2018', NULL, NULL, '<p>Subscription commerce is ever evolving. A few years ago, who would have expected&nbsp;<a href=\"https://techcrunch.com/2017/10/10/porsche-launches-on-demand-subscription-for-its-sports-cars-and-suvs/\" target=\"_blank\">Porsche</a>&nbsp;to launch a subscription service? Or that monthly boxes of beauty samples or shaving supplies and&nbsp;<a href=\"https://www.pymnts.com/subscription-commerce/2017/how-over-the-top-services-came-out-on-top/\" target=\"_blank\">OTT services</a>&nbsp;would propel the subscription model to new heights? And how will these trends shape the subscription space going forward&mdash;and drive growth and innovation?</p>\r\n\r\n<p>Regardless of your billing model, there&rsquo;s an opportunity for you to capitalize on many of the current trends in subscription commerce&mdash;trends that will help you to continue to compete and succeed in your industry.</p>\r\n\r\n<h3><strong>What are these trends and how can you learn more?</strong></h3>\r\n\r\n<p>These trends are outlined in our &ldquo;Top Ten Trends for 2018&rdquo; which we publish every year to help subscription businesses understand the drivers which may impact them in 2018 and beyond.</p>\r\n\r\n<p>One trend, for example, is machine learning and data science which the payments industry is increasingly utilizing to deliver more powerful results for their customers.</p>\r\n\r\n<p>Another trend which is driving new revenue is the adoption of a hybrid billing model&mdash; subscription businesses seamlessly sell one-time items and &lsquo;traditional&rsquo; businesses add a subscription component as a means to introduce a new revenue stream.</p>\r\n\r\n<p>And while subscriber acquisition is not a new trend, there are some sophisticated ways to acquire new customers that subscription businesses are putting to work for increasingly positive effect.</p>\r\n\r\n<p>Download this year&rsquo;s edition and see how these trends and insights can help your subscription business succeed in 2018.</p>\r\n\r\n<p>&nbsp;</p>', 1, '2017-12-04 11:18:23', 0, 0, 'post', NULL, NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(7, 'Using Machine Learning to Optimize Subscription Billing', 'using-machine-learning-to-optimize-subscription-billing', NULL, NULL, '<p>As a data scientist at Recurly, my job is to use the vast amount of data that we have collected to build products that make subscription businesses more successful. One way to think about data science at Recurly is as an extended R&amp;D department for our customers. We use a variety of tools and techniques, attack problems big and small, but at the end of the day, our goal is to put all of Recurly&rsquo;s expertise to work in service of your business.</p>\r\n\r\n<p>Managing a successful subscription business requires a wide range of decisions. What is the optimum structure for subscription plans and pricing? What are the most effective subscriber acquisition methods? What are the most efficient collection methods for delinquent subscribers? What strategies will reduce churn and increase revenue?</p>\r\n\r\n<p>At Recurly, we&#39;re focused on building the most flexible subscription management platform, a platform that provides a competitive advantage for your business. We reduce the complexity of subscription billing so you can focus on winning new subscribers and delighting current subscribers.</p>\r\n\r\n<p>Recently, we turned to data science to tackle a big problem for subscription businesses: involuntary churn.</p>\r\n\r\n<h3><strong>The Problem: The Retry Schedule</strong></h3>\r\n\r\n<p>One of the most important factors in subscription commerce is subscriber retention. Every billing event needs to occur flawlessly to avoid adversely impacting the subscriber relationship or worse yet, to lose that subscriber to churn.</p>\r\n\r\n<p>Every time a subscription comes up for renewal, Recurly creates an invoice and initiates a transaction using the customer&rsquo;s stored billing information, typically a credit card. Sometimes, this transaction is declined by the payment processor or the customer&rsquo;s bank. When this happens, Recurly sends reminder emails to the customer, checks with the Account Updater service to see if the customer&#39;s card has been updated, and also attempts to collect payment at various intervals over a period of time defined by the subscription business. The timing of these collection attempts is called the &ldquo;retry schedule.&rdquo;</p>\r\n\r\n<p>Our ability to correct and successfully retry these cards prevents lost revenue, positively impacts your bottom line, and increases your customer retention rate.</p>\r\n\r\n<p>Other subscription providers typically offer a static, one-size-fits-all retry schedule, or leave the schedule up to the subscription business, without providing any guidance. In contrast, Recurly can use machine learning to craft a retry schedule that is tailored to each individual invoice based on our historical data with hundreds of millions of transactions. Our approach gives each invoice the best chance of success, without any manual work by our customers.</p>\r\n\r\n<p>A key component of Recurly&rsquo;s values is to test, learn and iterate. How did we call on those values to build this critical component of the Recurly platform?</p>\r\n\r\n<h3><strong>Applying Machine Learning</strong></h3>\r\n\r\n<p>We decided to use statistical models that leverage Recurly&rsquo;s data on transactions (hundreds of millions of transactions built up over years from a wide variety of different businesses) to predict which transactions are likely to succeed. Then, we used these models to craft the ideal retry schedule for each individual invoice. The process of building the models is known as machine learning.</p>\r\n\r\n<p>The term &quot;machine learning&quot; encompasses many different processes and methods, but at its heart is an effort to go past explicitly programmed logic and allow a computer to arrive at the best logic on its own.</p>\r\n\r\n<p>While humans are optimized for learning certain tasks&mdash;like how children can speak a new language after simply listening for a few months&mdash;computers can also be trained to learn patterns. Aggregating hundreds of millions of transactions to look for the patterns that lead to transaction success is a classic machine learning problem.</p>\r\n\r\n<p>A typical machine learning project involves gathering data, training a statistical model on that data, and then evaluating the performance of the model when presented with new data. A model is only as good as the data it&rsquo;s trained on, and here we had a huge advantage.</p>', 1, '2017-12-04 11:21:25', 0, 0, 'post', NULL, NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:44', '2018-09-16 02:05:44'),
(8, 'Why You Need A Blog Subscription Landing Page', 'why-you-need-a-blog-subscription-landing-page', NULL, NULL, '<p>Whether subscribing via email or RSS, your site visitor is individually volunteering to add your content to their day; a day that is already crowded with content from emails, texts, voicemails, site content, and even snail mail. &nbsp;</p>\r\n\r\n<p>As a business, each time you receive a new blog subscriber, you have received validation or &quot;a vote&quot; that your audience has identified YOUR content as adding value to their day. With each new blog subscriber, your content is essentially being awarded as being highly relevant to conversations your readers are having on a regular basis.&nbsp;</p>\r\n\r\n<p>To best promote the content your blog subscribers can expect to receive on an ongoing basis,&nbsp;<strong>consider adding a blog subscription landing page.&nbsp;</strong>This is a quick win that will help your company enhance the blogging subscription experience and help you measure and manage the success of this offer with analytical insight.</p>\r\n\r\n<p>Holistically, your goal with this landing page is to provide visitors with a sneak preview of the experience they will receive by becoming a blog subscriber.<strong>&nbsp;Your blog subscription landing page should include:</strong></p>\r\n\r\n<ul>\r\n	<li><strong>A high-level overview of topics, categories your blog will discuss.&nbsp;&nbsp;</strong>For example, HubSpot&#39;s blog covers &quot;all of the inbound marketing - SEO, Blogging, Social Media, Landing Pages, Lead Generation, and Analytics.&quot;</li>\r\n	<li><strong>Insight into &quot;who&quot; your blog will benefit.&nbsp;&nbsp;</strong>Examples may include HR Directors, Financial Business professionals, Animal Enthusiasts, etc.&nbsp; If your blog appeals to multiple personas, feel free to spell this out.&nbsp; This will help assure your visitor that they are joining a group of like-minded individuals who share their interests and goals.&nbsp;&nbsp;</li>\r\n	<li><strong>How your blog will help to drive the relevant conversation.&nbsp;</strong>Examples may include &quot;updates on industry events&quot;, &quot;expert editorials&quot;, &quot;insider tips&quot;, etc.&nbsp;&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>To create your blog subscription landing page, consider the following steps:</strong></p>\r\n\r\n<p>1) Create your landing page following&nbsp;landing page best practices.&nbsp; Consider the &quot;subscribing to your blog&quot; offer as similar to other offers you promote using Landing Pages.&nbsp;</p>\r\n\r\n<p>2) Create a Call To Action button that will link to this landing page.&nbsp; Use this button as a call to action within your blog articles or on other website pages to link to a blog subscription landing page&nbsp;Make sure your CTA button is supercharged!</p>\r\n\r\n<p>3)&nbsp;Create a Thank You Page&nbsp;to complete the sign-up experience with gratitude and a follow-up call to action.&nbsp;</p>\r\n\r\n<p>4) Measure the success of your blog subscription landing page.&nbsp;Consider the 3 Secrets to Optimizing Landing Page Copy.&nbsp;</p>\r\n\r\n<p>For more information on Blogging Success Strategies,&nbsp;check out more Content Camp Resources and recorded webinars.&nbsp;</p>', 1, '2018-10-03 08:28:33', 0, 0, 'post', NULL, NULL, NULL, 1, 1, 1, NULL, '2018-09-16 02:05:44', '2018-10-03 15:28:33'),
(9, 'Faq', 'faq', 'Faq', 'Faq', '<div class=\"help-faq\">\r\n<div class=\"container\">\r\n<div class=\"col-md-3\">\r\n<div class=\"sidebar-container\">\r\n<h4 class=\"sidebar-title\">Help</h4>\r\n\r\n<ul class=\"sidebar-menu\">\r\n	<li class=\"current\" data-event-cate=\"Help\" data-event-label=\"Help &amp; FAQ\" data-event-name=\"Help Navigation\"><a href=\"faq.php\">Help &amp; FAQ</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Order Tracking\" data-event-name=\"Help Navigation\"><a href=\"order-tracking.php\">Order Tracking</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Shipping &amp; Returns\" data-event-name=\"Help Navigation\"><a href=\"shipping-and-returns.php\">Shipping &amp; Returns</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Contact Us\" data-event-name=\"Help Navigation\"><a href=\"contact-us.php\">Contact Us</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Affiliate Program\" data-event-name=\"Help Navigation\"><a href=\"affiliate-program.php\">Affiliate Program</a></li>\r\n</ul>\r\n\r\n<h4 class=\"sidebar-title\">How to</h4>\r\n\r\n<ul class=\"sidebar-menu\">\r\n	<li data-event-cate=\"Help\" data-event-label=\"How to Buy\" data-event-name=\"Help Navigation\"><a href=\"/guides/how-to-buy-prescription-eyeglasses-online\">Buy Eyeglasses Online</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Prescriptions\" data-event-name=\"Help Navigation\"><a href=\"/guides/how-to-read-a-prescription\">Read a Prescription</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"PD\" data-event-name=\"Help Navigation\"><a href=\"/guides/how-to-measure-your-pd\">Measure Your PD</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Find Your Fit\" data-event-name=\"Help Navigation\"><a href=\"/guides/frames-and-face-shapes\">Find Your Fit</a></li>\r\n	<li data-event-cate=\"Help\" data-event-label=\"Measure a Frame\" data-event-name=\"Help Navigation\"><a href=\"/guides/frame-measurements\">Measure a Frame</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-9\">\r\n<div class=\"col-md-12\">\r\n<h1 class=\"faq-title text-light\">Frequently Asked Questions</h1>\r\n</div>\r\n\r\n<div class=\"col-md-6\">\r\n<div class=\"main-container\">\r\n<div class=\"accordion-option\">&nbsp;</div>\r\n\r\n<div class=\"clearfix\">&nbsp;</div>\r\n\r\n<div aria-multiselectable=\"true\" class=\"panel-group\" id=\"accordion\" role=\"tablist\">\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingOne\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseOne\" aria-expanded=\"true\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseOne\" role=\"button\">Orders / General </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingOne\" class=\"panel-collapse collapse .in\" id=\"collapseOne\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>How do I place an order</li>\r\n	<li>Where can I get my prescription?</li>\r\n	<li>How is EyeBuyDirect so inexpensive?</li>\r\n	<li>Oh no! I need to change something about my order. What should I do?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingTwo\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseTwo\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseTwo\" role=\"button\">Frames </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingTwo\" class=\"panel-collapse collapse .in\" id=\"collapseTwo\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>Is there a way to try out your frames and see which ones fit me best?</li>\r\n	<li>Is it possible to order EyeBuyDirect glasses with non-prescription lenses?</li>\r\n	<li>Do you offer children&#39;s frames?</li>\r\n	<li>What about prescription sunglasses?</li>\r\n	<li>Do you offer glasses that turn into sunglasses outdoors?</li>\r\n	<li>Are there any limitations for bifocals or progressive lenses?</li>\r\n	<li>How do I measure my frames?</li>\r\n	<li>What kind of frames are sold at EyeBuyDirect?</li>\r\n	<li>Can I order lenses that are both color tinted and photochromic?</li>\r\n	<li>Can I buy the frames alone?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingThree\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseThree\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseThree\" role=\"button\">Delivery and Shipping </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingThree\" class=\"panel-collapse collapse .in\" id=\"collapseThree\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>Can I track my order?</li>\r\n	<li>How long do I have to wait for my glasses after I have ordered them?</li>\r\n	<li>What shipping methods do you offer?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingFour\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseThree\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseFour\" role=\"button\">Support </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingFour\" class=\"panel-collapse collapse .in\" id=\"collapseFour\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>Oops! I entered some of my prescription incorrectly. Now what?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6\">\r\n<div class=\"main-container\">\r\n<div class=\"accordion-option\">&nbsp;</div>\r\n\r\n<div class=\"clearfix\">&nbsp;</div>\r\n\r\n<div aria-multiselectable=\"true\" class=\"panel-group\" id=\"accordion\" role=\"tablist\">\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingfive\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseThree\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapsefive\" role=\"button\">Prescriptions </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingfive\" class=\"panel-collapse collapse .in\" id=\"collapsefive\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>How do I read my prescription?</li>\r\n	<li>What is &quot;PD&quot; and how do I get mine?</li>\r\n	<li>How can I measure my PD?</li>\r\n	<li>Can I use contact lens prescription for ordering glasses?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingsix\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseThree\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapsesix\" role=\"button\">Lenses </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingsix\" class=\"panel-collapse collapse .in\" id=\"collapsesix\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>Do you offer prism correction?</li>\r\n	<li>What are EyeBuyDirect lenses made of?</li>\r\n	<li>How are bifocal and progressive distances decided?</li>\r\n	<li>What materials do you use for your bifocal lenses?</li>\r\n	<li>Can I purchase lenses by themselves?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingseven\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseThree\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseseven\" role=\"button\">Payment </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingseven\" class=\"panel-collapse collapse .in\" id=\"collapseseven\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>Do you accept insurance?</li>\r\n	<li>What payment methods do you accept?</li>\r\n	<li>How are your payments secured?</li>\r\n	<li>How can I obtain a sales invoice</li>\r\n	<li>Is it possible to have double discounts on my order?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\" id=\"headingsix\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a aria-controls=\"collapseThree\" aria-expanded=\"true\" class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapsesix\" role=\"button\">Returns </a></h4>\r\n</div>\r\n\r\n<div aria-labelledby=\"headingsix\" class=\"panel-collapse collapse .in\" id=\"collapsesix\" role=\"tabpanel\">\r\n<div class=\"panel-body\">\r\n<ul>\r\n	<li>What is your return policy?</li>\r\n	<li>How do I make an exchange or return?</li>\r\n	<li>Anything else I need to know about making an exchange?</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h1 class=\"faq-title text-light\">HOW CAN WE HELP?</h1>\r\n</div>\r\n\r\n<div class=\"col-md-4\">\r\n<div class=\"how-can\">\r\n<ul>\r\n	<li>Order Status</li>\r\n	<li><a href=\"\">Track Order</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-8\">\r\n<div class=\"how-can-2\">\r\n<p>Check order status, view order history or track packages</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4\">\r\n<div class=\"how-can\">\r\n<ul>\r\n	<li>Return &amp; Exchange</li>\r\n	<li><a href=\"\">Shipping &amp; Return</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-8\">\r\n<div class=\"how-can-2\">\r\n<p>We offer 14 days free exchange or refund and 365 day product guarantee</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4\">\r\n<div class=\"how-can\">\r\n<ul>\r\n	<li>Contact us 0-000-000-0000</li>\r\n	<li><a href=\"\">Shipping &amp; Return</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-8\">\r\n<div class=\"how-can-2\">\r\n<p>Chat with us via live chat or give us a call. Mon-Fri 9am-10pm EST</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 1, '2018-09-23 15:16:37', 0, 0, 'page', 'default', NULL, NULL, 1, 1, 1, NULL, '2018-09-23 10:08:24', '2018-09-23 10:16:37');
INSERT INTO `posts` (`id`, `title`, `slug`, `meta_keywords`, `meta_description`, `content`, `published`, `published_at`, `private`, `internal`, `type`, `template`, `featured_image_link`, `extras`, `author_id`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(10, 'TERMS OF USE', 'terms-of-use', NULL, NULL, '<section class=\"p-t-lg p-b-lg bannerSection custom_margin_top\">\r\n			   	<div class=\"container\">\r\n			   		<div class=\"row\">\r\n						\r\n						<div class=\"m-t-lg col-md-12 col-sm-12 col-xs-12 sm-center\">\r\n							<h1>Terms and Conditions</h1>\r\n							<p></p><h3>1. Contractual Commitment</h3>\r\nThese Terms of Use (\"Terms\") govern your access or use, from within the United States and its territories and possessions, of the applications, websites, content, products, and services (the \"Services,\" as more fully defined below in Section 3) made available in the United States and its territories and possessions by VendSpin LLC and its parents, subsidiaries, representatives, affiliates, officers and directors (collectively, \"VendSpin\"). PLEASE READ THESE TERMS CAREFULLY, AS THEY CONSTITUTE A LEGAL AGREEMENT BETWEEN YOU AND VENDSPIN. In these Terms, the words \"including\" and \"include\" mean \"including, but not limited to.\"\r\n<br>\r\n\r\nBy accessing or using the Services, you confirm your agreement to be bound by these Terms. If you do not agree to these Terms, you may not access or use the Services. These Terms expressly supersede prior agreements or arrangements with you. VendSpin may immediately terminate these Terms or any Services with respect to you, or generally cease offering or deny access to the Services or any portion thereof, at any time for any reason.\r\n<br>\r\nIMPORTANT: PLEASE REVIEW THE ARBITRATION AGREEMENT SET FORTH BELOW CAREFULLY, AS IT WILL REQUIRE YOU TO RESOLVE DISPUTES WITH VENDSPIN ON AN INDIVIDUAL BASIS THROUGH FINAL AND BINDING ARBITRATION. BY ENTERING THIS AGREEMENT, YOU EXPRESSLY ACKNOWLEDGE THAT YOU HAVE READ AND UNDERSTAND ALL OF THE TERMS OF THIS AGREEMENT AND HAVE TAKEN TIME TO CONSIDER THE CONSEQUENCES OF THIS IMPORTANT DECISION.\r\n<br>\r\nSupplemental terms may apply to certain Services, such as policies for a particular event, program, activity or promotion, and such supplemental terms will be disclosed to you in separate region-specific disclosures (e.g., a particular city webpage on VendSpin.com) or in connection with the applicable Service(s). Supplemental terms are in addition to, and shall be deemed a part of, the Terms for the purposes of the applicable Service(s). Supplemental terms shall prevail over these Terms in the event of a conflict with respect to the applicable Services.\r\n<br>\r\nVendSpin may amend the Terms from time to time. Amendments will be effective upon VendSpin\'s posting of such updated Terms at this location or in the amended policies or supplemental terms on the applicable Service(s). Your continued access or use of the Services after such posting confirms your consent to be bound by the Terms, as amended. If VendSpin changes these Terms after the date you first agreed to the Terms (or to any subsequent changes to these Terms), you may reject any such change by providing VendSpin written notice of such rejection within 30 days of the date such change became effective, as indicated in the \"Effective\" date above. This written notice must be provided either (a) by mail or hand delivery to our registered agent for service of process, c/o VendSpin LLC or by email from the email address associated with your Account to: accounts@VendSpin.com .In order to be effective, the notice must include your full name and clearly indicate your intent to reject changes to these Terms. By rejecting changes, you are agreeing that you will continue to be bound by the provisions of these Terms as of the date you first agreed to the Terms (or to any subsequent changes to these Terms).\r\n<br>\r\nVendSpin’s collection and use of personal information in connection with the Services is described in VendSpin\'s Privacy Statements which you also agree to by using service. Privacy policy will be displayed on VendSpin\'s website.\r\n<br>\r\n<h3>2. Arbitration Agreement</h3>\r\n\r\nBy agreeing to the Terms, you agree that you are required to resolve any claim that you may have against VendSpin on an individual basis in arbitration, as set forth in this Arbitration Agreement. This will preclude you from bringing any class, collective, or representative action against VendSpin, and also preclude you from participating in or recovering relief under any current or future class, collective, consolidated, or representative action brought against VendSpin by someone else.\r\n<br>\r\nAgreement to Binding Arbitration Between You and VendSpin.\r\n<br>\r\nYou and VendSpin agree that any dispute, claim or controversy arising out of or relating to (a) these Terms or the existence, breach, termination, enforcement, interpretation or validity thereof, or (b) your access to or use of the Services at any time, whether before or after the date you agreed to the Terms, will be settled by binding arbitration between you and VendSpin, and not in a court of law.\r\n<br>\r\nYou acknowledge and agree that you and VendSpin are each waiving the right to a trial by jury or to participate as a plaintiff or class member in any purported class action or representative proceeding. Unless both you and VendSpin otherwise agree in writing, any arbitration will be conducted only on an individual basis and not in a class, collective, consolidated, or representative proceeding. However, you and VendSpin each retain the right to bring an individual action in small claims court and the right to seek injunctive or other equitable relief in a court of competent jurisdiction to prevent the actual or threatened infringement, misappropriation or violation of a party\'s copyrights, trademarks, trade secrets, patents or other intellectual property rights.\r\n<br>\r\nRules and Governing Law.\r\n<br>\r\nThe arbitration will be administered by the American Arbitration Association (\"AAA\") in accordance with the AAA’s Consumer Arbitration Rules and the Supplementary Procedures for Consumer Related Disputes (the \"AAA Rules\") then in effect, except as modified by this Arbitration Agreement. \r\n<br>\r\nThe parties agree that the arbitrator (“Arbitrator”), and not any federal, state, or local court or agency, shall have exclusive authority to resolve any disputes relating to the interpretation, applicability, enforceability or formation of this Arbitration Agreement, including any claim that all or any part of this Arbitration Agreement is void or voidable. The Arbitrator shall also be responsible for determining all threshold arbitrability issues, including issues relating to whether the Terms are unconscionable or illusory and any defense to arbitration, including waiver, delay, laches, or estoppel.\r\n<br>\r\nNotwithstanding any choice of law or other provision in the Terms, the parties agree and acknowledge that this Arbitration Agreement evidences a transaction involving interstate commerce and that the Federal Arbitration Act, 9 U.S.C. § 1 et seq. (“FAA”), will govern its interpretation and enforcement and proceedings pursuant thereto. It is the intent of the parties that the FAA and AAA Rules shall preempt all state laws to the fullest extent permitted by law. If the FAA and AAA Rules are found to not apply to any issue that arises under this Arbitration Agreement or the enforcement thereof, then that issue shall be resolved under the laws of the state of Pennsylvania.\r\n<br>\r\n<h3>Process.</h3>\r\nAll users are required to agree to these terms. By continuing use you fully understand VendSpin has made it clear that there is ABSOLUTELY no tolerance for objectionable content or abusive users. Any user accounts found in violation for objectionable content or abuse will be deleted immediately with no refund and no recovery of data stored within app.\r\n\r\nA party who desires to initiate arbitration must provide the other party with a written Demand for Arbitration as specified in the AAA Rules. The Arbitrator will be either (1) a retired judge or (2) an attorney specifically licensed to practice law in the state of Pennsylvania and will be selected by the parties from the AAA\'s roster of consumer dispute arbitrators. If the parties are unable to agree upon an Arbitrator within seven (7) days of delivery of the Demand for Arbitration, then the AAA will appoint the Arbitrator in accordance with the AAA Rules.\r\n<br>\r\n<h3>Location and Procedure.</h3>\r\n\r\nUnless you and VendSpin otherwise agree, the arbitration will be conducted in the county where you reside. If your claim does not exceed $10,001, then the arbitration will be conducted solely on the basis of documents you and VendSpin submit to the Arbitrator, unless you request a hearing or the Arbitrator determines that a hearing is necessary. If your claim exceeds $10,001, your right to a hearing will be determined by the AAA Rules. Subject to the AAA Rules, the Arbitrator will have the discretion to direct a reasonable exchange of information by the parties, consistent with the expedited nature of the arbitration.\r\n<br>\r\n<h3>Arbitrator\'s Decision.</h3>\r\n\r\nThe Arbitrator will render an award within the time frame specified in the AAA Rules. Judgment on the arbitration award may be entered in any court having competent jurisdiction to do so. The Arbitrator may award declaratory or injunctive relief only in favor of the claimant and only to the extent necessary to provide relief warranted by the claimant\'s individual claim. An Arbitrator’s decision shall be final and binding on all parties. An Arbitrator’s decision and judgment thereon shall have no precedential or collateral estoppel effect. VendSpin will seek reimbursement of all attorneys fees or cost if VendSpin prevails.\r\n<br>\r\n<h3>Fees.</h3>\r\n\r\nYour responsibility to pay any AAA filing, administrative and arbitrator fees will be solely as set forth in the AAA Rules. VendSpin may be entitled to pay such fees, unless the Arbitrator finds that either the substance of your claim or the relief sought in your Demand for Arbitration was frivolous or was brought for an improper purpose (as measured by the standards set forth in Federal Rule of Civil Procedure 11(b)).\r\n<br>\r\n<h3>Changes.</h3>\r\n\r\nNotwithstanding the provisions in Section I above, regarding consent to be bound by amendments to these Terms, if VendSpin changes this Arbitration Agreement after the date you first agreed to the Terms (or to any subsequent changes to the Terms), you may reject any such change by providing VendSpin written notice of such rejection within 30 days of the date such change became effective, as indicated in the \"Effective\" date above. This written notice must be provided either (a) by mail or hand delivery to our registered agent for service of process, c/o VendSpin LLC or by email from the email address associated with your Account to: accounts@VendSpin.com. In order to be effective, the notice must include your full name and clearly indicate your intent to reject changes to this Arbitration Agreement. By rejecting changes, you are agreeing that you will arbitrate any dispute between you and VendSpin in accordance with the provisions of this Arbitration Agreement as of the date you first agreed to the Terms (or to any subsequent changes to the Terms).\r\n<br>\r\n<h3>Severability and Survival.</h3>\r\n\r\nIf any portion of this Arbitration Agreement is found to be unenforceable or unlawful for any reason, (1) the unenforceable or unlawful provision shall be severed from these Terms; (2) severance of the unenforceable or unlawful provision shall have no impact whatsoever on the remainder of the Arbitration Agreement or the parties’ ability to compel arbitration of any remaining claims on an individual basis pursuant to the Arbitration Agreement; and (3) to the extent that any claims must therefore proceed on a class, collective, consolidated, or representative basis, such claims must be litigated in a civil court of competent jurisdiction and not in arbitration, and the parties agree that litigation of those claims shall be stayed pending the outcome of any individual claims in arbitration.\r\n<br>\r\n<h3>3. The Services</h3>\r\n\r\nThe Services comprise mobile applications and related services (each, an \"Application\"), which enable users to select, arrange and schedule sales of various products or items, logistics and/or delivery services and/or to purchase certain goods, including with third party providers of such services and goods under agreement with VendSpin or certain of VendSpin affiliates (\"Third Party Providers\"). In certain instances the Services may also include an option to receive products or items, logistics and/or delivery services for an upfront price, subject to acceptance by the respective Third Party Providers. Unless otherwise agreed by VendSpin in a separate written agreement with you, the Services are made available solely for your personal, noncommercial use. YOU ACKNOWLEDGE THAT YOUR ABILITY TO OBTAIN PRODUCTS OR ITEMS, LOGISTICS AND/OR DELIVERY SERVICES THROUGH THE USE OF THE SERVICES DOES NOT ESTABLISH VENDSPIN AS A PROVIDER OF PRODUCTS OR ITEMS, LOGISTICS OR DELIVERY SERVICES CARRIER.\r\n<br>\r\n<h3>License.</h3>\r\n\r\nSubject to your compliance with these Terms, VendSpin grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferable license to: (i) access and use the Applications on your personal device solely in connection with your use of the Services; and (ii) access and use any content, information and related materials that may be made available through the Services, in each case solely for your personal, noncommercial use. Any rights not expressly granted herein are reserved by VendSpin and VendSpin\'s licensors.\r\n<br>\r\n<h3>Restrictions.</h3>\r\n\r\nYou may not: (i) remove any copyright, trademark or other proprietary notices from any portion of the Services; (ii) reproduce, modify, prepare derivative works based upon, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services except as expressly permitted by VendSpin; (iii) decompile, reverse engineer or disassemble the Services except as may be permitted by applicable law; (iv) link to, mirror or frame any portion of the Services or suggested equipment; (v) cause or launch any programs or scripts for the purpose of scraping, indexing, surveying, or otherwise data mining any portion of the Services or unduly burdening or hindering the operation and/or functionality of any aspect of the Services; or (vi) attempt to gain unauthorized access to or impair any aspect of the Services or its related systems or networks.\r\n<br>\r\n<h3>Provision of the Services.</h3>\r\n\r\nYou acknowledge that portions of the Services may be made available under VendSpin various brands or request options associated with products/items, deliveries or logistics. You also acknowledge that the Services may be made available under such brands or request options by or in connection with: (i) certain of VendSpin\'s subsidiaries and affiliates; or (ii) independent Third Party Providers, including Delivery Network Company drivers, Delivery Permit holders or holders of similar delivery permits, authorizations or licenses.\r\n<br>\r\n<h3>Third Party Services and Content.</h3>\r\n\r\nThe Services may be made available or accessed in connection with third party services and content (including advertising) that VendSpin does not control. You acknowledge that different terms of use and privacy policies may apply to your use of such third party services and content. VendSpin does not endorse such third party services and content and in no event shall VendSpin be responsible or liable for any products or services of such third party providers. Additionally, Apple Inc., Google, Inc., Microsoft Corporation or BlackBerry Limited will be a third-party beneficiary to this contract if you access the Services using Applications developed for Apple iOS, Android, Microsoft Windows, or Blackberry-powered mobile devices, respectively. These third party beneficiaries are not parties to this contract and are not responsible for the provision or support of the Services in any manner. Your access to the Services using these devices is subject to terms set forth in the applicable third party beneficiary\'s terms of service.\r\n<br>\r\n<h3>Ownership.</h3><h3>\r\n\r\nThe Services and all rights therein are and shall remain VendSpin property or the property of VendSpin\'s licensors. Neither these Terms nor your use of the Services convey or grant to you any rights: (i) in or related to the Services except for the limited license granted above; or (ii) to use or reference in any manner VendSpin\'s company names, logos, product and service names, trademarks or services marks or those of VendSpin\'s licensors.\r\n<br>\r\n</h3><h3>4. Access and Use of the Services</h3>\r\n<br>\r\n<h3>User Accounts.</h3>\r\n\r\nIn order to use most aspects of the Services, you must register for and maintain an active personal user Services account (\"Account\"). You must be at least 18 years of age, or the age of legal majority in your jurisdiction (if different than 18), to obtain an Account, unless a specific Service permits otherwise. Account registration requires you to submit to VendSpin certain personal information, such as your name, address, mobile phone number and age, as well as at least one valid payment method supported by VendSpin. You agree to maintain accurate, complete, and up-to-date information in your Account. Your failure to maintain accurate, complete, and up-to-date Account information, including having an invalid or expired payment method on file, may result in your inability to access or use the Services. You are responsible for all activity that occurs under your Account, and you agree to maintain the security and secrecy of your Account username and password at all times. Unless otherwise permitted by VendSpin in writing, you may only possess one Account.\r\n<br>\r\n<h3>User Requirements and Conduct.</h3>\r\n\r\nThe Service is not available for use by persons under the age of 18. You may not authorize third parties to use your Account, and you may not allow persons under the age of 18 to receive products/items, delivery or logistics services from Third Party Providers unless they are accompanied by you. You may not assign or otherwise transfer your Account to any other person or entity. You agree to comply with all applicable laws when accessing or using the Services, and you may only access or use the Services for lawful purposes (e.g., no deliveries of unlawful or hazardous materials or products/items ). You may not in your access or use of the Services cause nuisance, annoyance, inconvenience, or property damage, whether to the Third Party Provider or any other party. In certain instances you may be asked to provide proof of identity to access or use the Services, and you agree that you may be denied access to or use of the Services if you refuse to provide proof of identity.\r\n<br>\r\n<h3>Text Messaging,Telephone Calls&amp; Emails.</h3>\r\n\r\nYou agree that VendSpin may contact you by telephone, text messages or email (including by an automatic telephone dialing system) at any of the phone numbers or email address provided by you or on your behalf in connection with an VendSpin account, including for marketing purposes. You understand that you are not required to provide this consent as a condition of purchasing any property, goods or services. You also understand that you may opt out of receiving text messages or emails from VendSpin at any time, by emailing NoAds@VendSpin.com. If you do not choose to opt out, VendSpin may contact you.\r\n<br>\r\nReferrals and Promotional Codes.                                                       VendSpin may, in its sole discretion, create referral and/or promotional codes (\"Promo Codes\") that may be redeemed for discounts on future Services and/or a Third Party Provider\'s services, or other features or benefits related to the Services and/or a Third Party Provider\'s services, subject to any additional terms that VendSpin establishes. You agree that Promo Codes: (i) must be used for the intended audience and purpose, and in a lawful manner; (ii) may not be duplicated, sold or transferred in any manner, or made available to the general public (whether posted to a public form or otherwise), unless expressly permitted by VendSpin; (iii) may be disabled by VendSpin at any time for any reason without liability to VendSpin; (iv) may only be used pursuant to the specific terms that VendSpin establishes for such Promo Code; (v) are not valid for cash; and (vi) may expire prior to your use. VendSpin reserves the right to withhold or deduct credits or other features or benefits obtained through the use of the referral system or Promo Codes by you or any other user in the event that VendSpin determines or believes that the use of the referral system or use or redemption of the Promo Code was in error, fraudulent, illegal, or otherwise in violation of VendSpin’s Terms.\r\n<br>\r\nUser Provided Content.       VendSpin may, in VendSpin\'s sole discretion, permit you from time to time to submit, upload, publish or otherwise make available to VendSpin through the Services textual, audio, and/or visual content and information, including commentary and feedback related to the Services, initiation of support requests, and submission of entries for competitions and promotions (\"User Content\"). Any User Content provided by you remains your property. However, by providing User Content to VendSpin, you grant VendSpin a worldwide, perpetual, irrevocable, transferable, royalty-free license, with the right to sublicense, to use, copy, modify, create derivative works of, distribute, publicly display, publicly perform, and otherwise exploit in any manner such User Content in all formats and distribution channels now known or hereafter devised (including in connection with the Services and VendSpin\'s business and on third-party sites and services), without further notice to or consent from you, and without the requirement of payment to you or any other person or entity.\r\n<br>\r\nYou represent and warrant that: (i) you either are the sole and exclusive owner of all User Content or you have all rights, licenses, consents and releases necessary to grant VendSpin the license to the User Content as set forth above; and (ii) neither the User Content, nor your submission, uploading, publishing or otherwise making available of such User Content, nor VendSpin\'s use of the User Content as permitted herein will infringe, misappropriate or violate a third party\'s intellectual property or proprietary rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.\r\n<br>\r\nYou agree to not provide User Content that is defamatory, libelous, hateful, violent, obscene, pornographic, unlawful, or otherwise offensive, as determined by VendSpin in its sole discretion, whether or not such material may be protected by law. VendSpin may, but shall not be obligated to, review, monitor, or remove User Content, at VendSpin\'s sole discretion and at any time and for any reason, without notice to you.\r\n<br>\r\n<h3>Network Access and Devices.</h3>\r\n\r\nYou are responsible for obtaining the data network access necessary to use the Services. Your mobile network\'s data and messaging rates and fees may apply if you access or use the Services from your device. You are responsible for acquiring and updating compatible hardware or devices necessary to access and use the Services and Applications and any updates thereto. VendSpin does not guarantee that the Services, or any portion thereof, will function on any particular hardware or devices. In addition, the Services may be subject to malfunctions and delays inherent in the use of the Internet and electronic communications.\r\n<br>\r\n<h3>5. Payment</h3>\r\n\r\nYou understand that use of the Services may result in charges to you for the services or goods you receive (\"Charges\"). VendSpin will receive and/or enable your payment of the applicable Charges for services or goods obtained through your use of the Services. Charges will be inclusive of applicable taxes where required by law. Charges may include other applicable fees, depending on your particular location. Each individual merchant is responsible for paying their own taxes or any fees accumulated through use of service required by law.\r\n<br>\r\nAll Charges and payments will be enabled by VendSpin using the preferred payment method designated in your Account, after which you will receive a receipt by email. If your primary Account payment method is determined to be expired, invalid or otherwise not able to be charged, you agree that VendSpin may use a secondary payment method in your Account, if available. Charges paid by you are final and non-refundable, unless otherwise determined by VendSpin.\r\n<br>\r\nAs between you and VendSpin, VendSpin reserves the right to establish, remove and/or revise Charges for any or all services or goods obtained through the use of the Services at any time in VendSpin\'s sole discretion. Further, you acknowledge and agree that Charges applicable in certain geographical areas may increase substantially during times of high demand. VendSpin will use reasonable efforts to inform you of Charges that may apply, provided that you will be responsible for Charges incurred under your Account regardless of your awareness of such Charges or the amounts thereof. VendSpin may from time to time provide certain users with promotional offers and discounts that may result in different amounts charged for the same or similar services or goods obtained through the use of the Services, and you agree that such promotional offers and discounts, unless also made available to you, shall have no bearing on your use of the Services or the Charges applied to you. You may elect to cancel your request for Services at any time prior to the commencement of such Services, in which case you may be charged a cancellation fee on a Third Party Provider’s behalf. After you have received services or goods obtained through the Service, you will have the opportunity to rate your experience and leave additional feedback. VendSpin may use the proceeds of any Charges for any purpose, subject to any payment obligations it has agreed to with any Third Party Providers or other third parties.\r\n<br>\r\nIn certain cases, with respect to Third Party Providers, Charges you incur will be owed directly to Third Party Providers, and VendSpin will collect payment of those charges from you, on the Third Party Provider’s behalf as their limited payment collection agent, and payment of the Charges shall be considered the same as payment made directly by you to the Third Party Provider. In such cases, you retain the right to request lower Charges from a Third Party Provider for services or goods received by you from such Third Party Provider at the time you receive such services or goods, and Charges you incur will be owed to the Third Party Provider. VendSpin will respond accordingly to any request from a Third Party Provider to modify the Charges for a particular service or goods. This payment structure is intended to fully compensate a Third Party Provider, if applicable, for the services or goods obtained in connection with your use of the Services. In all other cases, Charges you incur will be owed and paid directly to VendSpin or its affiliates, where VendSpin is solely liable for any obligations to Third Party Providers. In such cases, you retain the right to request lower Charges from VendSpin for services or goods received by you from a Third Party Provider at the time you receive such services or goods, and VendSpin will respond accordingly to any request from you to modify the Charges for a particular service or good. VendSpin does not designate any portion of your payment as a tip or gratuity or delivery fees to a Third Party Provider. Any representation by VendSpin (on VendSpin\'s website, in the Application, or in VendSpin\'s marketing materials) to the effect that tipping is \"voluntary,\" \"not required,\" and/or \"included\" in the payments you make for services or goods provided is not intended to suggest that VendSpin provides any additional amounts, beyond those described above, to a Third Party Provider you may use. You understand and agree that, while you are free to provide additional payment as a gratuity to any Third Party Provider who provides you with services or goods obtained through the Service, you are under no obligation to do so. Gratuities are voluntary.\r\n<br>\r\n<h3>Repair or Cleaning Fees.</h3>\r\n<br>\r\nYou shall be responsible for the cost of repair for damage to, or necessary cleaning of, vehicles and property resulting from use of the Services under your Account in excess of normal \"wear and tear\" damages and necessary cleaning (\"Repair or Cleaning\"). In the event that a Repair or Cleaning request is verified by VendSpin in VendSpin\'s reasonable discretion, VendSpin reserves the right to facilitate payment for the reasonable cost of such Repair or Cleaning using your payment method designated in your Account. Such amounts will be transferred by VendSpin to a Third Party Provider, if applicable, and are non-refundable.\r\n<br>\r\n<h3>6. Disclaimers; Limitation of Liability; Indemnity.</h3>\r\n<br>\r\n<h3>DISCLAIMER.</h3>\r\n\r\nTHE SERVICES ARE PROVIDED \"AS IS\" AND \"AS AVAILABLE.\" VENDSPIN DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED, OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN ADDITION, VENDSPIN MAKES NO REPRESENTATION, WARRANTY, OR GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, CLEANLINESS, SUITABILITY, OR AVAILABILITY OF THE SERVICES OR ANY SERVICES OR GOODS REQUESTED THROUGH THE USE OF THE SERVICES, OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. VENDSPIN DOES NOT GUARANTEE THE QUALITY, SUITABILITY, SAFETY OR ABILITY OF THIRD PARTY PROVIDERS OR THE PRODUCTS OR ITEMS THEY OFFER FOR PURCHASE. YOU AGREE THAT THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE SERVICES, AND ANY SERVICE OR GOOD REQUESTED IN CONNECTION THEREWITH, REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.\r\n<br>\r\nLIMITATION OF LIABILITY. VENDSPIN SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOST DATA, PERSONAL INJURY, OR PROPERTY DAMAGE RELATED TO, IN CONNECTION WITH, OR OTHERWISE RESULTING FROM ANY USE OF THE SERVICES, REGARDLESS OF THE NEGLIGENCE (EITHER ACTIVE, AFFIRMATIVE, SOLE, OR CONCURRENT) OF VENDSPIN, EVEN IF VENDSPIN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.\r\n<br>\r\nVENDSPIN SHALL NOT BE LIABLE FOR ANY DAMAGES, LIABILITY OR LOSSES ARISING OUT OF: (i) YOUR USE OF OR RELIANCE ON THE SERVICES OR YOUR INABILITY TO ACCESS OR USE THE SERVICES; OR (ii) ANY TRANSACTION OR RELATIONSHIP BETWEEN YOU AND ANY THIRD PARTY PROVIDER, EVEN IF VENDSPIN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. VENDSPIN SHALL NOT BE LIABLE FOR DELAY OR FAILURE IN PERFORMANCE RESULTING FROM CAUSES BEYOND VENDSPIN\'S REASONABLE CONTROL. YOU ACKNOWLEDGE THAT THIRD PARTY PROVIDERS PROVIDING DELIVERY SERVICES REQUESTED THROUGH SOME REQUEST PRODUCTS AND MAY NOT BE PROFESSIONALLY LICENSED OR PERMITTED.\r\n<br>\r\nTHE SERVICES MAY BE USED BY YOU TO REQUEST AND SCHEDULE DELIVERIES, GOODS, OR LOGISTICS SERVICES WITH THIRD PARTY PROVIDERS, BUT YOU AGREE THAT VENDSPIN HAS NO RESPONSIBILITY OR LIABILITY TO YOU RELATED TO ANY DELIVERIES, GOODS OR LOGISTICS SERVICES PROVIDED TO YOU BY THIRD PARTY PROVIDERS OTHER THAN AS EXPRESSLY SET FORTH IN THESE TERMS.\r\n<br>\r\nTHE LIMITATIONS AND DISCLAIMER IN THIS SECTION DO NOT PURPORT TO LIMIT LIABILITY OR ALTER YOUR RIGHTS AS A CONSUMER THAT CANNOT BE EXCLUDED UNDER APPLICABLE LAW. BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS, VENDSPIN’S LIABILITY SHALL BE LIMITED TO THE EXTENT PERMITTED BY LAW. THIS PROVISION SHALL HAVE NO EFFECT ON VENDSPIN’S CHOICE OF LAW PROVISION SET FORTH BELOW.\r\n<br>\r\n<h3>Indemnity.</h3>\r\n\r\nYou agree to indemnify and hold VendSpin and its affiliates and their officers, directors, employees, and agents harmless from any and all claims, demands, losses, liabilities, and expenses (including attorneys\' fees), arising out of or in connection with: (i) your use of the Services or services or goods obtained through your use of the Services; (ii) your breach or violation of any of these Terms; (iii) VendSpin\'s use of your User Content; or (iv) your violation of the rights of any third party, including Third Party Providers.\r\n<br>\r\n<h3>7. Other Provisions</h3>\r\n<br>\r\n<h3>Choice of Law.</h3>\r\n\r\nThese Terms are governed by and construed in accordance with the laws of the State of Pennsylvania, U.S.A., without giving effect to any conflict of law principles, except as may be otherwise provided in the Arbitration Agreement above or in supplemental terms applicable to your region. However, the choice of law provision regarding the interpretation of these Terms is not intended to create any other substantive right to non-Pennsylvanians to assert claims under Pennsylvania law whether that be by statute, common law, or otherwise. These provisions, and except as otherwise provided in Section 2 of these Terms, are only intended to specify the use of Pennsylvania law to interpret these Terms and the forum for disputes asserting a breach of these Terms, and these provisions shall not be interpreted as generally extending Pennsylvania law to you if you do not otherwise reside in Pennsylvania. The foregoing choice of law and forum selection provisions do not apply to the arbitration clause in Section 2 or to any arbitrable disputes as defined therein. Instead, as described in Section 2, the Federal Arbitration Act shall apply to any such disputes.\r\n<br>\r\nClaims of Copyright Infringement.\r\n<br>\r\nClaims of copyright infringement should be sent to VendSpin\'s designated agent. \r\n<br>\r\nNotice                   VendSpin may give notice by means of a general notice on the Services, electronic mail to your email address in your Account, telephone or text message to any phone number provided in connection with your account, or by written communication sent by first class mail or pre-paid post to any address connected with your Account. Such notice shall be deemed to have been given upon the expiration of 48 hours after mailing or posting (if sent by first class mail or pre-paid post) or 12 hours after sending (if sent by email or telephone). You may give notice to VendSpin, with such notice deemed given when received by VendSpin, at any time by first class mail or pre-paid post to our registered agent for service of process, c/o VendSpin LLC. \r\n<br>\r\n<h3>General.</h3>\r\n\r\nYou may not assign these Terms without VendSpin\'s prior written approval. VendSpin may assign these Terms without your consent to: (i) a subsidiary or affiliate; (ii) an acquirer of VendSpin\'s equity, business or assets; or (iii) a successor by merger. Any purported assignment in violation of this section shall be void. No joint venture, partnership, employment, or agency relationship exists between you, VendSpin or any Third Party Provider as a result of this Agreement or use of the Services. If any provision of these Terms is held to be invalid or unenforceable, such provision shall be struck and the remaining provisions shall be enforced to the fullest extent under law. VendSpin\'s failure to enforce any right or provision in these Terms shall not constitute a waiver of such right or provision unless acknowledged and agreed to by VendSpin in writing. This provision shall not affect the Severability and Survivability section of the Arbitration Agreement of these Terms.<p></p> \r\n \r\n						</div>\r\n\r\n			   		</div>\r\n			   	</div>\r\n		   </section>', 1, '2018-10-09 08:21:19', 0, 0, 'page', NULL, NULL, NULL, 1, 1, 1, NULL, '2018-10-07 19:29:33', '2018-10-09 15:21:19'),
(11, 'Privacy Policy', 'privacy-policy', NULL, NULL, '<section class=\"p-t-lg p-b-lg bannerSection custom_margin_top\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"m-t-lg col-md-12 col-sm-12 col-xs-12 sm-center\">\r\n<h1>Privacy Policy</h1>\r\nOur Privacy Policy was last updated and posted on June 11, 2017. It governs the privacy terms of our Website, located at http://www.VendSpin.com. Any capitalized terms not defined in our Privacy Policy, have the meaning as specified in our Terms of Service.<br />\r\nYour privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. We use your Personal Information only for providing and improving the Site. By using the Site, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible at http://www.VendSpin.com. The following outlines our privacy policy.<br />\r\n<br />\r\n&nbsp;\r\n<ul class=\"static\" style=\"font-family: \'OpenSansRegular\'; font-size:18px; color:#444444; line-height:24px; padding-left:0px; text-align: left;\">\r\n	<li>Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</li>\r\n	<li>We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.</li>\r\n	<li>We will only retain personal information as long as necessary for the fulfillment of those purposes.</li>\r\n	<li>We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</li>\r\n	<li>Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</li>\r\n	<li>We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</li>\r\n	<li>We will make readily available to customers information about our policies and practices relating to the management of personal information.</li>\r\n</ul>\r\n<br />\r\n<br />\r\nVendSpin LLC, follows all legal requirements to protect your privacy. Our Privacy Policy is a legal statement that explains how we may collect information from you, how we may share your information, and how you can limit our sharing of your information. You will see terms in our Privacy Policy that are capitalized. These terms have meanings as described in the Definitions section below.<br />\r\nDefinitions &quot;Non Personal Information&quot; is information that is not personally identifiable to you and that we automatically collect when you access our Website with a web browser. It may also include publicly available information that is shared between you and others. &quot;Personally Identifiable Information&quot; is non-public information that is personally identifiable to you and obtained in order for us to provide you within our Website. Personally Identifiable Information may include information such as your name, email address, and other related information that you provide to us or that we obtain about you.<br />\r\nInformation We Collect Generally, you control the amount and type of information you provide to us when using our Website. As a Visitor, you can browse our website to find out more about our Website. You are not required to provide us with any Personally Identifiable Information as a Visitor. Computer Information Collected<br />\r\nWhen you use our Website, we automatically collect certain computer information by the interaction of your mobile phone or web browser with our Website. Such information is typically considered Non Personal Information. We also collect the following:\r\n<h3>Cookies</h3>\r\nOur Website uses &quot;Cookies&quot; to identify the areas of our Website that you have visited. A Cookie is a small piece of data stored on your computer or mobile device by your web browser. We use Cookies to personalize the Content that you see on our Website. Most web browsers can be set to disable the use of Cookies. However, if you disable Cookies, you may not be able to access functionality on our Website correctly or at all. We never place Personally Identifiable Information in Cookies.\r\n\r\n<h3>Automatic Information</h3>\r\nWe automatically receive information from your web browser or mobile device. This information includes the name of the website from which you entered our Website, if any, as well as the name of the website to which you&#39;re headed when you leave our website. This information also includes the IP address of your computer/proxy server that you use to access the Internet, your Internet Website provider name, web browser type, type of mobile device, and computer operating system. We use all of this information to analyze trends among our Users to help improve our Website.\r\n\r\n<h3>Log Data</h3>\r\nLike many site operators, we collect information that your browser sends whenever you visit our Site (&quot;Log Data&quot;). This Log Data may include information such as your computer&#39;s Internet Protocol (&quot;IP&quot;) address, browser type, browser version, the pages of our Site that you visit, the time and date of your visit, the time spent on those pages and other statistics. How We Use Your Information We use the information we receive from you as follows:\r\n\r\n<h3>Customizing Our Website</h3>\r\nWe may use the Personally Identifiable information you provide to us along with any computer information we receive to customize our Website.\r\n\r\n<h3>Sharing Information with Affiliates and Other Third Parties</h3>\r\nWe do not sell, rent, or otherwise provide your Personally Identifiable Information to third parties for marketing purposes. We may provide your Personally Identifiable Information to affiliates that provide services to us with regards to our Website (i.e. payment processors, Website hosting companies, etc.); such affiliates will only receive information necessary to provide the respective services and will be bound by confidentiality agreements limiting the use of such information.\r\n\r\n<h3>Data Aggregation</h3>\r\nWe retain the right to collect and use any Non Personal Information collected from your use of our Website and aggregate such data for internal analytics that improve our Website and Service as well as for use or resale to others. At no time is your Personally Identifiable Information included in such data aggregations.\r\n\r\n<h3>Legally Required Releases of Information</h3>\r\nWe may be legally required to disclose your Personally Identifiable Information, if such disclosure is (a) required by subpoena, law, or other legal process; (b) necessary to assist law enforcement officials or government enforcement agencies; (c) necessary to investigate violations of or otherwise enforce our Legal Terms; (d) necessary to protect us from legal action or claims from third parties including you and/or other Members; and/or (e) necessary to protect the legal rights, personal/real property, or personal safety of Vendspin LLC, our Users, employees, and affiliates. Links to Other Websites Our Website may contain links to other websites that are not under our direct control. These websites may have their own policies regarding privacy. We have no control of or responsibility for linked websites and provide these links solely for the convenience and information of our visitors. You access such linked Websites at your own risk. These websites are not subject to this Privacy Policy. You should check the privacy policies, if any, of those individual websites to see how the operators of those third-party websites will utilize your personal information. In addition, these websites may contain a link to Websites of our affiliates. The websites of our affiliates are not subject to this Privacy Policy, and you should check their individual privacy policies to see how the operators of such websites will utilize your personal information.<br />\r\nSecurity The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security. Privacy Policy Updates We reserve the right to modify this Privacy Policy at any time. You should review this Privacy Policy frequently. If we make material changes to this policy, we may notify you on our Website, by a blog post, by email, or by any method we determine. The method we chose is at our sole discretion. We will also change the &quot;Last Updated&quot; date at the beginning of this Privacy Policy. Any changes we make to our Privacy Policy are effective as of this Last Updated date and replace any prior Privacy Policies.<br />\r\nQuestions About Our Privacy Practices or This Privacy Policy We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. If you have any questions about our Privacy Practices or this Policy, please contact us.<br />\r\nWilliam Parker Founder/CEO of Vendspin www.VendSpin.com &quot;We&#39;ll send a store to your door&quot; or enable you to make thousands.</div>\r\n</div>\r\n</div>\r\n</section>', 1, '2018-10-09 05:16:25', 0, 0, 'page', NULL, NULL, NULL, 1, 1, 1, NULL, '2018-10-07 19:34:36', '2018-10-09 12:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscription_required` tinyint(1) NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `subscription_required`, `guard_name`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'superuser', 'Super User', 0, 'web', NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(2, 'member', 'Member', 1, 'web', NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(25, 2),
(26, 2),
(27, 2),
(32, 2),
(33, 2),
(46, 2),
(74, 2),
(75, 2),
(76, 2),
(77, 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('BOOLEAN','NUMBER','DATE','TEXT','SELECT','FILE','TEXTAREA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'General',
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `type`, `category`, `label`, `value`, `editable`, `hidden`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'TEXT', 'General', 'Site Name', 'GABA Optics', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-23 06:05:05'),
(2, 'super_user_id', 'NUMBER', 'General', 'Super user id', '1', 0, 1, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(3, 'super_user_role_id', 'NUMBER', 'General', 'Super user role id', '1', 0, 1, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(4, 'default_user_role', 'TEXT', 'User', 'Default User Role', 'Member', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(5, 'social_links', 'SELECT', 'General', 'Social Links', '{\"facebook\":\"https:\\/\\/www.facebook.com\\/coralslaraship\",\"twitter\":\"https:\\/\\/twitter.com\\/corals_laraship\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/\",\"instagram\":\"https:\\/\\/www.instagram.com\\/\",\"pinterest\":\"https:\\/\\/www.pinterest.com\\/\"}', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(6, 'twitter_id', 'TEXT', 'General', 'twitter_id', 'GABA Optics', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-23 06:05:38'),
(7, 'footer_text', 'TEXTAREA', 'General', 'Footer Text', '© 2018 <a target=\"_blank\" href=\"#\"\r\n                               title=GABA\">GABA</a>.\r\n                All Rights Reserved.', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-23 06:04:24'),
(8, 'terms_and_policy', 'TEXTAREA', 'General', 'Terms and Policy Text', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis iaculis ante eget urna tincidunt, sed tristique velit fermentum. Vivamus viverra urna sed quam semper feugiat. Mauris accumsan imperdiet metus, vitae porttitor mi egestas sit amet. Duis a nibh quam. Sed sit amet purus fringilla, auctor tellus et, consectetur libero. Nullam non orci tristique, sollicitudin magna sed, convallis est. Aenean fermentum arcu aliquet purus placerat, ut aliquam libero commodo. Pellentesque tortor purus, gravida rhoncus porttitor in, pulvinar eu mi. Sed vitae consectetur justo.\r\n</p>\r\n<p>\r\nAliquam aliquam, elit ac malesuada blandit, nulla ligula posuere nisl, non mattis arcu eros et enim. Proin dapibus erat sit amet egestas egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis vulputate tortor a massa porttitor, sit amet posuere mi pharetra. Cras efficitur lobortis condimentum. Vivamus dapibus cursus eros bibendum finibus. Donec rhoncus libero a sem volutpat, ut mattis orci sollicitudin. Pellentesque malesuada metus quis ullamcorper vestibulum. Aenean erat dui, elementum finibus ligula vitae, feugiat placerat tellus. Cras placerat in dolor in iaculis. Suspendisse tempor gravida vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi odio urna, lobortis sed euismod eget, semper sed lorem.\r\n</p>', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(9, 'site_logo', 'FILE', 'General', 'Site Logo', '1537772492-logo.jpg', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-24 14:01:32'),
(10, 'site_logo_white', 'FILE', 'General', 'Site White Logo', '1537772085-logo.jpg', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-24 13:54:45'),
(11, 'site_favicon', 'FILE', 'General', 'Site favicon', '1537772048-logo.jpg', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-24 13:54:08'),
(12, 'login_background', 'TEXTAREA', 'General', 'Login Background', 'background: url(/media/demo/login_backgrounds/login_background.png);\r\nbackground-repeat: repeat-y;\r\nbackground-size: 100% auto;\r\nbackground-position: center top;\r\nbackground-attachment: fixed;', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(13, 'google_map_url', 'TEXT', 'General', 'Google Map URL', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3387.331591494841!2d35.19981536504809!3d31.897586781246385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x518201279a8595!2sLeaders!5e0!3m2!1sen!2s!4v1512481232226', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(14, 'contact_form_email', 'TEXT', 'General', 'Contact Email', 'atheriqbal1@gmail.com', 1, 0, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-23 06:03:25'),
(15, 'google_analytics_id', 'TEXT', 'General', 'Google Analytics Id', 'UA-76211720-1', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(16, 'active_admin_theme', 'TEXT', 'Theme', 'Active admin theme', 'corals-admin', 1, 1, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(17, 'active_frontend_theme', 'TEXT', 'Theme', 'Active frontend theme', 'corals-ecommerce-basic', 1, 1, NULL, 1, NULL, '2018-09-16 01:02:02', '2018-09-16 02:05:22'),
(18, 'two_factor_auth_enabled', 'BOOLEAN', 'User', 'Two factor auth enabled?', 'false', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(19, 'two_factor_auth_provider', 'TEXT', 'User', 'Two factor auth provider?', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(20, 'address_types', 'SELECT', 'User', 'Address Types', '{\"home\":\"Home\",\"office\":\"Office\",\"shipping\":\"Shipping\",\"billing\":\"Billing\"}', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(21, 'custom_js', 'TEXTAREA', 'Theme', 'Custom JS', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(22, 'custom_css', 'TEXTAREA', 'Theme', 'Custom CSS', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(23, 'custom_admin_js', 'TEXTAREA', 'Theme', 'Custom Admin JS', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(24, 'custom_admin_css', 'TEXTAREA', 'Theme', 'Custom Admin CSS', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(25, 'supported_languages', 'SELECT', 'General', 'Supported system languages', '{\"en\":\"English\",\"pt-br\":\"Brazilian\",\"ar\":\"Arabic\"}', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(26, 'confirm_user_registration_email', 'BOOLEAN', 'User', 'Confirm email on registration?', 'false', 1, 0, NULL, NULL, NULL, '2018-09-16 01:02:02', '2018-09-16 01:02:02'),
(27, 'utility_google_address_api_key', 'TEXT', 'Utilities', 'Google address api key', 'AIzaSyBrMjtZWqBiHz1Nr9XZTTbBLjvYFICPHDM', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(28, 'utility_mailchimp_api_key', 'TEXT', 'Utilities', 'Mailchimp API Key', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(29, 'utility_mailchimp_list_id', 'TEXT', 'Utilities', 'Mailchimp List Id', '', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(30, 'schedule_time', 'SELECT', 'General', 'Schedule Time', '{\"Off\":\"- Off -\",\"06\":\"06 AM\",\"07\":\"07 AM\",\"08\":\"08 AM\",\"09\":\"09 AM\",\"10\":\"10 AM\",\"11\":\"11 AM\",\"12\":\"Noon\",\"13\":\"01 PM\",\"14\":\"02 PM\",\"15\":\"03 PM\",\"16\":\"04 PM\",\"17\":\"05 PM\",\"18\":\"06 PM\",\"19\":\"07 PM\",\"20\":\"08 PM\",\"21\":\"09 PM\",\"22\":\"10 PM\"}', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(31, 'days_of_the_week', 'SELECT', 'General', 'Days of the week', '{\"Mon\":\"Mon\",\"Tue\":\"Tue\",\"Wed\":\"Wed\",\"Thu\":\"Thu\",\"Fri\":\"Fri\",\"Sat\":\"Sat\",\"Sun\":\"Sun\"}', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:27', '2018-09-16 01:03:27'),
(32, 'home_page_slug', 'TEXT', 'CMS', 'Home page slug', 'home', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(33, 'blog_page_slug', 'TEXT', 'CMS', 'Blog page slug', 'blog', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(34, 'pricing_page_slug', 'TEXT', 'CMS', 'Pricing page slug', 'pricing', 1, 0, NULL, NULL, NULL, '2018-09-16 01:03:38', '2018-09-16 01:03:38'),
(35, 'supported_payment_gateway', 'SELECT', 'Payment', 'Supported payment gateway', '{\"PayPal_Rest\":\"PayPal\",\"Stripe\":\"Stripe\",\"Cash\":\"Cash on delivery (COD)\"}', 1, 0, NULL, 1, NULL, '2018-09-16 01:04:02', '2018-10-07 20:16:16'),
(36, 'admin_currency_code', 'TEXT', 'Payment', 'Administration Currency Code', 'PKR', 1, 0, NULL, 1, NULL, '2018-09-16 01:04:02', '2018-10-01 22:04:09'),
(37, 'payment_paypal_rest_live_client_id', 'TEXT', 'Payment', 'payment_paypal_rest_live_client_id', 'live_client_id_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(38, 'payment_paypal_rest_live_client_secret', 'TEXT', 'Payment', 'payment_paypal_rest_live_client_secret', 'live_client_secret_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(39, 'payment_paypal_rest_live_access_token', 'TEXT', 'Payment', 'payment_paypal_rest_live_access_token', 'live_access_token_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(40, 'payment_paypal_rest_live_access_token_expiry', 'TEXT', 'Payment', 'payment_paypal_rest_live_access_token_expiry', 'live_access_token_expiry_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(41, 'payment_paypal_rest_sandbox_mode', 'TEXT', 'Payment', 'payment_paypal_rest_sandbox_mode', 'true', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(42, 'payment_paypal_rest_sandbox_client_id', 'TEXT', 'Payment', 'payment_paypal_rest_sandbox_client_id', 'AZUgKksKCFROYGrnArvGgD8gQfNDyn31IqXTI1-EOOXNG41VX_PDT09Jv-bGoEnDx26WVOem01qrM-yb', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(43, 'payment_paypal_rest_sandbox_client_secret', 'TEXT', 'Payment', 'payment_paypal_rest_sandbox_client_secret', 'EK-mfBgXmaT3C-AjGNygNUd6_7tKNGexJbmJAeN4-TCyshheRu1P2RkYyCz1Fs8gPMX7Te33Zt_nKrDX', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(44, 'payment_paypal_rest_sandbox_access_token', 'TEXT', 'Payment', 'payment_paypal_rest_sandbox_access_token', NULL, 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(45, 'payment_paypal_rest_sandbox_access_token_expiry', 'TEXT', 'Payment', 'payment_paypal_rest_sandbox_access_token_expiry', NULL, 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:32', '2018-09-16 01:05:32'),
(46, 'payment_stripe_live_public_key', 'TEXT', 'Payment', 'payment_stripe_live_public_key', 'live_public_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(47, 'payment_stripe_live_secret_key', 'TEXT', 'Payment', 'payment_stripe_live_secret_key', 'live_secret_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(48, 'payment_stripe_live_webhook_key', 'TEXT', 'Payment', 'payment_stripe_live_webhook_key', 'live_webhook_xxxxxxxxxxxxxxxxxxxxxx', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(49, 'payment_stripe_sandbox_mode', 'TEXT', 'Payment', 'payment_stripe_sandbox_mode', 'true', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(50, 'payment_stripe_sandbox_public_key', 'TEXT', 'Payment', 'payment_stripe_sandbox_public_key', 'pk_test_zwrWUut1CmIPmEG1a3AMfOVO', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(51, 'payment_stripe_sandbox_secret_key', 'TEXT', 'Payment', 'payment_stripe_sandbox_secret_key', 'sk_test_jJcbMlrT1DvS7DuxTE9Ax0Ig', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(52, 'payment_stripe_sandbox_webhook_key', 'TEXT', 'Payment', 'payment_stripe_sandbox_webhook_key', 'whsec_8PVxYbVnESo9WWMn9KUrFTtx4tnQ6fc9', 1, 1, NULL, NULL, NULL, '2018-09-16 01:05:37', '2018-09-16 01:05:37'),
(53, 'supported_shipping_methods', 'SELECT', 'Ecommerce', 'Supported Shipping methods', '{\"FlatRate\":\"Flat Rate\",\"Shippo\":\"Shippo\"}', 1, 0, NULL, NULL, NULL, '2018-09-16 01:12:38', '2018-09-16 01:12:38'),
(54, 'ecommerce_company_owner', 'TEXT', 'General', 'ecommerce_company_owner', 'GABA-Opticals', 0, 1, 1, 1, NULL, '2018-09-16 02:05:47', '2018-10-07 19:56:56'),
(55, 'ecommerce_company_name', 'TEXT', 'General', 'ecommerce_company_name', 'GABA-Opticals', 0, 1, 1, 1, NULL, '2018-09-16 02:05:47', '2018-10-07 19:56:56'),
(56, 'ecommerce_company_street1', 'TEXT', 'General', 'ecommerce_company_street1', '5543 Aliquet St.', 0, 1, 1, 1, NULL, '2018-09-16 02:05:47', '2018-09-24 12:25:47'),
(57, 'ecommerce_company_city', 'TEXT', 'General', 'ecommerce_company_city', 'Karachi', 0, 1, 1, 1, NULL, '2018-09-16 02:05:47', '2018-10-07 19:56:57'),
(58, 'ecommerce_company_state', 'TEXT', 'General', 'ecommerce_company_state', 'PK', 0, 1, 1, 1, NULL, '2018-09-16 02:05:47', '2018-10-07 19:56:57'),
(59, 'ecommerce_company_zip', 'TEXT', 'General', 'ecommerce_company_zip', '75800', 0, 1, 1, 1, NULL, '2018-09-16 02:05:48', '2018-10-07 19:56:57'),
(60, 'ecommerce_company_country', 'TEXT', 'General', 'ecommerce_company_country', 'PKR', 0, 1, 1, 1, NULL, '2018-09-16 02:05:48', '2018-10-07 19:56:57'),
(61, 'ecommerce_company_phone', 'TEXT', 'General', 'ecommerce_company_phone', '(717) 450-4729', 0, 1, 1, 1, NULL, '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(62, 'ecommerce_company_email', 'TEXT', 'General', 'ecommerce_company_email', 'gaba@gmail.com', 0, 1, 1, 1, NULL, '2018-09-16 02:05:48', '2018-10-07 19:56:57'),
(63, 'ecommerce_shipping_weight_unit', 'TEXT', 'General', 'ecommerce_shipping_weight_unit', 'oz', 0, 1, 1, 1, NULL, '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(64, 'ecommerce_shipping_dimensions_unit', 'TEXT', 'General', 'ecommerce_shipping_dimensions_unit', 'in', 0, 1, 1, 1, NULL, '2018-09-16 02:05:48', '2018-09-16 02:05:48'),
(65, 'ecommerce_shipping_shippo_live_token', 'TEXT', 'General', 'ecommerce_shipping_shippo_live_token', NULL, 0, 1, 1, 1, NULL, '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(66, 'ecommerce_shipping_shippo_test_token', 'TEXT', 'General', 'ecommerce_shipping_shippo_test_token', 'shippo_test_b3aab6f5d5ee5fb9e981906a449d74fe2e7bf9eb', 0, 1, 1, 1, NULL, '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(67, 'ecommerce_shipping_shippo_sandbox_mode', 'TEXT', 'General', 'ecommerce_shipping_shippo_sandbox_mode', 'true', 0, 1, 1, 1, NULL, '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(68, 'ecommerce_tax_calculate_tax', 'TEXT', 'General', 'ecommerce_tax_calculate_tax', 'true', 0, 1, 1, 1, NULL, '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(69, 'ecommerce_rating_enable', 'TEXT', 'General', 'ecommerce_rating_enable', 'true', 0, 1, 1, 1, NULL, '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(70, 'ecommerce_wishlist_enable', 'TEXT', 'General', 'ecommerce_wishlist_enable', 'true', 0, 1, 1, 1, NULL, '2018-09-16 02:05:49', '2018-09-16 02:05:49'),
(71, 'ecommerce_appearance_page_limit', 'TEXT', 'General', 'ecommerce_appearance_page_limit', '15', 0, 1, 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(72, 'ecommerce_search_title_weight', 'TEXT', 'General', 'ecommerce_search_title_weight', '3', 0, 1, 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(73, 'ecommerce_search_content_weight', 'TEXT', 'General', 'ecommerce_search_content_weight', '1.5', 0, 1, 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(74, 'ecommerce_search_enable_wildcards', 'TEXT', 'General', 'ecommerce_search_enable_wildcards', 'true', 0, 1, 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-16 02:05:50'),
(75, 'payment_cash_cash_notes', 'TEXTAREA', 'Payment', 'payment_cash_cash_notes', 'Payment should be made upon delivery, make sure you check and confirm your shipment', 1, 1, NULL, NULL, NULL, '2018-10-07 20:16:16', '2018-10-07 20:16:16');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `type` enum('images','videos','html') COLLATE utf8mb4_unicode_ci NOT NULL,
  `init_options` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `key`, `status`, `type`, `init_options`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Home Page Slider', 'home-page-slider', 'active', 'images', '{\"items\":{\"number\":\"1\"},\"margin\":{\"number\":\"0\"},\"loop\":{\"boolean\":\"false\"},\"center\":{\"boolean\":\"false\"},\"mouseDrag\":{\"boolean\":\"true\"},\"touchDrag\":{\"boolean\":\"true\"},\"stagePadding\":{\"number\":\"0\"},\"merge\":{\"boolean\":\"false\"},\"mergeFit\":{\"boolean\":\"true\"},\"autoWidth\":{\"boolean\":\"false\"},\"URLhashListener\":{\"boolean\":\"false\"},\"nav\":{\"boolean\":\"false\"},\"rewind\":{\"boolean\":\"true\"},\"navText\":{\"array\":\"[\'next\',\'prev\']\"},\"dots\":{\"boolean\":\"true\"},\"dotsEach\":{\"number\\/boolean\":\"false\"},\"dotData\":{\"boolean\":\"false\"},\"lazyLoad\":{\"boolean\":\"true\"},\"lazyContent\":{\"boolean\":\"true\"},\"autoplay\":{\"boolean\":\"true\"},\"autoplayTimeout\":{\"number\":\"1000\"},\"autoplayHoverPause\":{\"boolean\":\"true\"},\"autoplaySpeed\":{\"number\\/boolean\":\"false\"},\"navSpeed\":{\"number\\/boolean\":\"false\"},\"dotsSpeed\":{\"number\\/boolean\":\"false\"},\"dragEndSpeed\":{\"number\\/boolean\":\"false\"},\"callbacks\":{\"boolean\":\"true\"},\"responsive\":{\"object\":\"false\"},\"video\":{\"boolean\":\"false\"},\"videoHeight\":{\"number\\/boolean\":\"false\"},\"videoWidth\":{\"number\\/boolean\":\"false\"},\"animateOut\":{\"array\\/boolean\":\"false\"},\"animateIn\":{\"array\\/boolean\":\"false\"}}', NULL, 1, NULL, '2017-12-03 17:53:20', '2018-11-09 23:35:27'),
(2, 'Tech Show Slider', 'tech-show', 'active', 'videos', '{\"items\":{\"number\":\"1\"},\"margin\":{\"number\":\"0\"},\"loop\":{\"boolean\":\"true\"},\"center\":{\"boolean\":\"true\"},\"mouseDrag\":{\"boolean\":\"true\"},\"touchDrag\":{\"boolean\":\"true\"},\"stagePadding\":{\"number\":\"0\"},\"merge\":{\"boolean\":\"false\"},\"mergeFit\":{\"boolean\":\"true\"},\"autoWidth\":{\"boolean\":\"false\"},\"URLhashListener\":{\"boolean\":\"false\"},\"nav\":{\"boolean\":\"false\"},\"rewind\":{\"boolean\":\"true\"},\"navText\":{\"array\":\"[\'next\',\'prev\']\"},\"dots\":{\"boolean\":\"true\"},\"dotsEach\":{\"number\\/boolean\":\"false\"},\"dotData\":{\"boolean\":\"false\"},\"lazyLoad\":{\"boolean\":\"true\"},\"lazyContent\":{\"boolean\":\"true\"},\"autoplay\":{\"boolean\":\"false\"},\"autoplayTimeout\":{\"number\":\"5000\"},\"autoplayHoverPause\":{\"boolean\":\"false\"},\"autoplaySpeed\":{\"number\\/boolean\":\"false\"},\"navSpeed\":{\"number\\/boolean\":\"false\"},\"dotsSpeed\":{\"number\\/boolean\":\"false\"},\"dragEndSpeed\":{\"number\\/boolean\":\"false\"},\"callbacks\":{\"boolean\":\"true\"},\"responsive\":{\"object\":\"false\"},\"video\":{\"boolean\":\"true\"},\"videoHeight\":{\"number\\/boolean\":\"400\"},\"videoWidth\":{\"number\\/boolean\":\"false\"},\"animateOut\":{\"array\\/boolean\":\"false\"},\"animateIn\":{\"array\\/boolean\":\"false\"}}', NULL, NULL, NULL, '2017-12-03 17:53:20', '2017-12-03 18:13:24'),
(3, 'E-commerce Home Page Slider', 'e-commerce-home-page-slider', 'active', 'images', '{\"items\":{\"number\":\"1\"},\"margin\":{\"number\":\"0\"},\"loop\":{\"boolean\":\"false\"},\"center\":{\"boolean\":\"false\"},\"mouseDrag\":{\"boolean\":\"true\"},\"touchDrag\":{\"boolean\":\"true\"},\"stagePadding\":{\"number\":\"0\"},\"merge\":{\"boolean\":\"false\"},\"mergeFit\":{\"boolean\":\"true\"},\"autoWidth\":{\"boolean\":\"false\"},\"URLhashListener\":{\"boolean\":\"false\"},\"nav\":{\"boolean\":\"false\"},\"rewind\":{\"boolean\":\"true\"},\"navText\":{\"array\":\"[\'next\',\'prev\']\"},\"dots\":{\"boolean\":\"true\"},\"dotsEach\":{\"number\\/boolean\":\"false\"},\"dotData\":{\"boolean\":\"false\"},\"lazyLoad\":{\"boolean\":\"true\"},\"lazyContent\":{\"boolean\":\"true\"},\"autoplay\":{\"boolean\":\"true\"},\"autoplayTimeout\":{\"number\":\"3000\"},\"autoplayHoverPause\":{\"boolean\":\"true\"},\"autoplaySpeed\":{\"number\\/boolean\":\"false\"},\"navSpeed\":{\"number\\/boolean\":\"false\"},\"dotsSpeed\":{\"number\\/boolean\":\"false\"},\"dragEndSpeed\":{\"number\\/boolean\":\"false\"},\"callbacks\":{\"boolean\":\"true\"},\"responsive\":{\"object\":\"false\"},\"video\":{\"boolean\":\"false\"},\"videoHeight\":{\"number\\/boolean\":\"false\"},\"videoWidth\":{\"number\\/boolean\":\"false\"},\"animateOut\":{\"array\\/boolean\":\"false\"},\"animateIn\":{\"array\\/boolean\":\"false\"}}', 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-16 02:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `slider_options`
--

CREATE TABLE `slider_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `values` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'OwlCarousel2',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_options`
--

INSERT INTO `slider_options` (`id`, `key`, `default`, `type`, `values`, `description`, `slider_type`, `hidden`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'items', '1', 'number', '', 'The number of items you want to see on the screen.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(2, 'margin', '0', 'number', '', 'margin-right(px) on item.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(3, 'loop', 'false', 'boolean', '[false,true]', 'Infinity loop. Duplicate last and first items to get loop illusion.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(4, 'center', 'false', 'boolean', '[false,true]', 'Center item. Works well with even an odd number of items.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(5, 'mouseDrag', 'true', 'boolean', '[false,true]', 'Mouse drag enabled.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(6, 'touchDrag', 'true', 'boolean', '[false,true]', 'Touch drag enabled.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(7, 'pullDrag', 'true', 'boolean', '[false,true]', 'Stage pull to edge.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(8, 'freeDrag', 'false', 'boolean', '[false,true]', 'Item pull to edge.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(9, 'stagePadding', '0', 'number', '', 'Padding left and right on stage (can see neighbours).', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(10, 'merge', 'false', 'boolean', '[false,true]', 'Merge items. Looking for data-merge=\'{number}\' inside item..\r\n\r\n', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(11, 'mergeFit', 'true', 'boolean', '[false,true]', 'Fit merged items if screen is smaller than items value.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(12, 'autoWidth', 'false', 'boolean', '[false,true]', 'Set non grid content. Try using width style on divs.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(13, 'startPosition', '0', 'number/string', '', 'Start position or URL Hash string like \'#id\'.\r\n\r\n', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(14, 'URLhashListener', 'false', 'boolean', '[false,true]', 'Listen to url hash changes. data-hash on items is required.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(15, 'nav', 'false', 'boolean', '[false,true]', 'Show next/prev buttons.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(16, 'rewind', 'true', 'boolean', '[false,true]', 'Go backwards when the boundary has reached.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(17, 'navText', '[\'next\',\'prev\']', 'array', '[[\"&#x27;next&#x27;\",\"&#x27;prev&#x27;\"]]', 'HTML allowed.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(18, 'navElement', 'div', 'string', '', 'DOM element type for a single directional navigation link.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(19, 'slideBy', '1', 'number/string', '', 'Navigation slide by x. \'page\' string can be set to slide by page.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(20, 'dots', 'true', 'boolean', '[false,true]', 'Show dots navigation.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(21, 'dotsEach', 'false', 'number/boolean', ' ', 'Show dots each x item.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(22, 'dotData', 'false', 'boolean', '[false,true]', 'Used by data-dot content.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(23, 'lazyLoad', 'true', 'boolean', '', 'Lazy load images. data-src and data-src-retina for highres. Also load images into background inline style if element is not <img>', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(24, 'lazyContent', 'true', 'boolean', '[false,true]', 'lazyContent was introduced during beta tests but i removed it from the final release due to bad implementation. It is a nice options so i will work on it in the nearest feature.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(25, 'autoplay', 'false', 'boolean', '[false,true]', 'Autoplay.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(26, 'autoplayTimeout', '5000', 'number', '', 'Autoplay interval timeout.\r\n\r\n', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(27, 'autoplayHoverPause', 'false', 'boolean', '[false,true]', 'Pause on mouse hover.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(28, 'smartSpeed', '250', 'number', '', 'Speed Calculate. More info to come..', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(29, 'autoplaySpeed', 'false', 'number/boolean', '', 'autoplay speed.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(30, 'navSpeed', 'false', 'number/boolean', '', 'Navigation speed.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(31, 'dotsSpeed', 'false', 'number/boolean', '', 'Pagination speed.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(32, 'fluidSpeed', 'false', 'number/boolean', '', 'Speed Calculate. More info to come..', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(33, 'dragEndSpeed', 'false', 'number/boolean', '', 'Drag end speed.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(34, 'callbacks', 'true', 'boolean', '', 'Enable callback events.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(35, 'responsive', 'false', 'object', '', 'Object containing responsive options. Can be set to false to remove responsive capabilities.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(36, 'responsiveRefreshRate', '200', 'number', '', 'Responsive refresh rate.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(37, 'responsiveBaseElement', 'window', 'string', '', 'Set on any DOM element. If you care about non responsive browser (like ie8) then use it on main wrapper. This will prevent from crazy resizing.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(38, 'video', 'false', 'boolean', '[false,true]', 'Enable fetching YouTube/Vimeo/Vzaar videos.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(39, 'videoHeight', 'false', 'number/boolean', ' ', 'Set height for videos.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(40, 'videoWidth', 'false', 'number/boolean', '', '', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(41, 'animateOut', 'false', 'array/boolean', '', 'Class for CSS3 animation out.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(42, 'animateIn', 'false', 'array/boolean', '', 'Class for CSS3 animation in.', 'OwlCarousel2', 0, NULL, NULL, NULL, NULL, NULL),
(43, 'fallbackEasing', 'swing', 'string', '', 'Easing for CSS2 $.animate.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(44, 'info', 'false', 'function', '', 'Callback to retrieve basic information (current item/pages/widths). Info function second parameter is Owl DOM object reference.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(45, 'nestedItemSelector', 'false', 'string/class', '', 'Use it if owl items are deep nested inside some generated content. E.g \'youritem\'. Dont use dot before class name.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(46, 'itemElement', 'div', 'string', '', 'DOM element type for owl-item.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(47, 'stageElement', 'div', 'string', '', 'DOM element type for owl-stage.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(48, 'navContainer', 'false', 'string/class/id/boolean', '', 'Set your own container for nav.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL),
(49, 'dotsContainer', 'false', 'string/class/id/boolean', '', 'Set your own container for nav.', 'OwlCarousel2', 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `slider_id` int(10) UNSIGNED NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `name`, `content`, `slider_id`, `status`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'First Slider', '/media/user_v1oz1Yz27j/13/silde-1.jpg', 1, 'active', NULL, 1, NULL, '2017-12-03 17:53:48', '2018-09-23 07:10:41'),
(2, 'Second Slider', '/media/user_v1oz1Yz27j/14/silde-2.jpg', 1, 'active', NULL, 1, NULL, '2017-12-03 18:24:55', '2018-09-23 07:11:06'),
(4, 'Q8 Smart Band Review', 'https://youtu.be/4MqbwYPAHCg', 2, 'active', NULL, NULL, NULL, '2017-12-03 18:25:09', '2017-12-03 18:25:09'),
(5, 'camera FPV', 'https://youtu.be/L8vL19CBF-8', 2, 'active', NULL, NULL, NULL, '2017-12-03 18:25:09', '2017-12-03 18:25:09'),
(6, 'iPhone, iPad, iPod l IOS 11', 'https://youtu.be/8iRk-G3dVKo', 2, 'active', NULL, NULL, NULL, '2017-12-03 18:25:09', '2017-12-03 18:25:09'),
(7, 'E-First Slide', '/media/user_v1oz1Yz27j/22/silde-1.jpg', 3, 'active', 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-24 12:38:04'),
(8, 'E-Second Slide', '/media/user_v1oz1Yz27j/23/silde-2.jpg', 3, 'active', 1, 1, NULL, '2018-09-16 02:05:50', '2018-09-24 12:38:19'),
(9, 'Third Slider', '/media/user_v1oz1Yz27j/15/silde-3.jpg', 1, 'active', 1, 1, NULL, '2018-09-23 07:11:36', '2018-09-23 07:11:36'),
(10, 'E-Third Slide', '/media/user_v1oz1Yz27j/24/silde-3.jpg', 3, 'active', 1, 1, NULL, '2018-09-23 07:16:05', '2018-09-24 12:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxables`
--

CREATE TABLE `taxables` (
  `id` int(10) UNSIGNED NOT NULL,
  `taxable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxable_id` int(10) UNSIGNED NOT NULL,
  `tax_class_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_class_id` int(10) UNSIGNED NOT NULL,
  `priority` int(11) NOT NULL,
  `status` enum('active','inactive','deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `compound` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_classes`
--

CREATE TABLE `tax_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translatable_translations`
--

CREATE TABLE `translatable_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `translatable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `translatable_id` int(11) NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `translation` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_options` text COLLATE utf8mb4_unicode_ci,
  `integration_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `payment_method_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_preferences` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `job_title`, `phone_country_code`, `phone_number`, `two_factor_options`, `integration_id`, `gateway`, `card_brand`, `card_last_four`, `trial_ends_at`, `payment_method_token`, `properties`, `confirmation_code`, `confirmed_at`, `remember_token`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`, `notification_preferences`) VALUES
(1, 'Super User', 'superuser@corals.io', '$2y$10$KqFDCZzel8NinAwvyUs2NOd//aVKGxnFOhCcAE5.0S5LyrUe4ynsO', '{\"billing\":{\"email\":\"Aj@gma.com\",\"address_1\":\"Sunset\",\"address_2\":\"PhASE2\",\"type\":\"billing\",\"city\":\"kARACHI\",\"state\":\"sINDH\",\"zip\":\"7500\",\"country\":\"PK\"}}', 'Administrator', '1', '03033160138', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"about\":null}', NULL, '2018-09-16 06:02:02', '0s4bORXd5c7T6ugnJ8nsaG0uSuY5NWDpN2jwG21sdklznp6hoZmEM8Vf4CiW', NULL, 1, NULL, '2018-09-16 01:02:02', '2018-12-18 03:18:14', '[]'),
(2, 'Corals Member', 'member@corals.io', '$2y$10$zul7WdnCFDTVXEJzj4DBcO.kwoc9Jzil8FEORwalF1AnY3ez752me', '{\"billing\":{\"address_1\":\"Sunset Club Road\",\"address_2\":\"phase 2\",\"type\":\"billing\",\"city\":\"Karachi\",\"state\":\"Sindh\",\"zip\":\"7500\",\"country\":\"PK\"}}', 'Ads Coordinator', '1', '03118286793', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"about\":null}', NULL, NULL, 'ZdWwJ9mEQ8BB4UwpgYgHwhbVOtYB8l0K0Zitsvkt4ZBZ6PgGM8nvnH1v04Om', 0, 2, NULL, '2018-09-16 01:02:02', '2018-11-23 01:32:23', NULL),
(3, 'Demo', 'myidmailinator.com@mailinator.com', '$2y$10$NCj1T1Z1HzAhbFt/eEb3AuQwtGTk6tgrZRzlklEdl1s1DAbOHSK2O', NULL, NULL, '44', '13458455', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2018-10-30 15:29:04', '2018-10-30 15:29:04', NULL),
(4, 'Ather', 'atheriqbal1@gmail.com', '$2y$10$azQ.lzP4oG5wwm47Mqxfw.3aWpO2ZkHiFLeLbrEZTTJ/AE5Avj6gO', NULL, NULL, '92', '3222230090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'iHSxXJ81RbABV1AbglOZYOlkMJeYTBmT6lzo9doN8ZhMqqYdq3MdopGNjgvN', 0, 4, NULL, '2018-11-02 16:40:16', '2018-11-02 16:40:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `utility_attributes`
--

CREATE TABLE `utility_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  `use_as_filter` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_attribute_options`
--

CREATE TABLE `utility_attribute_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `option_order` int(11) NOT NULL,
  `option_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_display` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_categories`
--

CREATE TABLE `utility_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT '0',
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_category_attributes`
--

CREATE TABLE `utility_category_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_locations`
--

CREATE TABLE `utility_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_model_attribute_options`
--

CREATE TABLE `utility_model_attribute_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `attribute_option_id` int(10) UNSIGNED DEFAULT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_value` double DEFAULT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_model_has_category`
--

CREATE TABLE `utility_model_has_category` (
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_ratings`
--

CREATE TABLE `utility_ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reviewrateable_id` int(10) UNSIGNED NOT NULL,
  `reviewrateable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `author_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_schedules`
--

CREATE TABLE `utility_schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `scheduleable_id` int(10) UNSIGNED NOT NULL,
  `scheduleable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `day_of_the_week` enum('Mon','Tue','Wed','Thu','Fri','Sat','Sun') COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_taggables`
--

CREATE TABLE `utility_taggables` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_tags`
--

CREATE TABLE `utility_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `module` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_wishlists`
--

CREATE TABLE `utility_wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `wishlistable_id` int(10) UNSIGNED NOT NULL,
  `wishlistable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `webhook_calls`
--

CREATE TABLE `webhook_calls` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `exception` text COLLATE utf8mb4_unicode_ci,
  `gateway` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_created_by_index` (`created_by`),
  ADD KEY `categories_updated_by_index` (`updated_by`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD UNIQUE KEY `category_post_post_id_category_id_unique` (`post_id`,`category_id`),
  ADD KEY `category_post_category_id_foreign` (`category_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `countries_code_index` (`code`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currencies_code_index` (`code`),
  ADD KEY `currencies_created_by_index` (`created_by`),
  ADD KEY `currencies_updated_by_index` (`updated_by`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_fields_created_by_index` (`created_by`),
  ADD KEY `custom_fields_updated_by_index` (`updated_by`);

--
-- Indexes for table `custom_field_settings`
--
ALTER TABLE `custom_field_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_field_settings_created_by_index` (`created_by`),
  ADD KEY `custom_field_settings_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_attributes`
--
ALTER TABLE `ecommerce_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_attributes_created_by_index` (`created_by`),
  ADD KEY `ecommerce_attributes_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_attribute_options`
--
ALTER TABLE `ecommerce_attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_attribute_options_attribute_id_index` (`attribute_id`),
  ADD KEY `ecommerce_attribute_options_created_by_index` (`created_by`),
  ADD KEY `ecommerce_attribute_options_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_brands`
--
ALTER TABLE `ecommerce_brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_brands_name_unique` (`name`),
  ADD UNIQUE KEY `ecommerce_brands_slug_unique` (`slug`),
  ADD KEY `ecommerce_brands_created_by_index` (`created_by`),
  ADD KEY `ecommerce_brands_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_categories`
--
ALTER TABLE `ecommerce_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_categories_name_unique` (`name`),
  ADD UNIQUE KEY `ecommerce_categories_slug_unique` (`slug`),
  ADD KEY `ecommerce_categories_created_by_index` (`created_by`),
  ADD KEY `ecommerce_categories_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_category_product`
--
ALTER TABLE `ecommerce_category_product`
  ADD UNIQUE KEY `ecommerce_category_product_product_id_category_id_unique` (`product_id`,`category_id`),
  ADD KEY `ecommerce_category_product_category_id_foreign` (`category_id`);

--
-- Indexes for table `ecommerce_colors`
--
ALTER TABLE `ecommerce_colors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_colors_name_unique` (`title`);

--
-- Indexes for table `ecommerce_coupons`
--
ALTER TABLE `ecommerce_coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `ecommerce_coupons_created_by_index` (`created_by`),
  ADD KEY `ecommerce_coupons_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_coupon_product`
--
ALTER TABLE `ecommerce_coupon_product`
  ADD KEY `ecommerce_coupon_product_coupon_id_index` (`coupon_id`),
  ADD KEY `ecommerce_coupon_product_product_id_index` (`product_id`);

--
-- Indexes for table `ecommerce_coupon_user`
--
ALTER TABLE `ecommerce_coupon_user`
  ADD KEY `ecommerce_coupon_user_coupon_id_index` (`coupon_id`),
  ADD KEY `ecommerce_coupon_user_user_id_index` (`user_id`);

--
-- Indexes for table `ecommerce_genders`
--
ALTER TABLE `ecommerce_genders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_genders_name_unique` (`name`);

--
-- Indexes for table `ecommerce_lense_category`
--
ALTER TABLE `ecommerce_lense_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_lense_mapping_table`
--
ALTER TABLE `ecommerce_lense_mapping_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`len_cat_id`) USING BTREE,
  ADD KEY `len` (`len_subcat_id`) USING BTREE;

--
-- Indexes for table `ecommerce_lense_number`
--
ALTER TABLE `ecommerce_lense_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_lense_sub_category`
--
ALTER TABLE `ecommerce_lense_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_lense_type`
--
ALTER TABLE `ecommerce_lense_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_materials`
--
ALTER TABLE `ecommerce_materials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_materials_name_unique` (`name`);

--
-- Indexes for table `ecommerce_orders`
--
ALTER TABLE `ecommerce_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_orders_user_id_foreign` (`user_id`),
  ADD KEY `ecommerce_orders_created_by_index` (`created_by`),
  ADD KEY `ecommerce_orders_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_order_items`
--
ALTER TABLE `ecommerce_order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_order_items_order_id_foreign` (`order_id`),
  ADD KEY `ecommerce_order_items_created_by_index` (`created_by`),
  ADD KEY `ecommerce_order_items_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_order_lens`
--
ALTER TABLE `ecommerce_order_lens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_order_subproduct`
--
ALTER TABLE `ecommerce_order_subproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_products`
--
ALTER TABLE `ecommerce_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_products_slug_unique` (`slug`),
  ADD KEY `ecommerce_products_color_id_foreign` (`color_id`),
  ADD KEY `ecommerce_products_material_id_foreign` (`material_id`),
  ADD KEY `ecommerce_products_shape_id_foreign` (`shape_id`),
  ADD KEY `ecommerce_products_style_id_foreign` (`style_id`),
  ADD KEY `ecommerce_products_gender_id_foreign` (`gender_id`),
  ADD KEY `ecommerce_products_brand_id_index` (`brand_id`),
  ADD KEY `ecommerce_products_created_by_index` (`created_by`),
  ADD KEY `ecommerce_products_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_product_attributes`
--
ALTER TABLE `ecommerce_product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_product_attributes_attribute_id_index` (`attribute_id`),
  ADD KEY `ecommerce_product_attributes_product_id_index` (`product_id`),
  ADD KEY `ecommerce_product_attributes_created_by_index` (`created_by`),
  ADD KEY `ecommerce_product_attributes_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_product_tag`
--
ALTER TABLE `ecommerce_product_tag`
  ADD UNIQUE KEY `ecommerce_product_tag_product_id_tag_id_unique` (`product_id`,`tag_id`),
  ADD KEY `ecommerce_product_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `ecommerce_shapes`
--
ALTER TABLE `ecommerce_shapes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_shapes_name_unique` (`name`);

--
-- Indexes for table `ecommerce_shippings`
--
ALTER TABLE `ecommerce_shippings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_shippings_created_by_index` (`created_by`),
  ADD KEY `ecommerce_shippings_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_size`
--
ALTER TABLE `ecommerce_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_sku`
--
ALTER TABLE `ecommerce_sku`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_sku_product_id_index` (`product_id`),
  ADD KEY `ecommerce_sku_created_by_index` (`created_by`),
  ADD KEY `ecommerce_sku_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_sku_options`
--
ALTER TABLE `ecommerce_sku_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ecommerce_sku_options_sku_id_index` (`sku_id`),
  ADD KEY `ecommerce_sku_options_attribute_id_index` (`attribute_id`),
  ADD KEY `ecommerce_sku_options_attribute_option_id_index` (`attribute_option_id`),
  ADD KEY `ecommerce_sku_options_created_by_index` (`created_by`),
  ADD KEY `ecommerce_sku_options_updated_by_index` (`updated_by`);

--
-- Indexes for table `ecommerce_styles`
--
ALTER TABLE `ecommerce_styles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_styles_name_unique` (`name`);

--
-- Indexes for table `ecommerce_sub_product`
--
ALTER TABLE `ecommerce_sub_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_sub_product_media`
--
ALTER TABLE `ecommerce_sub_product_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecommerce_tags`
--
ALTER TABLE `ecommerce_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ecommerce_tags_name_unique` (`name`),
  ADD UNIQUE KEY `ecommerce_tags_slug_unique` (`slug`),
  ADD KEY `ecommerce_tags_created_by_index` (`created_by`),
  ADD KEY `ecommerce_tags_updated_by_index` (`updated_by`);

--
-- Indexes for table `fulltext_search`
--
ALTER TABLE `fulltext_search`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fulltext_search_indexable_type_indexable_id_unique` (`indexable_type`,`indexable_id`),
  ADD KEY `fulltext_search_created_by_index` (`created_by`),
  ADD KEY `fulltext_search_updated_by_index` (`updated_by`);
ALTER TABLE `fulltext_search` ADD FULLTEXT KEY `fulltext_title` (`indexed_title`);
ALTER TABLE `fulltext_search` ADD FULLTEXT KEY `fulltext_title_content` (`indexed_title`,`indexed_content`);

--
-- Indexes for table `gateway_status`
--
ALTER TABLE `gateway_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gateway_status_created_by_index` (`created_by`),
  ADD KEY `gateway_status_updated_by_index` (`updated_by`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_code_unique` (`code`),
  ADD KEY `invoices_user_id_foreign` (`user_id`),
  ADD KEY `invoices_created_by_index` (`created_by`),
  ADD KEY `invoices_updated_by_index` (`updated_by`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_items_code_unique` (`code`),
  ADD KEY `invoice_items_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_items_created_by_index` (`created_by`),
  ADD KEY `invoice_items_updated_by_index` (`updated_by`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_key_unique` (`key`),
  ADD KEY `menus_created_by_index` (`created_by`),
  ADD KEY `menus_updated_by_index` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `modules_code_unique` (`code`),
  ADD KEY `modules_created_by_index` (`created_by`),
  ADD KEY `modules_updated_by_index` (`updated_by`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `notification_templates`
--
ALTER TABLE `notification_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_templates_updated_by_index` (`updated_by`),
  ADD KEY `notification_templates_created_by_index` (`created_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postables`
--
ALTER TABLE `postables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postables_postable_id_postable_type_index` (`postable_id`,`postable_type`),
  ADD KEY `postables_sourcable_id_sourcable_type_index` (`sourcable_id`,`sourcable_type`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_created_by_index` (`created_by`),
  ADD KEY `posts_updated_by_index` (`updated_by`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_created_by_index` (`created_by`),
  ADD KEY `roles_updated_by_index` (`updated_by`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_code_unique` (`code`),
  ADD KEY `settings_created_by_index` (`created_by`),
  ADD KEY `settings_updated_by_index` (`updated_by`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sliders_name_unique` (`name`),
  ADD UNIQUE KEY `sliders_key_unique` (`key`),
  ADD KEY `sliders_created_by_index` (`created_by`),
  ADD KEY `sliders_updated_by_index` (`updated_by`);

--
-- Indexes for table `slider_options`
--
ALTER TABLE `slider_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slider_options_created_by_index` (`created_by`),
  ADD KEY `slider_options_updated_by_index` (`updated_by`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slides_slider_id_foreign` (`slider_id`),
  ADD KEY `slides_created_by_index` (`created_by`),
  ADD KEY `slides_updated_by_index` (`updated_by`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_accounts_user_id_foreign` (`user_id`),
  ADD KEY `social_accounts_created_by_index` (`created_by`),
  ADD KEY `social_accounts_updated_by_index` (`updated_by`);

--
-- Indexes for table `taxables`
--
ALTER TABLE `taxables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taxables_created_by_index` (`created_by`),
  ADD KEY `taxables_updated_by_index` (`updated_by`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taxes_tax_class_id_foreign` (`tax_class_id`),
  ADD KEY `taxes_created_by_index` (`created_by`),
  ADD KEY `taxes_updated_by_index` (`updated_by`);

--
-- Indexes for table `tax_classes`
--
ALTER TABLE `tax_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_classes_created_by_index` (`created_by`),
  ADD KEY `tax_classes_updated_by_index` (`updated_by`);

--
-- Indexes for table `translatable_translations`
--
ALTER TABLE `translatable_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_created_by_index` (`created_by`),
  ADD KEY `users_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_attributes`
--
ALTER TABLE `utility_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_attributes_created_by_index` (`created_by`),
  ADD KEY `utility_attributes_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_attribute_options`
--
ALTER TABLE `utility_attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_attribute_options_attribute_id_index` (`attribute_id`),
  ADD KEY `utility_attribute_options_created_by_index` (`created_by`),
  ADD KEY `utility_attribute_options_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_categories`
--
ALTER TABLE `utility_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `utility_categories_name_unique` (`name`),
  ADD UNIQUE KEY `utility_categories_slug_unique` (`slug`),
  ADD KEY `utility_categories_created_by_index` (`created_by`),
  ADD KEY `utility_categories_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_category_attributes`
--
ALTER TABLE `utility_category_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_category_attributes_attribute_id_index` (`attribute_id`),
  ADD KEY `utility_category_attributes_category_id_index` (`category_id`);

--
-- Indexes for table `utility_locations`
--
ALTER TABLE `utility_locations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `utility_locations_slug_unique` (`slug`),
  ADD KEY `utility_locations_created_by_index` (`created_by`),
  ADD KEY `utility_locations_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_model_attribute_options`
--
ALTER TABLE `utility_model_attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_model_attribute_options_attribute_id_index` (`attribute_id`),
  ADD KEY `utility_model_attribute_options_attribute_option_id_index` (`attribute_option_id`),
  ADD KEY `utility_model_attribute_options_created_by_index` (`created_by`),
  ADD KEY `utility_model_attribute_options_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_model_has_category`
--
ALTER TABLE `utility_model_has_category`
  ADD KEY `utility_model_has_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `utility_ratings`
--
ALTER TABLE `utility_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_ratings_reviewrateable_id_reviewrateable_type_index` (`reviewrateable_id`,`reviewrateable_type`),
  ADD KEY `utility_ratings_author_id_author_type_index` (`author_id`,`author_type`),
  ADD KEY `utility_ratings_created_by_index` (`created_by`),
  ADD KEY `utility_ratings_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_schedules`
--
ALTER TABLE `utility_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_schedules_user_id_foreign` (`user_id`),
  ADD KEY `utility_schedules_created_by_index` (`created_by`),
  ADD KEY `utility_schedules_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_taggables`
--
ALTER TABLE `utility_taggables`
  ADD KEY `utility_taggables_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `utility_tags`
--
ALTER TABLE `utility_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_tags_created_by_index` (`created_by`),
  ADD KEY `utility_tags_updated_by_index` (`updated_by`);

--
-- Indexes for table `utility_wishlists`
--
ALTER TABLE `utility_wishlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utility_wishlists_user_id_foreign` (`user_id`),
  ADD KEY `utility_wishlists_created_by_index` (`created_by`),
  ADD KEY `utility_wishlists_updated_by_index` (`updated_by`);

--
-- Indexes for table `webhook_calls`
--
ALTER TABLE `webhook_calls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `webhook_calls_created_by_index` (`created_by`),
  ADD KEY `webhook_calls_updated_by_index` (`updated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1661;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_field_settings`
--
ALTER TABLE `custom_field_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_attributes`
--
ALTER TABLE `ecommerce_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ecommerce_attribute_options`
--
ALTER TABLE `ecommerce_attribute_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_brands`
--
ALTER TABLE `ecommerce_brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ecommerce_categories`
--
ALTER TABLE `ecommerce_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `ecommerce_colors`
--
ALTER TABLE `ecommerce_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ecommerce_coupons`
--
ALTER TABLE `ecommerce_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ecommerce_genders`
--
ALTER TABLE `ecommerce_genders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_lense_category`
--
ALTER TABLE `ecommerce_lense_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ecommerce_lense_mapping_table`
--
ALTER TABLE `ecommerce_lense_mapping_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ecommerce_lense_number`
--
ALTER TABLE `ecommerce_lense_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `ecommerce_lense_sub_category`
--
ALTER TABLE `ecommerce_lense_sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ecommerce_lense_type`
--
ALTER TABLE `ecommerce_lense_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ecommerce_materials`
--
ALTER TABLE `ecommerce_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_orders`
--
ALTER TABLE `ecommerce_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ecommerce_order_items`
--
ALTER TABLE `ecommerce_order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ecommerce_order_lens`
--
ALTER TABLE `ecommerce_order_lens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ecommerce_order_subproduct`
--
ALTER TABLE `ecommerce_order_subproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_products`
--
ALTER TABLE `ecommerce_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ecommerce_product_attributes`
--
ALTER TABLE `ecommerce_product_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_shapes`
--
ALTER TABLE `ecommerce_shapes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_shippings`
--
ALTER TABLE `ecommerce_shippings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ecommerce_size`
--
ALTER TABLE `ecommerce_size`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ecommerce_sku`
--
ALTER TABLE `ecommerce_sku`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ecommerce_sku_options`
--
ALTER TABLE `ecommerce_sku_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_styles`
--
ALTER TABLE `ecommerce_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_sub_product`
--
ALTER TABLE `ecommerce_sub_product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ecommerce_sub_product_media`
--
ALTER TABLE `ecommerce_sub_product_media`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecommerce_tags`
--
ALTER TABLE `ecommerce_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fulltext_search`
--
ALTER TABLE `fulltext_search`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `gateway_status`
--
ALTER TABLE `gateway_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `notification_templates`
--
ALTER TABLE `notification_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `postables`
--
ALTER TABLE `postables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slider_options`
--
ALTER TABLE `slider_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taxables`
--
ALTER TABLE `taxables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_classes`
--
ALTER TABLE `tax_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translatable_translations`
--
ALTER TABLE `translatable_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `utility_attributes`
--
ALTER TABLE `utility_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_attribute_options`
--
ALTER TABLE `utility_attribute_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_categories`
--
ALTER TABLE `utility_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_category_attributes`
--
ALTER TABLE `utility_category_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_locations`
--
ALTER TABLE `utility_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_model_attribute_options`
--
ALTER TABLE `utility_model_attribute_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_ratings`
--
ALTER TABLE `utility_ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_schedules`
--
ALTER TABLE `utility_schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_tags`
--
ALTER TABLE `utility_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_wishlists`
--
ALTER TABLE `utility_wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `webhook_calls`
--
ALTER TABLE `webhook_calls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_post`
--
ALTER TABLE `category_post`
  ADD CONSTRAINT `category_post_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_attribute_options`
--
ALTER TABLE `ecommerce_attribute_options`
  ADD CONSTRAINT `ecommerce_attribute_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `ecommerce_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_category_product`
--
ALTER TABLE `ecommerce_category_product`
  ADD CONSTRAINT `ecommerce_category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ecommerce_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ecommerce_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_coupon_product`
--
ALTER TABLE `ecommerce_coupon_product`
  ADD CONSTRAINT `ecommerce_coupon_product_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `ecommerce_coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_coupon_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ecommerce_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_coupon_user`
--
ALTER TABLE `ecommerce_coupon_user`
  ADD CONSTRAINT `ecommerce_coupon_user_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `ecommerce_coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_coupon_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_lense_mapping_table`
--
ALTER TABLE `ecommerce_lense_mapping_table`
  ADD CONSTRAINT `ecommerce_lense_mapping_table_ibfk_1` FOREIGN KEY (`len_cat_id`) REFERENCES `ecommerce_lense_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ecommerce_lense_mapping_table_ibfk_2` FOREIGN KEY (`len_subcat_id`) REFERENCES `ecommerce_lense_sub_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ecommerce_orders`
--
ALTER TABLE `ecommerce_orders`
  ADD CONSTRAINT `ecommerce_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_order_items`
--
ALTER TABLE `ecommerce_order_items`
  ADD CONSTRAINT `ecommerce_order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ecommerce_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_products`
--
ALTER TABLE `ecommerce_products`
  ADD CONSTRAINT `ecommerce_products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `ecommerce_brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_products_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `ecommerce_colors` (`id`),
  ADD CONSTRAINT `ecommerce_products_gender_id_foreign` FOREIGN KEY (`gender_id`) REFERENCES `ecommerce_genders` (`id`),
  ADD CONSTRAINT `ecommerce_products_material_id_foreign` FOREIGN KEY (`material_id`) REFERENCES `ecommerce_materials` (`id`),
  ADD CONSTRAINT `ecommerce_products_shape_id_foreign` FOREIGN KEY (`shape_id`) REFERENCES `ecommerce_shapes` (`id`),
  ADD CONSTRAINT `ecommerce_products_style_id_foreign` FOREIGN KEY (`style_id`) REFERENCES `ecommerce_styles` (`id`);

--
-- Constraints for table `ecommerce_product_attributes`
--
ALTER TABLE `ecommerce_product_attributes`
  ADD CONSTRAINT `ecommerce_product_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `ecommerce_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ecommerce_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_product_tag`
--
ALTER TABLE `ecommerce_product_tag`
  ADD CONSTRAINT `ecommerce_product_tag_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ecommerce_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_product_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `ecommerce_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_sku`
--
ALTER TABLE `ecommerce_sku`
  ADD CONSTRAINT `ecommerce_sku_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ecommerce_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ecommerce_sku_options`
--
ALTER TABLE `ecommerce_sku_options`
  ADD CONSTRAINT `ecommerce_sku_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `ecommerce_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_sku_options_attribute_option_id_foreign` FOREIGN KEY (`attribute_option_id`) REFERENCES `ecommerce_attribute_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ecommerce_sku_options_sku_id_foreign` FOREIGN KEY (`sku_id`) REFERENCES `ecommerce_sku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD CONSTRAINT `invoice_items_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `taxes`
--
ALTER TABLE `taxes`
  ADD CONSTRAINT `taxes_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utility_attribute_options`
--
ALTER TABLE `utility_attribute_options`
  ADD CONSTRAINT `utility_attribute_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `utility_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utility_category_attributes`
--
ALTER TABLE `utility_category_attributes`
  ADD CONSTRAINT `utility_category_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `utility_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `utility_category_attributes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `utility_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utility_model_attribute_options`
--
ALTER TABLE `utility_model_attribute_options`
  ADD CONSTRAINT `utility_model_attribute_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `utility_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `utility_model_attribute_options_attribute_option_id_foreign` FOREIGN KEY (`attribute_option_id`) REFERENCES `utility_attribute_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utility_model_has_category`
--
ALTER TABLE `utility_model_has_category`
  ADD CONSTRAINT `utility_model_has_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `utility_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utility_schedules`
--
ALTER TABLE `utility_schedules`
  ADD CONSTRAINT `utility_schedules_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utility_taggables`
--
ALTER TABLE `utility_taggables`
  ADD CONSTRAINT `utility_taggables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `utility_tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `utility_wishlists`
--
ALTER TABLE `utility_wishlists`
  ADD CONSTRAINT `utility_wishlists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
