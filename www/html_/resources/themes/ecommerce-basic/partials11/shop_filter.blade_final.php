<div class="row filter-bg">
    <div class="container">
    <div class="search-row">
<!--        <h3 class="widget-title">@lang('corals-ecommerce-basic::labels.template.shop.shop_categories')</h3>-->
<ul>
                  @foreach(\Shop::getActiveCategories() as $category)
                <li class="dropdown {{ $hasChildren = $category->hasChildren()?'has-children':'' }} parent-category">
<!--
                    @if($hasChildren)
                        <a href="#">{{ $category->name }}</a>
                        <span>({{
                                            \Shop::getCategoryAvailableProducts($category->id, true)
                                            }})
                                            </span>
                    
                    @else
                       @if($category->name == 'Glasses')
                    
                       @else
                        <div class="">
                            <input class=""
                                   name="category[]" value="{{ $category->slug }}"
                                   type="checkbox"
                                   id="ex-check-{{ $category->id }}"
                                    {{ \Shop::checkActiveKey($category->slug,'category')?'checked':'' }}>
                            <label class=""
                                   for="ex-check-{{ $category->id }}">
                                {{ $category->name }}
                                ({{ \Shop::getCategoryAvailableProducts($category->id, true)}})
                            </label>
                        </div>
                       @endif
                    
                    @endif
                    @if($hasChildren)
                        <ul>
                            @foreach($category->children as $child)
                                <li>
                                    <div class="">
                                        <input class=""
                                               name="category[]" value="{{ $child->slug }}"
                                               type="checkbox"
                                               id="ex-check-{{ $child->id }}"
                                                {{ \Shop::checkActiveKey($child->slug,'category')?'checked':'' }}>
                                        <label class=""
                                               for="ex-check-{{ $child->id }}">
                                            {{ $child->name }}
                                            ({{
                                                                \Shop::getCategoryAvailableProducts($child->id, true)
                                                                }})
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
-->
<!--    <li class="dropdown {{ $hasChildren = $category->hasChildren()?'has-children':'' }} parent-category">-->
<!--    <li class="dropdown">-->
        @if($category->name=='Category' ) 
        
        @else            
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ $category->name }}<span class="caret"></span></a>
         @if($hasChildren)
        <ul class="dropdown-menu" role="menu">
          @foreach($category->children as $child)
            <li><a id="{{$child->name}}" onclick="fun(this.id)">{{$child->name}}</a></li>
          @endforeach
        </ul>
         @endif
        @endif
    </li>

     @endforeach
     <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Price <span class="caret"></span></a>
<!--
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ url('/shop?price[min]=800.00&price[max]=1199.99') }}">Rs. 800.00 - Rs. 1,199.99</a></li>
            <li><a href="{{ url('/shop?price[min]=1200.00&price[max]=1599.99') }}">Rs. 1,200.00 - Rs. 1,599.99</a></li>
            <li><a href="{{ url('/shop?price[min]=7600.00&price[max]=100000000') }}">Rs. 7,600.00 and above</a></li>
        </ul>
-->
          <ul class="dropdown-menu price_div" role="menu">
<!--
            <li><a href="{{ url('/shop?price[min]=800.00&price[max]=1199.99') }}">Rs. 800.00 - Rs. 1,199.99</a></li>
            <li><a href="{{ url('/shop?price[min]=1200.00&price[max]=1599.99') }}">Rs. 1,200.00 - Rs. 1,599.99</a></li>
            <li><a href="{{ url('/shop?price[min]=7600.00&price[max]=100000000') }}">Rs. 7,600.00 and above</a></li>
-->
            <li>
            
        <h3 class="widget-title">@lang('corals-ecommerce-basic::labels.template.shop.price_range')</h3>
        <div class="price-range-slider"
             data-min="{{ $min = \Shop::getSKUMinPrice()??0 }}"
             data-max="{{ $max= \Shop::getSKUMaxPrice()??99999 }}"
             data-start-min="{{ request()->input('price.min', $min) }}"
             data-start-max="{{ request()->input('price.max', $max) }}"
             data-step="1">
            <div class="ui-range-slider"></div>
            <footer class="ui-range-slider-footer">
                <div class="column">
                    <div class="ui-range-values">
                        <div class="ui-range-value-min"><span id="minprice"></span>
                            <input name="price[min]" type="hidden">
                        </div>&nbsp;-&nbsp;
                        <div class="ui-range-value-max"><span id="maxprice"></span>
                            <input name="price[max]" type="hidden">
                        </div>
                    </div>
                </div>
               
            </footer>
             <button onclick="fun11()" class="btn add-to-cart btn-sm btn-primary " style="width:100%">Filter</button>
        </div>
 
            </li>
        </ul>
    </li>
</ul>
    </div>
    </div>
</div>

<script>
        function fun11(){
//        var min=document.getElementById('minprice').innerHTML;
//        var max=document.getElementById('maxprice').innerHTML;
//       // alert(min);
//        a = window.location.href;
//         var n = a.includes("?");
//        if(n){
//            n = a.includes("price[min]");
//            if(n){
//                a.replace('price[min]=','');
//                a.replace('price[max]=','');
//                
//          window.location.href =   a+"&price[min]="+min+"&price[max]="+max;
//        
//            
//            }else{
//                 window.location.href =   a+"&price[min]="+min+"&price[max]="+max;
//            }
//        }
//        else{
//             window.location.href =   a+"?price[min]="+min+"&price[max]="+max;
//        }
    }
    window.onload = function(e){ 
     a = window.location.href;
    var n = a.includes("?");
        if(n){
            var b =a.split('?');
            
            n = b[1].includes("&");
            
            if(n){
                 var c =b[1].split('&');
                  var len = c.length;
            var i;
           
                for (i = 0; i < len; i++) { 
                 // alert(c[i]);  
                    var k = c[i].split('=');
                   // alert(k[1]); 
                     var div = document.getElementById('cateee');
                div.innerHTML +=' <div class="alert alert-success alert-dismissible fade in custom_filters"><a href="#" class="close" data-dismiss="alert" onclick="rem(this.id)" id="'+k[1]+'" aria-label="close">&times;</a> <strong>'+k[1]+'</strong></div>' ;
                    
                    
                    
                  
//                    <div class="alert alert-success alert-dismissible fade in custom_filters">
//                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
//                        <strong>Success!</strong>
//                    </div>
    //document.getElementById('cateee').innerHTML += "<span>"+k[1]+"</span>";

           
            }
                
            }else{
                //alert(b[1]);
                  var k = b[1].split('=');
                 //alert(k[1]); 
                var div = document.getElementById('cateee');
               // div.innerHTML += "<span onclick='rem(this.id)' id='"+k[1]+"'>"+k[1]+"</span>";
                                div.innerHTML +=' <div class="alert alert-success alert-dismissible fade in custom_filters"><a href="#" class="close" data-dismiss="alert" onclick="rem(this.id)" id="'+k[1]+'" aria-label="close">&times;</a> <strong>'+k[1]+'</strong></div>' ;
                //alert(div.innerHTML);
            
            } 
          
             //document.getElementById('cateee').innerHTML = "<span>"+id+"</span>";
        }
    
    }
function rem(id){
   
    a = window.location.href;
     var n = a.includes("&");
    if(n){
    var ret = a.replace('&category='+id,'');
     window.location.href = ret;
    }
    else{
        n = a.includes("?");
       // alert(a);
        if(n){
            var ret = a.replace('?category='+id,'');
            //alert(ret);
                 window.location.href = ret;

        }
    }
    console.log(ret);  
}
function fun(id){
    
    a = window.location.href;
    var n = a.includes("?");
    if(n){
         window.location.href = a+'&category='+id; 
       
   // alert();
    }
    else{
        
         window.location.href = a+'?category='+id;
               

    }
    
    
   
}

</script>
@php \Actions::do_action('post_display_ecommerce_filter') @endphp