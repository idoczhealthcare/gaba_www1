<!-- Off-Canvas Mobile Menu-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->


<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GADA | Home page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css"/>


    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/fontello.css">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/jquery.slitslider.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/main.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/slick.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/responsive.css">
    
    <noscript>
        <link rel="stylesheet" type="text/css" href="../assets/themes/ecommerce-basic/css/styleNoJS.css" />
    </noscript>
    <body>
</head>
<header class="navbar navbar-sticky">
    <!-- Search-->

   <div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<nav class="navbar navbar-default navbar" >

    <div class="row top-navbar">
        <div class="container">
        <ul>
            <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="product.php" href="about-us.php">Eyewear</a></li>
            <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="product.php" href="services.php">Contact Lenses</a></li>
            <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="#">Blog</a></li>
        </ul>
    </div>
    </div>
    <div class="container">
       <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="../assets/themes/ecommerce-basic/img/logo.jpg" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <ul class="button navbar-right">
                <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-search" aria-hidden="true"></i></button>
                <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-user" aria-hidden="true"></i></button>
                <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-heart-o" aria-hidden="true"></i></button>
                <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>

            </ul>
            <ul class="main-nav nav navbar-nav navbar-right">
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="about-us.php">Glasses</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="services.php">Sunglasses</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="contact-us.php">Clearnce</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
</body>
</html>