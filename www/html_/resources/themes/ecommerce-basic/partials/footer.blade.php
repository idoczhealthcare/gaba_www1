<!--Newletter area-->
<section class="newletter">
    <div class="container">
        <div class="row">
            <div class="col-md-4 signup-text">
                <h3>SIGN UP FOR EXCLUSIVE OFFERS</h3>
            </div>
            <div class="col-md-4 signup-form">
                <div class="input-group news-form">
                    <form class="form-inline">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="inputPassword2" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="" placeholder="Email">
                            <button type="button" class="btn btn-primary mb-2 custom_submit_btn">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-md-4 social2">
                <ul>
                    <li><a class="wow fadeInUp animated animated" href="https://twitter.com/kimarotec" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://www.facebook.com/kimarotec" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://plus.google.com/kimarotec" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;"><i class="fa fa-dribbble"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- Footer area-->
<div class="footer-area">


    <div class="footer">
        <div class="container">
            <div class="row">

                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>GABA Opticals </h4>
                        <ul class="footer-menu">
                           <li><a href="{{ url('/About-us') }}">About us</a></li>
                            <li><a href="javascript:;">About Gaba Optical</a></li>
                            <li><a href="javascript:;">Our Vision</a></li>
                            <li><a href="javascript:;">Our Mission</a></li>
                            <li><a href="javascript:;">Chairman Message</a></li>
                            <li><a href="javascript:;">Our Core Team</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Quick links </h4>
                        <ul class="footer-menu">
                            <li><a href="javascript:;">20% off on Lenses</a></li>
                            <li><a href="javascript:;">20% off on Frames</a></li>
                            <li><a href="javascript:;">Buy one get one Free</a></li>
                            <li><a href="javascript:;">Starting from 2000</a></li>
                            <li><a href="javascript:;">New Arrivals</a></li>
                            <li><a href="javascript:;">Clearnace Sale</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Help Center</h4>
                        <ul class="footer-menu">
                            <li><a href="{{ url('/faq') }}">FAQs</a></li>
                            <li><a href="order-tracking.php">Order Tracking</a></li>
                            <li><a href="javascript:;">Shipping & Return Policy</a></li>
                            <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                            <li><a href="{{ url('/terms-of-use') }}">Terms of Use</a></li>
                            <li><a href="{{ url('/contact-us') }}">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Blogs</h4>
                        <ul class="footer-menu">
                            <li><a href="javascript:;">Blogs one</a></li>
                            <li><a href="javascript:;">Blogs tow</a></li>
                            <li><a href="javascript:;">Blogs three</a></li>
                            <li><a href="javascript:;">Blogs four</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 wow fadeInRight animated">
                   <div class="single-footer news-letter">
                        <h4></h4>
                       <p></p>

                    <!--<form>
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="E-mail ... ">
                            <span class="input-group">
                                <button class="btn btn-primary subscribe" type="button">Submit</button>
                            </span>
                        </div></form>-->
                    </div>
                    <div class="footer-payment-link">
<!--                        <img src="../assets/themes/ecommerce-basic/img/payment-method-image.png">-->
                        <ul class="footer-menu">
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-1.jpg"></a></li>
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-2.png"></a></li>
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-3.jpg"></li>
                            <li><a><img src="../assets/themes/ecommerce-basic/img/footer-img-4.jpg"></a></li>
                            
                        </ul>
                    </div>
                </div>

            </div>
        </div>


</div>
    <div class="footer-copy">
        <div class="container">
            <div class="row">
                    <hr>
                <div class="col-md-5 col-xs-12">
                    <span class="wow fadeInUp animated animated"> © Copyrights GABA Opticals 2018. All rights reserved </span>
                </div>
                <div class="col-md-4 col-xs-12">
                    <ul class="policy_ex">
                        <li><a class="wow fadeInUp animated animated" href="javascript:;">Terms & Conditions</a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;">Privacy Policy</a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;">Sitemap</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12">
                    <ul class="social_ex">
                        <li><a class="wow fadeInUp animated animated" href="javascript:;" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-cc-paypal" aria-hidden="true"></i></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="javascript:;" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a></li>
                </ul>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="../assets/themes/ecommerce-basic/js/modernizr-2.6.2.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/modernizr.custom.79639.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap-select.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap-hover-dropdown.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.easypiechart.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/owl.carousel.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/wow.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/slick.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/scripts.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/icheck.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.slitslider.js"></script>

    <script type="text/javascript" src="../assets/themes/ecommerce-basic/js/lightslider.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/main2.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/main.js"></script>
   <script src="../tracking.js-master/build/tracking-min.js"></script>
    <script src="../tracking.js-master/build/data/eye-min.js"></script>
    <script src="../tracking.js-master/build/data/face-min.js"></script>
	
   
<script>
    $('.price-slider').slick( {
            dots:true, infinite:false, speed:300, slidesToShow:3, slidesToScroll:1, autoplay:true, autoplaySpeed:2000, responsive:[ {
                breakpoint:1024, settings: {
                    slidesToShow: 2, slidesToScroll: 1, infinite: true, dots: true
                }
            }
                , {
                    breakpoint:600, settings: {
                        slidesToShow: 2, slidesToScroll: 2
                    }
                }
                , {
                    breakpoint:480, settings: {
                        slidesToShow: 1, slidesToScroll: 1
                    }
                }
            ]
        }
    );
    $(".sliderxs").slick( {
            arrows:false, dots:true, autoplay:true, adaptiveHeight:true, responsive:[ {
                breakpoint: 10000, settings: "unslick"
            }
                , {
                    breakpoint:767, settings: {
                        unslick: true
                    }
                }
            ]
        }
    );
</script>
<script type="text/javascript">
    $(function () {

        var Page = (function () {

            var $nav = $('#nav-dots > span'),
                slitslider = $('#slider').slitslider({
                    onBeforeChange: function (slide, pos) {

                        $nav.removeClass('nav-dot-current');
                        $nav.eq(pos).addClass('nav-dot-current');

                    }
                }),
                init = function () {

                    initEvents();

                },
                initEvents = function () {

                    $nav.each(function (i) {

                        $(this).on('click', function (event) {

                            var $dot = $(this);

                            if (!slitslider.isActive()) {

                                $nav.removeClass('nav-dot-current');
                                $dot.addClass('nav-dot-current');

                            }

                            slitslider.jump(i + 1);
                            return false;

                        });

                    });

                };

            return {init: init};

        })();

        Page.init();

        /**
         * Notes:
         *
         * example how to add items:
         */

        /*

         var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');

         // call the plugin's add method
         ss.add($items);

         */

    });
</script>
<script>
      
    $(document).ready(function() { 
        function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
       
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}
        var countries;
           $.get(window.base_url + "/products",function(data, status){
                //console.log(data[0]['price']);
             countries = data;
              // alert(countries);
                autocomplete(document.getElementById("search_bar"), countries);
            });

          $(".color_select_btn").first().addClass("active");  

          $(".color_select_btn").click(function ($this) {
            
                $(".color_select_btn").removeClass("active");
                $(this).addClass("active");    

          });


          $(".size").click(function ($this) {
            
                $(".size").removeClass("active");
                $(this).addClass("active");    

          });


          $(".nav-item").first().addClass("active");  
          $(".nav-link").first().addClass("show");
          $(".tab-pane").first().addClass("active");
          $(".tab-pane").first().addClass("show");
          $(".tab-pane").first().addClass("in");
          $(".nav-link").first().addClass("active");



          $('.left_checkbox').on('ifChecked', function(event){
            event.preventDefault();
            $('.left_disable_dropdown').prop("disabled", false);
        });

        $('.left_checkbox').on('ifUnchecked', function(event){
            event.preventDefault();
            $('.left_disable_dropdown').prop("disabled", true);
        });

        $('.right_checkbox').on('ifChecked', function(event){
            event.preventDefault();
            $('.right_disable_dropdown').prop("disabled", false);
        });

        $('.right_checkbox').on('ifUnchecked', function(event){
            event.preventDefault();
            $('.right_disable_dropdown').prop("disabled", true);
        });

        
        ///
     $(".dropdown").hover(
                function() {
                    $(this).children('.dropdown-menu').not('.in .dropdown-menu').stop().slideDown("400");
                    $(".dropdown-menu").removeClass('open');
                    $(this).toggleClass('open');
                },
                function() {
                    $(this).children('.dropdown-menu').not('.in .dropdown-menu').stop().slideUp("400");
                    $(".dropdown-menu").removeClass('open');
                    $(this).toggleClass('open');
                }
             );
        var url = window.location.href;

                if(url.includes("category=contact-lens"))
                {
                    $(".topper_head").children(":nth-child(2)").addClass("active2");
                }
            else
                if(url.includes("blog"))
                {
                    $(".topper_head").children(":nth-child(3)").addClass("active2");
                }
            else
               if (url.includes("category=Eyewear")) 
            {
                $(".topper_head").children(":nth-child(1)").addClass("active2");
//            }
//            else
            }
        ///

// if (url.includes("eyeglasses")) 
//   {
//     $(".navbar-nav").children(":nth-child(1)").addClass("active");
//   }
//   else
//   if(url.includes("sunglasses"))
//   {
//   	$(".navbar-nav").children(":nth-child(2)").addClass("active");
//   }
//   else
//   if(url.includes("clearance"))
//   {
//   	alert("3");
//   	$(".navbar-nav").children(":nth-child(3)").addClass("active");
//   }
      
    $(".main-nav li").hover(function ($this) {
        $(this).addClass("active");
    }, function ($this) {
        $(this).removeClass("active");
    });

         $(".dropdown").hover(function() {
                $(this).children(".dropdown-menu").toggleClass("show");
            });
        $(window).scroll(function(){
            if($(document).scrollTop() > 0) {
                $(".top-navbar").css("display","none");
                $(".tools").css("vertical-align","top");
                $(".navbar").css("cssText", "min-height: 80px !important;");
                $(".main-nav").css("cssText","margin-top: 16px !important");
                $(".toolbar").css("cssText", "top: 30% !important;");
            } else {
                $(".top-navbar").css("display","block");
                $(".tools").css("vertical-align","middle");
                $(".navbar").css("cssText", "min-height: 85px !important;");
                $(".main-nav").css("cssText","margin-top: 4px !important");
                $(".toolbar").css("cssText", "top: 42% !important;");
            }
        });

        $(window).scroll(function(){
            if($(document).scrollTop() > 380) {
                $(".filter-bg").css("position","fixed");
                $(".filter-bg").css("top","80px");
                $(".filter-bg").css("width","102%");
                $(".filter-bg").css("z-index","9");

            }
            else{
                $(".filter-bg").css("position","relative");
                $(".filter-bg").css("top","0");
            }
        });

        $('#media').carousel({
            pause: true,
            interval: false,
        });
        
//        var modal = document.getElementById('myModal');
//
//        // Get the button that opens the modal
//        var btn = document.getElementById("myBtn");
//
//        // Get the <span> element that closes the modal
//        var span = document.getElementsByClassName("close")[0];
        
//        $(document).click(function () {
//           
//            $('#myModal').modal("hide");
//        });
//        $('.close').click(function () {
//          
//            $('#myModal').toggle();
//        });

    });

        // Get the modal


//        // When the user clicks the button, open the modal
//        btn.onclick = function() {
//            modal.style.display = "block";
//        }
//
//        // When the user clicks on <span> (x), close the modal
//        span.onclick = function() {
////            modal.style.display = "none";
//        }
//
//        // When the user clicks anywhere outside of the modal, close it
//        window.onclick = function(event) {
//            if (event.target == modal) {
//                modal.style.display = "none";
//            }
//        }
    
      var moving = false;
	
		$(function () {
		 $('.image1').on('change',function () {                
                var prod_ID = $(this).attr('data');
            
				$('#face'+prod_ID).hide();
				var reader = new FileReader();
               document.getElementById('prod_ID').value=prod_ID;

				reader.onload = function (e) {
                  
					$('#face'+prod_ID).show();
					$('#face'+prod_ID).attr("src", e.target.result);
				}
				reader.readAsDataURL($(this)[0].files[0]);
			});
			 $('.image2').on('change',function () {                
                var prod_ID = $(this).attr('data');
               
				$('#glasses'+prod_ID).hide();
				var reader = new FileReader();
				reader.onload = function (e) {
                    
					$('#glasses'+prod_ID).show();
					$('#glasses'+prod_ID).attr("src", e.target.result);
                     //alert($('#glasses1').attr("src"));
					document.getElementById("glasses"+prod_ID).addEventListener("mousedown", initialClick, false);
				}
				reader.readAsDataURL($(this)[0].files[0]);
			});	
		});
	
        function showGlass(id) {
           
			var x = -1, y = -1, width = -1, height = -1;
            var face = document.getElementById('face'+id);
           
            var tracker = new tracking.ObjectTracker("eye");
            tracker.setStepSize(1.7);

            tracking.track('#face'+id, tracker);

            tracker.on('track', function (event) {
                event.data.forEach(function (rect) {
					if (x != -1) {
						width = (rect.x + rect.width) - x + 20;
					}				
				
					if (x == -1 || rect.x < x)
						x = rect.x - 20;
						
					if (y == -1)
						y = rect.y;
						
					if (height = -1)
						height = rect.height + 20;
                });
				window.plot(x, y, width, height,id);

            });
        }
		// rect;
		function increaseGlassHeight(id) {
           var  rect = document.getElementById('glasses'+id);
            //rect.style.width =  (rect.clientWidth + 10) + 'px';
            rect.style.height = (rect.clientHeight + 10) + 'px';			
		}
		
		function decreaseGlassHeight(id) {
             var rect = document.getElementById('glasses'+id);
            //rect.style.width =  (rect.clientWidth - 10) + 'px';
            rect.style.height = (rect.clientHeight - 10) + 'px';
		}

		function increaseGlassWidth(id) {
            var rect = document.getElementById('glasses'+id);
            rect.style.width =  (rect.clientWidth + 10) + 'px';
            //rect.style.height = (rect.clientHeight + 10) + 'px';			
		}
		
		function decreaseGlassWidth(id) {
             var rect = document.getElementById('glasses'+id);
            rect.style.width =  (rect.clientWidth - 10) + 'px';
            //rect.style.height = (rect.clientHeight - 10) + 'px';
			//var left = window.getComputedStyle('glasses',null).getPropertyValue('left');
			//rect.style.left = (left - 10) + 'px';
		}		
		
        window.plot = function (x, y, w, h,id) {
            // var rect=document.getElementById('prod_ID').value;
             var face = document.getElementById('face'+id);
            rect = document.getElementById('glasses'+id);
            rect.style.width = w + 'px';
            rect.style.height = h + 'px';
            rect.style.left = (face.offsetLeft + x) + 'px';
            rect.style.top = (face.offsetTop + y) + 'px';
        };
		
		
		function move(e){
			
            
            
            var p = $( ".modal-content" );
            var position = p.position();
            
            var newX = e.clientX - 180;
			var newY = e.clientY - 180;
            
			image.style.left = newX + "px";
			image.style.top = newY + "px";
		}

	
		function initialClick(e) {
			if(moving){
				document.removeEventListener("mousemove", move);
				moving = !moving;
				return;
			}
  
			moving = !moving;
			image = this;

			document.addEventListener("mousemove", move, false);
		}			
    
  
//        var moving = false;
//
//        $(function () {
//            $('#image1').change(function () {
//                $('#face').hide();
//                var reader = new FileReader();
//                reader.onload = function (e) {
//                    $('#face').show();
//                    $('#face').attr("src", e.target.result);
//                }
//                reader.readAsDataURL($(this)[0].files[0]);
//            });
//            $('#image2').change(function () {
//                $('#glasses123').hide();
//                var reader = new FileReader();
//                reader.onload = function (e) {
//                    $('#glasses123').show();
//                    $('#glasses123').css("display","block");
//                    $('#glasses123').attr("src", e.target.result);
//                    document.getElementById("glasses123").addEventListener("mousedown", initialClick, false);
//                }
//                reader.readAsDataURL($(this)[0].files[0]);
//            });
//        });
//
//        function showGlass() {
//
//            var x = -1, y = -1, width = -1, height = -1;
//            var face = document.getElementById('face');
//
//            var tracker = new tracking.ObjectTracker("eye");
//            tracker.setStepSize(1.7);
//
//            tracking.track('#face', tracker);
//
//            tracker.on('track', function (event) {
//                event.data.forEach(function (rect) {
//                    if (x != -1) {
//                        width = (rect.x + rect.width) - x + 0;
//                    }
//
//                    if (x == -1 || rect.x < x)
//                        x = rect.x - 0;
//
//                    if (y == -1)
//                        y = rect.y;
//
//                    if (height = -1)
//                        height = rect.height + 0;
//                });
//                window.plot(x, y, width, height);
//
//            });
//        }
//
//        function increaseGlassHeight() {
//            var rect = document.getElementById('glasses123');
//            //rect.style.width =  (rect.clientWidth + 10) + 'px';
//            rect.style.height = (rect.clientHeight + 10) + 'px';
//        }
//
//        function decreaseGlassHeight() {
//            var rect = document.getElementById('glasses123');
//            //rect.style.width =  (rect.clientWidth - 10) + 'px';
//            rect.style.height = (rect.clientHeight - 10) + 'px';
//        }
//
//        function increaseGlassWidth() {
//            var rect = document.getElementById('glasses123');
//            rect.style.width =  (rect.clientWidth + 10) + 'px';
//            //rect.style.height = (rect.clientHeight + 10) + 'px';
//        }
//
//        function decreaseGlassWidth() {
//            var rect = document.getElementById('glasses123');
//            rect.style.width =  (rect.clientWidth - 10) + 'px';
//            rect.style.height = (rect.clientHeight - 10) + 'px';
//            // var left = window.getComputedStyle('glasses',null).getPropertyValue('left');
//            rect.style.left = (left - 10) + 'px';
//        }
//
//        // window.plot = function (x, y, w, h) {
//        //     var rect = document.getElementById('glasses123');
//        //     rect.style.width = w + 'px';
//        //     rect.style.height = h + 'px';
//        //     rect.style.left = (face.offsetLeft + x) + 'px';
//        //     rect.style.top = (face.offsetTop + y) + 'px';
//        // };
//
//
//        // function move(e){
//
//        //     var newX;
//        //     var newY;
//
//        //     e = e || window.event;
//        //     e.preventDefault();
//        //     // calculate the new cursor position:
//        //     var pos1 = newX - e.clientX;
//        //     var pos2 = newY - e.clientY;
//        //     newX = e.clientX;
//        //     newY = e.clientY;
//
//        //     image.style.left = newX + "px";
//        //     image.style.top = newY + "px";
//
//
//        //     // image.style.top = (image.offsetTop - pos2) + "px";
//        //     // image.style.left = (image.offsetLeft - pos1) + "px";
//        // }
//
//        function initialClick(e) {
//            if(moving){
//                document.removeEventListener("mousemove", move);
//                moving = !moving;
//                return;
//            }
//
//            moving = !moving;
//            image = this;
//
//            document.addEventListener("mousemove", move, false);
//        }
//
//        //Make the DIV element draggagle:
//dragElement(document.getElementById("glasses123"));
//
//function dragElement(elmnt) {
//  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
//  elmnt.onmousedown = dragMouseDown;
//
//  function dragMouseDown(e) {
//    e = e || window.event;
//    e.preventDefault();
//    // get the mouse cursor position at startup:
//    pos3 = e.clientX;
//    pos4 = e.clientY;
//    document.onmouseup = closeDragElement;
//    // call a function whenever the cursor moves:
//    document.onmousemove = elementDrag;
//  }
//
//  function elementDrag(e) {
//    e = e || window.event;
//    e.preventDefault();
//    // calculate the new cursor position:
//    pos1 = pos3 - e.clientX;
//    pos2 = pos4 - e.clientY;
//    pos3 = e.clientX;
//    pos4 = e.clientY;
//    // set the element's new position:
//    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
//    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
//  }
//
//  function closeDragElement() {
//    /* stop moving when mouse button is released:*/
//    document.onmouseup = null;
//    document.onmousemove = null;
//  }
//}
    </script>

    <!--3step form script start-->
    <script>
        $(document).ready(function () {


            $("fa-cart-plus").removeClass("fa-cart-plus");
            $(this).addClass("fa-shopping-cart")   

            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-indigo').addClass('btn-default');
                    $item.addClass('btn-indigo');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allPrevBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                prevStepSteps.removeAttr('disabled').trigger('click');
            });

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i< curInputs.length; i++){
                    if (!curInputs[i].validity.valid){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-indigo').trigger('click');
        });
    </script>
</body>
</html>