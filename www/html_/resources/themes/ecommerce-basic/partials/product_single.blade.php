@extends('layouts.master')
<link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/w3.css">
@section('css')
    <style type="text/css">
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
    </style>
@endsection

@section('editable_content')
    @php \Actions::do_action('pre_content',$product, null) @endphp

    <!-- Page Content-->
    <div class="container padding-bottom-3x my-5 m_50 m_t_12em">
        <div class="row">
            <!-- Product Gallery-->
            <div class="col-md-6">
                <div class="product-gallery">

                    
                    @if($product->discount)
                        <div class="product-badge text-danger">{{ $product->discount }}% Off</div>
                    @endif
                    @if(!($medias = $product->getMedia('ecommerce-product-gallery'))->isEmpty())
<!--
                        <div class="product-carousel owl-carousel text-center">
                            @foreach($medias as $media)
                                <a href="{{ $media->getUrl() }}" data-hash="gItem_{{ $media->id }}"
                                   data-lightbox="product-gallery">
                                    <img src="{{ $media->getUrl() }}" class="mx-auto" alt="Product"
                                         style="max-height: 300px;width: auto;"/>
                                </a>
                            @endforeach
                        </div>
-->
                                     <p style="display:none">{{$i=1}}</p>
<div class="w3-content w3-section w3-display-container">
     @foreach($medias as $media)
    <img src="{{ $media->getUrl() }}" class="mx-auto mySlides w3-animate-fading" alt="Product"
                                         style="max-height: 300px;width: auto;"/>
  
     @endforeach
    <div class="w3-center w3-display-bottommiddle" style="width:100%">
    @foreach($medias as $media)
     <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv({{$i++}})"></span>
           @endforeach
   
  </div>
     
  
 
</div>
                        <ul class="product-thumbnails">
                            @foreach($medias as $media)
                                <li class="{{ $media->getCustomProperty('featured', false)?'active':'' }}">
                                    <a href="#gItem_{{ $media->id }}">
                                        <img src="{{ $media->getUrl() }}" alt="Product"
                                             style="max-height: 100px;width: auto;"></a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div class="text-center text-muted">
                            <small>@lang('corals-ecommerce-basic::labels.template.product_single.image_unavailable')</small>
                        </div>
                    @endif
                </div>
                <!-- Product Tabs-->
                <div class="row mb-3">
                    <div class="col-lg-10 offset-lg-1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" href="#description" data-toggle="tab"
                                                    role="tab">@lang('corals-ecommerce-basic::labels.template.product_single.description')</a>
                            </li>
                            @if(\Settings::get('ecommerce_rating_enable',true) === 'true')

                                <li class="nav-item"><a class="nav-link" href="#reviews" data-toggle="tab"
                                                        role="tab">@lang('corals-ecommerce-basic::labels.template.product_single.reviews',['count'=>$product->ratings->count()])</a>
                                </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="description" role="tabpanel">
                                <div>
                                    {!! $product->description !!}
                                </div>
                            </div>
                            @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
                                <br>
                                @include('partials.tabs.reviews',['reviews'=>$product->ratings])
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <!-- Product Info-->
            <div class="col-md-6">
                <div class="padding-top-2x mt-2 hidden-md-up"></div>

                <p class="custom_sub_text1 text-normal">NEW ARRIVALS</p>
                <h1 class="padding-top-1x text-normal">{{ $product->name }}</h1>
                <div class="row">
                    <div class="col-md-6">
                        <div class="rating">
                            <span class="review-no">COLOR AVAILABLE</span>
                            <div class="color-available">

                                <ul>
                                    @foreach($product->activeCategories as $category)
                                        @if($category->parent_id=='')

                                        @else
                                            <li class="color-1" style='color:{{ $category->name }}'> </li>
                                            &nbsp;&nbsp;
                                        @endif
                                    @endforeach


                                </ul>

                                <!--
                                .color-1{color: #946647}
                                .color-2{color: #6c819a}
                                .color-3{color: #302d2d}
                                .color-4{color: #c9996a}
                                -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rating">
                            <span class="review-no">RATING $ REVIEWS</span>
                            <div class="stars">
                                @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
                                    @include('partials.components.rating',['rating'=> $product->averageRating(1)[0],'rating_count'=>$product->countRating()[0] ])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="rating">
                            <span class="review-no">SIZES AVAILABLE</span>
                            <div class="sizechat">
                                <span class="size">S</span>
                                <span class="size">M</span>
                                <span class="size">L</span>
                                <span class="size" style="border: none;box-shadow:none;"><a href="#">View Size Guide</a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="rating" style="margin-top: 20px;">
                            <span class="review-no">PRICE</span>
                            <div class="ratess">
                                <span> <strong> {!! $product->price  !!}</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rating" style="margin-top: 20px;">
                            <span class="review-no">Quanitity</span>
<div class="pro-quantity"> {!! CoralsForm::number('quantity','corals-ecommerce-basic::attributes.template.quantity', false, 1, ['min' => 1,'class'=>'form-control form-control-sm']) !!}
</div>
                        </div>
                    </div>
                </div>

                <!--<h4 class=" text-normal">{{ $product->caption }}</h4>-->
                {!! Form::open(['url'=>'cart/'.$product->hashed_id.'/add-to-cart','method'=>'POST','class'=> 'ajax-form','data-page_action'=>"updateCart"]) !!}

                @if(!$product->isSimple)
                    @foreach($product->activeSKU as $sku)
                        @if($loop->index%4 == 0)
                            <div class="d-flex flex-wrap">
                                @endif
                                <div class="text-center sku-item mr-2" style="width: 240px;">
                                    <img src="{{ asset($sku->image) }}" class="img-responsive img-radio mx-auto">
                                    <div class="middle">
                                        <div class="text text-success"><i class="fa fa-check fa-4x"></i></div>
                                    </div>
                                    <div>
                                        {!! !$sku->options->isEmpty() ? $sku->presenter()['options']:'' !!}
                                    </div>
                                    @if($sku->stock_status == "in_stock")
                                        <button type="button"
                                                class="btn btn-block btn-sm btn-default btn-secondary btn-radio m-t-5">
                                            <b>{!! $sku->discount?'<del class="text-muted">'.\Payments::currency($sku->regular_price).'</del>':''  !!} {!! currency()->format($sku->price, \Settings::get('admin_currency_code'), currency()->getUserCurrency()) !!}</b>
                                        </button>

                                    @else
                                        <button type="button"
                                                class="btn btn-block btn-sm m-t-5 btn-danger">
                                            <b> @lang('corals-ecommerce-basic::labels.partial.out_stock')</b>
                                        </button>
                                    @endif
                                    <input type="checkbox" id="left-item" name="sku_hash" value="{{ $sku->hashed_id }}"
                                           class="hidden d-none disable-icheck"/>
                                </div>
                                @if($lastLoop = $loop->index%4 == 3)
                            </div>
                        @endif
                    @endforeach
                    @if(!$lastLoop)</div>@endif
            <div class="form-group">
                <span data-name="sku_hash"></span>
            </div>
            @else
                <input type="hidden" name="sku_hash" value="{{ $product->activeSKU(true)->hashed_id }}"/>
            @endif

            <div class="row">
                <div class="col-md-6">
                    @if($product->globalOptions->count())
                        {!! $product->renderProductOptions('global_options',null, ['class'=>'form-control form-control-sm']) !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(\Settings::get('ecommerce_wishlist_enable', 'true') == 'true')
                        @include('partials.components.wishlist',['wishlist'=> $product->inWishList() ])
                    @endif

                    @if($product->external_url)
                        <a href="{{ $product->external_url }}" target="_blank" class="btn btn-success"
                           title="@lang('corals-ecommerce-basic::labels.template.product_single.buy_product')">
                            <i class="fa fa-fw shopping-bag"
                               aria-hidden="true"></i> @lang('corals-ecommerce-basic::labels.template.product_single.buy_product')
                        </a>
                    @elseif($product->isSimple && $product->activeSKU(true)->stock_status != "in_stock")
                        <a href="#" class="btn btn-sm btn-outline-danger"
                           title="Out Of Stock">
                            @lang('corals-ecommerce-basic::labels.partial.out_stock')
                        </a>
                    @else
                        {!! CoralsForm::button('corals-ecommerce-basic::labels.partial.add_to_cart',
                        ['class'=>'btn add-to-cart btn-sm btn-secondary'], 'submit') !!}

                    <!-- <a data-word="SELECT LENSES" href="" id='href' data-event-cate="Product Page" data-event-name="Product Features" data-event-label="Select Lenses" class="btn btn-select-lenses" id="select-lens">
                     SELECT LENSES
                    </a> -->

                    <a href="" id='href' class="btn add-to-cart btn-sm btn-secondary ladda-button" ><span class="ladda-label">Select Lenses</span><span class="ladda-spinner"></span></a>

                    @endif
                </div>
            </div>

            {{ Form::close() }}
        
            <hr class="mb-3">
            <div class="d-flex flex-wrap justify-content-between">
                @include('partials.components.social_share',['url'=> URL::current() , 'title'=>$product->name ])

            </div>
        </div>
    </div>
    @include('partials.featured_products',['title'=>trans('corals-ecommerce-basic::labels.template.product_single.title')])
    </div>
        <div id="myModal" class="modal" role="dialog">

        <!-- Modal content -->
        <div class="try-on-box">
        <div class="modal-content">
           <button class="close" type="button" data-dismiss="modal" aria-label="close"><span
                                aria-hidden="true">&times;</span></button>

                <div  class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <div class="row">
                                <div class="tryon-uploader">
                                    <h4> Select Photo and Then Click Try Glasses.</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                <div class="pull-left">
                                        <label>
                                            Browse Your Photo
                                            <input id="image1" type="file" />
                                        </label>
                                </div>
                                        </div>
                                        <div class="col-md-6">
                                    <div class="pull-right">
                                        <label>
                                            Browse Image of Glasses
                                            <input id="image2" type="file" />
                                        </label>
                                    </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="try-bth-di">
                                    <ul>
                                        <li><button type="button" onclick="showGlass()">Try Glasses !</button></li>
                                        <li><button type="button" onclick="increaseGlassHeight()"><img src="../assets/themes/ecommerce-basic/img/top.png"> </button></li>
                                        <li><button type="button" onclick="decreaseGlassHeight()"><img src="../assets/themes/ecommerce-basic/img/bottom.png"></button></li>
                                        <li><button type="button" onclick="increaseGlassWidth()"><img src="../assets/themes/ecommerce-basic/img/right.png"></button></li>
                                        <li><button type="button" onclick="decreaseGlassWidth()"><img src="../assets/themes/ecommerce-basic/img/left.png"></button></li>
                                        <li><p> You can also click on the glasses and move it around to adjust</p></li>

                                    </ul>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="eyetry-sidebar" id="sidebar" style="display: block;">
                                        <div class="upload-list">
                                            <div class="try-first-upload-tips">
                                                <p>This is our virtual mirror. Please select a photo to upload.</p>
                                                <p>If you already have an account, please <span class="try-login">sign in</span> to view previously uploaded photos.</p></div>
                                            <h5>Use Photo From:</h5>
                                            <ul class="clearfix">
                                                <li class="computer current" data-type="computer"><i class="fa fa-desktop" aria-hidden="true"></i>My Computer</li>
                                                <li class="facebook" data-type="facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i>Facebook</li>
                                                <li class="links" data-type="link"><i class="fa fa-facebook-official" aria-hidden="true"></i>Link (URL)</li>
                                            </ul>
                                        </div>
                                        <span class="upload-back-btn"> &lt; Back</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tryon-try">
                                <figure>
                                <img id="face" src="" style="display:none;"/>
                                <img id="glasses" src="" class="rect" />
                                </figure>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    @parent
    <script>
            var myIndex = 0;
carousel();

 function currentDiv(n) {
  showDivs(myIndex = n);
}
function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {myIndex = 1}    
  if (n < 1) {myIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-black", "");
  }
  x[myIndex-1].style.display = "block";  
  dots[myIndex-1].className += " w3-black";
   //setTimeout(showDivs, 5000);
}
function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    currentDiv(myIndex);
    setTimeout(carousel, 5000);    
}
        var url =window.location.href;
        var name11 = url.split('/');
        var base_url = window.location.origin;
        document.getElementById("href").href =base_url+'/configure/'+ name11[4];

        </script>
    @include('Ecommerce::cart.cart_script')
@endsection