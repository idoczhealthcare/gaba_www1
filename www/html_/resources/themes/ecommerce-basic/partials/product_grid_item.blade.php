
<div class="grid-item">
    <div class="product-card">
        @if($product->discount)
            <div class="product-badge text-danger">{{ $product->discount }}% Off</div>
        @endif
        @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
            @include('partials.components.rating',['rating'=> $product->averageRating(1)[0],'rating_count'=>null])
        @endif
        <div style="min-height: 170px;" class="mt-4">
            <a  class="product-thumb link" href="{{ url('shop/'.$product->slug) }}">
                <img src="{{ $product->image }}" alt="{{ $product->name }}" class="mx-auto"
                     style="max-height: 150px;width: auto;">
            </a>
        </div>
            <div class="col-md-6">
        <h3 class="product-title">
            <span style="margin-right: 20px; color: #9da9b9;font-size: 14px;font-weight: 500;">Eye Q Eyewear</span>
            <a href="{{ url('shop/'.$product->slug) }}">{{ $product->name }}</a>
        </h3>
            </div>
            <div class="col-md-6">
        <h4 class="product-price">
            @if($product->discount)
                <del>{{ \Payments::currency($product->regular_price) }}</del>
            @endif
            <div class="">
            {!! $product->price !!}
            </div>
        </h4>
            </div>
            <div class="col-md-12">
                <div class="color-available">
                    <h6 class="">COLOR AVAILABLE</h6>
                    <div class="color-available">
                        @if($colors = \Shop::getSubProductColor($product->id))
                            @foreach($colors as $color)
                                <img title="{{ $color[0]['title'] }}" src = "{{ $color[0]['thumbnail'] }}" class="img-responsive img-rounded color_select_btn" style ="max-height: 20px;width:auto" alt = "Thumbnail"  />
                            @endforeach
                        @endif
                    
                    </div>
                </div>
        </div>

        <div class="shop-bth">
        <a id="myBtn" data-toggle="modal" data-target="#{{$product->sku}}" class="btn add-to-cart btn-sm btn-primary ladda-button hide1" data-style="expand-right"><span class="ladda-label">Try On</span><span class=""></span></a>

        </div>
           <div id="{{$product->sku}}" class="modal" role="dialog">

        <!-- Modal content -->
     <div class="">
        <div class="modal-content">
           <button class="close" type="button" data-dismiss="modal" aria-label="close"><span
                                aria-hidden="true">&times;</span></button>
            	<div>
	<ul>
	<li><p> Select Photo and Then Click Try Glasses.</p></li>
	<li>
		<label style="margin: 30px 0px 0px 0px;">
			Browse Your Photo
            
        
		<input id="image1" class='image1' data='{{ $product->id }}' type="file" />
        <input type="hidden" name="prod_ID" value="" id="prod_ID"  />
		</label>
	</li>
	<li>
		<label style="margin: 30px 0px 0px 0px;">
			Browse Image of Glasses
		<input id="image2" class='image2' data='{{ $product->id }}' type="file" />
		</label>
	</li>	
    <li><button type="button" onclick="showGlass({{ $product->id }})">Try Glasses !</button></li>
	<li><button type="button" onclick="increaseGlassHeight({{ $product->id }})"> increase height </button></li>
	<li><button type="button" onclick="decreaseGlassHeight({{ $product->id }})"> decrease height </button></li>
	<li><button type="button" onclick="increaseGlassWidth({{ $product->id }})"> increase width </button></li>
	<li><button type="button" onclick="decreaseGlassWidth({{ $product->id }})"> decrease width </button></li>
	<li><p> You can also click on the glasses <br/>and move it around to adjust</p></li>
	</ul>
	</div>
    <div class="demo-container">
        
		<img id="face{{ $product->id }}" class='face' src="" style="display:none;"/>
        <img id="glasses{{ $product->id }}" src="" class="rect1" />
    </div>
                
  
        </div>
</div>
    </div>
    </div>
    
</div>
       
