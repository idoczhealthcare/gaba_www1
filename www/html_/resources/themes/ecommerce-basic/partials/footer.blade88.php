<!--Newletter area-->
<section class="newletter">
    <div class="container">
        <div class="row">
            <div class="col-md-4 signup-text">
                <h3>SIGN UP FOR EXCLUSIVE OFFERS</h3>
            </div>
            <div class="col-md-4 signup-form">
                <div class="input-group news-form">
                    <input type="email" class="form-control" placeholder="Enter your email" style="border-radius: 10px;">
                    <span class="input-group-btn">
                    <button class="btn btn-theme" type="submit">Subscribe</button>
         </span>
                </div>
            </div>
            <div class="col-md-4 social2">
                <ul>
                    <li><a class="wow fadeInUp animated animated" href="https://twitter.com/kimarotec" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://www.facebook.com/kimarotec" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://plus.google.com/kimarotec" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-google-plus"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;"><i class="fa fa-dribbble"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- Footer area-->
<div class="footer-area">


    <div class="footer">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>GABA Opticals </h4>
                        <ul class="footer-menu">
                           <li><a href="#">About us</a></li>
                            <li><a href="#">About Gaba Optical</a></li>
                            <li><a href="#">Our Vision</a></li>
                            <li><a href="#">Our Mission</a></li>
                            <li><a href="#">Chairman Message</a></li>
                            <li><a href="#">Our Core Team</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Quick links </h4>
                        <ul class="footer-menu">
                            <li><a href="#">20% off on Lenses</a></li>
                            <li><a href="#">20% off on Frames</a></li>
                            <li><a href="#">Buy one get one Free</a></li>
                            <li><a href="#">Starting from 2000</a></li>
                            <li><a href="#">New Arrivals</a></li>
                            <li><a href="#">Clearnace Sale</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Help Center</h4>
                        <ul class="footer-menu">
                            <li><a href="faq.php">FAQs</a></li>
                            <li><a href="order-tracking.php">Order Tracking</a></li>
                            <li><a href="#">Shipping & Return Policy</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="">Terms of Use</a></li>
                            <li><a href="contact-us.php">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer news-letter">
                        <h4>Newsletter Subscription</h4>
                        <p>Apply for our monthly newsletter subscription for FREE</p>

                       <form>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="E-mail ... ">
                                <span class="input-group">
                                    <button class="btn btn-primary subscribe" type="button">Submit</button>
                                </span>
                            </div>
                            <!-- /input-group --></form>
                    </div>
                </div>

            </div>
        </div>


</div>
    <div class="footer-copy text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <span> © Copyrights GABA Opticals 2018. All rights reserved</span>
                </div>

            </div>
        </div>
    </div>



<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="../assets/themes/ecommerce-basic/js/modernizr-2.6.2.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/modernizr.custom.79639.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap-select.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/bootstrap-hover-dropdown.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.easypiechart.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/owl.carousel.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/wow.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/slick.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/scripts.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/icheck.min.js"></script>
    <script src="../assets/themes/ecommerce-basic/js/jquery.slitslider.js"></script>
    <script type="text/javascript" src="../assets/themes/ecommerce-basic/js/lightslider.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/main2.js"></script>
<script type="text/javascript">
    $(function () {

        var Page = (function () {

            var $nav = $('#nav-dots > span'),
                slitslider = $('#slider').slitslider({
                    onBeforeChange: function (slide, pos) {

                        $nav.removeClass('nav-dot-current');
                        $nav.eq(pos).addClass('nav-dot-current');

                    }
                }),
                init = function () {

                    initEvents();

                },
                initEvents = function () {

                    $nav.each(function (i) {

                        $(this).on('click', function (event) {

                            var $dot = $(this);

                            if (!slitslider.isActive()) {

                                $nav.removeClass('nav-dot-current');
                                $dot.addClass('nav-dot-current');

                            }

                            slitslider.jump(i + 1);
                            return false;

                        });

                    });

                };

            return {init: init};

        })();

        Page.init();

        /**
         * Notes:
         *
         * example how to add items:
         */

        /*

         var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');

         // call the plugin's add method
         ss.add($items);

         */

    });
</script>
<script>
    $(document).ready(function() {
        $('#media').carousel({
            pause: true,
            interval: false,
        });
        
        $(document).click(function () {
            console.log("Im Clicked");
            $('#myModal').modal("hide");
        });
        $('.close').click(function () {
            console.log("Im Clicked");
            $('#myModal').toggle();
        });

    });

</script>
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
    <script>
        var moving = false;

        $(function () {
            $('#image1').change(function () {
                $('#face').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#face').show();
                    $('#face').attr("src", e.target.result);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
            $('#image2').change(function () {
                $('#glasses').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#glasses').show();
                    $('#glasses').attr("src", e.target.result);
                    document.getElementById("glasses").addEventListener("mousedown", initialClick, false);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
        });

        function showGlass() {
            var x = -1, y = -1, width = -1, height = -1;
            var face = document.getElementById('face');

            var tracker = new tracking.ObjectTracker("eye");
            tracker.setStepSize(1.7);

            tracking.track('#face', tracker);

            tracker.on('track', function (event) {
                event.data.forEach(function (rect) {
                    if (x != -1) {
                        width = (rect.x + rect.width) - x + 20;
                    }

                    if (x == -1 || rect.x < x)
                        x = rect.x - 20;

                    if (y == -1)
                        y = rect.y;

                    if (height = -1)
                        height = rect.height + 20;
                });
                window.plot(x, y, width, height);

            });
        }

        function increaseGlassHeight() {
            var rect = document.getElementById('glasses');
            //rect.style.width =  (rect.clientWidth + 10) + 'px';
            rect.style.height = (rect.clientHeight + 10) + 'px';
        }

        function decreaseGlassHeight() {
            var rect = document.getElementById('glasses');
            //rect.style.width =  (rect.clientWidth - 10) + 'px';
            rect.style.height = (rect.clientHeight - 10) + 'px';
        }

        function increaseGlassWidth() {
            var rect = document.getElementById('glasses');
            rect.style.width =  (rect.clientWidth + 10) + 'px';
            //rect.style.height = (rect.clientHeight + 10) + 'px';
        }

        function decreaseGlassWidth() {
            var rect = document.getElementById('glasses');
            rect.style.width =  (rect.clientWidth - 10) + 'px';
            //rect.style.height = (rect.clientHeight - 10) + 'px';
            //var left = window.getComputedStyle('glasses',null).getPropertyValue('left');
            //rect.style.left = (left - 10) + 'px';
        }

        window.plot = function (x, y, w, h) {
            var rect = document.getElementById('glasses');
            rect.style.width = w + 'px';
            rect.style.height = h + 'px';
            rect.style.left = (face.offsetLeft + x) + 'px';
            rect.style.top = (face.offsetTop + y) + 'px';
        };


        function move(e){
            var newX = e.clientX;
            var newY = e.clientY;

            image.style.left = newX + "px";
            image.style.top = newY + "px";
        }

        function initialClick(e) {
            if(moving){
                document.removeEventListener("mousemove", move);
                moving = !moving;
                return;
            }

            moving = !moving;
            image = this;

            document.addEventListener("mousemove", move, false);
        }
    </script>
    <script>
        var moving = false;

        $(function () {
            $('#image1').change(function () {
                $('#face').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#face').show();
                    $('#face').attr("src", e.target.result);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
            $('#image2').change(function () {
                $('#glasses').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#glasses').show();
                    $('#glasses').attr("src", e.target.result);
                    document.getElementById("glasses").addEventListener("mousedown", initialClick, false);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
        });

        function showGlass() {
            var x = -1, y = -1, width = -1, height = -1;
            var face = document.getElementById('face');

            var tracker = new tracking.ObjectTracker("eye");
            tracker.setStepSize(1.7);

            tracking.track('#face', tracker);

            tracker.on('track', function (event) {
                event.data.forEach(function (rect) {
                    if (x != -1) {
                        width = (rect.x + rect.width) - x + 20;
                    }

                    if (x == -1 || rect.x < x)
                        x = rect.x - 20;

                    if (y == -1)
                        y = rect.y;

                    if (height = -1)
                        height = rect.height + 20;
                });
                window.plot(x, y, width, height);

            });
        }

        function increaseGlassHeight() {
            var rect = document.getElementById('glasses');
            //rect.style.width =  (rect.clientWidth + 10) + 'px';
            rect.style.height = (rect.clientHeight + 10) + 'px';
        }

        function decreaseGlassHeight() {
            var rect = document.getElementById('glasses');
            //rect.style.width =  (rect.clientWidth - 10) + 'px';
            rect.style.height = (rect.clientHeight - 10) + 'px';
        }

        function increaseGlassWidth() {
            var rect = document.getElementById('glasses');
            rect.style.width =  (rect.clientWidth + 10) + 'px';
            //rect.style.height = (rect.clientHeight + 10) + 'px';
        }

        function decreaseGlassWidth() {
            var rect = document.getElementById('glasses');
            rect.style.width =  (rect.clientWidth - 10) + 'px';
            //rect.style.height = (rect.clientHeight - 10) + 'px';
            //var left = window.getComputedStyle('glasses',null).getPropertyValue('left');
            //rect.style.left = (left - 10) + 'px';
        }

        window.plot = function (x, y, w, h) {
            var rect = document.getElementById('glasses');
            rect.style.width = w + 'px';
            rect.style.height = h + 'px';
            rect.style.left = (face.offsetLeft + x) + 'px';
            rect.style.top = (face.offsetTop + y) + 'px';
        };


        function move(e){
            var newX = e.clientX;
            var newY = e.clientY;

            image.style.left = newX + "px";
            image.style.top = newY + "px";
        }

        function initialClick(e) {
            if(moving){
                document.removeEventListener("mousemove", move);
                moving = !moving;
                return;
            }

            moving = !moving;
            image = this;

            document.addEventListener("mousemove", move, false);
        }
    </script>
    <script>
        var moving = false;

        $(function () {
            $('#image1').change(function () {
                $('#face').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#face').show();
                    $('#face').attr("src", e.target.result);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
            $('#image2').change(function () {
                $('#glasses').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#glasses').show();
                    $('#glasses').attr("src", e.target.result);
                    document.getElementById("glasses").addEventListener("mousedown", initialClick, false);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
        });

        function showGlass() {
            var x = -1, y = -1, width = -1, height = -1;
            var face = document.getElementById('face');

            var tracker = new tracking.ObjectTracker("eye");
            tracker.setStepSize(1.7);

            tracking.track('#face', tracker);

            tracker.on('track', function (event) {
                event.data.forEach(function (rect) {
                    if (x != -1) {
                        width = (rect.x + rect.width) - x + 20;
                    }

                    if (x == -1 || rect.x < x)
                        x = rect.x - 20;

                    if (y == -1)
                        y = rect.y;

                    if (height = -1)
                        height = rect.height + 20;
                });
                window.plot(x, y, width, height);

            });
        }

        function increaseGlassHeight() {
            var rect = document.getElementById('glasses');
            //rect.style.width =  (rect.clientWidth + 10) + 'px';
            rect.style.height = (rect.clientHeight + 10) + 'px';
        }

        function decreaseGlassHeight() {
            var rect = document.getElementById('glasses');
            //rect.style.width =  (rect.clientWidth - 10) + 'px';
            rect.style.height = (rect.clientHeight - 10) + 'px';
        }

        function increaseGlassWidth() {
            var rect = document.getElementById('glasses');
            rect.style.width =  (rect.clientWidth + 10) + 'px';
            //rect.style.height = (rect.clientHeight + 10) + 'px';
        }

        function decreaseGlassWidth() {
            var rect = document.getElementById('glasses');
            rect.style.width =  (rect.clientWidth - 10) + 'px';
            //rect.style.height = (rect.clientHeight - 10) + 'px';
            //var left = window.getComputedStyle('glasses',null).getPropertyValue('left');
            //rect.style.left = (left - 10) + 'px';
        }

        window.plot = function (x, y, w, h) {
            var rect = document.getElementById('glasses');
            rect.style.width = w + 'px';
            rect.style.height = h + 'px';
            rect.style.left = (face.offsetLeft + x) + 'px';
            rect.style.top = (face.offsetTop + y) + 'px';
        };


        function move(e){
            var newX = e.clientX;
            var newY = e.clientY;

            image.style.left = newX + "px";
            image.style.top = newY + "px";
        }

        function initialClick(e) {
            if(moving){
                document.removeEventListener("mousemove", move);
                moving = !moving;
                return;
            }

            moving = !moving;
            image = this;

            document.addEventListener("mousemove", move, false);
        }
    </script>
    <!--3step form script start-->
    <script>
        $(document).ready(function () {

            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-indigo').addClass('btn-default');
                    $item.addClass('btn-indigo');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allPrevBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                prevStepSteps.removeAttr('disabled').trigger('click');
            });

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i< curInputs.length; i++){
                    if (!curInputs[i].validity.valid){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-indigo').trigger('click');
        });
    </script>
</body>
</html>