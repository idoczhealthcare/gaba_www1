@extends('layouts.master')

@section('editable_content')
    @php \Actions::do_action('pre_content',$item, $home??null) @endphp

    {!! $item->rendered !!}

    @include('partials.featured_categories')
    @include('partials.featured_products')

    @include('partials.featured_brands')
<!--

    <section class="container padding-top-2x padding-bottom-2x">
        <div class="row">
            <div class="col-md-3 col-sm-6 text-center mb-30"><img
                        class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3"
                        src="/assets/themes/ecommerce-basic/img/services/01.png"
                        alt="Shipping">
                <h6>@lang('corals-ecommerce-basic::labels.template.home.free_worldwide')</h6>
                <p class="text-muted margin-bottom-none">@lang('corals-ecommerce-basic::labels.template.home.free_shipping')</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img
                        class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3"
                        src="/assets/themes/ecommerce-basic/img/services/02.png"
                        alt="Money Back">
                <h6>@lang('corals-ecommerce-basic::labels.template.home.money_back_guarantee')</h6>
                <p class="text-muted margin-bottom-none">@lang('corals-ecommerce-basic::labels.template.home.money_days')</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img
                        class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3"
                        src="/assets/themes/ecommerce-basic/img/services/03.png"
                        alt="Support">
                <h6>@lang('corals-ecommerce-basic::labels.template.home.customer_support')</h6>
                <p class="text-muted margin-bottom-none">@lang('corals-ecommerce-basic::labels.template.home.friendly_customer_support')</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img
                        class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3"
                        src="/assets/themes/ecommerce-basic/img/services/04.png"
                        alt="Payment">
                <h6>@lang('corals-ecommerce-basic::labels.template.home.secure_online_payment')</h6>
                <p class="text-muted margin-bottom-none">@lang('corals-ecommerce-basic::labels.template.home.posess_ssl')</p>
            </div>
        </div>
        <div class="text-center">
            @php \Actions::do_action('pre_display_footer') @endphp
        </div>
    </section>
-->


<div class="slide-2">
    <div id="slider" class="sl-slider-wrapper">
        <div class="sl-slider">

            <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">

                <div class="sl-slide-inner ">

                    <div class="bg-img bg-img-1" style="background-image: url(../assets/themes/ecommerce-basic/img/slide/silde-1.jpg);"></div>
                    <blockquote>
                        <cite>Choose your frame and</cite>
                        <article>Enhance your</article>
                        <article>Style Statement</article>
                        <cite>get discount upto 70% off</cite>
                    </blockquote>
                </div>
            </div>

            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">

                <div class="sl-slide-inner ">

                    <div class="bg-img bg-img-1" style="background-image: url(../assets/themes/ecommerce-basic/img/slide/silde-2.jpg);"></div>
                    <blockquote>
                        <cite>Choose your frame and</cite>
                        <article>Enhance your</article>
                        <article>Style Statement</article>
                        <cite>get discount upto 70% off</cite>
                    </blockquote>
                </div>
            </div>


            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">

                <div class="sl-slide-inner ">

                    <div class="bg-img bg-img-1" style="background-image: url(../assets/themes/ecommerce-basic/img/slide/silde-3.jpg);"></div>
                    <blockquote>
                        <cite>Choose your frame and</cite>
                        <article>Enhance your</article>
                        <article>Style Statement</article>
                        <cite>get discount upto 70% off</cite>
                    </blockquote>
                </div>
            </div>
        </div><!-- /sl-slider -->

        <nav id="nav-dots" class="nav-dots">
            <span class="nav-dot-current"></span>
            <span></span>
            <span></span>
            <span></span>
        </nav>
    </div><!-- /slider-wrapper -->
</div>
    <!--End of slider-->
    <section class="second-silder bg-g">
        <div class="container">
            <div class="row">
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/starting.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Glasses Starting</li>
                            <li>From PKR.2000</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/free.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Free</li>
                            <li>DELIVERY</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/return.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Free</li>
                            <li>RETURNS</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/customer-care.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Personalized</li>
                            <li>CUSTOMER CARE</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-------------Banner section--------------->
<section class="home-banners">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-1.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerone">
                                    <h4><strong>CLEARANCE</strong> Sale</h4>
                                    <p>Get upto 80% off Now!</p>
                                    <a href="">GET NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-2.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannertwo">
                                    <h4><strong>READY-TO-WEAR</strong> Frames</h4>
                                    <p>Get Your Now!</p>
                                    <a href="">GET NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hover ehover11">
                    <div class="item mdnone">
                        <img src="../assets/themes/ecommerce-basic/img/banner-3.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerthere">
                                    <h4><strong>New Arrivals</strong></h4>
                                    <p>Want to stay updated on the hottest style picks?<br>Be sure to check our New Arrivals</p>
                                    <a href="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row price-slider">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner4.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerfour">
                                    <h4><strong>20%</strong> OFF</h4>
                                    <p>on Frames</p>
                                    <a href="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-5.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerfive">
                                    <h4><strong>20%</strong> OFF</h4>
                                    <p>on Lenses</p>
                                    <a href="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-6.jpg">
                        <div class="item-overlay">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hover ehover12 ehover11">
                    <div class="item mdnone">
                        <img src="../assets/themes/ecommerce-basic/img/banner-7.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannersix">
                                    <div class="pull-left bor">
                                    <h5>COMPUTER<br>
                                        GLASSES</h5>
                                    </div>
                                    <div class="pull-left">
                                       <ul>
                                           <li>Get upto</li>
                                           <li>40% Discount</li>
                                           <li>on your favourite</li>
                                       </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 </div>
    </div>
</section>
<section class="heading-big">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="text-center">THE HOTTEST</h2>
            <h1><span>NEW ARRVIALS</span></h1>
            </div>
        </div>
    </div>
</section>
<!--Product section-->
<section class="Product-sec">
    <div class="container">
        <div class="row price-slider">
            <div class="col-md-3">
                <div class="product">
                    <figure>
                        <img src="../assets/themes/ecommerce-basic/img/product/glass-1.png">
                        <p>2217 Retro Eyeglasses</p>
                        <span>PKR <strong>2,850</strong></span></figure>
                </div>
            </div>
            <div class="col-md-3">
                <div class="product">
                    <figure>
                        <img src="../assets/themes/ecommerce-basic/img/product/glass-1.png">
                        <p>2217 Retro Eyeglasses</p>
                        <span>PKR <strong>2,850</strong></span></figure>
                </div>
            </div>
            <div class="col-md-3">
                <div class="product">
                    <figure>
                        <img src="../assets/themes/ecommerce-basic/img/product/glass-1.png">
                        <p>2217 Retro Eyeglasses</p>
                        <span>PKR <strong>2,850</strong></span></figure>

                </div>
            </div>
            <div class="col-md-3">
                <div class="product">
                    <figure>
                        <img src="../assets/themes/ecommerce-basic/img/product/glass-1.png">
                        <p>2217 Retro Eyeglasses</p>
                        <span>PKR <strong>2,850</strong></span></figure>

                </div>
            </div>
        </div>
    </div>
</section>
<!--blognvlog
    <section class="heading-blog">
<div class="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">BLOGS / VLOGS</h2>
                <h1><span>GUIDES FOR YOU</span></h1>
            </div>
        </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="blog-bg">
                    <div class="log pull-left">
                        <figure>
                        <img src="assets/img/blog-1.jpg">
                        </figure>
                    </div>
                    <div class="log pull-right">
                        <h5>Best Fitting Frames
                            Selection</h5>
                        <p>Lost in your search to find the perfect frames? Look no further!</p>
                        <p>Our product designers have put together a special collection....</p>
                    </div>
                    </div>
                </div>
                <div class="col-md-6 blog-bg">
                    <div class="log">
                        <div class="pull-left">
                            <figure>
                                <img src="assets/img/blog-1.jpg">
                            </figure>
                        </div>
                        <div class=" pull-right">
                            <h5>Best Fitting Frames
                                Selection</h5>
                            <p>Lost in your search to find the perfect frames? Look no further!</p>
                            <p>Our product designers have put together a special collection....</p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
    </section> -->





@stop


@section('js')
<script src="../assets/themes/ecommerce-basic/js/modernizr-2.6.2.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/jquery-1.10.2.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/bootstrap.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/bootstrap-select.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/bootstrap-hover-dropdown.js"></script>
<script src="../assets/themes/ecommerce-basic/js/jquery.easypiechart.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/owl.carousel.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/wow.js"></script>
<script src="../assets/themes/ecommerce-basic/js/slick.js"></script>
<script src="../assets/themes/ecommerce-basic/js/scripts.js"></script>
<script src="../assets/themes/ecommerce-basic/js/icheck.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/jquery.slitslider.js"></script>
<script type="text/javascript" src="../assets/themes/ecommerce-basic/js/lightslider.min.js"></script>
<script src="../assets/themes/ecommerce-basic/js/main.js"></script>
    @parent
    @include('Ecommerce::cart.cart_script')
@endsection