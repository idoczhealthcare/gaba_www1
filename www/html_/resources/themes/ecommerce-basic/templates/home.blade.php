<?php
session_start();

if(!isset($_SESSION['a']) || $_SESSION['a'] !="hi") {
$_SESSION['a']="hi";
   //  echo 'Set and not empty, and no undefined index error!1';
    echo "   <script>
      window.location.href = 'http://i-doczportal.com/country';
    </script>";   
     
     
}
else{
    
  //echo 'Set and not empty, and no undefined index error!';
    
 
}



?>

@extends('layouts.master')

 @section('editable_content') 
    @php \Actions::do_action('pre_content',$item, $home??null) @endphp

    {!! $item->rendered !!}



    <section class="">
        
    <!--End of slider-->
    <section class="second-silder bg-g">
        <div class="container">
            <div class="row">
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/starting.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Glasses Starting</li>
                            <li>From PKR.2000</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/free.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Free</li>
                            <li>DELIVERY</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/return.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Free</li>
                            <li>RETURNS</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 b-right none-b-right">
                    <div class="col-md-4">
                        <img src="../assets/themes/ecommerce-basic/img/customer-care.png">
                    </div>
                    <div class="col-md-8">
                        <ul>
                            <li>Personalized</li>
                            <li>CUSTOMER CARE</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-------------Banner section--------------->
<section class="home-banners">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-1.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerone">
                                    <h4><strong>CLEARANCE</strong> Sale</h4>
                                    <p>Get upto 80% off Now!</p>
                                    <a href="">GET NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-2.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannertwo">
                                    <h4><strong>READY-TO-WEAR</strong> Frames</h4>
                                    <p>Get Your Now!</p>
                                    <a href="">GET NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hover ehover11">
                    <div class="item mdnone">
                        <img src="../assets/themes/ecommerce-basic/img/banner-3.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerthere">
                                    <h4><strong>New Arrivals</strong></h4>
                                    <p>Want to stay updated on the hottest style picks?<br>Be sure to check our New Arrivals</p>
                                    <a href="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row price-slider">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner4.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerfour">
                                    <h4><strong>20%</strong> OFF</h4>
                                    <p>on Frames</p>
                                    <a href="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-5.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannerfive">
                                    <h4><strong>20%</strong> OFF</h4>
                                    <p>on Lenses</p>
                                    <a href="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="hover ehover11">
                    <div class="item">
                        <img src="../assets/themes/ecommerce-basic/img/banner-6.jpg">
                        <div class="item-overlay">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hover ehover12 ehover11">
                    <div class="item mdnone">
                        <img src="../assets/themes/ecommerce-basic/img/banner-7.jpg">
                        <div class="item-overlay">
                            <div class="midoverlay">
                                <div class="middlealign bannersix">
                                    <div class="pull-left bor">
                                    <h5>COMPUTER<br>
                                        GLASSES</h5>
                                    </div>
                                    <div class="pull-left">
                                       <ul>
                                           <li>Get upto</li>
                                           <li>40% Discount</li>
                                           <li>on your favourite</li>
                                       </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 </div>
    </div>
</section>
<section class="heading-big">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="text-center">THE HOTTEST</h2>
            <h1><span>NEW ARRVIALS</span></h1>
            </div>
        </div>
    </div>
</section>
<!--Product section-->
<section class="Product-sec">
    <div class="container">
        <div class="row price-slider">

    @if(!($newArrivalsProducts = \Shop::getNewArrivals())->isEmpty())
         @foreach($newArrivalsProducts as $product)
            <div class="col-md-4">
                <div class="product">
                    <figure>
                        <a  class="" href="{{ url('shop/'.$product->slug) }}">
                        <img src="{{ $product->image }}">
                        <p>{{ $product->name }}</p>
                        <span> <strong>{!! $product->price !!}</strong></span>
                             </a>
                            </figure>
                </div>
            </div>
        
     
         @endforeach
     @endif
        </div>
    </div>
</section>
        <div class="text-center">
            <!-- @php \Actions::do_action('pre_display_footer') @endphp -->
        </div>
    </section>


@stop


@section('js')
    @parent
    @include('Ecommerce::cart.cart_script')
@endsection