@extends('layouts.master')
<link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/w3.css">
@section('css')
    <style type="text/css">
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
 .owl-stage{
         width: 2590px !important;
     }
    </style>
@endsection

@section('editable_content')
    @php \Actions::do_action('pre_content',$product, null) @endphp

    <!-- Page Content-->

    <div class="main_container2">
    <div class="container padding-bottom-3x my-5 m_50 m_t_12em">
        <div class="row">
            <!-- Product Gallery-->
            <div class="col-md-6">
                <div class="product-gallery">
                    
                    <button class="top_left_btn" id="myBtn" data-toggle="modal" data-target="#{{$product->sku}}">Try On</button>
                     <div id="{{$product->sku}}" class="modal" role="dialog">

        <!-- Modal content -->
        <div class="">
        <div class="modal-content">
           <button class="close" type="button" data-dismiss="modal" aria-label="close"><span
                                aria-hidden="true">&times;</span></button>
            	<div>
	<ul>
	<li><p> Select Photo and Then Click Try Glasses.</p></li>
	<li>
		<label style="margin: 30px 0px 0px 0px;">
			Browse Your Photo
            
        
		<input id="image1" class='image1' data='{{ $product->id }}' type="file" />
        <input type="hidden" name="prod_ID" value="" id="prod_ID"  />
		</label>
	</li>
	<li>
		<label style="margin: 30px 0px 0px 0px;">
			Browse Image of Glasses
		<input id="image2" class='image2' data='{{ $product->id }}' type="file" />
		</label>
	</li>	
    <li><button type="button" onclick="showGlass({{ $product->id }})">Try Glasses !</button></li>
	<li><button type="button" onclick="increaseGlassHeight({{ $product->id }})"> increase height </button></li>
	<li><button type="button" onclick="decreaseGlassHeight({{ $product->id }})"> decrease height </button></li>
	<li><button type="button" onclick="increaseGlassWidth({{ $product->id }})"> increase width </button></li>
	<li><button type="button" onclick="decreaseGlassWidth({{ $product->id }})"> decrease width </button></li>
	<li><p> You can also click on the glasses <br/>and move it around to adjust</p></li>
	</ul>
	</div>
    <div class="demo-container">
        
		<img id="face{{ $product->id }}" class='face' src="" style="display:none;"/>
        <img id="glasses{{ $product->id }}" src="" class="rect1" />
    </div>
                
  
        </div>
</div>
    </div>

                    @if($product->discount)
                        <div class="product-badge text-danger">{{ $product->discount }}% Off</div>
                    @endif
        
        @if(!($medias = $product->getMedia('ecommerce-product-gallery'))->isEmpty())
            <p style="display:none">{{$i=1}}</p>
            <div id="resp_img" class="w3-content w3-section w3-display-container">
                <div id="test">
                @foreach($medias as $media)
                    <img src="{{$media->getUrl()}}" class="mx-auto mySlides w3-animate-fading" alt="Product" style="max-height: 300px;width: auto;"/>
                @endforeach
                </div>
                <div class="w3-center w3-display-bottommiddle" id="test1" style="width:100%">
                    @foreach($medias as $media)
                    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv({{$i++}})"></span>
                    @endforeach
                </div>
            </div>
        <ul class="product-thumbnails" id="product_thumbnails">
            @foreach($medias as $media)
                <li class="{{ $media->getCustomProperty('featured', false) ? 'active':'' }}">
                    <a href="#gItem_{{ $media->id }}">
                        <img src="{{ $media->getUrl() }}" alt="Product"
                             style="max-height: 100px;width: auto;">
                    </a>
                </li>
            @endforeach
        </ul>
        @else
        <div id="counterid">
            
        </div>

              
            <div id="resp_img" class="w3-content w3-section w3-display-container">
                <div id="test">
                
                </div>
                <div class="w3-center w3-display-bottommiddle" id="test1" style="width:100%">
                   
                </div>
            </div>
        <ul class="product-thumbnails" id="product_thumbnails">
           
        </ul>
            <!-- <div class="text-center text-muted">
                <small>@lang('corals-ecommerce-basic::labels.template.product_single.image_unavailable')</small>
            </div> -->


        @endif
    </div>

                <!-- Product Tabs-->
                <div class="row mb-3">
                    <div class="col-lg-12 offset-lg-1">
                        <div class="desc_wrap">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item"><a class="nav-link active" href="#description" data-toggle="tab" role="tab">@lang('corals-ecommerce-basic::labels.template.product_single.description')</a>
                                </li>
                                @if(\Settings::get('ecommerce_rating_enable',true) === 'true')

                                    <li class="nav-item"><a class="nav-link" href="#reviews" data-toggle="tab" role="tab">@lang('corals-ecommerce-basic::labels.template.product_single.reviews',['count'=>$product->ratings->count()])</a>
                                    </li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="description" role="tabpanel">
                                    <div>
                                        {!! $product->description !!}
                                    </div>
                                </div>
                                @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
                                    <!-- <br> -->
                                    @include('partials.tabs.reviews',['reviews'=>$product->ratings])
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Product Info-->
            <div class="col-md-6">
                <div class="padding-top-2x mt-2 hidden-md-up"></div>

            <div class="bg_white">
                <p class="custom_sub_text1 text-normal">NEW ARRIVALS</p>
                <h1 class="padding-top-1x text-normal">{{ $product->name }}</h1>
                <div class="row">
                    <div class="col-md-6">
                        <div class="rating">
                            <span class="review-no">COLOR AVAILABLE</span>
                            <div class="color-available" id="color_available">
                                <!-- <ul>
                                    @foreach($product->activeCategories as $category)
                                        @if($category->parent_id=='')

                                        @else
                                            <li class="color-1" style='color:{{ $category->name }}'> </li>
                                            &nbsp;&nbsp;
                                        @endif
                                    @endforeach
                                </ul> -->
                                 @foreach($color as $item)
                                    <img title="{{ $item[0]['title'] }}" src = "{{ $item[0]['thumbnail'] }}" class="img-responsive img-rounded color_select_btn" style ="max-height: 20px;width:auto" alt = "Thumbnail" onclick='getsize({{$item[0]["id"]}},{{$product->id}})' data-color=""  />
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rating">
                            <span class="review-no">RATING & REVIEWS</span>
                            <div class="stars">
                                @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
                                    @include('partials.components.rating',['rating'=> $product->averageRating(1)[0],'rating_count'=>$product->countRating()[0] ])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="rating">
                            <span class="review-no">SIZES AVAILABLE</span>
                            
                            
                            <div class="sizechat" id="sizechat">
                                <a href="#">View Size Guide</a></span>
                            </div>
                        </div>
                    </div>
                </div>
                                {!! Form::open(['url'=>'cart/'.$product->hashed_id.'/add-to-cart','method'=>'POST','class'=> 'ajax-form','data-page_action'=>"updateCart"]) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="rating" style="margin-top: 13px;">
                            <span class="review-no">PRICE</span>
                            <div class="ratess">
                                <span> <strong id="get_price"> {!! $product->price  !!}</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rating">
                            <span class="review-no">Quanitity</span>
                            <div class="pro-quantity"> {!! CoralsForm::number('quantity','corals-ecommerce-basic::attributes.template.quantity', false, 1, ['min' => 1,'class'=>'form-control form-control-sm']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <!--<h4 class=" text-normal">{{ $product->caption }}</h4>-->
               

                <!--                        update-->
                        <input type="hidden" name="color" id="hidden_color" value="">
<input type="hidden" name="size" id="hidden_size" value="">
<input type="hidden" name="new_price" id="hidden_price" value="">
<input type="hidden" name="new_prodID" id="hidden_productID" value="{{ $product->id }}">
                        <!--                        update-->
                @if(!$product->isSimple)
                    @foreach($product->activeSKU as $sku)
                        @if($loop->index%4 == 0)
                            <div class="d-flex flex-wrap">
                                @endif
                                <div class="text-center sku-item mr-2" style="width: 240px;">
                                    <img src="{{ asset($sku->image) }}" class="img-responsive img-radio mx-auto">
                                    <div class="middle">
                                        <div class="text text-success"><i class="fa fa-check fa-4x"></i></div>
                                    </div>
                                    <div>
                                        {!! !$sku->options->isEmpty() ? $sku->presenter()['options']:'' !!}
                                    </div>
                                    @if($sku->stock_status == "in_stock")
                                        <button type="button"
                                                class="btn btn-block btn-sm btn-default btn-secondary btn-radio m-t-5">
                                            <b>{!! $sku->discount?'<del class="text-muted">'.\Payments::currency($sku->regular_price).'</del>':''  !!} {!! currency()->format($sku->price, \Settings::get('admin_currency_code'), currency()->getUserCurrency()) !!}</b>
                                        </button>

                                    @else
                                        <button type="button"
                                                class="btn btn-block btn-sm m-t-5 btn-danger">
                                            <b> @lang('corals-ecommerce-basic::labels.partial.out_stock')</b>
                                        </button>
                                    @endif
                                    <input type="checkbox" id="left-item" name="sku_hash" value="{{ $sku->hashed_id }}"
                                           class="hidden d-none disable-icheck"/>
                                </div>
                                @if($lastLoop = $loop->index%4 == 3)
                            </div>
                        @endif
                    @endforeach
                    @if(!$lastLoop)</div>
                @endif

            <div class="form-group">
                <span data-name="sku_hash"></span>
            </div>
            @else
                <input type="hidden" name="sku_hash" value="{{ $product->activeSKU(true)->hashed_id }}"/>
            @endif

            <div class="row">
                <div class="col-md-6">
                    @if($product->globalOptions->count())
                        {!! $product->renderProductOptions('global_options',null, ['class'=>'form-control form-control-sm']) !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(\Settings::get('ecommerce_wishlist_enable', 'true') == 'true')
                        @include('partials.components.wishlist',['wishlist'=> $product->inWishList() ])
                    @endif

                    @if($product->external_url)
                        <a href="{{ $product->external_url }}" target="_blank" class="btn btn-success"
                           title="@lang('corals-ecommerce-basic::labels.template.product_single.buy_product')">
                            <i class="fa fa-fw fa-shopping-bag"
                               aria-hidden="true"></i> @lang('corals-ecommerce-basic::labels.template.product_single.buy_product')
                        </a>
                    @elseif($product->isSimple && $product->activeSKU(true)->stock_status != "in_stock")
                        <a href="#" class="btn btn-sm btn-outline-danger"
                           title="Out Of Stock">
                            @lang('corals-ecommerce-basic::labels.partial.out_stock')
                        </a>
                    @else
                        {!! CoralsForm::button('corals-ecommerce-basic::labels.partial.add_to_cart',
                        ['class'=>'btn add-to-cart btn-sm btn-secondary'], 'submit') !!}

                    <!-- <a data-word="SELECT LENSES" href="" id='href' data-event-cate="Product Page" data-event-name="Product Features" data-event-label="Select Lenses" class="btn btn-select-lenses" id="select-lens">
                     SELECT LENSES
                    </a> -->

                    <a href="" id='href' class="btn add-to-cart btn-sm btn-secondary ladda-button" ><span class="ladda-label">Select Lenses</span><span class="ladda-spinner"></span></a>

                    @endif
                </div>
            </div>

            {{ Form::close() }}
            <div class="mb-2" style="display: none;">
                <span class="text-medium">@lang('corals-ecommerce-basic::labels.template.product_single.category')</span>
                @foreach($product->activeCategories as $category)
                    <a class="" href="{{ url('shop?category='.$category->slug) }}"><b>{{ $category->name }}</b></a>
                    &nbsp;&nbsp;
                @endforeach
            </div>
            @if($product->activeTags->count())
                <div class="padding-bottom-1x mb-2" style="display: none;">
                    <span class="text-medium">@lang('corals-ecommerce-basic::labels.template.product_single.tag')</span>
                    @foreach($product->activeTags as $tag)
                        <a class="" href="{{ url('shop?tag='.$tag->slug) }}"><b>{{ $tag->name }}</b></a>&nbsp;&nbsp;
                    @endforeach
                </div>
            @endif

            <div class="d-flex flex-wrap justify-content-between" style="margin-top: 30;">
                @include('partials.components.social_share',['url'=> URL::current() , 'title'=>$product->name ])
            </div>
            <hr class="mb-3">
        </div>
    </div>
</div>
    @include('partials.featured_products',['title'=>trans('corals-ecommerce-basic::labels.template.product_single.title')])
    </div>
   

    </div>


@stop

@section('js')
    @parent
<!--
    <script>

        $('#color_available img:first').trigger('click');
        var myIndex = 0;
        carousel();
        var url =window.location.href;
        var name11 = url.split('/');
        var base_url = window.location.origin;
        document.getElementById("href").href =base_url+'/configure/'+ name11[4];


         function getsize(color,pid){
            //debugger;

            $.get(window.base_url + "/getsize?color=" + color +'&pid='+pid,function(data, status){
                //alert(data);
                $('#sizechat').empty();
                for (var key in data[0]) {
                    $('#sizechat').append('<span class="size" onclick="getprice('+data[0][key][0]['id']+","+color+","+pid+',this)" >'+ data[0][key][0]['title'] +'</span>');
                    //alert(data[0][key][0]['id']);
                    //alert(data[1][key]['collection_name']);
                }   
                $('#sizechat span:first').trigger('click');
                var img = '';
                var img_li = '';
                var counter = '';
                var count1= '';
                $('#test').empty();
                $('#test1').empty();
                $('#counterid').empty();
                var i=1;

                $('#counterid').append("<p style='display:none'>"+i+"</p>");     
                for (var key in data[1]) {
                    var count = parseInt(key)+1;
                    count1 += '<span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv('+ i++ +')"></span>';
                    if(key==0){
                        img += '<img src="'+data[1][key][0]+'" class="mx-auto mySlides w3-animate-fading" alt="Product" style="max-height: 300px;width: auto; display:block;"/>';
                    }else{
                        img += '<img src="'+data[1][key][0]+'" class="mx-auto mySlides w3-animate-fading" alt="Product" style="max-height: 300px;width: auto;"/>';
                    }
                    
                     
                    img_li +='<li class=""><a href="#gItem_'+data[1][key][1]+'"><img src="'+data[1][key][0]+'" alt="Product"style="max-height: 100px;width: auto;"></a></li>';
                    counter += '<span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv('+count+')"></span>'; 
                }
                var resp_img = img+'<div class="w3-center w3-display-bottommiddle" style="width:100%">'+counter+'</div>';
                //alert(resp_img);
                $('#product_thumbnails').empty();

                
                $('#test').append(img); 
                $('#test1').append(count1); 
                $('#product_thumbnails').append(img_li);
                

                 carousel();
            });
        }

        function getprice(size,color,pid,el){

            $(".size").removeClass("active");
            $(el).addClass("active");

            $.get(window.base_url + "/getprice?size=" + size+"&color="+color+"&pid="+pid ,function(data, status){
                //console.log(data[0]['price']);
                $('#get_price').empty(); 
                $('#get_price').append(data[0]['price']);
            });
        }



            function currentDiv(n) {
    showDivs(myIndex = n);
    }
    function showDivs(n) {
      var i;
      var x = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("demo");
      if (n > x.length) {myIndex = 1}    
      if (n < 1) {myIndex = x.length}
      for (i = 0; i < x.length; i++) {
         x[i].style.display = "none";  
      }
      for (i = 0; i < dots.length; i++) {
         dots[i].className = dots[i].className.replace(" w3-black", "");
      }
      x[myIndex-1].style.display = "block";  
      dots[myIndex-1].className += " w3-black";
   //setTimeout(showDivs, 5000);
    }
function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    currentDiv(myIndex);
    setTimeout(carousel, 5000);    
    }
        </script>
-->
 <script>
        $('#color_available img:first').trigger('click');
        var url =window.location.href;
        var name11 = url.split('/');
        var base_url = window.location.origin;
        var myIndex = 0;
        carousel();
        


         function getsize(color,pid){
            $.get(window.base_url + "/getsize?color=" + color +'&pid='+pid,function(data, status){
                //alert(data);
                $('#sizechat').empty();
                for (var key in data[0]) {
                    $('#sizechat').append('<span class="size" onclick="getprice('+data[0][key][0]['id']+","+color+","+pid+',this)" >'+ data[0][key][0]['title'] +'</span>');
                }   
                $('#sizechat span:first').trigger('click');
                var img = '';
                var img_li = '';
                var counter = '';
                var count1= '';
                $('#test').empty();
                $('#test1').empty();
                $('#counterid').empty();
                var i=1;

                $('#counterid').append("<p style='display:none'>"+i+"</p>");     
                for (var key in data[1]) {
                    var count = parseInt(key)+1;
                    count1 += '<span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv('+ i++ +')"></span>';
                    if(key==0){
                        img += '<img src="'+data[1][key][0]+'" class="mx-auto mySlides w3-animate-fading" alt="Product" style="max-height: 300px;width: auto; display:block;"/>';
                    }else{
                        img += '<img src="'+data[1][key][0]+'" class="mx-auto mySlides w3-animate-fading" alt="Product" style="max-height: 300px;width: auto;"/>';
                    }
                    
                     
                    img_li +='<li class=""><a href="#gItem_'+data[1][key][1]+'"><img src="'+data[1][key][0]+'" alt="Product"style="max-height: 100px;width: auto;"></a></li>';
                    counter += '<span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv('+count+')"></span>'; 
                }
                var resp_img = img+'<div class="w3-center w3-display-bottommiddle" style="width:100%">'+counter+'</div>';
                //alert(resp_img);
                $('#product_thumbnails').empty();

                
                $('#test').append(img); 
                $('#test1').append(count1); 
                $('#product_thumbnails').append(img_li);
                
                document.getElementById('hidden_color').value = color;
                 carousel();
            });
        }

        function getprice(size,color,pid,el){
            $('.add-to-cart').attr("disabled", true);

            $(".size").removeClass("active");
            $(el).addClass("active");

            $.get(window.base_url + "/getprice?size=" + size+"&color="+color+"&pid="+pid ,function(data, status){
                //console.log(data[0]['price']);
               var split_price = data.replace(/[^.\d\s]+/gi, "");
                url_price = data.replace(/\s/g, '');
                $('#get_price').empty(); 
                document.getElementById('hidden_size').value = size;
                document.getElementById('hidden_price').value = split_price;
                document.getElementById('hidden_productID').value = pid;
                $('#get_price').append(data);
                
                $('.add-to-cart').attr("disabled", false);
                document.getElementById("href").href =base_url+'/configure/'+ name11[4]+'?color='+color+'&size='+size+'&price='+url_price;
                
            });
        }



        function currentDiv(n) {
            showDivs(myIndex = n);
        }
        function showDivs(n) {
          var i;
          var x = document.getElementsByClassName("mySlides");
          var dots = document.getElementsByClassName("demo");
          if (n > x.length) {myIndex = 1}    
          if (n < 1) {myIndex = x.length}
          for (i = 0; i < x.length; i++) {
             x[i].style.display = "none";  
          }
          for (i = 0; i < dots.length; i++) {
             dots[i].className = dots[i].className.replace(" w3-black", "");
          }
          x[myIndex-1].style.display = "block";  
          dots[myIndex-1].className += " w3-black";
            //setTimeout(showDivs, 5000);
        }
        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
               x[i].style.display = "none";  
            }
            myIndex++;
            if (myIndex > x.length) {myIndex = 1}    
            x[myIndex-1].style.display = "block";  
            currentDiv(myIndex);
            setTimeout(carousel, 5000);    
        }
    </script>
    @include('Ecommerce::cart.cart_script')
@endsection