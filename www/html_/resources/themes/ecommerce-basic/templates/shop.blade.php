@extends('layouts.master')

@section('before_content')

@endsection

@section('editable_content')
    
    <div class="container-fluid">
        <div class="row">
            <div class="banner_wrapper">
                <img src="../assets/themes/ecommerce-basic/img/banner.jpg">
            </div>
        </div>        
    </div>

 @include('partials.shop_filter')
<!--
     Page Content

-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="filtering" id="cateee">
                
            </div>
        </div>
    </div>
</div>
    <div class="container padding-bottom-3x mb-1 mt-5 m_50">
        <div class="row">

            <!-- Products-->
            <div class="col-xl-12 col-lg-12 order-lg-2">
                <div class="text-center">
                    @php \Actions::do_action('pre_display_shop') @endphp
                </div>
                <!-- Shop Toolbar-->
<!--
                <div class="shop-toolbar padding-bottom-1x mb-2">
                    <div class="column">
                        <div class="shop-sorting">
                            <label for="sorting">@lang('corals-ecommerce-basic::labels.template.shop.sort')</label>

                            <select class="form-control" id="shop_sort">
                                <option disabled="disabled"
                                        selected>@lang('corals-ecommerce-basic::labels.template.shop.select_option')</option>
                                @foreach($sortOptions as $value => $text)
                                    <option value="{{ $value }}" {{ request()->get('sort') == $value?'selected':'' }}>
                                        {{ $text }}
                                    </option>
                                @endforeach
                            </select>

                            <span class="text-muted">@lang('corals-ecommerce-basic::labels.template.shop.show')
                                &nbsp;</span>
                            <span>{{trans('corals-ecommerce-basic::labels.template.shop.page',['current'=>$products->currentPage(),'total' => $products->lastPage()])}}</span>
                        </div>
                    </div>
                    <div class="column">
                        <div class="shop-view">
                            <a class="grid-view {{ $layout=='grid'?'active':'' }}"
                               href="{{ request()->fullUrlWithQuery(['layout'=>'grid']) }}">
                                <span></span><span></span><span></span>
                            </a>
                            <a class="list-view {{ $layout=='list'?'active':'' }}"
                               href="{{ request()->fullUrlWithQuery(['layout'=>'list']) }}">
                                <span></span><span></span><span></span>
                            </a>
                        </div>
                    </div>
                </div>
-->
                @isset($shopText)
                    <div class="row mb-3">
                        <div class="col-md-12">
                            {{ $shopText }}
                        </div>
                    </div>
                @endisset


                <div id="shop-items">
                    <div class="{{ $layout == 'grid'?'isotope-grid':'' }} cols-3 mb-2">
                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>
                        <!-- Product-->
                        @forelse($products as $product)

                            @include('partials.product_'.$layout.'_item',compact('product'))
                        @empty
                            <h4>@lang('corals-ecommerce-basic::labels.template.shop.sorry_no_result')</h4>
                        @endforelse
                    </div>

                    <!-- Pagination-->
                    {{ $products->appends(request()->except('page'))->links('partials.paginator') }}
                </div>
            </div>
            <!-- Sidebar          -->


        </div>
    </div>
@stop

@section('js')
    @parent
    @include('Ecommerce::cart.cart_script')

    <script type="text/javascript">
    window.onload = function(e){ 
         // GetElementInsideContainer();
        var a = window.location.href;
        
        var n = a.includes("contact-lens-type");
        if(n){
        //document.document.getElementById('myBtn').;
        $('.hide1').hide();
        var link = $('.link').attr('href');
        //alert(link);
        n = link.replace('shop','shop_contact');
        $('.link').attr("href", n);
            }
    }
        $(document).ready(function () {
            $("#shop_sort").change(function () {
                $("#filterSort").val($(this).val());

                $("#filterForm").submit();
            })
        });
        
//          function GetElementInsideContainer() {
//    var elm = {};
//    var elms = document.getElementById("filterForm").getElementsByTagName("li");
//    var len = elms.length;
//    
//        var a = window.location.href;
//        
//        var n = a.includes("contact-lens-type");
//        if(n){
//            for (var i = 0; i < len; i++) {
//        if(elms[i].id == "contact-lens-type1"){
//        //    / alert(elms[i].id);
//            document.getElementById(elms[i].id).style.display = "inline-block";
//            //document.getElementById('price').style.display = "block";
//            //break;
//
//        }
//        // else if(elms[i].id=='price'){
//        //     document.getElementById(elms[i].id).style.display = "block";
//        // }
//        // else{
//        //     document.getElementById(elms[i].id).style.display = "none";
//        // }
//        // if (elms[i].id === childID) {
//        //     elm = elms[i];
//        //     break;
//        //}
//       // alert( elms[i].id);
//    }
//    }else{
//        //alert(len);
//        for (var i = 0; i < len; i++) {
//       
//        //alert(elms[i].id);
//            document.getElementById(elms[i].id).style.display = "inline-block";
//            }
//    }
//   // return elm;
//}
    </script>
@endsection
