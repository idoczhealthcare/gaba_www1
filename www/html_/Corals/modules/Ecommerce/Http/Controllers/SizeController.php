<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\DataTables\SizeDataTable;
use Corals\Modules\Ecommerce\Http\Requests\SizeRequest;
use Corals\Modules\Ecommerce\Models\Size;

class SizeController extends BaseController
{


    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.size.resource_url');
        $this->title = 'Ecommerce::module.size.title';
        $this->title_singular = 'Ecommerce::module.size.title_singular';
        parent::__construct();
    }

    /**
     * @param LensTypeRequest $request
     * @param LensTypeDataTable $dataTable
     * @return mixed
     */
    public function index(SizeRequest $request, SizeDataTable $dataTable)
    {
        return $dataTable->render('Ecommerce::size.index');
    }

    /**
     * @param LensTypeRequest $request
     * @return $this
     */
    public function create(SizeRequest $request)
    {
        $size = new Size();

        $this->setViewSharedData(['title_singular' => trans('Corals::labels.create_title', ['title' => $this->title_singular])]);

        return view('Ecommerce::size.create_edit')->with(compact('size'));
    }

    /**
     * @param LensTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SizeRequest $request)
    {
        try {
            $data = $request->except('users', 'products');

            Size::create($data);

            flash(trans('Corals::messages.success.created', ['item' => $this->title_singular]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, Size::class, 'store');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param size $request
     * @param size $size
     * @return size
     */
    public function show(SizeRequest $request, Size $size)
    {
        return $size;
    }

    /**
     * @param sizeRequest $request
     * @param size $size
     * @return $this
     */
    public function edit(SizeRequest $request, Size $size)
    {
        //$size = new size();
        $this->setViewSharedData(['title_singular' => trans('Corals::labels.update_title', ['title' =>$this->title_singular])]);
        //ecommerce_size_create_edit
        return view('Ecommerce::size.create_edit')->with(compact('size'));
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(SizeRequest $request, Size $size)
    {
        try {
            $data = $request->except('users', 'products');

            $size->update($data);

            

            flash(trans('Corals::messages.success.updated', ['item' => trans('Ecommerce::module.size.index_title')]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, size::class, 'update');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(SizeRequest $request, Size $size)
    {
        try {
            $size->delete();

            $message = ['level' => 'success', 'message' => trans('Corals::messages.success.deleted', ['item' => trans('Ecommerce::module.size.index_title')])];
        } catch (\Exception $exception) {
            log_exception($exception, size::class, 'destroy');
            $message = ['level' => 'error', 'message' => $exception->getMessage()];
        }

        return response()->json($message);
    }

}