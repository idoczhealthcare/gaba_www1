<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\DataTables\ColorDataTable;
use Corals\Modules\Ecommerce\Http\Requests\ColorRequest;
use Corals\Modules\Ecommerce\Models\Color;

class ColorController extends BaseController
{


    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.color.resource_url');
        $this->title = 'Ecommerce::module.color.title';
        $this->title_singular = 'Ecommerce::module.color.title_singular';
        parent::__construct();
    }

    /**
     * @param LensTypeRequest $request
     * @param LensTypeDataTable $dataTable
     * @return mixed
     */
    public function index(ColorRequest $request, ColorDataTable $dataTable)
    {
        return $dataTable->render('Ecommerce::color.index');
    }

    /**
     * @param LensTypeRequest $request
     * @return $this
     */
    public function create(ColorRequest $request)
    {
        $color = new Color();

        $this->setViewSharedData(['title_singular' => trans('Corals::labels.create_title', ['title' => $this->title_singular])]);

        return view('Ecommerce::color.create_edit')->with(compact('color'));
    }

    /**
     * @param LensTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ColorRequest $request)
    {
        try {
            $data = $request->except('thumbnail');

            $color = Color::create($data);

            if ($request->hasFile('thumbnail')) {
                $color->addMedia($request->file('thumbnail'))
                    ->withCustomProperties(['root' => 'user_' . user()->hashed_id])
                    ->toMediaCollection($color->mediaCollectionName);
            }


            flash(trans('Corals::messages.success.created', ['item' => $this->title_singular]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, Color::class, 'store');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param Color $request
     * @param color $color
     * @return color
     */
    public function show(ColorRequest $request, Color $color)
    {
        return $color;
    }

    /**
     * @param colorRequest $request
     * @param color $color
     * @return $this
     */
    public function edit(ColorRequest $request, Color $color)
    {
        //$color = new color();
        $this->setViewSharedData(['title_singular' => trans('Corals::labels.update_title', ['title' =>$this->title_singular])]);
        //ecommerce_color_create_edit
        return view('Ecommerce::color.create_edit')->with(compact('color'));
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ColorRequest $request, Color $color)
    {
        try {
            $data = $request->except('thumbnail', 'clear');

            $color->update($data);

            if ($request->has('clear') || $request->hasFile('thumbnail')) {
                $color->clearMediaCollection($color->mediaCollectionName);
            }

            if ($request->hasFile('thumbnail') && !$request->has('clear')) {
                $color->addMedia($request->file('thumbnail'))
                    ->withCustomProperties(['root' => 'user_' . user()->hashed_id])
                    ->toMediaCollection($color->mediaCollectionName);
            }

            flash(trans('Corals::messages.success.updated', ['item' => trans('Ecommerce::module.color.index_title')]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, color::class, 'update');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ColorRequest $request, Color $color)
    {
        try {
            $color->delete();

            $message = ['level' => 'success', 'message' => trans('Corals::messages.success.deleted', ['item' => trans('Ecommerce::module.color.index_title')])];
        } catch (\Exception $exception) {
            log_exception($exception, color::class, 'destroy');
            $message = ['level' => 'error', 'message' => $exception->getMessage()];
        }

        return response()->json($message);
    }

}