<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class Size extends BaseModel 
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_size';
    
    /**
     *  Model configuration.
     * @var string
     */

    

    public $config = 'ecommerce.models.size';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

  
}
