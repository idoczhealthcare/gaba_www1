<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensType extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_lense_type';
    
    public $config = 'ecommerce.models.ecommerce_lense_type';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

    public function type()
    {
        return $this->hasMany(LensNumber::class, 'type_id');
    }
}
