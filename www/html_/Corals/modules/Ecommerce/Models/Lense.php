<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;


use Corals\Foundation\Search\Indexable;
use Corals\Foundation\Search\IndexedRecord;
  use Corals\Foundation\Transformers\PresentableTrait;
use Corals\Modules\CMS\Models\Content;
use Corals\Modules\Ecommerce\Facades\Ecommerce;
use Corals\Modules\Ecommerce\Traits\DownloadableModel;
use Corals\Modules\Payment\Models\TaxClass;
use Corals\Modules\Payment\Traits\GatewayStatusTrait;
use Corals\Modules\Utility\Traits\Wishlist\Wishlistable;
use Cviebrock\EloquentSluggable\Sluggable;
  use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\Filesystem\Filesystem;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\TemporaryDirectory\TemporaryDirectory;
use Corals\Modules\Utility\Traits\Rating\ReviewRateable as ReviewRateableTrait;


class Lense extends BaseModel 
{
    protected $table = 'ecommerce_lense_number';
    
    public $config = 'ecommerce.models.lense';
    
    protected static $logAttributes = [];
    
    protected $guarded = ['id'];

    public function type()
    {
        return $this->hasMany(LensType::class, 'lens_category');
    }
    
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    