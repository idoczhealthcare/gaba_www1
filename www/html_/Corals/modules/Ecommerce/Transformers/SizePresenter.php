<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\FractalPresenter;

class SizePresenter extends FractalPresenter
{

    /**
     * @return SizeTransformer
     */
    public function getTransformer()
    {
        return new SizeTransformer();
    }
}