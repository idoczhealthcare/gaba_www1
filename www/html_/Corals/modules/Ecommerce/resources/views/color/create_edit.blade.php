@extends('layouts.crud.create_edit')

@section('css')
@endsection

@section('content_header')
    @component('components.content_header')
        @slot('page_title')
            {{ $title_singular }}
        @endslot

        @slot('breadcrumb')
            {{ Breadcrumbs::render('ecommerce_color_create_edit') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    @parent
    <div class="row">
        <div class="col-md-8">
            @component('components.box')
                {!! CoralsForm::openForm($color) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! CoralsForm::text('title','Color',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                        {!! CoralsForm::file('thumbnail', 'Thumbnail') !!}
                    </div>
                    <div class="col-md-6">
                            
                    </div>
                </div>
                

                <div class="row">
                    <div class="col-md-12">
                        {!! CoralsForm::formButtons() !!}
                    </div>
                </div>
                {!! CoralsForm::closeForm($color) !!}
            @endcomponent
        </div>
    </div>
@endsection

@section('js')
@endsection