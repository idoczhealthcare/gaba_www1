<?php

namespace Corals\Modules\Ecommerce\DataTables;

use Corals\Foundation\DataTables\BaseDataTable;
use Corals\Modules\Ecommerce\Models\LensNumber;
use Corals\Modules\Ecommerce\Transformers\LensNumberTransformer;
use Yajra\DataTables\EloquentDataTable;

class LensNumberDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $this->setResourceUrl(config('ecommerce.models.lensnumber.resource_url'));

        $dataTable = new EloquentDataTable($query);

        return $dataTable->setTransformer(new LensNumberTransformer());
    }

    /**
     * Get query source of dataTable.
     * @param LensType $model
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function query(LensNumber $model)
    {
        return $model->newQuery();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        //TODO: Set Translations
        return [
            'id' => ['visible' => false],
            'subcat_id' => ['title' => 'Lens Sub Name','searchable' => true],
            'cat_id' => ['title' => 'Lens Name','searchable' => true],
            'type_id' => ['title' => 'Lens Type','searchable' => true],
            'sphere_min' => ['title' => 'Sphere Min','searchable' => false],
            
            'sphere_max' => ['title' => 'Sphere Max','searchable' => false],
            'cylinder_min' => ['title' => 'Cylinder Min','searchable' => false],
            'cylinder_max' =>['title' => 'Cylinder_max','searchable' => false],
         
            'price' => ['title' => 'Price','searchable' => true],

        ];
    }

    protected function getOptions()
    {
        return ['has_action' => true];
    }
}
