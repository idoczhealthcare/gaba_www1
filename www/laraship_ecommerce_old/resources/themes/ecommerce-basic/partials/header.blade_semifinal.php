<!-- Off-Canvas Mobile Menu-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->


<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GADA | Home page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css"/>


    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/font-awesome.min.css">
<!--    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/fontello.css">-->
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/jquery.slitslider.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/main.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/slick.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/responsive.css">
    
    <noscript>
        <link rel="stylesheet" type="text/css" href="../assets/themes/ecommerce-basic/css/styleNoJS.css" />
    </noscript>
   
</head>
 <body>
<header class="navbar navbar-sticky">
    <!-- Search-->

   <div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<nav class="navbar navbar-default navbar" >

    <div class="row top-navbar">
        <div class="container">
        <ul class="ul_left">
            <li class="wow fadeInDown " data-wow-delay="0.1s"><a class="product.php active1" href="{{ url('/shop?category=contact-lens') }}">Eyewear</a></li>
            <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="product.php" href="{{ url('/shop?category=contact-lens') }}">Contact Lenses</a></li>
            <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="#">Blog</a></li>
        </ul>
              <div class="topbar-column">

        <ul class="list-unstyled currencies" style="display: inline-block;">
            @php \Actions::do_action('post_display_frontend_menu') @endphp
        </ul>
        @if(count(\Settings::get('supported_languages', [])) > 1)
            <li class="dropdown locale" style="list-style-type: none;display: inline-block">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    {!! \Language::flag() !!} {!! \Language::getName() !!}
                </a>
                {!! \Language::flags('dropdown-menu') !!}

            </li>
        @endif
    </div>
    </div>
    </div>
    <div class="container">
       <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="{{ url('/') }}"><img src="{{ \Settings::get('site_logo') }}" alt="{{ \Settings::get('site_name', 'GABA') }}"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <ul class="button navbar-right">
                <div class="toolbar">
        <div class="inner">
            <div class="tools">
            <form class="search_form" method="get" action="{{ url('shop') }}">    
                <div class="custom_search"><input id="search_bar" value="{{ request()->get('search_bar') }}" type="text" name="search" placeholder="search"><i class="fa fa-search search_icon"></i></div> 
            </form>    
                <div class="account">
                    <a href="#"></a><i class="fa fa-user"></i>
                    <ul class="toolbar-dropdown">
                        @auth
                            <li class="sub-menu-user">
                                <div class="user-ava">
                                    <img src="{{ user()->picture_thumb }}"
                                         alt="{{ user()->name }}">
                                </div>
                                <div class="user-info">
                                    <h6 class="user-name">{{ user()->name }}</h6>
                                    <span class="text-xs text-muted">
                                       @lang('corals-ecommerce-basic::labels.partial.member_since')
                                        <br/>
                                        {{ format_date(user()->created_at) }}
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="{{ url('dashboard') }}">@lang('corals-ecommerce-basic::labels.partial.dashboard')</a>
                            </li>
                            <li>
                                <a href="{{ url('profile') }}">@lang('corals-ecommerce-basic::labels.partial.my_profile')</a>
                            </li>
                            <li class="sub-menu-separator"></li>
                            <li><a href="{{ route('logout') }}" data-action="logout">
                                    @lang('corals-ecommerce-basic::labels.partial.logout') <i
                                            class="fa fa-sign-out fa-fw"></i></a>
                            </li>
                            @else
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-sign-in fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.login')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('register') }}">
                                        <i class="fa fa-user fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.register')
                                    </a>
                                </li>
                                @endauth
                    </ul>
                </div>
                <!-- <span class="wrapper_icon">
                <a class="navbar-btn nav-button wow bounceInRight login" data-wow-delay="0.4s" href="{{ url('cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true">
                        </i>
                            <span class="count" id="cart-header-count">{{ count(\ShoppingCart::getItems()) }}</span>
                            <span class="subtotal" id="cart-header-total">
                                {{ \ShoppingCart::total() }}
                            </span>
                        </a>
                </span> -->
                <div class="cart bounceInRight login" data-wow-delay="0.4s"><a href="{{ url('cart') }}"></a>
                    <i class="fa fa-shopping-cart fa-fw"></i>
                    <span class="count" id="cart-header-count">{{ count(\ShoppingCart::getItems()) }}</span>
                    <span class="subtotal" id="cart-header-total">
                        {{ \ShoppingCart::total() }}
                    </span>
                </div>

            </ul>
            <ul class="main-nav nav navbar-nav">
                <li class="wow fadeInDown " data-wow-delay="0.1s"><a class="" href="{{ url('/shop?category=glasses') }}">Glasses</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="{{ url('/shop?category=sunglasses') }}">Sunglasses</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="#">Clearnce</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->


    </div><!-- /.container-fluid -->
    <form class="site-search" method="get" action="{{ url('shop') }}">
        <input type="text" name="search" value="{{ request()->get('search') }}" placeholder="Type to search..."/>

        <div class="search-tools"><span
                    class="clear-search"> @lang('corals-ecommerce-basic::labels.partial.clear')</span>
            <span class="close-search"><i class="icon-cross"></i></span>
        </div>
    </form>
</nav>
</body>
</html>