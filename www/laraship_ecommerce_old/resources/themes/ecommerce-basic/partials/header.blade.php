<!-- Off-Canvas Mobile Menu-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->


<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GADA | Home page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css"/>


    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/fontello.css">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="../assets/themes/ecommerce-basic/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/jquery.slitslider.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/main.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/slick.css">
    <link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/responsive.css">
    
    <noscript>
        <link rel="stylesheet" type="text/css" href="../assets/themes/ecommerce-basic/css/styleNoJS.css" />
    </noscript>
    <style>
        .icheckbox_square-purple, .iradio_square-purple {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 22px;
            height: 22px;
            background: url(../assets/themes/ecommerce-basic/img/purple.png) no-repeat;
            border: none;
            cursor: pointer;
        }
    .icheckbox_flat-blue, .iradio_flat-blue{    background: url(../assets/themes/ecommerce-basic/img/blue.png) no-repeat;}
    </style>
    <body>
</head>
<header class="navbar navbar-sticky">
    <!-- Search-->

   <div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<nav class="navbar navbar-default navbar" >

    <div class="row top-navbar">
        <div class="container">
        <ul class="topper_head">
            <li class="wow fadeInDown bottom_link_selector"  data-wow-delay="0.1s"><a class="product.php active" href="{{ url('/shop?category=Eyewear') }}">Eyewear</a></li>
            <li class="wow fadeInDown bottom_link_selector" data-wow-delay="0.1s"><a class="product.php" href="{{ url('/shop?category=contact-lens-type') }}">Contact Lenses</a></li>
            <li class="wow fadeInDown bottom_link_selector" data-wow-delay="0.4s"><a href="{{ url('/blog') }}">Blog</a></li>
        </ul>
    </div>
    </div>
    <div class="container">
       <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="{{ url('/') }}"><img src="{{ \Settings::get('site_logo') }}" alt="{{ \Settings::get('site_name', 'GABA') }}"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <ul class="button navbar-right">
                <div class="toolbar">
        <div class="inner">
            <div class="tools">
            <form class="search_form" method="get" action="{{ url('shop') }}">
                <div class="custom_search"><input id="search_bar" value="{{ request()->get('search_bar') }}" type="text" name="search_bar" placeholder="search"><i class="fa fa-search search_icon"></i></div>
            </form>
                <div class="account">
                    <a href="#"></a><i class="fa fa-user"></i>
                    <ul class="toolbar-dropdown">
                        @auth
                            <li class="sub-menu-user">
                                <div class="user-ava">
                                    <img src="{{ user()->picture_thumb }}"
                                         alt="{{ user()->name }}">
                                </div>
                                <div class="user-info">
                                    <h6 class="user-name">{{ user()->name }}</h6>
                                    <span class="text-xs text-muted">
                                       @lang('corals-ecommerce-basic::labels.partial.member_since')
                                        <br/>
                                        {{ format_date(user()->created_at) }}
                                    </span>
                                </div>
                            </li>
                            <li>
                                <a href="{{ url('dashboard') }}">@lang('corals-ecommerce-basic::labels.partial.dashboard')</a>
                            </li>
                            <li>
                                <a href="{{ url('profile') }}">@lang('corals-ecommerce-basic::labels.partial.my_profile')</a>
                            </li>
                            <li class="sub-menu-separator"></li>
                            <li><a href="{{ route('logout') }}" data-action="logout">
                                    @lang('corals-ecommerce-basic::labels.partial.logout') <i
                                            class="fa fa-sign-out fa-fw"></i></a>
                            </li>
                            @else
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-sign-in fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.login')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('register') }}">
                                        <i class="fa fa-user fa-fw"></i>
                                        @lang('corals-ecommerce-basic::labels.partial.register')
                                    </a>
                                </li>
                                @endauth
                    </ul>
                </div>
                <!-- <span class="wrapper_icon">
                <a class="navbar-btn nav-button wow bounceInRight login" data-wow-delay="0.4s" href="{{ url('cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true">
                        </i>
                            <span class="count" id="cart-header-count">{{ count(\ShoppingCart::getItems()) }}</span>
                            <span class="subtotal" id="cart-header-total">
                                {{ \ShoppingCart::total() }}
                            </span>
                        </a>
                </span> -->
                <div class="cart bounceInRight login" data-wow-delay="0.4s"><a href="{{ url('cart') }}"></a>
                    <i class="fa fa-shopping-cart fa-fw"></i>
                    <span class="count" id="cart-header-count">{{ count(\ShoppingCart::getItems()) }}</span>
                    <span class="subtotal" id="cart-header-total">
                        {{ \ShoppingCart::total() }}
                    </span>
                </div>

            </ul>

                <ul class="main-nav nav navbar-nav" >
                    <li class="wow fadeInDown dropdown mega-dropdown" data-wow-delay="0.4s">
    <a href="{{ url('/shop?category=glasses') }}">EYEGLASSES<span class="caret"></span></a>
    <ul class="dropdown-menu mega-dropdown-menu postion_absolute_left">

        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY GENDER</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Men</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Women</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Unisex</a></li>
                   </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY TYPE</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/full-frame-menu.jpg" class="img-fluid" alt="First sample image">Full Frame</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/rimless-menu.jpg" class="img-fluid" alt="First sample image">Semi-Rimless</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/semi-rimless-menu.jpg" class="img-fluid" alt="First sample image">Rimless Slim</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Metal Frame</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Thick Frame</a></li>
                </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY MATERIAL</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Acetate</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Plastic</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Metal</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Titanium</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">TR90</a></li>
            </ul>
        </li>
        <li class="col-sm-2 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY COLOR</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Black</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Green</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Purple</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Floral</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Pink</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Tortoiseshell</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Red</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Blue</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Metal</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Color</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">White</a></li>
               </ul>
        </li>
        <li class="col-sm-4 wow fadeIn">
            <ul>
                <li class="dropdown-header">SHOP BY SHAPE</li>
                <li class="divider"></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Oval</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Rectangle</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Round</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Browline</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Aviator</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Cat</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Eye</a></li>
                <li><a href="{{ url('/shop?category=glasses') }}">Geometric</a></li>
            </ul>
        </li>
       
    </ul>
</li>
                    <li class="wow fadeInDown dropdown" data-wow-delay="0.4s">
                        <a href="{{ url('/shop?category=glasses') }}">NEW IN</a>
                    </li>
                    <li class="wow fadeInDown dropdown" data-wow-delay="0.4s">
                        <a href="{{ url('/shop?category=glasses') }}">LIMITED SALE</a>
                    </li>
                    <li class="wow fadeInDown dropdown mega-dropdown" data-wow-delay="0.4s">
                        <a href="{{ url('/shop?category=glasses') }}">FEATURED<span class="caret"></span></a>
                        <ul class="dropdown-menu mega-dropdown-menu wow fadeIn postion_absolute_left">

                            <li class="col-sm-2 wow fadeIn">
                                <ul>
                                    <a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/Featured_03.jpg" class="img-fluid" alt="First sample image">
                                   <li class="divider"></li>
                                    <li class="dropdown-header">CATEYE</li></a>
                                </ul>
                            </li>
                            <li class="col-sm-2 wow fadeIn">
                                <ul>
                                    <a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/Featured_03_1.jpg" class="img-fluid" alt="First sample image">
                                    <li class="divider"></li>
                                        <li class="dropdown-header">ULTRA-LIGHT</li></a>
                                </ul>
                            </li>
                            <li class="col-sm-2 wow fadeIn">
                                <ul>
                                    <a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/Featured_06.jpg" class="img-fluid" alt="First sample image">
                                    <li class="divider"></li>
                                        <li class="dropdown-header">SPRING HINGES</li></a>
                                </ul>
                            </li>
                            <li class="col-sm-2 wow fadeIn">
                                <ul>
                                    <a href="{{ url('/shop?category=glasses') }}"> <img src="../assets/themes/ecommerce-basic/img/Featured_09.jpg" class="img-fluid" alt="First sample image">
                                    <li class="divider"></li>
                                        <li class="dropdown-header">SHOP BY STYLE</li></a>

                                </ul>
                            </li>
                            <li class="col-sm-2 wow fadeIn">
                                <ul>
                                    <a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/Featured_11.jpg" class="img-fluid" alt="First sample image">
                                    <li class="divider"></li>
                                        <li class="dropdown-header">SHOP BY SHAPE</li></a>
                                </ul>
                            </li>
                            <li class="col-sm-2 wow fadeIn">
                                <ul>
                                    <a href="{{ url('/shop?category=glasses') }}"><img src="../assets/themes/ecommerce-basic/img/shape.jpg" class="img-fluid" alt="First sample image">
                                    <li class="divider"></li>
                                        <li class="dropdown-header">SHOP BY COLOR</li></a>
                                </ul>
                            </li>
                        </ul>
                    </li>
            </ul>


        </div><!-- /.navbar-collapse -->


    </div><!-- /.container-fluid -->
    <form class="site-search" method="get" action="{{ url('shop') }}">
        <input type="text" name="search" value="{{ request()->get('search') }}" placeholder="Type to search..."/>

        <div class="search-tools"><span
                    class="clear-search"> @lang('corals-ecommerce-basic::labels.partial.clear')</span>
            <span class="close-search"><i class="icon-cross"></i></span>
        </div>
    </form>
</nav>
</body>
</html>