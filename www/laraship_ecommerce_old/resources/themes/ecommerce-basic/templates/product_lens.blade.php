@extends('layouts.master')
<link rel="stylesheet" href="../assets/themes/ecommerce-basic/css/w3.css">
@section('css')
    <style type="text/css">

    </style>

@endsection

@section('editable_content')
    @php \Actions::do_action('pre_content',$product, null) @endphp

    <!-- Page Content-->
    <div class="container padding-bottom-3x my-5 m_50 m_t_12em" style="margin-top: 10%">
        <div class="row">
            <!-- Product Gallery-->
            <div class="col-md-8">
                <div class="product-gallery">
                    @if($product->discount)
                        <div class="product-badge text-danger">{{ $product->discount }}% Off</div>
                    @endif
                    @if(!($medias = $product->getMedia('ecommerce-product-gallery'))->isEmpty())
<!--
                        <div class="product-carousel owl-carousel text-center">
                            @foreach($medias as $media)
                                <a href="{{ $media->getUrl() }}" data-hash="gItem_{{ $media->id }}"
                                   data-lightbox="product-gallery">
                                    <img src="{{ $media->getUrl() }}" class="mx-auto" alt="Product"
                                         style="max-height: 300px;width: auto;"/>
                                </a>
                            @endforeach
                        </div>
-->
<!--
                    <div class="w3-content w3-section w3-display-container" style="max-width:800px">
               
                        @foreach($medias as $media)
                        
                                <a href="{{ $media->getUrl() }}" data-hash="gItem_{{ $media->id }}"
                                   data-lightbox="product-gallery">
                                    <img src="{{ $media->getUrl() }}" class="mx-auto mySlides w3-animate-fading" alt="Product"
                                         style="max-height: 300px;width: auto;"/>
                                   
                                </a>
                      <div class="w3-center w3-display-bottommiddle" style="width:100%">
   
     <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
                        </div>
                       
                        @endforeach
                      
                    </div>
-->
                    <p style="display:none">{{$i=1}}</p>
<div class="w3-content w3-section w3-display-container" style="max-width:800px">
     @foreach($medias as $media)
  <img class="mySlides w3-animate-fading" src="{{ $media->getUrl() }}" style="width:100%">
     @endforeach
    <div class="w3-center w3-display-bottommiddle" style="width:100%">
    @foreach($medias as $media)
     <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv({{$i++}})"></span>
           @endforeach
   
  </div>
     
  
 
</div>
                    <style>
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
</style>
                        <ul class="product-thumbnails">
                            @foreach($medias as $media)
                                <li class="{{ $media->getCustomProperty('featured', false)?'active':'' }}">
                                    <a href="#gItem_{{ $media->id }}">
                                        <img src="{{ $media->getUrl() }}" alt="Product"
                                             style="max-height: 100px;width: auto;"></a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div class="text-center text-muted">
                            <small>@lang('corals-ecommerce-basic::labels.template.product_single.image_unavailable')</small>
                        </div>
                    @endif
                </div>
                <!-- Product Tabs-->
                <div class="row mb-3">
                    <div class="col-lg-10 offset-lg-1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" href="#description" data-toggle="tab"
                                                    role="tab">@lang('corals-ecommerce-basic::labels.template.product_single.description')</a>
                            </li>
                            @if(\Settings::get('ecommerce_rating_enable',true) === 'true')

                                <li class="nav-item"><a class="nav-link" href="#reviews" data-toggle="tab"
                                                        role="tab">@lang('corals-ecommerce-basic::labels.template.product_single.reviews',['count'=>$product->ratings->count()])</a>
                                </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="description" role="tabpanel">
                                <div>
                                    {!! $product->description !!}
                                </div>
                            </div>
                            @if(\Settings::get('ecommerce_rating_enable',true) === 'true')
                                <br>
                                @include('partials.tabs.reviews',['reviews'=>$product->ratings])
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <!-- Product Info-->
             
            <div class="col-md-4 product_detail_right_card">
                 {!! Form::open(['url'=>'cart/'.$product->hashed_id.'/add-to-cart','method'=>'POST','class'=> 'ajax-form','data-page_action'=>"updateCart"]) !!}
                <h1 class="main_head">{{ $product->name }}</h1>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <p class="sub_head">Eye</p>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" name="lefteye" class="left_checkbox"><label class="check_box_label">LEFT</label>
                    </div>
                    <div class="col-md-4">
                        <input type="checkbox" name="righteye" class="right_checkbox"><label class="check_box_label">RIGHT</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="sub_head">Basic Curve</p>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown left_disable_dropdown" name="lbasic_curve" disabled>
                            <option>8.4</option>
                            <option>8.7</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown right_disable_dropdown" name="rbasic_curve" disabled>
                            <option>8.4</option>
                            <option>8.7</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="sub_head">Diameter</p>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown left_disable_dropdown" name="ldiameter" disabled>
                            <option>14.00</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown right_disable_dropdown" name="rdiameter" disabled>
                            <option>14.00</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="sub_head">Power</p>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown left_disable_dropdown" name="lpower" >
                            <option>-0.50</option>
                            <option>-.0.75</option>
                            <option>-.1.0</option>
                            <option>-.1.25</option>
                            <option>-.1.80</option>
                            <option>-.1.75</option>
                            <option>-.2.0</option>
                            <option>-.2.25</option>
                            <option>-.2.50</option>
                            <option>-.2.75</option>
                            <option>-.3.0</option>
                            <option>-.3.25</option>
                            <option>-.3.50</option>
                            <option>-.3.75</option>
                            <option>-.4.0</option>
                            <option>-.4.25</option>
                            <option>-.4.50</option>
                            <option>-.4.75</option>
                            <option>-.5.00</option>
                            <option>-.5.25</option>
                            <option>-.5.50</option>
                            <option>-.5.75</option>
                            <option>-.6.0</option>
                            <option>-.6.25</option>
                            <option>-.6.50</option>
                            <option>-.7.75</option>
                            <option>-.8.00</option>
                            <option>-.8.25</option>
                            <option>-.8.50</option>
                            <option>-.8.75</option>
                            <option>-.9.00</option>
                            <option>-.9.25</option>
                            <option>-.9.50</option>
                            <option>-.9.75</option>
                            <option>-.10.0</option>
                            <option>-.10.25</option>
                            <option>-.10.50</option>
                            <option>-.10.75</option>
                            <option>-.11.00</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown right_disable_dropdown" name="rpower" >
                            <option>-0.50</option>
                            <option>-.0.75</option>
                            <option>-.1.0</option>
                            <option>-.1.25</option>
                            <option>-.1.80</option>
                            <option>-.1.75</option>
                            <option>-.2.0</option>
                            <option>-.2.25</option>
                            <option>-.2.50</option>
                            <option>-.2.75</option>
                            <option>-.3.0</option>
                            <option>-.3.25</option>
                            <option>-.3.50</option>
                            <option>-.3.75</option>
                            <option>-.4.0</option>
                            <option>-.4.25</option>
                            <option>-.4.50</option>
                            <option>-.4.75</option>
                            <option>-.5.00</option>
                            <option>-.5.25</option>
                            <option>-.5.50</option>
                            <option>-.5.75</option>
                            <option>-.6.0</option>
                            <option>-.6.25</option>
                            <option>-.6.50</option>
                            <option>-.7.75</option>
                            <option>-.8.00</option>
                            <option>-.8.25</option>
                            <option>-.8.50</option>
                            <option>-.8.75</option>
                            <option>-.9.00</option>
                            <option>-.9.25</option>
                            <option>-.9.50</option>
                            <option>-.9.75</option>
                            <option>-.10.0</option>
                            <option>-.10.25</option>
                            <option>-.10.50</option>
                            <option>-.10.75</option>
                            <option>-.11.00</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="sub_head">Packs</p>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown left_disable_dropdown" name="lpack"  disabled>
                            <option>1 bottle = 1 lens</option>
                            <option>2 bottle = 2 lens</option>
                            <option>3 bottle = 3 lens</option>
                            <option>4 bottle = 4 lens</option>
                            <option>5 bottle = 5 lens</option>
                            <option>6 bottle = 6 lens</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="dropdown right_disable_dropdown" name="rpack" disabled>
                            <option>1 bottle = 1 lens</option>
                            <option>2 bottle = 2 lens</option>
                            <option>3 bottle = 3 lens</option>
                            <option>4 bottle = 4 lens</option>
                            <option>5 bottle = 5 lens</option>
                            <option>6 bottle = 6 lens</option>
                        </select>
                    </div>
                </div>  

                <div class="row">
                    <div class="col-md-12">
                        <div class="price_box">
                            <p class="pull-left price_label">Price Per Box</p>
                            <p class="pull-right price_amount_label">Rs. 1500</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="price_box">
                            <p class="pull-left subtotal_label">Sub Total</p>
                            <p class="pull-right subtotal_amount_label">{!! $product->price  !!}</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="price_box">
                            <p class="pull-left price_label">Price Per Box</p>
                            <p class="pull-right price_amount_label">Rs. 1500</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <p class="pull-left price_label">Price Per Box</p>
                            <p class="pull-right price_amount_label">Rs. 1500</p>                
                        </div>
                    </div>
                </div>

                @if(!$product->isSimple)
                    @foreach($product->activeSKU as $sku)
                        @if($loop->index%4 == 0)
                            <div class="d-flex flex-wrap">
                                @endif
                                <div class="text-center sku-item mr-2" style="width: 240px;">
                                    <img src="{{ asset($sku->image) }}" class="img-responsive img-radio mx-auto">
                                    <div class="middle">
                                        <div class="text text-success"><i class="fa fa-check fa-4x"></i></div>
                                    </div>
                                    <div>
                                        {!! !$sku->options->isEmpty() ? $sku->presenter()['options']:'' !!}
                                    </div>
                                    @if($sku->stock_status == "in_stock")
                                        <button type="button"
                                                class="btn btn-block btn-sm btn-default btn-secondary btn-radio m-t-5">
                                            <b>{!! $sku->discount?'<del class="text-muted">'.\Payments::currency($sku->regular_price).'</del>':''  !!} {!! currency()->format($sku->price, \Settings::get('admin_currency_code'), currency()->getUserCurrency()) !!}</b>
                                        </button>

                                    @else
                                        <button type="button"
                                                class="btn btn-block btn-sm m-t-5 btn-danger">
                                            <b> @lang('corals-ecommerce-basic::labels.partial.out_stock')</b>
                                        </button>
                                    @endif
                                    <input type="checkbox" id="left-item" name="sku_hash" value="{{ $sku->hashed_id }}"
                                           class="hidden d-none disable-icheck"/>
                                </div>
                                @if($lastLoop = $loop->index%4 == 3)
                            </div>
                        @endif
                    @endforeach
                    @if(!$lastLoop)</div>@endif
            <div class="form-group">
                <span data-name="sku_hash"></span>
            </div>
            @else
                <input type="hidden" name="sku_hash" value="{{ $product->activeSKU(true)->hashed_id }}"/>
            @endif
            <div class="row m-t-20">
                <div class="col-md-4">
                          </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    @if($product->globalOptions->count())
                        {!! $product->renderProductOptions('global_options',null, ['class'=>'form-control form-control-sm']) !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(\Settings::get('ecommerce_wishlist_enable', 'true') == 'true')
                        @include('partials.components.wishlist',['wishlist'=> $product->inWishList() ])
                    @endif

                    @if($product->external_url)
                        <a href="{{ $product->external_url }}" target="_blank" class="btn btn-success"
                           title="@lang('corals-ecommerce-basic::labels.template.product_single.buy_product')">
                            <i class="fa fa-fw fa-cart-plus"
                               aria-hidden="true"></i> @lang('corals-ecommerce-basic::labels.template.product_single.buy_product')
                        </a>
                    @elseif($product->isSimple && $product->activeSKU(true)->stock_status != "in_stock")
                        <a href="#" class="btn btn-sm btn-outline-danger"
                           title="Out Of Stock">
                            @lang('corals-ecommerce-basic::labels.partial.out_stock')
                        </a>
                    @else
                        {!! CoralsForm::button('corals-ecommerce-basic::labels.partial.add_to_cart',
                        ['class'=>'btn add-to-cart btn-sm btn-primary'], 'submit') !!}

                    <!-- <a data-word="SELECT LENSES" href="" id='href' data-event-cate="Product Page" data-event-name="Product Features" data-event-label="Select Lenses" class="btn btn-select-lenses" id="select-lens">
                     SELECT LENSES
                    </a> -->

                    @endif
                </div>
            </div>

            {{ Form::close() }}
                    </div>             
                </div> 
            </div>
        </div>
    @include('partials.featured_products',['title'=>trans('corals-ecommerce-basic::labels.template.product_single.title')])
    </div>

@stop

@section('js')
    @parent
    <script>
        var myIndex = 0;
carousel();

 function currentDiv(n) {
  showDivs(myIndex = n);
}
function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {myIndex = 1}    
  if (n < 1) {myIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[myIndex-1].style.display = "block";  
  dots[myIndex-1].className += " w3-white";
   //setTimeout(showDivs, 5000);
}
function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    currentDiv(myIndex);
    setTimeout(carousel, 5000);    
}
        var url =window.location.href;
        var name11 = url.split('/');
        var base_url = window.location.origin;
        document.getElementById("href").href =base_url+'/configure/'+ name11[4];

        </script>
    @include('Ecommerce::cart.cart_script')
@endsection