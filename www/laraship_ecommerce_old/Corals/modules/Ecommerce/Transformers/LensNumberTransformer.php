<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\BaseTransformer;
use Corals\Modules\Ecommerce\Models\LensNumber;

class LensNumberTransformer extends BaseTransformer
{
    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.lensnumber.resource_url');

        parent::__construct();
    }

    /**
     * @param Shipping $shipping
     * @return array
     * @throws \Throwable
     */
    public function transform(LensNumber $lensNumber)
    {

        return [
            'id' => $lensNumber->id,
            'sphere_min' => $lensNumber->sphere_min,
            'sphere_max' => $lensNumber->sphere_max,
            'cylinder_min' => $lensNumber->cylinder_min,
            'cylinder_max' => $lensNumber->cylinder_max,
            'subcat_id' => $lensNumber->lenssubcat->len_subcat_name,
            'cat_id' => $lensNumber->lenscat->len_cat_name,
            'type_id' => $lensNumber->lenstype->type_name,
            'price' => $lensNumber->price,
            'action' => $this->actions($lensNumber)
        ];
    }
}








