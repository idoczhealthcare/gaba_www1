<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensSubCat extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_lense_sub_category';
    
    public $config = 'ecommerce.models.ecommerce_lense_sub_category';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

    public function type()
    {
        return $this->hasMany(LensNumber::class, 'len_subcat_id');
    }
}
