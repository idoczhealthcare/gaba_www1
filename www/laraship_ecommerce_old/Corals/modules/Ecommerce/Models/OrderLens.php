<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class OrderLens extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'ecommerce_order_lens';
    /**
     *  Model configuration.
     * @var string
     */



    public $config = 'ecommerce.models.ecommerce_order_lens';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

   
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
