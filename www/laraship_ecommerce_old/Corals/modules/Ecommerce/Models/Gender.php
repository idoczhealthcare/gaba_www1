<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;

class Gender extends BaseModel
{
    protected $table = 'ecommerce_genders';

    public $config = 'ecommerce.models.gender';
}
