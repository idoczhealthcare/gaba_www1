<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;

class Color extends BaseModel
{
    protected $table = 'ecommerce_colors';

    public $config = 'ecommerce.models.color';
}
