@extends('layouts.crud.create_edit')

@section('css')
@endsection

@section('content_header')
    @component('components.content_header')
        @slot('page_title')
            {{ $title_singular }}
        @endslot

        @slot('breadcrumb')
            {{ Breadcrumbs::render('ecommerce_lensnumber_create_edit') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    @parent
    <div class="row">
        <div class="col-md-8">
            @component('components.box')
                {!! CoralsForm::openForm($lensnumber) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! CoralsForm::text('sphere_min','Ecommerce::attributes.lensnumber.sphere_min',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         {!! CoralsForm::text('sphere_max','Ecommerce::attributes.lensnumber.sphere_max',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         {!! CoralsForm::text('cylinder_min','Ecommerce::attributes.lensnumber.cylinder_min',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         {!! CoralsForm::text('cylinder_max','Ecommerce::attributes.lensnumber.cylinder_max',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                        {!! CoralsForm::text('price','Ecommerce::attributes.lensnumber.price',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                         
                        
                        {!! CoralsForm::select('cat_id','Ecommerce::attributes.lensnumber.ecommerce_lense_sub_category', \Ecommerce::getLensCatList(),false,null,[], 'select2') !!}
                           {!! CoralsForm::select('subcat_id','Ecommerce::attributes.lensnumber.ecommerce_lense_sub_category', \Ecommerce::getLensSubCatList(),false,null,[], 'select2') !!}
<!--                       {!!  CoralsForm::select('id','Ecommerce::attributes.lensnumber.ecommerce_lense_category', \Ecommerce::getLensCatList(),false,null,[], 'select2') !!}-->
                        {!!  CoralsForm::select('type_id','Ecommerce::attributes.lensnumber.ecommerce_lense_type', \Ecommerce::getLensTypeList(),false,null,[], 'select2') !!}
<!--
                        {!! CoralsForm::number('price','Ecommerce::attributes.lensnumber.id',true,$lensnumber->price ?? 0.0,
                       array_merge(['right_addon'=>'<i class="fa fa-fw fa-'.strtolower(  \Settings::get('admin_currency_code', 'USD')).'"></i>',
                       'step'=>0.01,'min'=>0,'max'=>999999])) !!}
-->
                       
                    </div>
                    <div class="col-md-6">
                            
                    </div>
                </div>
                {!! CoralsForm::customFields($lensnumber, 'col-md-6') !!}

                <div class="row">
                    <div class="col-md-12">
                        {!! CoralsForm::formButtons() !!}
                    </div>
                </div>
                {!! CoralsForm::closeForm($lensnumber) !!}
            @endcomponent
        </div>
    </div>
@endsection

@section('js')
@endsection