<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Modules\CMS\Traits\SEOTools;
use Corals\Foundation\Http\Controllers\PublicBaseController;
use Corals\Modules\Ecommerce\Facades\Shop;
use Corals\Modules\Ecommerce\Models\Product;
use Corals\Modules\Ecommerce\Models\LensCategory;
use Corals\Modules\Ecommerce\Models\LensSubCat;
use Corals\Modules\Ecommerce\Models\LensType;
use Corals\Modules\Ecommerce\Models\LensNumber;
use Illuminate\Http\Request;

class ShopPublicController extends PublicBaseController
{
    use SEOTools;

    /**
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $layout = $request->get('layout', 'grid');

        $products = Shop::getProducts($request);

        $item = [
            'title' => 'Shop',
            'meta_description' => 'E-commerce Shop',
            'url' => url('shop'),
            'type' => 'shop',
            'image' => \Settings::get('site_logo'),
            'meta_keywords' => 'shop,e-commerce,products'
        ];

        $this->setSEO((object)$item);

        $shopText = null;

        if ($request->has('search')) {
            $shopText = trans('Ecommerce::labels.shop.search_results_for', ['search' => $request->get('search')]);
        }

        $sortOptions = trans(config('ecommerce.models.shop.sort_options'));


        if (\Settings::get('ecommerce_rating_enable') == "true") {
            $sortOptions['average_rating'] = trans('Ecommerce::attributes.product.average_rating');
        }

        return view('templates.shop')->with(compact('layout', 'products', 'shopText', 'sortOptions'));
    }
    public function show1()
    {
       return view('templates.welcome1');
    }


    public function show(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);

        }
        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());

        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $this->setSEO((object)$item);
        $lens_subcat = LensSubCat::all();
        $lens_lenstype = LensType::all();
        $lens_category = LensCategory::all();
        

        return view('templates/product_single')->with(compact('product','lens_subcat','lens_lenstype','lens_category'));
    }
        public function configure(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);

        }
        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());

        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $this->setSEO((object)$item);
        $lens_subcat = LensSubCat::all();
        $lens_lenstype = LensType::all();
        $lens_category = LensCategory::all();
        

        return view('templates/configure')->with(compact('product','lens_subcat','lens_lenstype','lens_category'));
    }
    
    
    
//    public function configurejson(Request $request)
//    {
//        
//        //SELECT * FROM `ecommerce_lense_number` WHERE ((`sphere_min`<= 0 AND `sphere_max`>=0) AND (`cylinder_min`<=0 AND `cylinder_max` >=0)) and type_id =1 and `mapping_id`=1
//        $value = $request->get("value");
//        $value1 = $request->get("value1");
//        $typeid = $request->get("typeid");
//        $lenstype = LensType::where('type_name',$typeid)->get();
//        $cat_name = $request->get("cat_id");
//        $cat_id = LensCategory::where('len_cat_name',$cat_name)->get();
//        $subcat_name = $request->get("sub_id");
//        $lens_subcat = LensSubCat::where('len_subcat_name', $subcat_name)->get();
//        //var_dump($lens_subcat);
//        
//        //$lensnumber = LensNumber::where('sphere_min','<=', $value)->where('sphere_max','>=', $value)->get();
//       // var_dump($lensnumber);
//        $lensnumber = LensNumber::where('sphere_min','<=', $value)->where('sphere_max','>=', $value)->where('cylinder_min','<=', $value1)->where('cylinder_max','>=', $value1)->where('type_id',$lenstype[0]->id)->where('cat_id', $cat_id[0]->id)->where('subcat_id', $lens_subcat[0]->id)->get();
//        //var_dump($lensnumber);
//       
//      
//        
//        return response()->json($lensnumber);
//
//    }
    public function configurejson(Request $request)
    {
        
        //SELECT * FROM `ecommerce_lense_number` WHERE ((`sphere_min`<= 0 AND `sphere_max`>=0) AND (`cylinder_min`<=0 AND `cylinder_max` >=0)) and type_id =1 and `mapping_id`=1
        $value = $request->get("value");
        $value1 = $request->get("value1");
        $typeid = $request->get("typeid");
        $lenstype = LensType::where('type_name',$typeid)->get();
        $cat_name = $request->get("cat_id");
        $cat_id = LensCategory::where('len_cat_name',$cat_name)->get();
        $subcat_name = $request->get("sub_id");
        $lens_subcat = LensSubCat::where('len_subcat_name', $subcat_name)->get();
        //var_dump($lens_subcat);
        
        //$lensnumber = LensNumber::where('sphere_min','<=', $value)->where('sphere_max','>=', $value)->get();
       // var_dump($lensnumber);
        $lensnumber = LensNumber::where('sphere_min','<=', abs($value))->where('sphere_max','>=', abs($value))->where('cylinder_min','<=', abs($value1))->where('cylinder_max','>=', abs($value1))->where('type_id',$lenstype[0]->id)->where('cat_id', $cat_id[0]->id)->where('subcat_id', $lens_subcat[0]->id)->get();
        //var_dump($lensnumber);
       
      
        
        return response()->json($lensnumber);

    }
        public function getsubcat(Request $request)
    {
            //$products = $products->join('ecommerce_category_product', 'ecommerce_category_product.product_id', 'ecommerce_products.id')
//            ->join('ecommerce_categories', 'ecommerce_category_product.category_id', 'ecommerce_categories.id')
//            ->where(function ($query) use ($categories) {
//                $query->whereIn('ecommerce_categories.id', $categories)
//                    ->orWhereIn('ecommerce_categories.parent_id', $categories);
                //SELECT ecommerce_lense_sub_category.len_subcat_name FROM ecommerce_lense_sub_category WHERE ecommerce_lense_sub_category.id = (SELECT `subcat_id` FROM `ecommerce_lense_number` WHERE `cat_id`=(SELECT id FROM ecommerce_lense_category WHERE ecommerce_lense_category.len_cat_name = 'Near'))
        $value = $request->get("value");
        $catid = LensCategory::where("len_cat_name",$value)->get();
         $subid = LensNumber::where('cat_id', $catid[0]->id)->get();
            //loop
            $id=array();
            $var =count($subid);
        for($i = 0;$i< $var;$i++)
{
            $id[]= $subid[$i]->subcat_id;
   //$subname = $subname.LensSubCat::where('id',$subid[$i]->id )->get();  
}
          
        $subname = LensSubCat::whereIn('id',$id)->get();
//        $subid = LensNumber::where('cat_id', $catid[0]->id)>join('ecommerce_lense_sub_category', 'ecommerce_lense_number.cat_id', '=', 'ecommerce_lense_sub_category.id')->get();
//            $subid = LensSubCat::join('ecommerce_lense_number','ecommerce_lense_number.cat_id','ecommerce_lense_sub_category.id')->select('ecommerce_lense_sub_category.*')->where('ecommerce_lense_number.cat_id',2)
//            ->get();
        //$subname = LensSubCat::where('id',$subid[0]->id )->get();
        return response()->json($subname);
    }

    
        public function gettype(Request $request)
    {
         
        $value = $request->get("value");
        $var = explode("/",$value);
        $catid = LensCategory::where("len_cat_name",$var[0])->get();
        $subid = LensSubCat::where('len_subcat_name', $var[1])->get();
          
        $subname = LensNumber::where('cat_id',$catid[0]->id)->where('subcat_id',$subid[0]->id)->get();
              //loop
            $id=array();
            $var =count($subname);
        for($i = 0;$i< $var;$i++)
{
            $id[]= $subname[$i]->type_id;
   
}
            $lenstype = LensType::whereIn('id',$id)->get();

        return response()->json($lenstype);
    }
    
        //shop_contact
    public function shop_contact(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);

        }
        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());

        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $this->setSEO((object)$item);
        $lens_subcat = LensSubCat::all();
        $lens_lenstype = LensType::all();
        $lens_category = LensCategory::all();
        

        return view('templates/product_lens')->with(compact('product','lens_subcat','lens_lenstype','lens_category'));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}