<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\Classes\Ecommerce;
//use Corals\Modules\Ecommerce\DataTables\ProductsDataTable;
//use Corals\Modules\Ecommerce\Http\Requests\ProductRequest;
use Corals\Modules\Ecommerce\Models\Lense;
use Corals\Modules\Ecommerce\Models\SKU;
use Corals\Modules\Ecommerce\Models\Tag;
use Corals\Modules\Ecommerce\Traits\DownloadableController;
use Corals\Modules\Ecommerce\Traits\EcommerceGallery;
use Corals\Modules\Utility\Http\Requests\Rating\RatingRequest;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Media;
use Corals\Modules\Utility\Classes\Rating\RatingManager;


class LenseController extends BaseController
{ 
    
     public function __construct()
    {

    }

    public function index()
    {

    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function show()
    {

    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }


}