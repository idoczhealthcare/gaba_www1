@extends('layouts.master')

@section('css')
    <style type="text/css">
        .sku-item {
            position: relative;
        }

        .sku-item .badge {
            font-size: 75%;
            /*width: 100%;*/
        }

        .img-radio {
            max-height: 100px;
            margin: 5px auto;
        }

        .middle {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 30%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .selected-radio > img {
            opacity: .45;
        }

        .selected-radio .middle {
            opacity: 1;
        }

        .product .btn-cart > span {
            font-size: 15px;
            opacity: 1;
            top: 10px;
            justify-content: center;
        }

        .product .ladaa > span > span {
            font-size: 12px;
            white-space: nowrap;
            opacity: 1;
            padding-left: 3px;
            color: black;
        }

        .product .ladaa > span > i {
            color: black;
        }

        .product .product-button:hover > i {
            -webkit-transform: translateY(-10px);
            -ms-transform: translateY(-10px);
            transform: translateY(-10px);
        }

        .product .product-button:hover > span, .product .ladaa:hover > span > span {
            -webkit-transform: translateY(0);
            -ms-transform: translateY(0);
            transform: translateY(0);
            opacity: 1;
        }

        .product .ladaa.active > span > i, .product .btn-wishlist.active > span > i {
            color: #f44336;
        }
    </style>
@endsection
@section('editable_content')
    @php \Actions::do_action('pre_content',$product, null) @endphp

    <div class="container padding-bottom-3x mt-5">
        <div class="row">
            <div id="section0" class="col-md-8">
                <input type="radio" name="cat" value="1"> Distance<br>
                <input type="radio" name="cat" value="2"> Reading<br>
                <input type="radio" name="cat" value="3"> Bifocal<br>
                <input type="radio" name="cat" value="0"> Non Prescription<br>
                <button id="section0Next">Next</button>
            </div>
            <div id="section1" class="col-md-8" style="display:none;">
                <label>Clindrical Value</label>
                <select id="cylinder">
                @foreach ($lens_cylindrical as $lens)
                    <option value="{{ $lens->id }}">{{ $lens->value }}</option>
                @endforeach
                </select>

                <label>Spherical Value</label>
                <select id="sphere">
                @foreach ($lens_spherical as $lens)
                <option value="{{ $lens->id }}">{{ $lens->value }}</option>
                @endforeach
                </select>
                <button id="section1Next">Next</button>
            </div>
            <div id="section2" class="col-md-8" style="display:none;">
                    @foreach ($lens_category as $lens)
                    <input type="radio" name="category" value="{{ $lens->id }}"> {{ $lens->value }}<br>
                    @endforeach
                
                <button id="sectionTWONext">Next</button>
            </div>
            <div id="section3" class="col-md-8" style="display:none;">
                
            </div>
            <div class="col-md-4"> <span id="price">asd</span></div>
    </div>
@stop

@section('js')
<script>
    @parent
    var price = {{ $product->sku->first()->sale_price }};
    $("#price").html("$"+price);
    $("#section0Next").click(function() {
        var val = +$("input[name=cat]:checked").val();
        $("#section0").hide();
        if(val != 0)
            $("#section1").show();
    });
    $("#section1Next").click(function() {
        $("#section1").hide();
        $("#section2").show();
    });
    $("#sectionTWONext").click(function(){
        $.get(window.base_url + "/configurejson?category=" + $("input[name=category]:checked").val() + "&sphere=" + $("#sphere").val() + "&cylinder=" + $("#cylinder").val(), function(data, status){
            $("#section2").hide();
            $("#section3").show();
            var tmpString = "";
            for (var key in data) {
                tmpString += "<input type='radio' name='cc' value="+data[key]['price']+">"+data[key]['name']+"<br>"
                //alert("Data: " + data[key] + "\nStatus: " + status);
            }
            $("#section3").html(tmpString + "<button id='section3Update'>Update</button>");
            $("#section3Update").click(function() {
                $("#price").html("$"+(+price + +$("input[name=cc]:checked").val()) );
            });
        });
    });
</script>

@endsection
