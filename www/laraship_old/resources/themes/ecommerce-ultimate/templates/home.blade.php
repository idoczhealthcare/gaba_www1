<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GADA | Home page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700" rel="stylesheet">

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/fontello.css">
    <link href="assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/jquery.slitslider.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <noscript>
        <link rel="stylesheet" type="text/css" href="assets/css/styleNoJS.css" />
    </noscript>
</head>
<style>
    #myBtn {
        display: none;
        position: fixed;
        bottom: 46px;
        right: 0;
        z-index: 99;
        border: none;
        outline: none;
        background-color: #ec008c;
        color: white;
        cursor: pointer;
        padding: 15px;
        /* border-radius: 10px; */
        font-family: Banana-Yeti-light;
        font-size: 30px;
        border-bottom-left-radius: 26px;
        border-top-left-radius: 26px;
    }

    #myBtn:hover {
        background-color: #7d3d97;
    }


</style>
<body>
        <div id="preloader">
                <div id="status">&nbsp;</div>
            </div>
            <nav class="navbar navbar-default navbar" >
            
                <div class="row top-navbar">
                    <div class="container">
                    <ul>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="about-us">Eyewear</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="services.php">Contact Lenses</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.4s"><a href="contact-us">Blog</a></li>
                    </ul>
                </div>
                </div>
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"><img src="assets/img/logo.jpg" alt=""></a>
                    </div>
            
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse yamm" id="navigation">
                        <ul class="button navbar-right">
                            <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-search" aria-hidden="true"></i></button>
                            <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-user" aria-hidden="true"></i></button>
                            <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-heart-o" aria-hidden="true"></i></button>
                            <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/')" data-wow-delay="0.4s"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
            
                        </ul>
                        <ul class="main-nav nav navbar-nav navbar-right">
                            @foreach ( Menus::getMenu('frontend_top','active') as $menu)
                                @if($menu->user_can_access)
                                    <li class="{{ \Request::is(explode(',',$menu->active_menu_url))?'active':'' }} wow fadeInDown">
                                        <a href="{{ url($menu->url) }}" target="{{ $menu->target??'_self' }}">
                                            {{ $menu->name }}
                                        </a>
                                    </li>
                                @endif
                            {{--  <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="about-us">Glasses</a></li>  --}}
                            @endforeach
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            
            <!-- End of nav bar -->
            <div class="slide-2">
        <div id="slider" class="sl-slider-wrapper">
            <div class="sl-slider">
    
                <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
    
                    <div class="sl-slide-inner ">
    
                        <div class="bg-img bg-img-1" style="background-image: url(assets/img/slide/silde-1.jpg);"></div>
                        <blockquote>
                            <cite>Choose your frame and</cite>
                            <article>Enhance your</article>
                            <article>Style Statement</article>
                            <cite>get discount upto 70% off</cite>
                        </blockquote>
                    </div>
                </div>
    
                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
    
                    <div class="sl-slide-inner ">
    
                        <div class="bg-img bg-img-1" style="background-image: url(assets/img/slide/silde-2.jpg);"></div>
                        <blockquote>
                            <cite>Choose your frame and</cite>
                            <article>Enhance your</article>
                            <article>Style Statement</article>
                            <cite>get discount upto 70% off</cite>
                        </blockquote>
                    </div>
                </div>
    
    
                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">
    
                    <div class="sl-slide-inner ">
    
                        <div class="bg-img bg-img-1" style="background-image: url(assets/img/slide/silde-3.jpg);"></div>
                        <blockquote>
                            <cite>Choose your frame and</cite>
                            <article>Enhance your</article>
                            <article>Style Statement</article>
                            <cite>get discount upto 70% off</cite>
                        </blockquote>
                    </div>
                </div>
            </div><!-- /sl-slider -->
    
            <nav id="nav-dots" class="nav-dots">
                <span class="nav-dot-current"></span>
                <span></span>
                <span></span>
                <span></span>
            </nav>
        </div><!-- /slider-wrapper -->
    </div>
        <!--End of slider-->
        <section class="second-silder bg-g">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 b-right">
                        <div class="col-md-4">
                            <img src="assets/img/starting.png">
                        </div>
                        <div class="col-md-8">
                            <ul>
                                <li>Glasses Starting</li>
                                <li>From PKR.2000</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 b-right">
                        <div class="col-md-4">
                            <img src="assets/img/free.png">
                        </div>
                        <div class="col-md-8">
                            <ul>
                                <li>Free</li>
                                <li>DELIVERY</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 b-right">
                        <div class="col-md-4">
                            <img src="assets/img/return.png">
                        </div>
                        <div class="col-md-8">
                            <ul>
                                <li>Free</li>
                                <li>RETURNS</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-4">
                            <img src="assets/img/customer-care.png">
                        </div>
                        <div class="col-md-8">
                            <ul>
                                <li>Personalized</li>
                                <li>CUSTOMER CARE</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-------------Banner section--------------->
    <section class="home-banners">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner-1.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner-2.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner-3.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner4.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner-5.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner-6.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="hover ehover11">
                        <img class="img-responsive" src="assets/img/banner-7.jpg" alt="">
                    </div>
                </div>
    
            </div>
        </div>
    </section>
    
    <!--Product section-->
    <section class="Product-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="heading">THE HOSTEST<strong>NEW ARRVIALS</strong></span>
                </div>
            </div>
        </div>
        @php $products = \Shop::getTopSellers(); @endphp

        @if(!$products->isEmpty())
        <div class="container">
            <div class="row">
                @foreach($products as $product)
                <div class="col-md-3">
                    <div class="product">
                            <a href="{{ url('shop/'.$product->slug) }}"><figure>
                            <img src="{{ $product->image }}">
                            </figure>
                            <p>{{ $product->name }}</p>
                            <p>{!! $product->price !!}</p>
                            </a>
    
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </section>
    <!--blognvlog -->
    <div class="blog">
        <div class="container">
                <div class="row">
                    <div class="col-md-6 blog-vlog-bg">
                        <div class="col-md-4">
                            <img src="assets/img/blog-1.jpg">
                        </div>
                        <div class="col-md-8 blog-vlog-bg">
                            <h5>Best Fitting Frames
                                Selection</h5>
                            <p>Lost in your search to find the perfect frames? Look no further!</p>
                            <p>Our product designers have put together a special collection....</p>
                        </div>
                    </div>
                    <div class="col-md-6 blog-vlog-bg">
                        <div class="col-md-4">
                            <img src="assets/img/blog-2.jpg">
                        </div>
                        <div class="col-md-8">
                            <h5>Comprehensive color
                                guide for glasses</h5>
                            <p>If you have ever purchased eyewear in a store before, you are familiar with the hours of fittings, trying on frame after frame to find a pair that looks good on your face...</p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!--Newletter area-->
    <section class="newletter">
        <div class="container">
            <div class="row">
                <div class="col-md-4 signup-text">
                    <h3>Signup now for latest Updates</h3>
                    <p>We respect your privacy your email won’t be shared...</p>
                </div>
                <div class="col-md-4 signup-form">
                    <div class="input-group news-form">
                        <input type="email" class="form-control" placeholder="Enter your email" style="border-radius: 10px;">
                        <span class="input-group-btn">
                        <button class="btn btn-theme" type="submit">Subscribe</button>
             </span>
                    </div>
                </div>
                <div class="col-md-4 social2">
                    <ul>
                        <li><a class="wow fadeInUp animated animated" href="https://twitter.com/kimarotec" style="visibility: visible; animation-name: fadeInUp;"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="https://www.facebook.com/kimarotec" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="https://plus.google.com/kimarotec" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i class="fa fa-instagram"></i></a></li>
                        <li><a class="wow fadeInUp animated animated" href="https://instagram.com/kimarotec" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    
    <!-- Footer area-->
    <div class="footer-area">
    
    
        <div class="footer">
            <div class="container">
                <div class="row">
    
                    <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                        <div class="single-footer">
                            <h4>GABA Opticals </h4>
                            <ul class="footer-menu">
                               <li><a href="#">About us</a></li>
                                <li><a href="#">About Gaba Optical</a></li>
                                <li><a href="#">Our Vision</a></li>
                                <li><a href="#">Our Mission</a></li>
                                <li><a href="#">Chairman Message</a></li>
                                <li><a href="#">Our Core Team</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                        <div class="single-footer">
                            <h4>Quick links </h4>
                            <ul class="footer-menu">
                                <li><a href="#">20% off on Lenses</a></li>
                                <li><a href="#">20% off on Frames</a></li>
                                <li><a href="#">Buy one get one Free</a></li>
                                <li><a href="#">Starting from 2000</a></li>
                                <li><a href="#">New Arrivals</a></li>
                                <li><a href="#">Clearnace Sale</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                        <div class="single-footer">
                            <h4>Help Center</h4>
                            <ul class="footer-menu">
                                <li><a href="#">FAQs</a></li>
                                <li><a href="#">Order Tracking</a></li>
                                <li><a href="#">Shipping & Return Policy</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Contact us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                        <div class="single-footer news-letter">
                            <h4>Newsletter Subscription</h4>
                            <p>Apply for our monthly newsletter subscription for FREE</p>
    
                           <form>
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="E-mail ... ">
                                    <span class="input-group">
                                        <button class="btn btn-primary subscribe" type="button">Submit</button>
                                    </span>
                                </div>
                                <!-- /input-group --></form>
                        </div>
                    </div>
    
                </div>
            </div>
    
    
    </div>
        <div class="footer-copy text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <span> © Copyrights GABA Opticals 2018. All rights reserved</span>
                    </div>
    
                </div>
            </div>
        </div>
    
    
    
    <script src="assets/js/modernizr-2.6.2.min.js"></script>
    <script src="assets/js/jquery-1.10.2.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/bootstrap-hover-dropdown.js"></script>
    
    
    <script src="assets/js/jquery.easypiechart.min.js"></script>
    
    <script src="assets/js/owl.carousel.min.js"></script>
    
    <script src="assets/js/wow.js"></script>
    
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/jquery.slitslider.js"></script>
    <script type="text/javascript" src="assets/js/lightslider.min.js"></script>
    <script src="assets/js/main.js"></script>
    
    <script type="text/javascript">
        $(function () {
    
            var Page = (function () {
    
                var $nav = $('#nav-dots > span'),
                    slitslider = $('#slider').slitslider({
                        onBeforeChange: function (slide, pos) {
    
                            $nav.removeClass('nav-dot-current');
                            $nav.eq(pos).addClass('nav-dot-current');
    
                        }
                    }),
                    init = function () {
    
                        initEvents();
    
                    },
                    initEvents = function () {
    
                        $nav.each(function (i) {
    
                            $(this).on('click', function (event) {
    
                                var $dot = $(this);
    
                                if (!slitslider.isActive()) {
    
                                    $nav.removeClass('nav-dot-current');
                                    $dot.addClass('nav-dot-current');
    
                                }
    
                                slitslider.jump(i + 1);
                                return false;
    
                            });
    
                        });
    
                    };
    
                return {init: init};
    
            })();
    
            Page.init();
    
            /**
             * Notes:
             *
             * example how to add items:
             */
    
            /*
    
             var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');
    
             // call the plugin's add method
             ss.add($items);
    
             */
    
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#media').carousel({
                pause: true,
                interval: false,
            });
        });
    </script>
    
    </body>
    </html>    
