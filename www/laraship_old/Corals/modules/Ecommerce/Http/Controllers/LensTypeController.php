<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Foundation\Http\Controllers\BaseController;
use Corals\Modules\Ecommerce\DataTables\LensTypeDataTable;
use Corals\Modules\Ecommerce\Http\Requests\LensTypeRequest;
use Corals\Modules\Ecommerce\Models\LensType;

class LensTypeController extends BaseController
{


    public function __construct()
    {


        $this->resource_url = config('ecommerce.models.lenstype.resource_url');
        $this->title = 'Ecommerce::module.lenstype.title';
        $this->title_singular = 'Ecommerce::module.lenstype.title_singular';
        parent::__construct();
    }

    /**
     * @param LensTypeRequest $request
     * @param LensTypeDataTable $dataTable
     * @return mixed
     */
    public function index(LensTypeRequest $request, LensTypeDataTable $dataTable)
    {
        return $dataTable->render('Ecommerce::lenstype.index');
    }

    /**
     * @param LensTypeRequest $request
     * @return $this
     */
    public function create(LensTypeRequest $request)
    {
        $lenstype = new LensType();

        $this->setViewSharedData(['title_singular' => trans('Corals::labels.create_title', ['title' => $this->title_singular])]);

        return view('Ecommerce::lenstype.create_edit')->with(compact('lenstype'));
    }

    /**
     * @param LensTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(LensTypeRequest $request)
    {
        try {
            $data = $request->except('users', 'products');

            LensType::create($data);

            flash(trans('Corals::messages.success.created', ['item' => $this->title_singular]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, LensType::class, 'store');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return LensType
     */
    public function show(LensTypeRequest $request, LensType $lenstype)
    {
        return $lenstype;
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return $this
     */
    public function edit(LensTypeRequest $request, LensType $lenstype)
    {
        $this->setViewSharedData(['title_singular' => trans('Corals::labels.update_title', ['title' =>$this->title_singular])]);

        return view('Ecommerce::lenstype.create_edit')->with(compact('lenstype'));
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(LensTypeRequest $request, LensType $lenstype)
    {
        try {
            $data = $request->except('users', 'products');

            $lenstype->update($data);

            flash(trans('Corals::messages.success.updated', ['item' => trans('Ecommerce::module.lenstype.index_title')]))->success();
        } catch (\Exception $exception) {
            log_exception($exception, LensType::class, 'update');
        }

        return redirectTo($this->resource_url);
    }

    /**
     * @param LensTypeRequest $request
     * @param LensType $lenstype
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(LensTypeRequest $request, LensType $lenstype)
    {
        try {
            $lenstype->delete();

            $message = ['level' => 'success', 'message' => trans('Corals::messages.success.deleted', ['item' => trans('Ecommerce::module.lenstype.index_title')])];
        } catch (\Exception $exception) {
            log_exception($exception, LensType::class, 'destroy');
            $message = ['level' => 'error', 'message' => $exception->getMessage()];
        }

        return response()->json($message);
    }

}