<?php

namespace Corals\Modules\Ecommerce\Http\Controllers;

use Corals\Modules\CMS\Traits\SEOTools;
use Corals\Foundation\Http\Controllers\PublicBaseController;
use Corals\Modules\Ecommerce\Facades\Shop;
use Corals\Modules\Ecommerce\Models\Product;
use Corals\Modules\Ecommerce\Models\LensCategory;
use Corals\Modules\Ecommerce\Models\LensCylindrical;
use Corals\Modules\Ecommerce\Models\LensSpherical;
use Corals\Modules\Ecommerce\Models\LensType;
use Illuminate\Http\Request;

class ShopPublicController extends PublicBaseController
{
    use SEOTools;

    /**
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $layout = $request->get('layout', 'grid');

        $products = Shop::getProducts($request);

        $item = [
            'title' => 'Shop',
            'meta_description' => 'E-commerce Shop',
            'url' => url('shop'),
            'type' => 'shop',
            'image' => \Settings::get('site_logo'),
            'meta_keywords' => 'shop,e-commerce,products'
        ];

        $this->setSEO((object)$item);

        $shopText = null;

        if ($request->has('search')) {
            $shopText = trans('Ecommerce::labels.shop.search_results_for', ['search' => $request->get('search')]);
        }

        $sortOptions = trans(config('ecommerce.models.shop.sort_options'));


        if (\Settings::get('ecommerce_rating_enable') == "true") {
            $sortOptions['average_rating'] = trans('Ecommerce::attributes.product.average_rating');
        }

        return view('templates.shop')->with(compact('layout', 'products', 'shopText', 'sortOptions'));
    }

    public function show(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);

        }
        $categories = join(',', $product->activeCategories->pluck('name')->toArray());
        $tags = join(',', $product->activeTags->pluck('name')->toArray());

        $item = [
            'title' => $product->name,
            'meta_description' => str_limit(strip_tags($product->description), 500),
            'url' => url('shop/' . $product->slug),
            'type' => 'product',
            'image' => $product->image,
            'meta_keywords' => $categories . ',' . $tags
        ];

        $this->setSEO((object)$item);

        return view('templates/product_single')->with(compact('product'));
    }

    public function configure(Request $request, $slug)
    {

        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            abort(404);
        }

        $lens_cylindrical = LensCylindrical::all();
        $lens_spherical = LensSpherical::all();
        $lens_category = LensCategory::all();

        return view('templates/product_configure')->with(compact('product','lens_cylindrical','lens_spherical', 'lens_category'));
    }

    public function configurejson(Request $request)
    {
        $lensTypes = LensType::where('lens_category', $request->get("category"))->where('lens_spherical', $request->get("sphere"))->where('lens_cylindrical', $request->get("cylinder"))->get();

        return response()->
        json($lensTypes);
        //json($request->get("cylinder"));
    }

}