@extends('layouts.crud.create_edit')

@section('css')
@endsection

@section('content_header')
    @component('components.content_header')
        @slot('page_title')
            {{ $title_singular }}
        @endslot

        @slot('breadcrumb')
            {{ Breadcrumbs::render('ecommerce_lenstype_create_edit') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    @parent
    <div class="row">
        <div class="col-md-8">
            @component('components.box')
                {!! CoralsForm::openForm($lenstype) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! CoralsForm::text('name','Ecommerce::attributes.lenstype.name',true,null,['help_text'=>'Ecommerce::attributes.shipping.help_shipping_name']) !!}
                        {!! CoralsForm::number('price','Ecommerce::attributes.lenstype.price',true,$lenstype->price ?? 0.0,
                       array_merge(['right_addon'=>'<i class="fa fa-fw fa-'.strtolower(  \Settings::get('admin_currency_code', 'USD')).'"></i>',
                       'step'=>0.01,'min'=>0,'max'=>999999])) !!}
                       {!! CoralsForm::select('lens_category', 'Ecommerce::attributes.lenstype.category', \Ecommerce::getLensCategoriesList(), true, null, [], 'select2') !!}
                    </div>
                    <div class="col-md-6">
                            {!! CoralsForm::select('lens_spherical', 'Ecommerce::attributes.lenstype.spherical', \Ecommerce::getLensSphericalList(), true, null, [], 'select2') !!}
                            {!! CoralsForm::select('lens_cylindrical', 'Ecommerce::attributes.lenstype.cylindrical', \Ecommerce::getLensCylindricalList(), true, null, [], 'select2') !!}
                    </div>
                </div>
                {!! CoralsForm::customFields($lenstype, 'col-md-6') !!}

                <div class="row">
                    <div class="col-md-12">
                        {!! CoralsForm::formButtons() !!}
                    </div>
                </div>
                {!! CoralsForm::closeForm($lenstype) !!}
            @endcomponent
        </div>
    </div>
@endsection

@section('js')
@endsection