<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\FractalPresenter;

class LensTypePresenter extends FractalPresenter
{

    /**
     * @return LensTypeTransformer
     */
    public function getTransformer()
    {
        return new LensTypeTransformer();
    }
}