<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\BaseTransformer;
use Corals\Modules\Ecommerce\Models\LensType;

class LensTypeTransformer extends BaseTransformer
{
    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.lenstype.resource_url');

        parent::__construct();
    }

    /**
     * @param Shipping $shipping
     * @return array
     * @throws \Throwable
     */
    public function transform(LensType $lensType)
    {

        return [
            'id' => $lensType->id,
            'name' => $lensType->name,
            'category' => $lensType->category->value,
            'spherical' => $lensType->spherical->value,
            'cylindrical' => $lensType->cylindrical->value,
            'price' => $lensType->price,
            'action' => $this->actions($lensType)
        ];
    }
}