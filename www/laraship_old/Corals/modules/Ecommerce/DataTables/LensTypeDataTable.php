<?php

namespace Corals\Modules\Ecommerce\DataTables;

use Corals\Foundation\DataTables\BaseDataTable;
use Corals\Modules\Ecommerce\Models\LensType;
use Corals\Modules\Ecommerce\Transformers\LensTypeTransformer;
use Yajra\DataTables\EloquentDataTable;

class LensTypeDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $this->setResourceUrl(config('ecommerce.models.lenstype.resource_url'));

        $dataTable = new EloquentDataTable($query);

        return $dataTable->setTransformer(new LensTypeTransformer());
    }

    /**
     * Get query source of dataTable.
     * @param LensType $model
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function query(LensType $model)
    {
        return $model->newQuery();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        //TODO: Set Translations
        return [
            'id' => ['visible' => false],
            'name' => ['title' => 'Name'],
            'category' => ['title' => 'Category'],
            'spherical' => ['title' => 'Spherical'],
            'cylindrical' => ['title' => 'Cylindrical'],
            'price' => ['title' => 'Price'],

        ];
    }

    protected function getOptions()
    {
        return ['has_action' => true];
    }
}
