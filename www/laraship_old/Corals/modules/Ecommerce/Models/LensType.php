<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensType extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'lens_type';
    
    public $config = 'ecommerce.models.lens_type';

    protected $guarded = ['id'];

    public function spherical()
    {
        return $this->belongsTo(LensSpherical::class, 'lens_spherical');
    }

    public function category()
    {
        return $this->belongsTo(LensCategory::class, 'lens_category');
    }

    public function cylindrical()
    {
        return $this->belongsTo(LensCylindrical::class, 'lens_cylindrical');
    }

    public function pd()
    {
        return $this->belongsTo(LensPupillaryDistance::class, 'lens_pd');
    }
}
