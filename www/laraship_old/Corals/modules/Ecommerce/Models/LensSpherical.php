<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensSpherical extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'lens_spherical';
    
    public $config = 'ecommerce.models.lens_spherical';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

    public function type()
    {
        return $this->hasMany(LensType::class, 'lens_spherical');
    }
}
