<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensPupillaryDistance extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'lens_pupillary_distance';
    
    public $config = 'ecommerce.models.lens_pupillary_distance';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

    public function type()
    {
        return $this->hasMany(LensType::class, 'lens_pd');
    }
}
