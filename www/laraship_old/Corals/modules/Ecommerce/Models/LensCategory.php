<?php

namespace Corals\Modules\Ecommerce\Models;

use Corals\Foundation\Models\BaseModel;
use Corals\Foundation\Transformers\PresentableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class LensCategory extends BaseModel
{
    use PresentableTrait, LogsActivity;

    protected $table = 'lens_category';
    
    public $config = 'ecommerce.models.lens_category';

    protected static $logAttributes = [];

    protected $guarded = ['id'];

    public function type()
    {
        return $this->hasMany(LensType::class, 'lens_category');
    }
}
